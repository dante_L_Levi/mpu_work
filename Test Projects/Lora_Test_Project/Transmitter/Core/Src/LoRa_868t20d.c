/*
 * LoRa_868t20d.c
 *
 *  Created on: May 13, 2020
 *      Author: AlexPirs
 */


#include "LoRa_868t20d.h"


GPIO_InitTypeDef GPIO_InitStruct;
LoRa_E32 MyLoRaDef;
extern UART_HandleTypeDef huart2;
#define UART_LoRa	huart2




/****************************Init Uart **************************/
static void UART_Init_LoRa_868t20d(void);
/********************Config Pin Configuration******************/
void Init_Config_GPIO_868t20d(void);
/*************************Function Set M0/M1*****************************/
RET_STATUS LoRa_SetMBits(GPIO_CONTROL_MBITS stat);
/*************************Function Reset M0/M1*****************************/
RET_STATUS LoRa_ResetMBits(GPIO_CONTROL_MBITS stat);


/********************Config Pin Configuration******************/
void Init_Config_GPIO_868t20d(void)
{

	/*Configure GPIO pin : AUX_Pin */
		GPIO_InitStruct.Pin = AUX_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
	  HAL_GPIO_Init(AUX_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin : M1_LoRa_Pin */
	  GPIO_InitStruct.Pin = M1_LoRa_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(M1_LoRa_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin : M0_LoRa_Pin */
	  GPIO_InitStruct.Pin = M0_LoRa_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(M0_LoRa_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin Output Level */
	    HAL_GPIO_WritePin(M1_LoRa_GPIO_Port, M1_LoRa_Pin, GPIO_PIN_RESET);

	    /*Configure GPIO pin Output Level */
	    HAL_GPIO_WritePin(M0_LoRa_GPIO_Port, M0_LoRa_Pin, GPIO_PIN_RESET);

	  //=========================================================================

}

/****************************Init Uart **************************/
static void UART_Init_LoRa_868t20d(void)
{
	UART_LoRa.Instance = USART2;
	UART_LoRa.Init.BaudRate = 9600;
	UART_LoRa.Init.WordLength = UART_WORDLENGTH_8B;
	UART_LoRa.Init.StopBits = UART_STOPBITS_1;
	UART_LoRa.Init.Parity = UART_PARITY_NONE;
	UART_LoRa.Init.Mode = UART_MODE_TX_RX;
	UART_LoRa.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	UART_LoRa.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&UART_LoRa) != HAL_OK)
	  {
	    Error_Handler();
	  }
}

/*************************Function Set M0/M1*****************************/
RET_STATUS LoRa_SetMBits(GPIO_CONTROL_MBITS stat)
{
	if(stat==M0_Bits)
	{
		HAL_GPIO_WritePin(M0_LoRa_GPIO_Port, M0_LoRa_Pin, GPIO_PIN_SET);
		return RET_SUCCESS;
	}
	else if(stat==M1_Bits)
	{
		HAL_GPIO_WritePin(M1_LoRa_GPIO_Port, M1_LoRa_Pin, GPIO_PIN_SET);
		return RET_SUCCESS;
	}
	else
	{
		return RET_INVALID_PARAM;
	}
}

/*************************Function Reset M0/M1*****************************/
RET_STATUS LoRa_ResetMBits(GPIO_CONTROL_MBITS stat)
{
	if(stat==M0_Bits)
	{
		HAL_GPIO_WritePin(M0_LoRa_GPIO_Port, M0_LoRa_Pin, GPIO_PIN_RESET);
		return RET_SUCCESS;
	}
	else if(stat==M1_Bits)
	{
		HAL_GPIO_WritePin(M1_LoRa_GPIO_Port, M1_LoRa_Pin, GPIO_PIN_RESET);
		return RET_SUCCESS;
	}
	else
	{
		return RET_INVALID_PARAM;
	}
}

/***********************Main Init LoRa*******************/
void Init_LoRa_HW(void)
{
	Init_Config_GPIO_868t20d();
	UART_Init_LoRa_868t20d();
}

/********************Set Mode Work***********************/
RET_STATUS LoRa_ConfigModeWork(LoRa_ModeDef mode)
{
	switch(mode)
	{
		case MODE_NORMAL:
		{
			LoRa_ResetMBits(M0_Bits);
			LoRa_ResetMBits(M1_Bits);
			break;
		}
		case MODE_WAKE_UP:
		{
			LoRa_SetMBits(M0_Bits);
			LoRa_ResetMBits(M1_Bits);
			break;
		}
		case MODE_POWER_SAVING:
		{
			LoRa_SetMBits(M1_Bits);
			LoRa_ResetMBits(M0_Bits);
			break;
		}
		case MODE_SLEEP:
		{
			LoRa_SetMBits(M1_Bits);
			LoRa_SetMBits(M0_Bits);
			break;
		}
		default:
		{
			return RET_INVALID_PARAM;
		}


	}

	return RET_SUCCESS;
}


/********************Set Mode Work***********************/
void LoRa_ConfigSet(LoRa_868t20_SPED_BPSDef _speed,
		LoRa_868t20_SPED_UARTDef _uart,LoRa_868t20_SPED_UART_PARITYDef _parity,
		LoRa_868t20_CHANNEL_CONFIGDef _channel, LoRa_868t20_OPTION_Def _option)
{
	LoRa_SetMBits(M0_Bits);
	LoRa_SetMBits(M1_Bits);
	uint8_t payLoad[6]={0};
	payLoad[0]=0xC0;
	payLoad[1]=0x00;
	payLoad[2]=0x00;
	payLoad[3]|=(uint8_t)(_speed|_parity|_uart);
	payLoad[4]=(uint8_t)_channel;
	payLoad[5]=(uint8_t)_option;
	HAL_UART_Transmit(&UART_LoRa, (uint8_t*)payLoad,6, 0x1000);
	LoRa_ResetMBits(M0_Bits);
	LoRa_ResetMBits(M1_Bits);
}

/**************Function Transmit Command********************/
void Get_Configuration_LoRa(void)
{
	 uint8_t geetParam[3]={0xC1,0xC1,0xC1};
	 HAL_UART_Transmit(&UART_LoRa, (uint8_t*)geetParam, 3, 0x1000);
}


/*******************Config Standart LoRa 433MHz**********************/
void Set_Default_433MHz_LoRa(void)
{
	uint8_t data[6]={0xC0,0x00,0x00,0x1A,0x17,0x44};
	HAL_UART_Transmit(&UART_LoRa, (uint8_t *)data, 6, 0x1000);
}


/**********************Transmit LoRa*****************************/
void LoRa_TransmitData(uint8_t *dt,uint8_t length)
{
	if(length>BUFFER_PACKET_MAX)
	{
		return;
	}
	else
	{
		HAL_UART_Transmit(&UART_LoRa, (uint8_t *)dt, length, 0x100);
	}
}




