/*
 * ADC_Config.c
 *
 *  Created on: Apr 30, 2020
 *      Author: AlexPirs
 */


#include "ADC_Config.h"


uint16_t adc_samples[3]={0};

/**********************ADC Init***************************/
void ADC_Init(void)
{
	//change prescaler for ADC to not exceed 12Mhz
	RCC->CFGR|=RCC_CFGR_ADCPRE_DIV6;
	//RCC CLOCKS for ADC1 and ALT function
	RCC->APB2ENR|=RCC_APB2ENR_ADC1EN|RCC_APB2ENR_AFIOEN|RCC_APB2ENR_IOPAEN;
	//Enable DMA1
	RCC->AHBENR|=RCC_AHBENR_DMA1EN;
	//GPIO config input
	GPIOA->CRL|=GPIO_CRL_CNF0_1;
	GPIOA->CRL&=~(GPIO_CRL_CNF0_0);
	GPIOA->CRL &= ~GPIO_CRL_MODE0;

	GPIOA->CRL|=GPIO_CRL_CNF1_1;
	GPIOA->CRL&=~(GPIO_CRL_CNF1_0);
	GPIOA->CRL &= ~GPIO_CRL_MODE1;

	GPIOA->CRL|=GPIO_CRL_CNF2_1;
	GPIOA->CRL&=~(GPIO_CRL_CNF2_0);
	GPIOA->CRL &= ~GPIO_CRL_MODE2;
	//Enable End of Conversion interrupt
	//ADC1->CR1|=ADC_CR1_EOCIE;
	//enable that interrupt in the NVIC
	//NVIC_EnableIRQ(ADC1_2_IRQn);




	ADC1->SMPR2|=ADC_SMPR2_SMP0_2|ADC_SMPR2_SMP0_1|ADC_SMPR2_SMP0_0;
	ADC1->SMPR2|=ADC_SMPR2_SMP1_2|ADC_SMPR2_SMP1_1|ADC_SMPR2_SMP1_0;
	ADC1->SMPR2|=ADC_SMPR2_SMP2_2|ADC_SMPR2_SMP2_1|ADC_SMPR2_SMP2_0;
	/*
	ADC1->SQR2 |= ADC_SQR2_SQ8_2 | ADC_SQR2_SQ8_1 | ADC_SQR2_SQ8_0 //		канал 8
	        | ADC_SQR2_SQ7_2 | ADC_SQR2_SQ7_1;                             // канал 7

	        ADC1->SQR3 |=  ADC_SQR3_SQ6_2 | ADC_SQR3_SQ6_0                 //канал 6
	        | ADC_SQR3_SQ5_2                                               //канал 5
	        | ADC_SQR3_SQ4_1 | ADC_SQR3_SQ4_0                              //канал 4
	        | ADC_SQR3_SQ3_1                                               //канал 3
	        | ADC_SQR3_SQ2_0;                                              // канал 2
	        //PA0 0 включен по умолчанию т.к. в младших битах SQ3 0		//канал 1
	         * */

	ADC1->SQR1|=(1<<21);///number convections
	ADC1->SQR3&=~(ADC_SQR3_SQ1);//PA0
	ADC1->SQR3|=ADC_SQR3_SQ2_0;//PA1
	ADC1->SQR3|=ADC_SQR3_SQ3_1;//PA2


	ADC1->CR1|=ADC_CR1_SCAN;
	//ON DMA
	ADC1->CR2|=ADC_CR2_DMA;
	//-------------------------DMA Settings-------------------------------
	DMA1_Channel1->CPAR=(uint32_t)(&(ADC1->DR));
	DMA1_Channel1->CMAR=(uint32_t)adc_samples;
	DMA1_Channel1->CNDTR=3;//number values
	DMA1_Channel1->CCR|=DMA_CCR_CIRC|DMA_CCR_MINC|DMA_CCR_PSIZE_0|DMA_CCR_MSIZE_0;
	DMA1_Channel1->CCR |= DMA_CCR_PL; //Приоритет - очень высокий
	DMA1_Channel1->CCR &= ~DMA_CCR_DIR; // из периферии
	DMA1_Channel1->CCR|=DMA_CCR_EN;
		//--------------------------------------------------------------------
	//Enable the ADc for the first time and set it to continous mode
	ADC1->CR2|=ADC_CR2_ADON|ADC_CR2_CONT;
	HAL_Delay(1);
	ADC1->CR2|=ADC_CR2_ADON;
	HAL_Delay(1);
	ADC1->CR2 |= ADC_CR2_CAL;
	while (!(ADC1->CR2 & ADC_CR2_CAL));                   // waiting end calibration




	//ADC1->CR2 |= ADC_CR2_EXTSEL;      				// event start conversion SWSTART
	//ADC1->CR2 |= ADC_CR2_EXTTRIG;                   // enable start conversion external signal
	//ON SCAN MODE






	//ADC1->CR2 |= ADC_CR2_SWSTART;
/*
	ADC1->CR2|=ADC_CR2_ADON|ADC_CR2_CONT;
	HAL_Delay(1);
	ADC1->CR2|=ADC_CR2_ADON;
	HAL_Delay(1);
	ADC1->CR2|=ADC_CR2_CAL;//Calibrations
	HAL_Delay(2);
	//	while(!(ADC1->CR2 & ADC_CR2_CAL));
	ADC1->CR2 |= ADC_CR2_SWSTART;
*/
}

/*********************READ Analog Input*****************************/
void Read_ADC(uint16_t *dt)
{
	dt[0]=adc_samples[0];
	dt[1]=adc_samples[1];
	dt[2]=adc_samples[2];
}
