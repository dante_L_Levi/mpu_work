/*
 * ADC_Config.h
 *
 *  Created on: Apr 30, 2020
 *      Author: AlexPirs
 */

#ifndef INC_ADC_CONFIG_H_
#define INC_ADC_CONFIG_H_

#include "main.h"

#define ADC_A0		0
#define ADC_A1		1
#define ADC_A2		2



/**********************ADC Init***************************/
void ADC_Init(void);
/*********************READ Analog Input*****************************/
void Read_ADC(uint16_t *dt);


#endif /* INC_ADC_CONFIG_H_ */
