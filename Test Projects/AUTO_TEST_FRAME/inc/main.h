/*
 * main.h
 *
 *  Created on: 27 ���. 2020 �.
 *      Author: AlexPirs
 */

#ifndef INC_MAIN_H_
#define INC_MAIN_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"
#include "driverlib/adc.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"
#include "driverlib/i2c.h"


#include "InitPeripheral.h"
#include "DataTransfer.h"


#pragma pack(push, 1)
typedef struct
{
    uint8_t ID_DEV;
    uint8_t CMD;
    int16_t kOmI;
    int16_t kOmII;
    int16_t mOmI;
    int16_t mOmII;
    int16_t OmI;
    int16_t OmII;
    int16_t I_YPT4;
    int16_t II_YPT4;
    int16_t  I_CP1_2;
    int16_t II_CP1_2;
    int16_t  bI;
    int16_t bII;
    int16_t I_OY13;
    int16_t II_OY13;
    int16_t I_YPT3;
    int16_t II_YPT3;
    int16_t I_YPT5;
    int16_t II_YPT5;
    int16_t duu_v1;
    int16_t duu_v2;
    int16_t duu_u1;
    int16_t duu_u2;
    int16_t dlu_n1;
    int16_t dlu_n2;
    int16_t dlu_n3;
    int16_t out_del_ku2;
    int16_t out_del_ku1;
    int16_t I_OY14;
    int16_t II_OY14;
    int16_t res1;
    int16_t res2;

    unsigned READY:1;
    unsigned PPS:1;
    unsigned RAZ_DLU:1;
    unsigned ON_CNT:1;
    unsigned OTD:1;
    unsigned SREZ_CNT:1;
    unsigned Ktim:1;


    uint16_t CRC;

}Test_Framedef;


#define SIZE_FRAME          sizeof(Test_Framedef)


#pragma pack(pop)


/********************Hundler TIM1**************************************/
void handler_TIM1(void);
void Handler_Ressieve_UART2(void);
void Handler_Ressieve_UART1(void);

#endif /* INC_MAIN_H_ */
