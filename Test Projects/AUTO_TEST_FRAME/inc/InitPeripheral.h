/*
 * InitPeripheral.h
 *
 *  Created on: 8 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_INITPERIPHERAL_H_
#define INC_INITPERIPHERAL_H_


#include "main.h"
#include "stdint.h"


typedef enum
{
    GPIO_Input,
    GPIO_Output,
    GPIO_Blocked


}typeGPIO;

typedef struct
{
    uint32_t Port;
    uint8_t  Pin;
    typeGPIO statusgpio;
    bool valuepin;
    uint32_t countPress;

}GPIO_Def;


#define PORT_RGB    GPIO_PORTF_BASE
#define PIN_R       GPIO_PIN_1
#define PIN_G       GPIO_PIN_2
#define PIN_B       GPIO_PIN_3




/**********************Init GPIO*********************************/
void GPIO_Init(void);
/**********************Set Value GPIO Output***********************/
void SetGPIO_Bit(GPIO_Def *out,uint8_t val);
/***********************Init UART 7*****************************/
void Init_UartBase(uint32_t Baud);
/*****************************Init Inner Temperature Sensor*******************/
void Init_Inter_TempSensor(void);
/********************timer update Data struct Motor & Led Status*****************/
void Tim1_Init(void);
/*****************************ADC Init Channels*******************/
void Init_ADC_Channels(void);

/***********************Init UART 1*****************************/
void UART1_Init(uint32_t Baud);

#endif /* INC_INITPERIPHERAL_H_ */
