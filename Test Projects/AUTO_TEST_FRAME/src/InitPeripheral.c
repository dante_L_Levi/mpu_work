/*
 * InitPeripheral.c
 *
 *  Created on: 8 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */


#include "InitPeripheral.h"

extern GPIO_Def LED_R;
extern GPIO_Def LED_G;
extern GPIO_Def LED_B;
extern uint32_t CLOCK_SYS;

void InitGpio_Struct(void)
{
    LED_R.Port=PORT_RGB;
    LED_R.Pin=GPIO_PIN_1;
    LED_R.statusgpio=GPIO_Output;
    LED_R.valuepin=false;

    LED_G.Port=PORT_RGB;
    LED_G.Pin=GPIO_PIN_2;
    LED_G.statusgpio=GPIO_Output;
    LED_G.valuepin=false;

    LED_B.Port=PORT_RGB;
    LED_B.Pin=GPIO_PIN_3;
    LED_B.statusgpio=GPIO_Output;
    LED_B.valuepin=false;

}

/**********************Init GPIO*********************************/
void GPIO_Init(void)
{
    InitGpio_Struct();
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(LED_R.Port,LED_R.Pin);
    GPIOPinTypeGPIOOutput(LED_G.Port,LED_G.Pin);
    GPIOPinTypeGPIOOutput(LED_B.Port,LED_B.Pin);
    GPIOPinWrite(LED_R.Port,LED_R.Pin, 0x00);
    GPIOPinWrite(LED_G.Port,LED_G.Pin, 0x00);
    GPIOPinWrite(LED_B.Port,LED_B.Pin, 0x00);
}


/**********************Set Value GPIO Output***********************/
void SetGPIO_Bit(GPIO_Def *out,uint8_t val)
{
   GPIOPinWrite(out->Port,out->Pin, val);
   out->valuepin=~(out->valuepin);
}


/***********************Init UART 2*****************************/
void Init_UartBase(uint32_t Baud)
{
        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
        GPIOPinConfigure(GPIO_PD6_U2RX);
        GPIOPinConfigure(GPIO_PD7_U2TX);
        GPIOPinTypeUART(GPIO_PORTD_BASE,GPIO_PIN_6|GPIO_PIN_7);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD))
        {
        }
        SysCtlPeripheralEnable(SYSCTL_PERIPH_UART2);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART2))
        {

        }

        UARTClockSourceSet(UART2_BASE,UART_CLOCK_SYSTEM);
        UARTConfigSetExpClk(UART2_BASE, CLOCK_SYS, Baud,
                           (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                            UART_CONFIG_PAR_NONE));
        UARTIntRegister(UART2_BASE, Handler_Ressieve_UART2);
        IntEnable(INT_UART2);
        IntPrioritySet(INT_UART2, 1);
        UARTIntEnable(UART2_BASE, UART_INT_RX | UART_INT_RT);
        UARTEnable(UART2_BASE);
}


/***********************Init UART 1*****************************/
void UART1_Init(uint32_t Baud)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    GPIOPinConfigure(GPIO_PB0_U1RX);
    GPIOPinConfigure(GPIO_PB1_U1TX);
    GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    GPIOPadConfigSet(GPIO_PORTB_BASE, GPIO_PIN_0, GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD_WPU);

    IntMasterEnable();
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
    //
        // Configure the UART for 115,200, 8-N-1 operation.
        //
        UARTConfigSetExpClk(UART1_BASE, CLOCK_SYS, Baud,
                            (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                             UART_CONFIG_PAR_NONE));
        UARTIntRegister(UART1_BASE, Handler_Ressieve_UART1);
        IntEnable(INT_UART1);
        IntPrioritySet(INT_UART1, 1);
        UARTIntEnable(UART1_BASE, UART_INT_RX | UART_INT_RT);
        UARTEnable(UART1_BASE);

}


/********************timer update Data struct Motor & Led Status*****************/
void Tim1_Init(void)
{

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1))
    {
    }
    TimerClockSourceSet(TIMER1_BASE, TIMER_CLOCK_SYSTEM);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_A_PERIODIC);
    TimerLoadSet(TIMER1_BASE, TIMER_A, CLOCK_SYS/4);
    TimerIntRegister(TIMER1_BASE, TIMER_A, handler_TIM1);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    TimerEnable(TIMER1_BASE, TIMER_A);
    //NVIC
    IntEnable(INT_TIMER1A);

}
