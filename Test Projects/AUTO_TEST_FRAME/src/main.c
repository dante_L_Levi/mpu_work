/*
 * main.c
 *
 *  Created on: 27 ���. 2020 �.
 *      Author: AlexPirs
 */


#include "main.h"

GPIO_Def LED_R;
GPIO_Def LED_G;
GPIO_Def LED_B;
uint32_t CLOCK_SYS;
uint32_t bAUD_t;
uint8_t cnt=0;
Test_Framedef  Frame_Data;

uint8_t buff[SIZE_FRAME]={0};


void Indicate_Led(uint8_t cnt_Led);

/*********************************Create_data*******************************/
void Random_StructData(void);
void Frame_Start_Init(void);
/**********************Calculate CRC16*****************************/
uint16_t TxCalculateCRC16(uint8_t *Data,uint8_t Length);
void Transmit_Frame(uint32_t  UartChannel,uint8_t* dt,uint8_t length);


char dataString[]="Test Data tivaC\r\n";

/********************Hundler TIM1**************************************/
void handler_TIM1(void)
{

    Indicate_Led(cnt);
    cnt++;
    if(cnt>3)
    {
        cnt=0;
    }

   //Transmit_UartString(UART1_BASE,dataString);
   Random_StructData();
   Transmit_Frame(UART1_BASE,buff,SIZE_FRAME);
    TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
}


void Handler_Ressieve_UART2(void)
{
  uint32_t  IntStatus=UARTIntStatus(UART2_BASE,true);
    UARTIntClear(UART2_BASE,IntStatus);
}

void Handler_Ressieve_UART1(void)
{
    uint32_t  IntStatus=UARTIntStatus(UART1_BASE,true);
        UARTIntClear(UART1_BASE,IntStatus);
}

void RCC_Init(void)
{
    // Set the clocking to run directly from the PLL at 20 MHz.
        // The following code:
        // -sets the system clock divider to 10 (200 MHz PLL divide by 10 = 20 MHz)
        // -sets the system clock to use the PLL
        // -uses the main oscillator
        // -configures for use of 16 MHz crystal/oscillator input
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                           SYSCTL_XTAL_16MHZ);
}


void Indicate_Led(uint8_t cnt_Led)
{
    switch(cnt_Led)
        {
            case 0:
            {
                SetGPIO_Bit(&LED_R,0x02);
                SetGPIO_Bit(&LED_G,0x00);
                SetGPIO_Bit(&LED_B,0x00);
                break;
            }
            case 1:
            {
                SetGPIO_Bit(&LED_R,0x00);
                SetGPIO_Bit(&LED_G,0x04);
                SetGPIO_Bit(&LED_B,0x00);
                break;
            }
            case 2:
            {
                SetGPIO_Bit(&LED_R,0x00);
                SetGPIO_Bit(&LED_G,0x00);
                SetGPIO_Bit(&LED_B,0x08);
                break;
            }
            default:
            {
                SetGPIO_Bit(&LED_R,0x00);
                SetGPIO_Bit(&LED_G,0x00);
                SetGPIO_Bit(&LED_B,0x00);
                break;
            }
        }
}

void main(void)
{
    RCC_Init();
    CLOCK_SYS=SysCtlClockGet();
    GPIO_Init();
    UART1_Init(115200);
    Frame_Start_Init();
    Tim1_Init();
    IntMasterEnable();

    while(1)
    {

    }
}


void Frame_Start_Init(void)
{
    Frame_Data.ID_DEV=0x1F;
    Frame_Data.CMD=0x0F;
}

/**********************Calculate CRC16*****************************/
uint16_t TxCalculateCRC16(uint8_t *Data,uint8_t Length)
{
    uint8_t j;
            uint16_t reg_crc = 0xFFFF;
            while (Length--)
            {
              reg_crc^= *Data++;
              for(j=0;j<8;j++)
               {
                 if(reg_crc & 0x01)
                  {
                    reg_crc = (reg_crc >> 1) ^ 0xA001;
                  }
                else
                  {
                    reg_crc = reg_crc >> 1;
                  }
               }
            }
            return reg_crc;
}


/*********************************Create_data*******************************/
void Random_StructData(void)
{
    Frame_Data.kOmI=rand ()% 65000;
    Frame_Data.kOmII=rand ()% 65000;
    Frame_Data.mOmI=rand ()% 65000;
    Frame_Data.mOmII=rand ()% 65000;
    Frame_Data.OmI=rand ()% 65000;
    Frame_Data.OmII=rand ()% 65000;
    Frame_Data.I_YPT4=rand ()% 65000;
    Frame_Data.II_YPT4=rand ()% 65000;
    Frame_Data.I_CP1_2=rand ()% 65000;
    Frame_Data.II_CP1_2=rand ()% 65000;
    Frame_Data.bI=rand ()% 65000;
    Frame_Data.bII=rand ()% 65000;
    Frame_Data.I_OY13=rand ()% 65000;
    Frame_Data.II_OY13=rand ()% 65000;
    Frame_Data.I_YPT3=rand ()% 65000;
    Frame_Data.II_YPT3=rand ()% 65000;
    Frame_Data.I_YPT5=rand ()% 65000;
    Frame_Data.II_YPT5=rand ()% 65000;
    Frame_Data.duu_v1=rand ()% 65000;
    Frame_Data.duu_v2=rand ()% 65000;
    Frame_Data.duu_u1=rand ()% 65000;
    Frame_Data.duu_u2=rand ()% 65000;
    Frame_Data.dlu_n1=rand ()% 65000;
    Frame_Data.dlu_n2=rand ()% 65000;
    Frame_Data.dlu_n3=rand ()% 65000;
    Frame_Data.out_del_ku2=rand ()% 65000;
    Frame_Data.out_del_ku1=rand ()% 65000;
    Frame_Data.I_OY14=rand ()% 65000;
    Frame_Data.II_OY14=rand ()% 65000;
    Frame_Data.res1=0;
    Frame_Data.res2=0;
    uint8_t dt[SIZE_FRAME-2]={0};
    memcpy(dt,&Frame_Data,SIZE_FRAME-2);
    Frame_Data.CRC=TxCalculateCRC16(dt,SIZE_FRAME-2);
    memcpy(buff,&Frame_Data,SIZE_FRAME);
}


void Transmit_Frame(uint32_t  UartChannel,uint8_t* dt,uint8_t length)
{
    uint8_t i=0;
    for(i=0;i<length;i++)
    {
        UARTCharPut(UartChannel, dt[i]);
    }

}

