/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "stdio.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
volatile uint16_t Timer2=0;
volatile uint16_t Timer1=0;


extern char USERPath[4]; /* logical drive path */
FATFS fs;
FIL fil;
FRESULT fresult;
char buffer[1024];
UINT br,bw;//file read/write count
FATFS *pfs;
DWORD fre_clust;
DIR dir;
FILINFO fileInfo;
uint32_t total,free_space;




/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void LED_Indication(uint16_t ind);
void ReadTest(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
/********************Tim update Even Hundler Interrupt********************/
void TIM1_ISQ_Hundler(void)
{
	/*******************Indication Work**************************************/
		LED_Indication(Timer2);
		Timer2++;
					if(Timer2>2000)
					{
						Timer2=0;
					}
		/**************************************************************************/
					Timer1++;
						if(Timer1>10)
							Timer1=0;


}


void send_uart(char *string)
{
	uint8_t len=strlen(string);
	HAL_UART_Transmit(&huart1, (uint8_t*)string, len, 0x1000);


}

int bufsize (char *buf)
{
	int i=0;
	while (*buf++ != '\0') i++;
	return i;
}

void bufclear (void)  // clear buffer
{
	for (int i=0; i<1024; i++)
	{
		buffer[i] = '\0';
	}
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_TIM1_Init();
  MX_USART1_UART_Init();
  MX_FATFS_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start_IT(&htim1);
  /* Mount SD Card */
      fresult = f_mount(&fs, "", 0);
      if (fresult != FR_OK) send_uart ("error in mounting SD CARD...\n");
      else send_uart("SD CARD mounted successfully...\n");


      /*************** Card capacity details ********************/

          /* Check free space */
          f_getfree("", &fre_clust, &pfs);

          total = (uint32_t)((pfs->n_fatent - 2) * pfs->csize * 0.5);
          sprintf (buffer, "SD CARD Total Size: \t%lu\n",total);
          send_uart(buffer);
          bufclear();
          free_space = (uint32_t)(fre_clust * pfs->csize * 0.5);
          sprintf (buffer, "SD CARD Free Space: \t%lu\n",free_space);
          send_uart(buffer);


          /************* The following operation is using PUTS and GETS *********************/


          /* Open file to write/ create a file if it doesn't exist */
          fresult = f_open(&fil, "file1.txt", FA_OPEN_ALWAYS | FA_READ | FA_WRITE);

          /* Writing text */
          fresult = f_puts("This data is from the First FILE\n\n", &fil);

          /* Close file */
          fresult = f_close(&fil);

          send_uart ("File1.txt created and the data is written \n");

          /* Open file to read */
          fresult = f_open(&fil, "file1.txt", FA_READ);

          /* Read string from the file */
          f_gets(buffer, fil.fsize, &fil);

          send_uart(buffer);

          /* Close file */
          f_close(&fil);

          bufclear();


          /**************** The following operation is using f_write and f_read **************************/

          /* Create second file with read write access and open it */
          fresult = f_open(&fil, "file2.txt", FA_OPEN_ALWAYS | FA_READ | FA_WRITE);

          /* Writing text */
          strcpy (buffer, "This is File 2 and it says Hello from controllerstech\n");

          fresult = f_write(&fil, buffer, bufsize(buffer), &bw);

          send_uart ("File2.txt created and data is written\n");

          /* Close file */
          f_close(&fil);



          // clearing buffer to show that result obtained is from the file
          bufclear();

          /* Open second file to read */
          fresult = f_open(&fil, "file2.txt", FA_READ);

          /* Read data from the file
           * Please see the function details for the arguments */
          f_read (&fil, buffer, fil.fsize, &br);
          send_uart(buffer);

          /* Close file */
          f_close(&fil);

          bufclear();


          /*********************UPDATING an existing file ***************************/

          /* Open the file with write access */
          fresult = f_open(&fil, "file2.txt", FA_OPEN_ALWAYS | FA_WRITE);

          /* Move to offset to the end of the file */
          fresult = f_lseek(&fil, fil.fsize);

          /* write the string to the file */
          fresult = f_puts("This is updated data and it should be in the end \n", &fil);

          f_close (&fil);

          /* Open to read the file */
          fresult = f_open (&fil, "file2.txt", FA_READ);

          /* Read string from the file */
          f_read (&fil, buffer, fil.fsize, &br);
          send_uart(buffer);

          /* Close file */
          f_close(&fil);

          bufclear();


          /*************************REMOVING FILES FROM THE DIRECTORY ****************************/

          fresult = f_unlink("/file1.txt");
          if (fresult == FR_OK) send_uart("file1.txt removed successfully...\n");

          fresult = f_unlink("/file2.txt");
          if (fresult == FR_OK) send_uart("file2.txt removed successfully...\n");

          /* Unmount SDCARD */
          fresult = f_mount(NULL, "", 1);
          if (fresult == FR_OK) send_uart ("SD CARD UNMOUNTED successfully...\n");
  /* USER CODE END 2 */
 
 

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL5;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void LED_Indication(uint16_t ind)
{
	ind%2==0?HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET):HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
}



void ReadTest(void)
{
	//read


}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
