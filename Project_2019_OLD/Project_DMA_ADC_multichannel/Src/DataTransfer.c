/*
 * DataTransfer.c
 *
 *  Created on: Jun 29, 2019
 *      Author: Tatiana_Potrebko
 */


#include "DataTransfer.h"


extern UART_HandleTypeDef huart2;

/******************************Transmit Struct****************************/
void UART_Transmit_Data(void)
{
	//uint8_t sizeData=sizeof(dataLSM6DS0);
	//uint8_t buffer[sizeData];
	//memcpy((void*)buffer,&dataLSM6DS0,sizeData);
	//HAL_UART_Transmit_DMA(&huart2, (uint8_t*)buffer, sizeData);
}


/*************************Transmit String*****************************/
void Transmit_String(char *data,uint8_t Length)
{
	HAL_UART_Transmit(&huart2, (uint8_t*)data, Length,0x1000);
}

/*************************Transmit Const String*****************************/
void Transmit_ConstString(const char *data)
{
	HAL_UART_Transmit(&huart2, (uint8_t*)data, strlen(data),0x1000);
}
