/*
 * DataTransfer.h
 *
 *  Created on: Jun 29, 2019
 *      Author: Tatiana_Potrebko
 */

#ifndef DATATRANSFER_H_
#define DATATRANSFER_H_


#include "main.h"
#include "stm32f4xx_hal.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"



/******************************Transmit Struct****************************/
void UART_Transmit_Data(void);
/*************************Transmit String*****************************/
void Transmit_String(char *data,uint8_t Length);
/*************************Transmit Const String*****************************/
void Transmit_ConstString(const char *data);


#endif /* DATATRANSFER_H_ */
