/*
 * BUS_Protocol.c
 *
 *  Created on: 12 ��� 2019 �.
 *      Author: Tatiana_Potrebko
 */


#include "BUS_Protocol.h"

/***************************PIN SELECT Ressive/Transmit*******************/
#define RS485_TRANSMIT() GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_4, 0xFF)
#define RS485_RECEIVER() GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_4, 0)


extern uint32_t sysClock;//Clock MPU

/*******************Ressiver buffer**********************/
 uint8_t RX_BUFFER[BUFFER_SIZE];
 /******************Transmit Buffer RS 485***************/
 uint8_t TX_BUFFER[BUFFER_SIZE];
 /***************Counter Ressiver Byte*******************/
 uint32_t index_res=0;
 /*******************Flag end Message ressiever*****************/
 bool rx_message_end=true;
 /*********************flag new message transmit**************/
 bool tx_new_messange_ready_flag = false;
 /*********************flag new byte ressiever*****************/
 bool rx_new_messange_ready_flag = false;

 /********************off Transmit/Ressiver********************/
 bool isMuted_Data=false;
 extern Data_RS_485_half rs485_Data;
 /********************Hundler ressieve Byte UART*************/
 void RS485_RessiveData(uint8_t data);


 /************************** hundler RxD UART 6**********************/
 void Handler_UART6(void)
 {
     rx_new_messange_ready_flag=true;
     uint32_t intStatus;
     uint8_t Ressieve_Byte;
     intStatus=UARTIntStatus(RS485_UART,true);
     if((UARTCharsAvail(RS485_UART)))
     {
         UARTIntClear(RS485_UART,intStatus);
         Ressieve_Byte=UARTCharGet(RS485_UART);
         if(index_res<sizeof(Data_RS_485_half))
         {
           RS485_RessiveData(Ressieve_Byte);
           index_res++;
         }
     }

 }


/********************Hundler ressieve Byte UART*************/
 void RS485_RessiveData(uint8_t data)
 {
     if(index_res!=BUFF_ENDINDEX)
     {
         RX_BUFFER[index_res]=data;
         rx_message_end=false;
     }
     else if(index_res==BUFF_ENDINDEX)
     {
         RX_BUFFER[index_res]=data;
         index_res=0;
         rx_message_end=true;
         rx_new_messange_ready_flag=false;
         if(CheckedRxDCRC()==true)
         {
             ParceStruct(&rs485_Data);
         }
         else
         {
             //Error
         }

     }
 }

 /**********************Get flag End Frame*****************/
 bool GetFlagRessiever_End()
 {
     return rx_message_end;
 }

 /****************Function Parse Array Bytes in Struct************************/
 void ParceStruct(Data_RS_485_half *data)
 {
     memcpy(data,RX_BUFFER,BUFFER_SIZE);
 }

 /****************************************************************/

/***********************GPIO RS-485**************************/
 void GPIO_RS485Init(void)
 {
     SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
     GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE,GPIO_PIN_4);
     GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_4, 0x00);

 }

 /*************************Init RS-485*********************/
 void RS485_Init(uint32_t baudrate, uint8_t id)
 {
     Tim1_Init();//update data timer
     GPIO_RS485Init();
     Init_UART6(baudrate);
     rs485_Data.dev_ID=id;
     ResetMuted();

 }


 /*********************Function Get Id Device***************/
 uint8_t Get_ID_RS485(void)
 {
     return rs485_Data.dev_ID;
 }

 /*********************Function Set Id Device***************/
 void Setup_Id_Device(uint8_t id)
 {
     rs485_Data.dev_ID=id;
 }

 /*********************Function Set Buad Device***************/
 void RS485_setBaudrate(uint32_t baudrate)
 {

     UARTConfigSetExpClk(RS485_UART, sysClock, baudrate,
                             (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                              UART_CONFIG_PAR_NONE));
 }



 void SetMuted(void)
 {
     isMuted_Data=true;
 }

 void ResetMuted(void)
 {
     isMuted_Data=false;
 }


/*********************return Stauts rxBuff*********************/
 bool RS485_rxPkgAvailable(void)
 {
     return rx_new_messange_ready_flag;
 }

 /***********************Function Calculate CRC******************************/
 void GenerateTxDCRC(Data_RS_485_half *data)
 {
     uint8_t sum=data->dev_ID;
     sum+=data->command;
     uint8_t i;
     for(i=2;i<BUFFER_SIZE-2;i++)
     {
         sum+=TX_BUFFER[i];
     }
     sum = ~sum;
     data->CRC=sum;
     TX_BUFFER[BUFFER_SIZE-1]=sum;
     TX_BUFFER[BUFFER_SIZE-2]=sum>>8;
 }

 /********************Function Check CRC RxD Packet********************/
 bool CheckedRxDCRC()
 {
     uint8_t sum=RX_BUFFER[0];//1 byte -ID
     sum+=RX_BUFFER[1];//2 byte -Command
     uint8_t i;
     for(i=2;i<BUFFER_SIZE-2;i++)
     {
         sum+= RX_BUFFER[i];
     }
     sum = ~sum;
     uint8_t lsb=sum;
     uint8_t hsb=sum>>8;
     if(lsb==RX_BUFFER[BUFFER_SIZE-1] && hsb==RX_BUFFER[BUFFER_SIZE-2])
     {
         return true;
     }
     else
     {
         return false;
     }
 }




 /*********************return Stauts rxBuff*********************/
  bool RS485_txPkgAvailable(void)
  {
      return tx_new_messange_ready_flag;
  }


  /************************Transmit Message*************************/
  void Set_RS485MessageTX(Data_RS_485_half *data)
  {
      if(isMuted_Data==true)
      {
          return;
      }
      RS485_TRANSMIT();


      memcpy((void*)TX_BUFFER,data,BUFFER_SIZE);
      //generate CRC TRANSMIT
      GenerateTxDCRC(data);
      RS485_Half_Transmit_Msg(TX_BUFFER,BUFFER_SIZE);
  }

  void handler_TIM1(void)
  {
      TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
  }

