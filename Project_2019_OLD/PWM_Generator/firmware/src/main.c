/*
 * main.c
 *
 *  Created on: 7 ��� 2019 �.
 *      Author: Alex Lepotsenko
 */
#include "Main.h"


uint16_t RS485_DEVICE_ID = 0x5F;
uint32_t sysClock;//Clock MPU

extern ButtonsDef Buttons[2];
extern OUTPUTDef Out[2];
extern pwmDEF pwm0;
SERVICE_DATA template;

Data_RS_485_half rs485_Data;
extern uint8_t RX_BUFFER[BUFFER_SIZE];



void Unit_Test(void);
/********************Function Read Status PWM & LED**********************/
void Update_State(void);
/********************Function Set Value PWM width Button**********************/
void Update_Manual(void);
/*************************Function Transmit Message on PC Command Response**************/
void Response_PC(uint8_t command);

/**************************Convert Data********************************/
void Parse_dataStruct(SERVICE_DATA *dtr);



void handler_TIM2(void)
{
    TimerIntClear(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
    Update_State();

}

/******************************Function Converted Ressieve Struct*******************/
void Converted_Data(SERVICE_DATA *dtr)
{

}

void Unit_Test(void)
{
    rs485_Data.command=COMMAND_EMPTY;
    uint8_t test_buff[10]={0x00,0x01,0x01,0x02,0x02,0x03,0x03,0x04,0x04,0x05};
    memcpy(rs485_Data.payload,test_buff,10);
    rs485_Data.CRC=0xFF;
    Set_RS485MessageTX(&rs485_Data);
}


void main(void)
{
    sysClock= SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                                           SYSCTL_OSC_MAIN |
                                           SYSCTL_USE_PLL |
                                           SYSCTL_CFG_VCO_480),
                                           120000000);

    GPIO_Init();
    PWM_Single_Init();
    RS485_Init(115200,RS485_DEVICE_ID);
    Tim2_Init();
    IntMasterEnable();
    while(true)
    {
        Update_Manual();
    }

}

/********************Function Read Status PWM & LED**********************/
void Update_State(void)
{
    //Read Buffer Ressiever RS-485
    uint8_t buuf[BUFFER_SIZE];
    if(GetFlagRessiever_End()==true)
    {
        memcpy(buuf,RX_BUFFER,BUFFER_SIZE);
        //Parse Buffer in Struct PWM +Struct LED
        Parse_dataStruct(&template);
       //Response on PC
       Response_PC(RESPONSE_OK);
    }


}
/**************************Convert Data in Peripheral********************************/
void ConvertToHardware(SERVICE_DATA *dtr)
{

}

/**************************Convert Data********************************/
void Parse_dataStruct(SERVICE_DATA *dtr)
{
    //������ ������ ������ � ��������� SERVICE_DATA
    memcpy(dtr,&rs485_Data.payload,sizeof(SERVICE_DATA));

    //��������� ��������� SERVICE_DATA �� pwmDEF,OUTPUTDef
    ConvertToHardware(dtr);
    //�������� ��������� ��� pwm � ��� gpio
    if((dtr->LedStatus|0x01)==0x01)
    {
        Update_Output(&Out[0],0x01);
    }
    else
    {
        Update_Output(&Out[0],0x00);
    }

    if((dtr->LedStatus|0x02)==0x02)
        {
            Update_Output(&Out[1],0x02);
        }
        else
        {
            Update_Output(&Out[1],0x00);
        }

    //�������� ��������� ���





}

/*************************Function Transmit Message on PC Command Response**************/
void Response_PC(uint8_t command)
{
    rs485_Data.command=RESPONSE_OK;
    uint8_t test_buff[BUFFER_SIZE]={0};
    memcpy(rs485_Data.payload,test_buff,BUFFER_SIZE);
    Set_RS485MessageTX(&rs485_Data);
}


/********************Function Set Value PWM width Button**********************/
void Update_Manual(void)
{
    //On System Work/��������� ������� ���������� ������� �������������
    if(Read_Button(&Buttons[0]))
    {
        //Response Eho Command- I_WORK/SLEEP
        Response_PC(RESPONSE_I_WORK);
        //Set Flag Start PWM

    }
    else
    {
        //�������� ����� Flag Start PWM
        //if true->Response 1 ��� Eho Command- I_WORK
        Response_PC(RESPONSE_I_WORK);
        //else Response Eho 1 ��� Command- I_SLEEP
        Response_PC(RESPONSE_I_SLEEP);
    }

    //Read Status Button 2-Set Duty
    if(Read_Button(&Buttons[1]))
    {
        //increment Duty 10
        //Response Eho Command- SET_DUTY-Value
        Response_PC(RESPONSE_M_DUTY);
    }


}

