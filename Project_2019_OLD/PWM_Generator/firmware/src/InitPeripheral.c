/*
 * InitPeripheral.c
 *
 *  Created on: 7 ��� 2019 �.
 *      Author: Alex Lepotsenko
 */
#include "InitPeripheral.h"
extern uint32_t sysClock;//Clock MPU

#define TIMER_FRQ       120000000
#define PWM_FREQ        20000
#define PWM_PERIOD      (TIMER_FRQ / PWM_FREQ)


typedef enum
{
    SinglePWM=0x01,
    ComplymentaryPWM=0x02

}modePWMDef;



ButtonsDef Buttons[2];
OUTPUTDef Out[2];
pwmDEF pwm0;
/**************************Init IO************************/
void GPIO_Init(void)
{
    //-------------------------Init LED-------------------------------/
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(TivaC129PORTLed,TivaC129LED1|TivaC129LED2); //config output Pin LED
    GPIOPinWrite(TivaC129PORTLed, TivaC129LED1, 0x00);
    GPIOPinWrite(TivaC129PORTLed, TivaC129LED2, 0x00);
    /***************************Bisness Logic********************************/
    Out[0].ID=0x01;//LED 1
    Out[0].Pin=TivaC129LED1;
    Out[0].Port=TivaC129PORTLed;
    Out[0].Status=0x00;

    Out[1].ID=0x02;//LED2
    Out[1].Pin=TivaC129LED2;
    Out[1].Port=TivaC129PORTLed;
    Out[1].Status=0x00;

    //-------------------------Init Button-------------------------------/
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);
    GPIOPinTypeGPIOInput(TivaC129PORTBTN, TivaC129BTN1);//config button
    GPIOPinTypeGPIOInput(TivaC129PORTBTN, TivaC129BTN2);//config button
    GPIOPadConfigSet(TivaC129PORTBTN,TivaC129BTN1,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);//config Input: current 2mA,pull-up
    GPIOPadConfigSet(TivaC129PORTBTN,TivaC129BTN2,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);//config Input: current 2mA,pull-up
    /***************************Bisness Logic********************************/
    Buttons[0].ID=0x01;//Button ON/OFF
    Buttons[0].Port=TivaC129PORTBTN;
    Buttons[0].Pin=TivaC129BTN1;
    Buttons[0].Status=0x01;

    Buttons[1].ID=0x02;//Button Duty Cicle PWM UP
    Buttons[1].Port=TivaC129PORTBTN;
    Buttons[1].Pin=TivaC129BTN2;
    Buttons[1].Status=0x02;


}


/****************Update Status Pin*************************/
void Update_Output(OUTPUTDef *out,uint8_t st)
{
    out->Status=st;
    GPIOPinWrite(out->Port,out->Pin,st);
}


/****************Read Button*************************/
uint8_t Read_Button(ButtonsDef *btn)
{
    return GPIOPinRead(btn->Port,btn->Pin);
}

/****************************Function Init Tim1 RS485**********************************/
void Timer1_Init(void)
{

}



/******************Init Single PWM *****************/
void PWM_Single_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinConfigure(GPIO_PF0_M0PWM0);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_0);
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_UP_DOWN |
                        PWM_GEN_MODE_NO_SYNC);
    // Set the PWM period to 250Hz
    //use the following equation: N = (1 / f) * SysClk
    // In this case you get: (1 / 100Hz) * 120MHz = 1200000 cycles.
    //TIMER_FRQ / PWM_FREQ=120000000/1000
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 120000);
    // 25% of the time or 16000 clock cycles (64000 / 4).
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0,
                         100000);
    // Enable the PWM0 Bit 0 (PD0) and Bit 1 (PD1) output signals.
    //
    PWMOutputState(PWM0_BASE,PWM_OUT_0_BIT, true);
    // Enables the counter for a PWM generator block.
    //
    PWMGenEnable(PWM0_BASE, PWM_GEN_0);

    PWMSyncTimeBase(PWM0_BASE, PWM_GEN_0_BIT);


    //pwm0.AddrReg=PWM0_BASE;
    pwm0.Freq=PWM_FREQ;
    //pwm0.ID=0x01;
    pwm0.Mode=(uint8_t)ComplymentaryPWM;
    pwm0.channel[0]=(uint8_t)PWM_OUT_0;
    pwm0.Duty=PWMPulseWidthGet(PWM0_BASE, pwm0.channel[0]);
    pwm0.status=0x81;//coding ON/off & Name PWM 0b100000000-PWM0

}


void Start_PWM(uint8_t flag)
{
    if(flag==0xFF)
    {

    }
}

/**********************Set Value Duty PWM Single Mode**********************/
void PWM_SetDuty_Single(pwmDEF *pwm,uint32_t val)
{
    switch(pwm->status)
    {
        case 0x81://pwm0
        {
            PWMPulseWidthSet(PWM0_BASE, pwm->channel[0],val);
            break;
        }
    }



}



/*****************************Settings Service UART6******************************/
void Init_UART6(uint32_t Baud)
{

        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
        GPIOPinConfigure(GPIO_PP0_U6RX);
        GPIOPinConfigure(GPIO_PP1_U6TX);
        GPIOPinTypeUART(GPIO_PORTP_BASE,GPIO_PIN_0|GPIO_PIN_1);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOP))
        {
        }
        SysCtlPeripheralEnable(SYSCTL_PERIPH_UART6);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART6))
        {
        }

        UARTClockSourceSet(UART6_BASE,UART_CLOCK_SYSTEM) ;
        UARTConfigSetExpClk(UART6_BASE, sysClock, Baud,
                           (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                            UART_CONFIG_PAR_NONE));
        UARTIntRegister(UART6_BASE, Handler_UART6);
        IntEnable(INT_UART6);
        UARTIntEnable(UART6_BASE, UART_INT_RX | UART_INT_RT);
        UARTEnable(UART6_BASE);

}



/********************timer update Data struct Led,pwm*****************/
void Tim1_Init(void)
{
    //Timer1 - 1000Hz
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1))
    {
    }
    TimerClockSourceSet(TIMER1_BASE, TIMER_CLOCK_SYSTEM);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_A_PERIODIC);
    TimerLoadSet(TIMER1_BASE, TIMER_A, sysClock/1000);
    TimerIntRegister(TIMER1_BASE, TIMER_A, handler_TIM1);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    TimerEnable(TIMER1_BASE, TIMER_A);
    //NVIC
    IntEnable(INT_TIMER1A);

}


/********************timer test transmit*****************/
void Tim2_Init(void)
{
        //Timer2 - 100Hz
        SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER2))
        {
        }
        TimerClockSourceSet(TIMER2_BASE, TIMER_CLOCK_SYSTEM);
        TimerConfigure(TIMER2_BASE, TIMER_CFG_A_PERIODIC);
        TimerLoadSet(TIMER2_BASE, TIMER_A, sysClock/100);
        TimerIntRegister(TIMER2_BASE, TIMER_A, handler_TIM2);
        TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
        TimerEnable(TIMER2_BASE, TIMER_A);
        //NVIC
        IntEnable(INT_TIMER2A);
}



























