/*
 * InitPeripheral.h
 *
 *  Created on: 7 ��� 2019 �.
 *      Author: Alex Lepotsenko
 */

#ifndef INC_INITPERIPHERAL_H_
#define INC_INITPERIPHERAL_H_


#include "Main.h"



#pragma pack(push, 1)
typedef struct
{

    uint8_t                 status;
    uint32_t                Freq;
    uint32_t                Duty;
    uint32_t                channel[2];
    uint8_t                 Mode;//Complymentary or Single
    //void (*Init_PWM)(void);

}pwmDEF;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
    uint8_t LedStatus;
    uint32_t Adress_Pwm;
    pwmDEF DataPwm;

}SERVICE_DATA;
#pragma pack(pop)



typedef struct
{
    uint8_t ID;
    bool Status;
    uint32_t Port;
    uint8_t Pin;

    //void (*Init_BTN)(void);

}ButtonsDef;

typedef struct
{
    uint8_t ID;
    uint8_t Status;
    uint32_t Port;
    uint8_t Pin;
    //void (*Init_OUTPUT)(void);


}OUTPUTDef;





#define TivaC129PORTLed         GPIO_PORTN_BASE
#define TivaC129LED1            GPIO_PIN_0
#define TivaC129LED2            GPIO_PIN_1

#define TivaC129PORTBTN         GPIO_PORTJ_BASE
#define TivaC129BTN1            GPIO_PIN_0
#define TivaC129BTN2            GPIO_PIN_1



/**************************Init IO************************/
void GPIO_Init(void);
/****************Update Status Pin*************************/
void Update_Output(OUTPUTDef *out,uint8_t st);
/****************Read Button*************************/
uint8_t Read_Button(ButtonsDef *btn);
/******************Init Single PWM *****************/
void PWM_Single_Init(void);
/**********************Set Value Duty PWM Single Mode**********************/
void PWM_SetDuty_Single(pwmDEF *pwm,uint32_t val);
/********************timer update Data struct Led,pwm*****************/
void Tim1_Init(void);
/*****************************Settings Service UART6******************************/
void Init_UART6(uint32_t Baud);
/********************timer test transmit*****************/
void Tim2_Init(void);




#endif /* INC_INITPERIPHERAL_H_ */
