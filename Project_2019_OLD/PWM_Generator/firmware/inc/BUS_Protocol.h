/*
 * BUS_Protocol.h
 *
 *  Created on: 11 ��� 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_BUS_PROTOCOL_H_
#define INC_BUS_PROTOCOL_H_


#include "Main.h"

#define PAYLOAD_LEN             32

#pragma pack(push, 1)
typedef struct
{
    uint8_t dev_ID;
    uint8_t command;
    uint8_t payload[PAYLOAD_LEN];
    uint16_t CRC;


}Data_RS_485_half;


//TODO:Frame LONG Data(������ ��� �������� ����� �������� ������ ������)
    /*-1 ��������� ����������(dev_ID=ID,command=command|FLAG_LONGDATA,payload->count byte,CRC)
     * -2 ��������� �� ��������� � ������� ����������� ������
     *
     *
     *
     *
     *
     *
     */





#pragma pack(pop)



#define BUFFER_SIZE             sizeof(Data_RS_485_half)
#define BUFF_ENDINDEX           (sizeof(Data_RS_485_half)-1)
#define RS485_UART                  UART6_BASE

#define REQUEST_START               0x02
#define REQUEST_STOP                0x01
#define REQUEST_SET                 0x3F
#define REQUEST_RESET               0x1F
#define REQUEST_BOOT                0x1C
#define COMMAND_EMPTY               0x00

#define RESPONSE_OK                 0xFF
#define RESPONSE_END                0x1B
#define RESPONSE_I_WORK             0x1D
#define RESPONSE_I_SLEEP            0x2D
#define RESPONSE_M_DUTY             0x3D

void Handler_UART6(void);



/*************************Init RS-485*********************/
void RS485_Init(uint32_t baudrate, uint8_t id);
/*********************Function Get Id Device***************/
uint8_t Get_ID_RS485(void);
/*********************Function Set Id Device***************/
void Setup_Id_Device(uint8_t id);
/*********************Function Set Buad Device***************/
void RS485_setBaudrate(uint32_t baudrate);
/*********************return Stauts rxBuff*********************/
bool RS485_rxPkgAvailable(void);
 /*********************return Stauts rxBuff*********************/
bool RS485_txPkgAvailable(void);
void SetMuted(void);
void ResetMuted(void);
/************************Transmit Message*************************/
void Set_RS485MessageTX(Data_RS_485_half *data);
/***********************Function Calculate CRC******************************/
void GenerateTxDCRC(Data_RS_485_half *data);
/********************Function Check CRC RxD Packet********************/
bool CheckedRxDCRC();
/****************Function Parse Array Bytes in Struct************************/
void ParceStruct(Data_RS_485_half *data);
/**********************Get flag End Frame*****************/
bool GetFlagRessiever_End();


void handler_TIM1(void);


#endif /* INC_BUS_PROTOCOL_H_ */
