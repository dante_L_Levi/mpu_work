/*
 * RS_DataProtocol.h
 *
 *  Created on: 27 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_RS_DATAPROTOCOL_H_
#define INC_RS_DATAPROTOCOL_H_


#include "Main.h"


#define PAYLOAD         12

#pragma pack(push, 1)
typedef struct
{
    uint8_t Id_device;
    uint8_t Command;
    uint8_t BUF[PAYLOAD];
    uint16_t CRC;
}RS485_data;


#pragma pack(pop)

#define BUFFER_SIZE     sizeof(RS485_data)
#define BUFF_ENDINDEX   BUFFER_SIZE-1



/*************************Ressive Handler**************************/
void RS485_Receiver(void);
/**********************Get Flag Ressive New Byte data*****************/
uint8_t GetState_rxPacket(void);
/************************Init Protocol RS 485***************************/
void RS485_Init(uint32_t Baud, uint8_t Id);
/*****************Function Set Id*********************/
void RS485_setId(uint8_t id);
/*********************Function Get Id Rs-485***************************/
uint8_t RS485_getId(void);
/**********************Function Transmit Data ******************/
void RS485_setTxMessange(RS485_data *p);
/***************************************Parse RS485 Data***************************************/
void ConvertToRS485Data(RS485_data *p);
/**********************Calculate CRC16*****************************/
uint16_t TxCalculateCRC16(uint8_t *Data,uint8_t Length);
/********************************Function Check Summ Recieve Buffer***************/
bool CheckCRC16_RECIEVE(uint8_t *Data,uint8_t Length);
/********************Property End Frame Ressieve***********************/
bool IsEndFrame(void);


#endif /* INC_RS_DATAPROTOCOL_H_ */
