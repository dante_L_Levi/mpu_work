/*
 * Steering_Control.h
 *
 *  Created on: 27 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_STEERING_CONTROL_H_
#define INC_STEERING_CONTROL_H_


#include "Main.h"

/******************PAYLOAD**************************/
#pragma pack(push, 1)
typedef struct
{
    unsigned FlagEnableStart:1;//Enable Start
    unsigned ModeWork:2;//Mode:NULL,MAX,NORMAL
    uint32_t ValueDuty;
}STERRING_CONTROL_Def;
#pragma pack(pop)



/*************************Init Controls Wheels***************************/
void Init_WheelsControl(void);
/*******************Main Loop Work Wheels***************/
void Main_ControlWheels(void);


#endif /* INC_STEERING_CONTROL_H_ */
