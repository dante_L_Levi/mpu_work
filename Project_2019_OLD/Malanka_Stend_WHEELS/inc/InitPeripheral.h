/*
 * InitPeripheral.h
 *
 *  Created on: 9 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_INITPERIPHERAL_H_
#define INC_INITPERIPHERAL_H_


#include "Main.h"




typedef enum
{
    GPIO_Input,
    GPIO_Output,
    GPIO_Blocked


}typeGPIO;

typedef struct
{
    uint32_t Port;
    uint8_t  Pin;
    typeGPIO statusgpio;
    bool valuepin;
    uint32_t countPress;

}GPIO_Def;


#define PORT_RGB    GPIO_PORTF_BASE
#define PIN_R       GPIO_PIN_1
#define PIN_G       GPIO_PIN_2
#define PIN_B       GPIO_PIN_3


#define UART_RS485 UART1_BASE








/**********************Init GPIO*********************************/
void GPIO_Init(void);
/********************timer update Data struct Motor & Led Status*****************/
void Tim1_Init(void);
/****************************Init Hardware RS-485 *******************************/
void Init_Peripheral_RS485(uint32_t Baud);
/**********************Set Value GPIO Output***********************/
void SetGPIO_Bit(GPIO_Def *out,uint8_t val);
/**********************Init WHEEL PWM Control*******************/
void PWM3_WheelInit(void);

/**************Function Start GENERATOR*****************/
void Start_pwm3(void);
/**************Function Stop GENERATOR*****************/
void Stop_pwm3(void);
/************************Function Set Duty PWM Generator**********************/
void Set_pwm3Duty(uint32_t DUty);
/******************Test Function PWM3*****************/
void Test_Pwm3(uint8_t step);






#endif /* INC_INITPERIPHERAL_H_ */
