#ifndef INC_MAIN_H_
#define INC_MAIN_H_



#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"
#include "driverlib/adc.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"
#include "driverlib/i2c.h"



#include "InitPeripheral.h"
#include "RS_DataProtocol.h"
#include "Steering_Control.h"


/*************************Interrupt Timer 1 for Update Sruct**********************************/
void handler_TIM1(void);


#endif /* INC_MAIN_H_ */
