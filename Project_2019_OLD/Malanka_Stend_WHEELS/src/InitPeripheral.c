/*
 * InitPeripheral.c
 *
 *  Created on: 27 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#include "InitPeripheral.h"



extern GPIO_Def LED_R;
extern GPIO_Def LED_G;
extern GPIO_Def LED_B;
extern uint32_t CLOCK_SYS;


#define TIMER_FRQ       CLOCK_SYS
#define PWM_FREQ        10000
#define PWM_PERIOD      (TIMER_FRQ / PWM_FREQ)
#define Motor1_ValStart         5

void InitGpio_Struct(void)
{
    LED_R.Port=PORT_RGB;
    LED_R.Pin=GPIO_PIN_1;
    LED_R.statusgpio=GPIO_Output;
    LED_R.valuepin=false;

    LED_G.Port=PORT_RGB;
    LED_G.Pin=GPIO_PIN_2;
    LED_G.statusgpio=GPIO_Output;
    LED_G.valuepin=false;

    LED_B.Port=PORT_RGB;
    LED_B.Pin=GPIO_PIN_3;
    LED_B.statusgpio=GPIO_Output;
    LED_B.valuepin=false;

}

/**********************Init GPIO*********************************/
void GPIO_Init(void)
{
    InitGpio_Struct();
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(LED_R.Port,LED_R.Pin);
    GPIOPinTypeGPIOOutput(LED_G.Port,LED_G.Pin);
    GPIOPinTypeGPIOOutput(LED_B.Port,LED_B.Pin);
    GPIOPinWrite(LED_R.Port,LED_R.Pin, 0x00);
    GPIOPinWrite(LED_G.Port,LED_G.Pin, 0x00);
    GPIOPinWrite(LED_B.Port,LED_B.Pin, 0x00);
}

/**********************Set Value GPIO Output***********************/
void SetGPIO_Bit(GPIO_Def *out,uint8_t val)
{
   GPIOPinWrite(out->Port,out->Pin, val);
   out->valuepin=~(out->valuepin);
}


/********************timer update Data struct Motor & Led Status*****************/
void Tim1_Init(void)
{
    //Timer1 - 2Hz
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1))
    {
    }
    TimerClockSourceSet(TIMER1_BASE, TIMER_CLOCK_SYSTEM);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_A_PERIODIC);
    TimerLoadSet(TIMER1_BASE, TIMER_A, CLOCK_SYS/2);
    TimerIntRegister(TIMER1_BASE, TIMER_A, handler_TIM1);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    TimerEnable(TIMER1_BASE, TIMER_A);
    //NVIC
    IntEnable(INT_TIMER1A);

}

/****************************Init Hardware RS-485 *******************************/
void Init_Peripheral_RS485(uint32_t Baud)
{
        SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART1))
        {
        }
        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOC))
        {
        }
        GPIOPinConfigure(GPIO_PC4_U1RX);
        GPIOPinConfigure(GPIO_PC5_U1TX);
        GPIOPinTypeUART(GPIO_PORTC_BASE,GPIO_PIN_4|GPIO_PIN_5);
        IntDisable(INT_UART1);
        UARTDisable(UART1_BASE);
        UARTClockSourceSet(UART1_BASE,UART_CLOCK_PIOSC);
        UARTConfigSetExpClk(UART1_BASE, CLOCK_SYS, Baud,
                                   (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                    UART_CONFIG_PAR_NONE));
        UARTFIFODisable(UART1_BASE);
        UARTIntEnable(UART1_BASE, UART_INT_RX | UART_INT_RT);
        UARTIntRegister(UART1_BASE, RS485_Receiver);
        IntEnable(INT_UART1);
        UARTEnable(UART1_BASE);
}

/**********************Init WHEEL PWM Control*******************/
void PWM3_WheelInit(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    GPIOPinConfigure(GPIO_PB5_M0PWM3);
    GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_5);
    PWMGenConfigure(PWM0_BASE, PWM_GEN_1, PWM_GEN_MODE_UP_DOWN |
                                PWM_GEN_MODE_NO_SYNC);
            // Set the PWM period to 250Hz
            //use the following equation: N = (1 / f) * SysClk
            // In this case you get: (1 / 100Hz) * 200MHz = 1200000 cycles.
            //TIMER_FRQ / PWM_FREQ=200000000/10000
     PWMGenPeriodSet(PWM0_BASE, PWM_GEN_1, PWM_PERIOD);
            // 25% of the time or 16000 clock cycles (120000 / 4).
     PWMPulseWidthSet(PWM0_BASE, PWM_OUT_3,Motor1_ValStart);

            // Enable the PWM3 Bit 6 (PB5) and Bit 1 (PD1) output signals.
            //
     PWMOutputState(PWM0_BASE,PWM_OUT_3_BIT, false);
            // Enables the counter for a PWM generator block.
            //
        //PWMGenEnable(PWM0_BASE, PWM_GEN_0);

     PWMSyncTimeBase(PWM0_BASE, PWM_GEN_1_BIT);
}

/**************Function Start GENERATOR*****************/
void Start_pwm3(void)
{

    PWMOutputState(PWM0_BASE,PWM_OUT_3_BIT, true);
    PWMGenEnable(PWM0_BASE, PWM_GEN_1_BIT);


}
/**************Function Stop GENERATOR*****************/
void Stop_pwm3(void)
{

    PWMOutputState(PWM0_BASE,PWM_OUT_3_BIT, false);
    PWMGenDisable(PWM0_BASE, PWM_GEN_1);

}

/************************Function Set Duty PWM Generator**********************/
void Set_pwm3Duty(uint32_t DUty)
{
    if(DUty<=19999)
    {
        // 25% of the time or 16000 clock cycles (120000 / 4).
        PWMPulseWidthSet(PWM0_BASE, PWM_OUT_3,DUty);
    }
    else
    {
        return;
    }

}


/******************Test Function PWM3*****************/
void Test_Pwm3(uint8_t step)
{
    switch(step)
    {
        case 1:
        {
            Set_pwm3Duty(0);
            break;
        }
        case 2:
        {
            Set_pwm3Duty(200);
            break;
        }
        case 3:
        {
            Set_pwm3Duty(5000);
            break;
        }
        case 4:
        {
            Set_pwm3Duty(10000);
            break;
        }
        case 5:
        {
            Set_pwm3Duty(15000);
            break;
        }
        case 6:
        {
            Set_pwm3Duty(19990);
            break;
        }

    }


}
