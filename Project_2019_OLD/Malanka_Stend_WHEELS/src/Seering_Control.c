/*
 * Seering_Control.c
 *
 *  Created on: 27 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */



#include "Steering_Control.h"

//buffer ReccieveData Uart Port
static uint8_t PayLoadBuffer_wheels[PAYLOAD]={0};
//channel work uart
extern  RS485_data channelData;
STERRING_CONTROL_Def DataWheels;


/*****************************Read UART Buffer PayLoad*******************/
static void Wheels_ConvertedPayload(void);
/***************************Update Status Struct STERRING_CONTROL_Def************/
static void Update_StatusWheels_Control(void);
/************************Update Status Work*************/
static void UpdateStatus(STERRING_CONTROL_Def *dt);
/******************************************************************************/


/*****************************Read UART Buffer PayLoad*******************/
static void Wheels_ConvertedPayload(void)
{
    memcpy((void*)PayLoadBuffer_wheels,(void*)channelData.BUF,sizeof(PayLoadBuffer_wheels));
    memcpy(&DataWheels,(void*)PayLoadBuffer_wheels,sizeof(STERRING_CONTROL_Def));
}

/************************Update Status Work*************/
static void UpdateStatus(STERRING_CONTROL_Def *dt)
{
    if(dt->FlagEnableStart==0x00)
    {
        Stop_pwm3();
    }
    else if(dt->FlagEnableStart==0x01)
    {
        Start_pwm3();
        switch(dt->ModeWork)
            {
                case 0x00:
                {
                    Set_pwm3Duty(0);
                    break;

                }
                case 0x01:
                {
                    Set_pwm3Duty(19999);
                    break;
                }
                case 0x02:
                {
                    Set_pwm3Duty(dt->ValueDuty);
                }

                default:
                {

                    return;
                }

            }
    }

}

/***************************Update Status Struct STERRING_CONTROL_Def************/
static void Update_StatusWheels_Control(void)
{
    Wheels_ConvertedPayload();
    UpdateStatus(&DataWheels);
}

/*************************Init Controls Wheels***************************/
void Init_WheelsControl(void)
{
    DataWheels.ModeWork=0x00;
    DataWheels.ModeWork=0x00;
    DataWheels.ValueDuty=1;
    PWM3_WheelInit();
    Stop_pwm3();
}


/*******************Main Loop Work Wheels***************/
void Main_ControlWheels(void)
{
    Update_StatusWheels_Control();
}









