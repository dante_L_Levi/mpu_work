/*
 * main.c
 *
 *  Created on: 27 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */
#include "Main.h"

const uint8_t ID_DEVICE=0x0C;
GPIO_Def LED_R;
GPIO_Def LED_G;
GPIO_Def LED_B;
uint32_t CLOCK_SYS;
uint8_t count_Led=0;
RS485_data channelData;
/*************************Indication Test********************************/
static void Indication_Led(uint8_t cnt);




void handler_TIM1(void)
{
    if(count_Led>2)
        {
            count_Led=0;
        }

        Indication_Led(count_Led);
         count_Led++;
        TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);

}




void RCC_Init(void)
{
    // Set the clocking to run directly from the PLL at 20 MHz.
        // The following code:
        // -sets the system clock divider to 10 (200 MHz PLL divide by 10 = 20 MHz)
        // -sets the system clock to use the PLL
        // -uses the main oscillator
        // -configures for use of 16 MHz crystal/oscillator input
    SysCtlClockSet(SYSCTL_SYSDIV_10 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                       SYSCTL_XTAL_16MHZ);
}


void main(void)
{
    RCC_Init();
    CLOCK_SYS=SysCtlClockGet();
    GPIO_Init();
    RS485_Init(115200,ID_DEVICE);
    Tim1_Init();
    IntMasterEnable();
    while(1)
    {

    }
}



/*************************Indication Test********************************/
static void Indication_Led(uint8_t cnt)
{
    switch(cnt)
    {
        case 0:
        {
            SetGPIO_Bit(&LED_R,0x02);
            SetGPIO_Bit(&LED_B,0x00);
            SetGPIO_Bit(&LED_G,0x00);
            break;
        }
        case 1:
        {
            SetGPIO_Bit(&LED_R,0x00);
            SetGPIO_Bit(&LED_B,0x00);
            SetGPIO_Bit(&LED_G,0x04);
            break;
        }
        case 2:
        {
            SetGPIO_Bit(&LED_R,0x00);
            SetGPIO_Bit(&LED_B,0x08);
            SetGPIO_Bit(&LED_G,0x00);
            break;
        }
        default:
        {
            SetGPIO_Bit(&LED_R,0x00);
            SetGPIO_Bit(&LED_B,0x00);
            SetGPIO_Bit(&LED_G,0x00);
            break;
        }


    }
}

