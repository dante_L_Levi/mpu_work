/*
 * RS_DataProtocol.c
 *
 *  Created on: 27 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#include "RS_DataProtocol.h"

#define RS485_TRANSMIT GPIOPinWrite(GPIO_PORTC_BASE, GPIO_PIN_6, 0xFF);
#define RS485_RECEIVE  GPIOPinWrite(GPIO_PORTC_BASE, GPIO_PIN_6, 0);

extern RS485_data channelData;


#define MCU_Clock 120000000

static uint8_t RS485_DEV_ID=0x0B;
static uint8_t isMuted = 0;
static volatile uint8_t RX_BUFF[BUFFER_SIZE]={0};
static volatile uint8_t rx_new_messange_ready_flag = 0;
bool FlagEndFrame=false;
uint32_t index_ressive=0;
static volatile uint8_t TX_BUFF[BUFFER_SIZE];
static volatile uint8_t tx_new_messange_ready_flag = 0;

/*******************helper Save bytes in Buffer***********************/
static void RS485_RessiveData_helper(void);
static void flush_read_fifo(void);


/*************************Ressive Handler**************************/
void RS485_Receiver(void)
{
    uint32_t intStatus;
    rx_new_messange_ready_flag=true;
    intStatus=UARTIntStatus(UART_RS485,true);
    while(UARTCharsAvail(UART_RS485)!=false)
        {

            RS485_RessiveData_helper();
            index_ressive++;//Count Packet


        }
    UARTIntClear(UART_RS485, UART_INT_RX);
}


/*******************helper Save bytes in Buffer***********************/
static void RS485_RessiveData_helper(void)
{
    FlagEndFrame=false;
    //uint8_t Ressieve_Byte;
    uint16_t i;
    for(i=0;i<BUFFER_SIZE;i++)
    {
      RX_BUFF[i] =UARTCharGet(UART_RS485);

    }

    FlagEndFrame=true;
    if(RX_BUFF[0]==RS485_DEV_ID)
    {
        //Control CRC
        if(CheckCRC16_RECIEVE((uint8_t *)RX_BUFF,(uint8_t)BUFFER_SIZE)==true)
        {
            ConvertToRS485Data(&channelData);
            flush_read_fifo();
        }
        else
        {
            return;
        }


    }
    else
    {
        return;
    }


}

/********************Property End Frame Ressieve***********************/
bool IsEndFrame(void)
{
    return FlagEndFrame;
}

static void flush_read_fifo(void)
{
    UARTIntDisable(UART_RS485, UART_INT_RX);
    UARTIntClear(UART_RS485, UART_INT_RX);
    UARTIntEnable(UART_RS485, UART_INT_RX);
}
static void GPIO_Init_RS485(void)
{
    //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);//PIN Control RS485
    GPIOPinTypeGPIOOutput(GPIO_PORTC_BASE,GPIO_PIN_6);

    RS485_RECEIVE;
}





/************************Init Protocol RS 485***************************/
void RS485_Init(uint32_t Baud, uint8_t Id)
{
    Init_Peripheral_RS485(Baud);
    //UARTFIFODisable(RS485_UART);
    GPIO_Init_RS485();
    RS485_setId(Id);

}

/**********************Get Flag Ressive New Byte data*****************/
uint8_t GetState_rxPacket(void)
{
    return rx_new_messange_ready_flag;

}

/*****************Function Set Id*********************/
void RS485_setId(uint8_t id)
{
    RS485_DEV_ID = id;
}

/*********************Function Get Id Rs-485***************************/
uint8_t RS485_getId(void)
{
    return RS485_DEV_ID;
}


/***************************SET Buad Channel****************************/
void RS485_setBaudrate(uint32_t baudrate)
{
    UARTConfigSetExpClk(UART_RS485, MCU_Clock, baudrate,
                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                         UART_CONFIG_PAR_NONE));
}

/**********************Function Transmit Data ******************/
void RS485_setTxMessange(RS485_data *p)
{
    if (isMuted)
            return; //
        RS485_TRANSMIT;
        memcpy((void *)TX_BUFF, p, BUFFER_SIZE);
        uint16_t tempCRC=TxCalculateCRC16((uint8_t *)TX_BUFF,(uint8_t)BUFFER_SIZE-2);
        p->CRC=tempCRC;
        TX_BUFF[BUFFER_SIZE-2]=(uint8_t)tempCRC;
        TX_BUFF[BUFFER_SIZE-1]=(uint8_t)(tempCRC>>8);
        uint16_t i=0;
        for(i=0;i<BUFFER_SIZE;i++)
        {
            UARTCharPut(UART_RS485,TX_BUFF[i]);
        }
        RS485_RECEIVE;

}
/**********************Calculate CRC16*****************************/
uint16_t TxCalculateCRC16(uint8_t *Data,uint8_t Length)
{
    uint8_t j;
        uint16_t reg_crc = 0xFFFF;
        while (Length--)
        {
          reg_crc^= *Data++;
          for(j=0;j<8;j++)
           {
             if(reg_crc & 0x01)
              {
                reg_crc = (reg_crc >> 1) ^ 0xA001;
              }
            else
              {
                reg_crc = reg_crc >> 1;
              }
           }
        }
        return reg_crc;

}

/********************************Function Check Summ Recieve Buffer***************/
bool CheckCRC16_RECIEVE(uint8_t *Data,uint8_t Length)
{

    uint16_t crc_buf=0;
    crc_buf=(Data[Length-1]<<8)|Data[Length-2];

    uint16_t crc = 0xFFFF;
    uint8_t pos;
    for(pos=0;pos<Length-2;pos++)
    {
        crc^=(uint16_t)Data[pos];
    }
    uint8_t i;
    for (i = 8; i != 0; i--)
    {
        if ((crc & 0x0001) != 0)
                        {
                            crc >>= 1;
                            crc ^= 0xA001;
                        }
                        else
                        {
                            crc >>= 1;
                        }
    }
    if(crc==crc_buf)
    {
        return true;
    }
    else
    {
        return false;
    }

}



/***************************************Parse RS485 Data***************************************/
void ConvertToRS485Data(RS485_data *p)
{
    memcpy(p,(void*)RX_BUFF,BUFFER_SIZE);
}








