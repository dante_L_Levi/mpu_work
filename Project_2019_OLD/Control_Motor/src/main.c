/*
 * main.c
 *
 *  Created on: 22 ��� 2019 �.
 *      Author: Tatiana_Potrebko
 */

#include "Main.h"

uint32_t sysClock;
uint8_t Id=0x1A;
GPIO_Def LED_status1;
GPIO_Def LED_status2;

GPIO_Def LBRITCH1;
GPIO_Def LBRITCH2;

GPIO_Def Btn_startW;//button start work
MotorDef Motor1;
SERVICE_DATA DataPayload;
RS485_data channelData;


uint8_t Flag_work=0x00;
/****************************Vector TIM1*******************/
void handler_TIM1(void)
{

}

/************************Main Work ************************/
void System_SCAN(void);


void RCC_Init(void)
{
    sysClock= SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                                               SYSCTL_OSC_MAIN |
                                               SYSCTL_USE_PLL |
                                               SYSCTL_CFG_VCO_480),
                                               120000000);
}




void main(void)
{

    RCC_Init();
    GPIO_Init();
    RS485_Init(115200,0x1A);
    Motor_init();
    IntMasterEnable();

    while(1)
    {
        if(!(Read_Button(&Btn_startW)))
        {
           if(Btn_startW.countPress%2!=0)
           {
               //Start Work
               SetGPIO_Bit(&LED_status1,0x01);
           }
           else if(Btn_startW.countPress%2==0)
           {
               SetGPIO_Bit(&LED_status1,0x00);
           }

        }

    }
}



void ConvertToMotoeData(MotorDef *p,SERVICE_DATA *sys)
{
    MotorDef *tempData=&(sys->DataPayLoadMotor);
    memcpy(p,tempData,sizeof(MotorDef));

}

/***************************************Parse RS485 Data***************************************/
void ConvertToSERVICEDATA(SERVICE_DATA *point,RS485_data *ch)
{
    memcpy(point,(void*)ch->BUF,8);
}


/************************Main Work ************************/
void System_SCAN(void)
{
    //***********************************Manual Mode-Function***************************
    //1-���������� ���� ������ ������ 1 ������ � ���� ������� � ���� ���������

    //2- ������ ������ �������� �� ��������� ����� ���/���� ������



    //============================================================================

    //*********************************Auto Mode Control PC*****************************
    //1) ��������� ���� ������ ������ if true �������� ������� void ConvertToSERVICEDATA(SERVICE_DATA *point,RS485_data *ch)
    if(IsEndFrame()==true)//��������� �������� ������ ������
    {
        ConvertToSERVICEDATA(&DataPayload,&channelData);
        //2)�� SERVICE_DATA->������������ � Motor1Def
        //3)��������� ������� ��������� Motor1def
        ConvertToMotoeData(&Motor1,&DataPayload);
        //4) ��������� hardware ������ �� ����� ���������
        Updte_Motor_HRW(&Motor1);
    }





}
