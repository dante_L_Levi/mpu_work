#include "InitPeripheral.h"

extern uint32_t sysClock;
extern GPIO_Def LED_status1;
extern GPIO_Def LED_status2;
extern GPIO_Def Btn_startW;//button start work
extern MotorDef Motor1;
extern GPIO_Def LBRITCH1;
extern GPIO_Def LBRITCH2;

#define TIMER_FRQ       120000000
#define PWM_FREQ        20000
#define PWM_PERIOD      (TIMER_FRQ / PWM_FREQ)

#define Motor1_ValStart         10


void Init_structGPIO(void)
{
    /****************GPIO OUT PORTN PIN0 --LED1**********************/
    LED_status1.Port=TivaC129LAUNCH_PORTLed;
    LED_status1.Pin=TivaC129LAUNCH_LED1;
    LED_status1.statusgpio=GPIO_Output;
    LED_status1.valuepin=false;
    /****************GPIO OUT PORTN PIN1 --LED1**********************/
    LED_status2.Port=TivaC129LAUNCH_PORTLed;
    LED_status2.Pin=TivaC129LAUNCH_LED2;
    LED_status2.statusgpio=GPIO_Output;
    LED_status2.valuepin=false;
    /****************GPIO OUT PORTJ PIN0 --LED1**********************/
    Btn_startW.Port=TivaC129LAUNCH_PORTBTN1;
    Btn_startW.Pin=TivaC129BTN1;
    Btn_startW.statusgpio=GPIO_Input;

}

/**********************Init GPIO*********************************/
void GPIO_Init(void)
{
    Init_structGPIO();
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(LED_status1.Port,LED_status1.Pin);
    GPIOPinTypeGPIOOutput(LED_status2.Port,LED_status2.Pin);

    GPIOPinWrite(LED_status1.Port,LED_status1.Pin, 0x00);
    GPIOPinWrite(LED_status2.Port,LED_status2.Pin, 0x00);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);
    GPIOPinTypeGPIOInput(Btn_startW.Port, Btn_startW.Pin);//config button
    GPIOPadConfigSet(Btn_startW.Port, Btn_startW.Pin,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);//config Input: current 2mA,pull-up

    //�������� ������������� ����� ���������� mosfet ��� � ������ ������ ������

}



/*****************************Settings Service UART6******************************/
void Init_UART6(uint32_t Baud)
{

        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
        GPIOPinConfigure(GPIO_PP0_U6RX);
        GPIOPinConfigure(GPIO_PP1_U6TX);
        GPIOPinTypeUART(GPIO_PORTP_BASE,GPIO_PIN_0|GPIO_PIN_1);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOP))
        {
        }
        SysCtlPeripheralEnable(SYSCTL_PERIPH_UART6);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART6))
        {
        }

        UARTClockSourceSet(UART6_BASE,UART_CLOCK_SYSTEM) ;
        UARTConfigSetExpClk(UART6_BASE, sysClock, Baud,
                           (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                            UART_CONFIG_PAR_NONE));
        UARTIntRegister(UART6_BASE, Handler_UART6);
        IntEnable(INT_UART6);
        UARTIntEnable(UART6_BASE, UART_INT_RX | UART_INT_RT);
        UARTEnable(UART6_BASE);

}

/********************timer update Data struct Led,pwm*****************/
void Tim1_Init(void)
{
    //Timer1 - 100Hz
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1))
    {
    }
    TimerClockSourceSet(TIMER1_BASE, TIMER_CLOCK_SYSTEM);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_A_PERIODIC);
    TimerLoadSet(TIMER1_BASE, TIMER_A, sysClock/100);
    TimerIntRegister(TIMER1_BASE, TIMER_A, handler_TIM1);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    TimerEnable(TIMER1_BASE, TIMER_A);
    //NVIC
    IntEnable(INT_TIMER1A);

}

/**********************Init Motor1 DC PWM Forward*******************/
void PWM0_DCMotorInit(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinConfigure(GPIO_PF0_M0PWM0);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_0);
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_UP_DOWN |
                            PWM_GEN_MODE_NO_SYNC);
        // Set the PWM period to 250Hz
        //use the following equation: N = (1 / f) * SysClk
        // In this case you get: (1 / 100Hz) * 120MHz = 1200000 cycles.
        //TIMER_FRQ / PWM_FREQ=120000000/1000
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, PWM_PERIOD);
        // 25% of the time or 16000 clock cycles (120000 / 4).
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0,Motor1_ValStart);

        // Enable the PWM0 Bit 0 (PD0) and Bit 1 (PD1) output signals.
        //
    PWMOutputState(PWM0_BASE,PWM_OUT_0_BIT, false);
        // Enables the counter for a PWM generator block.
        //
    //PWMGenEnable(PWM0_BASE, PWM_GEN_0);

    PWMSyncTimeBase(PWM0_BASE, PWM_GEN_0_BIT);
}


/**********************Set Duty PWM 0*************************/
void Set_Duty_Pwm0(uint32_t dt)
{
   PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0,dt);
}

/**********************Set Duty PWM 1*************************/
void Set_Duty_Pwm1(uint32_t dt)
{
   PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2,dt);
}

/**************Function Start GENERATOR*****************/
void Start_pwm0(void)
{

    PWMOutputState(PWM0_BASE,PWM_OUT_0_BIT, true);
    PWMGenEnable(PWM0_BASE, PWM_GEN_0);


}
/**************Function Stop GENERATOR*****************/
void Stop_pwm0(void)
{

    PWMOutputState(PWM0_BASE,PWM_OUT_0_BIT, false);
    PWMGenDisable(PWM0_BASE, PWM_GEN_0);

}

/**********************Init Motor1 DC PWM Reverse*******************/
void PWM1_DCMotorInit(void)
{
        SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
        GPIOPinConfigure(GPIO_PF2_M0PWM2);
        GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_2);
        PWMGenConfigure(PWM0_BASE, PWM_GEN_1, PWM_GEN_MODE_UP_DOWN |
                                PWM_GEN_MODE_NO_SYNC);
            // Set the PWM period to 250Hz
            //use the following equation: N = (1 / f) * SysClk
            // In this case you get: (1 / 100Hz) * 120MHz = 1200000 cycles.
            //TIMER_FRQ / PWM_FREQ=120000000/1000
        PWMGenPeriodSet(PWM0_BASE, PWM_GEN_1, PWM_PERIOD);
            // 25% of the time or 16000 clock cycles (120000 / 4).
        PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2,Motor1_ValStart);

            // Enable the PWM0 Bit 0 (PD0) and Bit 1 (PD1) output signals.
            //
        PWMOutputState(PWM0_BASE,PWM_OUT_2_BIT, true);
            // Enables the counter for a PWM generator block.
            //
        PWMGenEnable(PWM0_BASE, PWM_GEN_1);

        PWMSyncTimeBase(PWM0_BASE, PWM_GEN_1_BIT);
}
/**************Function Start GENERATOR*****************/
void Start_pwm1(void)
{

    PWMOutputState(PWM0_BASE,PWM_OUT_2_BIT, true);
    PWMGenEnable(PWM0_BASE, PWM_GEN_1);

}
/**************Function Stop GENERATOR*****************/
void Stop_pwm1(void)
{

    PWMOutputState(PWM0_BASE,PWM_OUT_2_BIT, false);
    PWMGenDisable(PWM0_BASE, PWM_GEN_1);

}

/*************************init Motor**************************/
void Motor_init(void)
{
    PWM0_DCMotorInit();
    PWM1_DCMotorInit();
    Motor1.id_m=0x01;
    //Motor1.Dir=0xFF;
    Motor1.pwmSpeed=PWMPulseWidthGet(PWM0_BASE, PWM_OUT_0);

}


/**********************Set Value GPIO Output***********************/
void SetGPIO_Bit(GPIO_Def *out,uint8_t val)
{
   GPIOPinWrite(out->Port,out->Pin, val);
   out->valuepin=~(out->valuepin);
}

/****************Read Button*************************/
uint8_t Read_Button(GPIO_Def *btn)
{
    int32_t stategpio=GPIOPinRead(btn->Port,btn->Pin);
    int32_t i_t=0;

    if(!stategpio)
    {
        btn->countPress++;
        while(i_t<15000)
            {
                i_t++;
            }
    }

    return stategpio;
}

/***************************Function Update Hardware Data**********************/
void Updte_Motor_HRW(MotorDef *data)
{
    if(data->id_m==0x01)
    {
        if(data->status==0xFF)//work
        {
            if(data->Dir==0xFF) //Forward
                    {
                         //����. pwm1
                         Stop_pwm1();
                         //����. LBRITCH1
                         SetGPIO_Bit(&LBRITCH1,0x00);
                        //��� pwm0
                        Start_pwm0();
                        //��� gpio LBRITCH2
                        SetGPIO_Bit(&LBRITCH2,0x02);
                        //���������� �������� ���

                        Set_Duty_Pwm0(data->pwmSpeed);
                    }
                    else //Reverse
                    {
                        //����. pwm0
                        Stop_pwm0();
                        //����. LBRITCH1
                        SetGPIO_Bit(&LBRITCH2,0x00);

                        //��� pwm1
                        Start_pwm1();
                       //��� gpio LBRITCH1
                        SetGPIO_Bit(&LBRITCH1,0x03);
                        //���������� �������� ���
                        Set_Duty_Pwm1(data->pwmSpeed);
                    }
        }
        else
        {
            Stop_pwm0();
            Stop_pwm1();
            SetGPIO_Bit(&LBRITCH2,0x00);
            SetGPIO_Bit(&LBRITCH1,0x00);
        }

    }
}



