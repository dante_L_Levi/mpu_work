/*
 * Data_Protocol.c
 *
 *  Created on: 22 ��� 2019 �.
 *      Author: Tatiana_Potrebko
 */




#include "Data_Protocol.h"

#define RS485_UART UART6_BASE

extern RS485_data channelData;
#define RS485_TRANSMIT GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_5, 0xFF);
#define RS485_RECEIVE  GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_5, 0);


#define MCU_SPEED 120000000

static uint8_t RS485_DEV_ID=0x0A;
static uint8_t isMuted = 0;
static volatile uint8_t RX_BUFF[BUFFER_SIZE]={0};
static volatile uint8_t rx_new_messange_ready_flag = 0;
bool FlagEndFrame=false;
uint32_t index_ressive=0;
static volatile uint8_t TX_BUFF[BUFFER_SIZE];
static volatile uint8_t tx_new_messange_ready_flag = 0;


/*******************helper Save bytes in Buffer***********************/
static void RS485_RessiveData_helper(void);
static void flush_read_fifo(void);


/*************************Ressive Handler**************************/
void Handler_UART6(void)
{
    uint32_t intStatus;

    rx_new_messange_ready_flag=true;
    intStatus=UARTIntStatus(RS485_UART,true);
    if ((UARTCharsAvail(RS485_UART)))
    {
        UARTIntClear(RS485_UART, UART_INT_RX);
        RS485_RessiveData_helper();
        index_ressive++;//Count Packet


    }
}


/*******************helper Save bytes in Buffer***********************/
static void RS485_RessiveData_helper(void)
{

            FlagEndFrame=false;
            //uint8_t Ressieve_Byte;
            uint16_t i;
            for(i=0;i<BUFFER_SIZE;i++)
            {
                RX_BUFF[i] =UARTCharGet(RS485_UART);

            }

            FlagEndFrame=true;
            ConvertToRS485Data(&channelData);
            flush_read_fifo();


}

/********************Property End Frame Ressieve***********************/
bool IsEndFrame(void)
{
    return FlagEndFrame;
}

static void flush_read_fifo(void)
{
    UARTIntDisable(RS485_UART, UART_INT_RX);
    UARTIntClear(RS485_UART, UART_INT_RX);
    UARTIntEnable(RS485_UART, UART_INT_RX);
}
static void GPIO_Init_RS485(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);//PIN Control RS485
    GPIOPinTypeGPIOOutput(GPIO_PORTK_BASE,GPIO_PIN_5);

    RS485_RECEIVE;
}

/************************Init Protocol RS 485***************************/
void RS485_Init(uint32_t Baud, uint8_t Id)
{
    Init_UART6(Baud);
    //UARTFIFODisable(RS485_UART);
    GPIO_Init_RS485();
    RS485_DEV_ID = Id;
}

/**********************Get Flag Ressive New Byte data*****************/
uint8_t GetState_rxPacket(void)
{
    return rx_new_messange_ready_flag;

}
/*****************Function Set Id*********************/
void RS485_setId(uint8_t id)
{
    RS485_DEV_ID = id;
}

uint8_t RS485_getId()
{
    return RS485_DEV_ID;
}

/***************************SET Buad Channel****************************/
void RS485_setBaudrate(uint32_t baudrate)
{
    UARTConfigSetExpClk(RS485_UART, MCU_SPEED, baudrate,
                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                         UART_CONFIG_PAR_NONE));
}

/********************Get Ressive Frame*************************/
void RS485_GetPacket(RS485_data *p)
{
    memcpy(p, (void *)RX_BUFF, BUFFER_SIZE);
}


/**********************Function Transmit Data ******************/
void RS485_setTxMessange(RS485_data *p)
{
    if (isMuted)
            return; //
        RS485_TRANSMIT;
        memcpy((void *)TX_BUFF, p, BUFFER_SIZE);
        //generateTxPkgCrc();
        //RS485_STATE = TX_MODE;
        uint16_t i=0;
        for(i=0;i<BUFFER_SIZE;i++)
        {
            UARTCharPut(RS485_UART,TX_BUFF[i]);
        }
        RS485_RECEIVE;

}


/***************************************Parse RS485 Data***************************************/
void ConvertToRS485Data(RS485_data *p)
{
    memcpy(p,(void*)RX_BUFF,BUFFER_SIZE);
}





