/*
 * InitPeripheral.h
 *
 *  Created on: 22 ��� 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_INITPERIPHERAL_H_
#define INC_INITPERIPHERAL_H_


#include "Main.h"
#include "Data_Protocol.h"


#pragma pack(push, 1)
typedef struct
{
    uint8_t id_m;
    uint8_t status;
    uint8_t Dir;//0xFF-   ->,  <-  0x00
    uint32_t pwmSpeed;

}MotorDef;
#pragma pack(pop)

#define  SIZE_FRAME_PWMDEF       sizeof(MotorDef)

#pragma pack(push, 1)
typedef struct
{
       MotorDef DataPayLoadMotor;
}SERVICE_DATA;
#pragma pack(pop)



typedef enum
{
    GPIO_Input,
    GPIO_Output,
    GPIO_Blocked


}typeGPIO;

typedef struct
{
    uint32_t Port;
    uint8_t  Pin;
    typeGPIO statusgpio;
    bool valuepin;
    uint32_t countPress;

}GPIO_Def;



#define TivaC129LAUNCH_PORTLed              GPIO_PORTN_BASE
#define TivaC129LAUNCH_LED1                 GPIO_PIN_0
#define TivaC129LAUNCH_LED2                 GPIO_PIN_1

#define TivaC129LAUNCH_PORTBTN1             GPIO_PORTJ_BASE
#define TivaC129LAUNCH_PORTBTN2             GPIO_PORTJ_BASE
#define TivaC129BTN1                        GPIO_PIN_0
#define TivaC129BTN2                        GPIO_PIN_1

/**********************Init GPIO*********************************/
void GPIO_Init(void);
/*****************************Settings Service UART6******************************/
void Init_UART6(uint32_t Baud);
/**********************Init Motor1 DC*******************/
void PWM0_DCMotorInit(void);
/**************Function Start GENERATOR*****************/
void Start_pwm0(void);
/**************Function Stop GENERATOR*****************/
void Stop_pwm0(void);
/**********************Init Motor1 DC PWM Reverse*******************/
void PWM1_DCMotorInit(void);

/*************************init Motor**************************/
void Motor_init(void);
/**********************Set Value GPIO Output***********************/
void SetGPIO_Bit(GPIO_Def *out,uint8_t val);
/****************Read Button*************************/
uint8_t Read_Button(GPIO_Def *btn);
/***************************Function Update Hardware Data**********************/
void Updte_Motor_HRW(MotorDef *data);

#endif /* INC_INITPERIPHERAL_H_ */
