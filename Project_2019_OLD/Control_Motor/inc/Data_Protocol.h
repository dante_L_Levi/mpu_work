/*
 * Data_Protocol.h
 *
 *  Created on: 22 ��� 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_DATA_PROTOCOL_H_
#define INC_DATA_PROTOCOL_H_


#include "Main.h"
#include "InitPeripheral.h"



#define PAYLOAD         24

#pragma pack(push, 1)
typedef struct
{
    uint8_t Id_device;
    uint8_t Command;
    uint8_t BUF[PAYLOAD];
    uint16_t CRC;
}RS485_data;


#pragma pack(pop)

#define BUFFER_SIZE     sizeof(RS485_data)
#define BUFF_ENDINDEX   BUFFER_SIZE-1
/*************************Ressive Handler**************************/
void Handler_UART6(void);
/************************Init Protocol RS 485***************************/
void RS485_Init(uint32_t Baud, uint8_t Id);
/**********************Get Flag Ressive New Byte data*****************/
uint8_t GetState_rxPacket(void);
/*****************Function Set Id*********************/
void RS485_setId(uint8_t id);
/**********************Function Transmit Data ******************/
void RS485_setTxMessange(RS485_data *p);
/********************Get Ressive Frame*************************/
void RS485_GetPacket(RS485_data *p);
/***************************SET Buad Channel****************************/
void RS485_setBaudrate(uint32_t baudrate);
/***************************************Parse Main Data***************************************/
void ConvertToRS485Data(RS485_data *p);
/********************Property End Frame Ressieve***********************/
bool IsEndFrame(void);



#endif /* INC_DATA_PROTOCOL_H_ */
