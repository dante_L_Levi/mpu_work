/*
 * Gyro_RF_Logic.h
 *
 *  Created on: Dec 7, 2019
 *      Author: Tatiana_Potrebko
 */

#ifndef GYRO_RF_LOGIC_H_
#define GYRO_RF_LOGIC_H_



#include "main.h"

#define UART_PC_CNT			 huart2

#pragma pack(push,1)
typedef struct
{


	int16_t 		Accelerometer_X; /*!< Accelerometer value X axis */
	int16_t 		Accelerometer_Y; /*!< Accelerometer value Y axis */
	int16_t 		Accelerometer_Z; /*!< Accelerometer value Z axis */
	int16_t 		Gyroscope_X;     /*!< Gyroscope value X axis */
	int16_t 		Gyroscope_Y;     /*!< Gyroscope value Y axis */
	int16_t 		Gyroscope_Z;     /*!< Gyroscope value Z axis */
	int16_t   		Temperature;       /*!< Temperature in degrees */



	uint16_t _CRC;
}_MAIN_DATA_Def;

#pragma pack(pop)
#define PAYLOAD_SIZE	sizeof(_MAIN_DATA_Def)

typedef enum
{
	NORMAL=0x01,
	WUP=0x02,
	PSS=0x04,
	SLM=0x08

}status_rf;

typedef enum
{
	NORMAL_ON=0x01,
	MPU6050_NONE=0x00

}status_mpu6050;

typedef enum
{
	NONE_GPS=0x00,
	GET_GEOLOK_GPS=0x20

}status_GPS;

#pragma pack(push,1)
typedef struct
{
	uint8_t ID_RF_DEV;
	unsigned status_RF :4;
	unsigned status_MPU_6050 :1;
	unsigned status_GPS:2;
	_MAIN_DATA_Def _PAYLOAD_UART;

}CNT_SYS_WORK_DEF;
#pragma pack(pop)

/*********************Function Read Data Gyro,Accel,Transmit RF channel****************/
void Sync_RF_Update(void);
/*********************Function Read Data Gyro,Accel,Transmit RF channel****************/
void Async_MPU6050_Update(void);
/*********************CRC Transmit LoRa****************/
uint16_t Control_CRC16(uint8_t *dt,uint8_t length);
void Init_SysTem(uint8_t ID);

#endif /* GYRO_RF_LOGIC_H_ */
