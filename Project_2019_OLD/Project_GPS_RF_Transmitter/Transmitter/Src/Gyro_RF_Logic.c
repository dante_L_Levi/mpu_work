/*
 * Gyro_RF_Logic.c
 *
 *  Created on: Dec 7, 2019
 *      Author: Tatiana_Potrebko
 */

#include "Gyro_RF_Logic.h"


extern SD_MPU6050 mpu1;
extern GPS_t GPS;
extern CNT_SYS_WORK_DEF _mainData;
SD_MPU6050_Result result;

uint8_t dataConf[2]={0x00,0x00};

static void Update_Status_Work(uint8_t dt);
static void SET_STATUS_RF(uint8_t st);
static void SET_STATUS_MPU6050(uint8_t st);
/*************************Transmit UART to PC**************************/
static void Transmit_Data_PC(uint8_t *dt,uint8_t length);

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart==&GPS_UART)
	{
		HAL_UART_Receive_IT(&GPS_UART, &GPS.rxTmp, 1);
	}

	else if(huart==&UART_PC_CNT)
	{

		HAL_UART_Receive_IT(&UART_PC_CNT, (uint8_t*)dataConf, 2);
		if(dataConf[0]==_mainData.ID_RF_DEV)
		{
			Update_Status_Work(dataConf[1]);
		}

	}

}


/*************************Transmit UART to PC**************************/
static void Transmit_Data_PC(uint8_t *dt,uint8_t length)
{
	HAL_UART_Transmit(&UART_PC_CNT, (uint8_t*)dt, length, 0x100);
}

void Init_SysTem(uint8_t ID)
{
	 MX_GPIO_Init();
	 MX_I2C1_Init();
	 MX_TIM1_Init();
	 MX_TIM4_Init();
	 MX_USART1_UART_Init();
	 MX_USART2_UART_Init();
	 MX_USART3_UART_Init();
	 //----------------------------------------------
	 _mainData.ID_RF_DEV=ID;

	 Init_Config_HRW_868t20d();
	 LoRa_ConfigModeWork(MODE_WAKE_UP);
	 result = SD_MPU6050_Init(&mpu1,SD_MPU6050_Device_0,SD_MPU6050_Accelerometer_16G,SD_MPU6050_Gyroscope_2000s );
	 GPS_Init();

	 HAL_TIM_Base_Start_IT(&htim1);
	 HAL_TIM_Base_Start_IT(&htim4);

}

/**********************************function update Status*********************/
static void Update_Status_Work(uint8_t dt)
{
	_mainData.status_RF=dt;
	_mainData.status_MPU_6050=(dt>>4);
	_mainData.status_GPS=(dt>>5);
	//Update status HW
	SET_STATUS_RF(_mainData.status_RF);
	SET_STATUS_MPU6050(_mainData.status_MPU_6050);
	//SET_GPS_


}

/*********************Function Read Data Gyro,Accel,Transmit RF channel****************/
void Sync_RF_Update(void)
{
	if(_mainData.status_RF==(uint8_t)NORMAL && _mainData.status_MPU_6050==(uint8_t)NORMAL_ON)
	{

		SD_MPU6050_ReadAccelerometer(&mpu1);
		_mainData._PAYLOAD_UART.Accelerometer_X=mpu1.Accelerometer_X;
		_mainData._PAYLOAD_UART.Accelerometer_Y=mpu1.Accelerometer_Y;
		_mainData._PAYLOAD_UART.Accelerometer_Z=mpu1.Accelerometer_Z;

		SD_MPU6050_ReadGyroscope(&mpu1);
		_mainData._PAYLOAD_UART.Gyroscope_X=mpu1.Gyroscope_X;
		_mainData._PAYLOAD_UART.Gyroscope_Y=mpu1.Gyroscope_Y;
		_mainData._PAYLOAD_UART.Gyroscope_Z=mpu1.Gyroscope_Z;
		SD_MPU6050_ReadTemperature(&mpu1);
		_mainData._PAYLOAD_UART.Temperature=(int16_t)(mpu1.Temperature*1000);

		uint8_t datatransmit[PAYLOAD_SIZE]={0};
		memcpy((void*)datatransmit,&_mainData._PAYLOAD_UART,PAYLOAD_SIZE-2);
		//CRC
		Control_CRC16(datatransmit,sizeof(datatransmit));
		LoRa_TransmitData(datatransmit,PAYLOAD_SIZE);
	}


}

/*********************Function Read Data Gyro,Accel,Transmit RF channel****************/
void Async_MPU6050_Update(void)
{
	SD_MPU6050_ReadAccelerometer(&mpu1);
	_mainData._PAYLOAD_UART.Accelerometer_X=mpu1.Accelerometer_X;
	_mainData._PAYLOAD_UART.Accelerometer_Y=mpu1.Accelerometer_Y;
	_mainData._PAYLOAD_UART.Accelerometer_Z=mpu1.Accelerometer_Z;

	SD_MPU6050_ReadGyroscope(&mpu1);
	_mainData._PAYLOAD_UART.Gyroscope_X=mpu1.Gyroscope_X;
	_mainData._PAYLOAD_UART.Gyroscope_Y=mpu1.Gyroscope_Y;
	_mainData._PAYLOAD_UART.Gyroscope_Z=mpu1.Gyroscope_Z;
	SD_MPU6050_ReadTemperature(&mpu1);
	_mainData._PAYLOAD_UART.Temperature=(int16_t)(mpu1.Temperature*1000);
	char buffer_service[64]={0};
	sprintf(buffer_service,"%d|%d|%d|%d|%d|%d|%d|\r\n",_mainData._PAYLOAD_UART.Accelerometer_X,
			_mainData._PAYLOAD_UART.Accelerometer_Y,_mainData._PAYLOAD_UART.Accelerometer_Z,
			_mainData._PAYLOAD_UART.Gyroscope_X,
			_mainData._PAYLOAD_UART.Gyroscope_Y,_mainData._PAYLOAD_UART.Gyroscope_Z,_mainData._PAYLOAD_UART.Temperature);
	Transmit_Data_PC((uint8_t*)buffer_service,sizeof(buffer_service));

}

/*********************Control RF CHANNEl****************/
static void SET_STATUS_RF(uint8_t st)
{
	switch(st)
	{
		case (uint8_t)NORMAL:
		{
			LoRa_ConfigModeWork(MODE_NORMAL);
			break;
		}
		case (uint8_t)WUP:
		{
			LoRa_ConfigModeWork(MODE_WAKE_UP);
			break;
		}
		case (uint8_t)PSS:
		{
			LoRa_ConfigModeWork(MODE_POWER_SAVING);
			break;
		}
		case (uint8_t)SLM:
		{
			LoRa_ConfigModeWork(MODE_SLEEP);
			break;
		}

		default:
		{

			break;
		}

	}

	_mainData.status_RF=st;
}



/*********************Control MPU6050****************/
static void SET_STATUS_MPU6050(uint8_t st)
{
	_mainData.status_MPU_6050=st==0x01?(uint8_t)NORMAL_ON:(uint8_t)MPU6050_NONE;
}


/*********************CRC Transmit LoRa****************/
uint16_t Control_CRC16(uint8_t *dt,uint8_t length)
{
	uint8_t j;
		        uint16_t reg_crc = 0xFFFF;
		        while (length--)
		        {
		          reg_crc^= *dt++;
		          for(j=0;j<8;j++)
		           {
		             if(reg_crc & 0x01)
		              {
		                reg_crc = (reg_crc >> 1) ^ 0xA001;
		              }
		            else
		              {
		                reg_crc = reg_crc >> 1;
		              }
		           }
		        }
		        return reg_crc;
}




