/*
 * VSENSE.h
 *
 *  Created on: Feb 15, 2020
 *      Author: AlexPirs
 */

#ifndef VSENSE_H_
#define VSENSE_H_

#include "main.h"

#define Amp_K			1



/***********************Start Convection*****************/
void ADC_Start(void);
/***********************Stop Convection*****************/
void ADC_Stop(void);
/********************Interrupt ADC TIM READ****************/
void Tim_ADC_IRQ(void);

/***********************Read data Convection*****************/
void Get_ADC_values(void);


#endif /* VSENSE_H_ */
