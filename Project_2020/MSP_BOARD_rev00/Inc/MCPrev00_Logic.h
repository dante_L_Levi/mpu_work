/*
 * MCPrev00_Logic.h
 *
 *  Created on: Jan 26, 2020
 *      Author: AlexPirs
 */

#ifndef MCPREV00_LOGIC_H_
#define MCPREV00_LOGIC_H_



#include "main.h"
#include "RS485_Prot.h"



#define ID_DEV_MCP00		0x1F

#define LED_STATUS_PORT		GPIOB
#define LED_PIN				GPIO_PIN_5

#define OUT1_PORT			GPIOB
#define OUT1_PIN			GPIO_PIN_2


#define OUT2_PORT			GPIOB
#define OUT2_PIN			GPIO_PIN_10


#define OUT3_PORT			GPIOB
#define OUT3_PIN			GPIO_PIN_11



#define INx_Port			GPIOC
#define IN1_PC13			GPIO_PIN_13
#define IN2_PC14			GPIO_PIN_14
#define IN3_PC15			GPIO_PIN_15


#define CS_SPI_PORT			GPIOA
#define CS_SPI_PIN			GPIO_PIN_4

#define CS_SET()			HAL_GPIO_WritePin(CS_SPI_PORT,CS_SPI_PIN,GPIO_PIN_RESET)
#define CS_RESET()			HAL_GPIO_WritePin(CS_SPI_PORT,CS_SPI_PIN,GPIO_PIN_SET)

//Work TIM LED update MPU status
#define MAIN_TIM_INDICATE_OK			htim1
#define ADC_N_CH						3

typedef enum
{
	NORMAL_WORK,
	CONTROL_PC,
	WORK_NULL

}STATUS_WORK_PROG;

typedef struct
{
	unsigned gpio_out:3;
	unsigned gpio_input:3;

}GPIO_OUT_INPUT_STATUS;


typedef struct
{

	int16_t ADC_CH[ADC_N_CH];

}ADC_INPUT_Def;

typedef struct
{
	uint8_t statusHRTIM:3;//Enable Work
	uint8_t pwmDuty[3];//Duty in %		0-100%

}HRTIM_PWM_STATUS;

typedef struct
{
	uint8_t state;//1- high Level,0-Low Level
	uint8_t configPull;//1-pull up, 0-no pull
	uint8_t configIO;//set GPIO input 0, output 1

}MCP23017_PORTS;


typedef struct
{
	uint16_t					addr;
	MCP23017_PORTS				Ports[2];

}MCP23017_hw;


/********************************HUNDLER INTERRUPT************************************/
void MAIN_TIM1_ISQ(void);




/*************************MAIN INIT DATA******************************/
void Main_MCPrev00_Init(void);

/*******************Test Function Indicate***********************/
void Indicate_Led(uint8_t status);

/************************Function Set GPIO OUT********************************/
void OUT_set(uint16_t pinName,bool status);
/*******************Test Function OUTPUT***********************/
void Test_OUTPUT(uint8_t dt);
/*******************Function Read Input EXT***********************/
void Read_Input_EXT(void);

/************************Main Logic Update*********************************/
void Main_Logit_GoTo_MSPrev00_Sync(void);
/************************Struct Logic Update*********************************/
void MCPrev00_UpdateStateStruct(uint8_t *dt,uint8_t length);



#endif /* MCPREV00_LOGIC_H_ */
