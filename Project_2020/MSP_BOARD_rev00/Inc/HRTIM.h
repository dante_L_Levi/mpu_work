/*
 * HRTIM.h
 *
 *  Created on: Feb 15, 2020
 *      Author: AlexPirs
 */

#ifndef HRTIM_H_
#define HRTIM_H_

#include "main.h"
#include "stm32f334x8.h"

#define HRTIM_INPUT_CLOCK       ((uint64_t)144000000)   /* Value in Hz */
#define _33KHz_PERIOD ((uint16_t)((HRTIM_INPUT_CLOCK * 8) / 33333))


#define HRTIM_TIMERINDEX_TIMER_A			0
#define HRTIM_TIMERINDEX_TIMER_C			2
#define HRTIM_TIMERINDEX_TIMER_D			3

/*************************Init PWM HRTIM*********************/
void InitHRTIM_PWM(void);
/*************************SET Duty PWM HRTIM*********************/
//100%-_33KHz_PERIOD
//25%-X
void SetDutyHRPWM(uint16_t pwmA,uint16_t pwmC,uint16_t pwmD);


#endif /* HRTIM_H_ */
