/*
 * HRTIM.c
 *
 *  Created on: Feb 15, 2020
 *      Author: AlexPirs
 */

#include "HRTIM.h"


GPIO_InitTypeDef GPIO_InitStruct;

/*************************Init PWM HRTIM*********************/
void InitHRTIM_PWM(void)
{
	/* Use the PLLx2 clock for HRTIM */
		  __HAL_RCC_HRTIM1_CONFIG(RCC_HRTIM1CLK_PLLCLK);

		  /* Enable HRTIM clock*/
		  __HAL_RCC_HRTIM1_CLK_ENABLE();

		  /* DLL calibration: periodic calibration enabled, period set to 14µs */
		  HRTIM1->sCommonRegs.DLLCR |= HRTIM_DLLCR_CAL | HRTIM_DLLCR_CALEN;

		  /* Check DLL end of calibration flag */
		  while((HRTIM1->sCommonRegs.ISR & HRTIM_ISR_DLLRDY) == RESET);

		 /* --------------------- Timer A initialization --------------------------- */
		  /* TIMA counter operating in continuous mode with prescaler = 010b (div. by 4) */
		  /* Preload enabled on REP event*/
		  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].TIMxCR = HRTIM_TIMCR_CONT
		                                                       + HRTIM_TIMCR_PREEN
		                                                       + HRTIM_TIMCR_TREPU
		                                                       + HRTIM_TIMCR_CK_PSC_1;

		  /* Set period to 33kHz and duty cycles to 25% */

		  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].PERxR = _33KHz_PERIOD;
		  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].CMP1xR = _33KHz_PERIOD/4;//25%
		  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].CMP2xR = _33KHz_PERIOD/2;
		  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].CMP3xR = (3*_33KHz_PERIOD)/4;

		  	    /* TA1 output set on TIMA period and reset on TIMA CMP1 event*/
		  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].SETx1R = HRTIM_SET1R_PER;
		  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].RSTx1R = HRTIM_RST1R_CMP1;

		  	    /* TA2 output set on TIMA CMP2 and reset on TIMA CMP3 event*/
		  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].SETx2R = HRTIM_SET2R_CMP2;
		  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].RSTx2R = HRTIM_RST2R_CMP3;
		  	  /* Enable TA1, TA2, TD1 and TD2 outputs */
		  	  HRTIM1->sCommonRegs.OENR = HRTIM_OENR_TA1OEN + HRTIM_OENR_TA2OEN;

		  	  HRTIM1->sMasterRegs.MCR = HRTIM_MCR_TACEN;



		  	/* Enable GPIOA clock for timer A outputs */
		  	  __HAL_RCC_GPIOA_CLK_ENABLE();

		  	  /* Configure HRTIM output: TA1 (PA8) and TA2 (PA9)*/
		  	  GPIO_InitStruct.Pin = GPIO_PIN_8 | GPIO_PIN_9;
		  	  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		  	  GPIO_InitStruct.Pull = GPIO_NOPULL;;
		  	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;;
		  	  GPIO_InitStruct.Alternate = GPIO_AF13_HRTIM1;

		  	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		  //------------------------------------------------------------------------------------------
		  	/* --------------------- Timer C initialization --------------------------- */
		  			  /* TIMC counter operating in continuous mode with prescaler = 010b (div. by 4) */
		  			  /* Preload enabled on REP event*/
		  			  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].TIMxCR = HRTIM_TIMCR_CONT
		  			                                                       + HRTIM_TIMCR_PREEN
		  			                                                       + HRTIM_TIMCR_TREPU
		  			                                                       + HRTIM_TIMCR_CK_PSC_1;

		  			  /* Set period to 33kHz and duty cycles to 25% */

		  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].PERxR = _33KHz_PERIOD;
		  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].CMP1xR = _33KHz_PERIOD/4;//25%
		  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].CMP2xR = _33KHz_PERIOD/2;
		  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].CMP3xR = (3*_33KHz_PERIOD)/4;

		  			  	    /* TA1 output set on TIMC period and reset on TIMC CMP1 event*/
		  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].SETx1R = HRTIM_SET1R_PER;
		  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].RSTx1R = HRTIM_RST1R_CMP1;

		  			  	    /* TA2 output set on TIMC CMP2 and reset on TIMC CMP3 event*/
		  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].SETx2R = HRTIM_SET2R_CMP2;
		  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].RSTx2R = HRTIM_RST2R_CMP3;
		  			  	  /* Enable TC1, TC2,  outputs */
		  			  	  HRTIM1->sCommonRegs.OENR = HRTIM_OENR_TC1OEN + HRTIM_OENR_TC2OEN;

		  			  	  HRTIM1->sMasterRegs.MCR = HRTIM_MCR_TCCEN;


		  			  	/* Enable GPIOB clock for timer C outputs */
		  			  	  __HAL_RCC_GPIOB_CLK_ENABLE();

		  			  	/* Configure HRTIM output: TC1 (PB12) and TC2 (PB13)*/
		  			  	 GPIO_InitStruct.Pin = GPIO_PIN_12 | GPIO_PIN_13;
		  			  	 GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		  			  	 GPIO_InitStruct.Pull = GPIO_NOPULL;
		  			  	 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;;
		  			  	 GPIO_InitStruct.Alternate = GPIO_AF13_HRTIM1;
		  			  	 HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		//------------------------------------------------------------------------------------------------------------------------------
		  			  	/* --------------------- Timer D initialization --------------------------- */
		  			  	/* TIMD counter operating in continuous mode with prescaler = 010b (div. by 4) */
		  			  			  			  /* Preload enabled on REP event*/
		  			  			  			  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].TIMxCR = HRTIM_TIMCR_CONT
		  			  			  			                                                       + HRTIM_TIMCR_PREEN
		  			  			  			                                                       + HRTIM_TIMCR_TREPU
		  			  			  			                                                       + HRTIM_TIMCR_CK_PSC_1;

		  			  			  			  /* Set period to 33kHz and duty cycles to 25% */

		  			  			  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].PERxR = _33KHz_PERIOD;
		  			  			  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].CMP1xR = _33KHz_PERIOD/4;//25%
		  			  			  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].CMP2xR = _33KHz_PERIOD/2;
		  			  			  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].CMP3xR = (3*_33KHz_PERIOD)/4;

		  			  			  			  	    /* TA1 output set on TIMC period and reset on TIMC CMP1 event*/
		  			  			  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].SETx1R = HRTIM_SET1R_PER;
		  			  			  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].RSTx1R = HRTIM_RST1R_CMP1;

		  			  			  			  	    /* TA2 output set on TIMC CMP2 and reset on TIMC CMP3 event*/
		  			  			  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].SETx2R = HRTIM_SET2R_CMP2;
		  			  			  			  	  HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].RSTx2R = HRTIM_RST2R_CMP3;
		  			  			  			  	  /* Enable TC1, TC2,  outputs */
		  			  			  			  	  HRTIM1->sCommonRegs.OENR = HRTIM_OENR_TD1OEN + HRTIM_OENR_TD2OEN;

		  			  			  			  	  HRTIM1->sMasterRegs.MCR = HRTIM_MCR_TCCEN;


		  			  			  			  	/* Enable GPIOB clock for timer D outputs */
		  			  			  			  	  __HAL_RCC_GPIOB_CLK_ENABLE();

		  			  			  			  	/* Configure HRTIM output: TD1 (PB14) and TD2 (PB15)*/
		  			  			  			  	 GPIO_InitStruct.Pin = GPIO_PIN_14 | GPIO_PIN_15;
		  			  			  			  	 GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		  			  			  			  	 GPIO_InitStruct.Pull = GPIO_NOPULL;
		  			  			  			  	 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;;
		  			  			  			  	 GPIO_InitStruct.Alternate = GPIO_AF13_HRTIM1;
		  			  			  			  	 HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}


/*************************SET Duty PWM HRTIM*********************/
//100%-_33KHz_PERIOD
//25%-X
void SetDutyHRPWM(uint16_t pwmA,uint16_t pwmC,uint16_t pwmD)
{
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].CMP1xR = pwmA;
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].CMP2xR = pwmA*2;
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_A].CMP3xR = pwmA*3;

	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].CMP1xR = pwmC;
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].CMP2xR = pwmC*2;
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_C].CMP3xR = pwmC*3;

	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].CMP1xR = pwmD;
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].CMP2xR = pwmD*2;
	HRTIM1->sTimerxRegs[HRTIM_TIMERINDEX_TIMER_D].CMP3xR = pwmD*3;
}


