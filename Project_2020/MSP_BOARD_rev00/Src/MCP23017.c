/*
 * MCP23017.c
 *
 *  Created on: 19 окт. 2019 г.
 *      Author: Tatiana_Potrebko
 */

#include "MCP23017.h"


extern MCP23017_hw	MCP23017_GPIO;
extern I2C_HandleTypeDef hi2c1;
#define MCP23017_I2C				hi2c1


/********************Error MCP23017*************************/
void MCP23017_Handler_Error(void)
{

}


/*****************************Write Byte I2C*************************************/
static HAL_StatusTypeDef MCP23017_writeByte_i2c(uint16_t Addr,uint8_t Reg,uint8_t value)
{

	 HAL_StatusTypeDef status = HAL_OK;
     status = HAL_I2C_Mem_Write(&MCP23017_I2C, Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, &value, 1, 0x1000);
     if(status != HAL_OK)
    	 {
    	 MCP23017_Handler_Error();
    	 }
     return status;

}


/*****************************Read Byte I2C*************************************/
static HAL_StatusTypeDef MCP23017_ReadByte_i2c(uint16_t Addr,uint8_t Reg,uint8_t *data)
{
	HAL_StatusTypeDef status = HAL_OK;
    status = HAL_I2C_Mem_Read(&MCP23017_I2C, Addr, Reg, I2C_MEMADD_SIZE_8BIT, data, 1, 0x1000);
    if(status != HAL_OK)
    	{
    	MCP23017_Handler_Error();
    	}
    return status;
}


/***********************************Init & Set Addres Device******************************/
void mcp23017_init(uint16_t addr)
{
	MX_I2C1_Init();
	MCP23017_GPIO.addr=addr << 1;
}

/********************************Function SET Direction PORT************************/
HAL_StatusTypeDef  mcp23017_iodir(uint8_t port, uint8_t iodir)
{
	MCP23017_GPIO.Ports[port].configIO=iodir;
	return MCP23017_writeByte_i2c(MCP23017_GPIO.addr, REGISTER_IODIRA|port, MCP23017_GPIO.Ports[port].configIO);
}

/********************************Function  INPUT POLARITY************************/
HAL_StatusTypeDef mcp23017_ipol(uint8_t port, uint8_t ipol)
{

	uint8_t data[1] = {ipol};
	return MCP23017_writeByte_i2c(MCP23017_GPIO.addr, REGISTER_IPOLA|port, data[0]);
}

/********************************Function  SET Pull-up Resistors************************/
HAL_StatusTypeDef mcp23017_ggpu(uint8_t port, uint8_t pull)
{

	MCP23017_GPIO.Ports[port].configPull=pull;
	return MCP23017_writeByte_i2c(MCP23017_GPIO.addr, REGISTER_GPPUA|port, MCP23017_GPIO.Ports[port].configPull);

}

/*********************************READ SET PORT********************************************/
uint8_t mcp23017_read_gpio(uint8_t port)
{
	uint8_t data[1];
	HAL_StatusTypeDef status;
	status = MCP23017_ReadByte_i2c(MCP23017_GPIO.addr, REGISTER_GPIOA|port, data);

	if (status == HAL_OK)
		MCP23017_GPIO.Ports[port].state=data[0];

	return status;
}

/*********************************Write SET PORT********************************************/
HAL_StatusTypeDef mcp23017_write_gpio(uint8_t port,uint8_t statePin)
{

	uint8_t data[1] = {statePin};
	return MCP23017_writeByte_i2c(MCP23017_GPIO.addr, REGISTER_GPIOA|port,data[0]);
}



