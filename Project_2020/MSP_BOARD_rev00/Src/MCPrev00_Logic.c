/*
 * MCPrev00_Logic.c
 *
 *  Created on: Jan 26, 2020
 *      Author: AlexPirs
 */


#include "MCPrev00_Logic.h"

GPIO_OUT_INPUT_STATUS 					GPIO_main;
MCP23017_hw								MCP23017_GPIO;
ADC_INPUT_Def							_adc_chs;
HRTIM_PWM_STATUS						_hrtim;
uint8_t count_Led=0;
/**********************Test function for Test********************/
void Test_transmit_RS485(void);



/********************************HUNDLER INTERRUPT************************************/
void MAIN_TIM1_ISQ(void)
{
	/*******************Indicationn Work******************************/

	Indicate_Led(count_Led);
	count_Led++;
	if(count_Led>5)
	{
		count_Led=0;
	}
	/************************************************************/
	Read_Input_EXT();
	//Test_transmit_RS485();
	//ADC_Read_Values(&(_adc_chs.ADC_CH[0]),&(_adc_chs.ADC_CH[1]),&(_adc_chs.ADC_CH[2]));
}




/************************************************************************************/


/*************************MAIN INIT DATA******************************/
void Main_MCPrev00_Init(void)
{
	MX_GPIO_Init();
	GPIO_main.gpio_out=0x00;
	GPIO_main.gpio_input=0x00;
	  MX_GPIO_Init();

	  MX_TIM6_Init();
	  MX_ADC1_Init();
	  MX_CAN_Init();
	  MX_I2C1_Init();
	  MX_SPI1_Init();
	  MX_TIM7_Init();
	  MX_USART1_UART_Init();
	  //MX_USART2_UART_Init();
	  MX_TIM1_Init();


	RS485_Init(115200);
	RS485_setId(ID_DEV_MCP00);
	ADC_Start();
	mcp23017_init(MCP23017_ADDRESS_20);
	mcp23017_iodir(MCP23017_PORTA,MCP23017_IODIR_ALL_OUTPUT);
	mcp23017_iodir(MCP23017_PORTB,MCP23017_IODIR_ALL_OUTPUT);
	InitHRTIM_PWM();

}

/************************Function Set GPIO OUT********************************/
void OUT_set(uint16_t pinName,bool status)
{
	GPIO_TypeDef *PORT;
		switch(pinName)
		{
			case LED_PIN:
			{
				PORT=LED_STATUS_PORT;

				break;
			}
			case OUT1_PIN:
			{
				PORT=OUT1_PORT;
				GPIO_main.gpio_out=status!=false?(GPIO_main.gpio_out|0x01):
																	(GPIO_main.gpio_out&0x06);
				break;
			}
			case OUT2_PIN:
			{
				PORT=OUT2_PORT;
				GPIO_main.gpio_out=status!=false?(GPIO_main.gpio_out|0x02):
																(GPIO_main.gpio_out&0x05);
				break;
			}
			case OUT3_PIN:
			{
				PORT=OUT3_PORT;
				GPIO_main.gpio_out=status!=false?(GPIO_main.gpio_out|0x04):
																(GPIO_main.gpio_out&0x03);
				break;
			}
			default:
			{
				return;
			}

		}
		status!=false?(HAL_GPIO_WritePin(PORT, pinName, GPIO_PIN_SET)):(HAL_GPIO_WritePin(PORT, pinName, GPIO_PIN_RESET));

}



/*******************Test Function Indicate***********************/
void Indicate_Led(uint8_t status)
{
	status%2==0?(OUT_set(LED_PIN,false)):(OUT_set(LED_PIN,true));

}

/*******************Test Function OUTPUT***********************/
void Test_OUTPUT(uint8_t dt)
{
	switch(dt)
	{
	case 0:
	{
		OUT_set(OUT1_PIN,true);
		OUT_set(OUT2_PIN,false);
		OUT_set(OUT3_PIN,false);
		break;
	}
	case 1:
	{
		OUT_set(OUT1_PIN,false);
		OUT_set(OUT2_PIN,true);
		OUT_set(OUT3_PIN,false);
		break;
	}




	case 2:
	{
		OUT_set(OUT3_PIN,true);
		OUT_set(OUT2_PIN,false);
		OUT_set(OUT1_PIN,false);

		break;
	}
	default:
	{
		OUT_set(OUT1_PIN,true);
		OUT_set(OUT2_PIN,true);
		OUT_set(OUT3_PIN,true);
		break;
	}
	}
}


/*******************Function Read Input EXT***********************/
void Read_Input_EXT(void)
{
	uint8_t status_in[3]={0};
	status_in[0]=HAL_GPIO_ReadPin(INx_Port, IN1_PC13);
	status_in[1]=HAL_GPIO_ReadPin(INx_Port, IN2_PC14);
	status_in[2]=HAL_GPIO_ReadPin(INx_Port, IN3_PC15);
	GPIO_main.gpio_input=status_in[0]==0x01?(GPIO_main.gpio_input|0x01):
																		(GPIO_main.gpio_input&0x06);
	GPIO_main.gpio_input=status_in[1]==0x01?(GPIO_main.gpio_input|0x02):
																			(GPIO_main.gpio_input&0x05);
	GPIO_main.gpio_input=status_in[2]==0x01?(GPIO_main.gpio_input|0x04):
																				(GPIO_main.gpio_input&0x03);

}

/************************Struct Logic Update*********************************/
void MCPrev00_UpdateStateStruct(uint8_t *dt,uint8_t length)
{
	MCP23017_GPIO.Ports[0].state=dt[1];
	GPIO_main.gpio_out=(dt[2]&0x38);
	_hrtim.statusHRTIM=dt[9];
	uint16_t pwm_duty[3];
	pwm_duty[0]=(dt[10]<<8)|dt[11];
	pwm_duty[1]=(dt[12]<<8)|dt[13];
	pwm_duty[2]=(dt[14]<<8)|dt[15];
	_hrtim.pwmDuty[0]=pwm_duty[0];
	_hrtim.pwmDuty[1]=pwm_duty[1];
	_hrtim.pwmDuty[2]=pwm_duty[2];



}

/************************Main Logic Update*********************************/
void Main_Logit_GoTo_MSPrev00_Sync(void)
{
	//------------------GPIO OUT----------------------
	uint8_t gpio_st=GPIO_main.gpio_out;
	(gpio_st&0x01)==1?(OUT_set(OUT1_PIN,true)):
						(OUT_set(OUT1_PIN,false));

	(gpio_st&0x02)==1?(OUT_set(OUT2_PIN,true)):
							(OUT_set(OUT2_PIN,false));

	(gpio_st&0x04)==1?(OUT_set(OUT3_PIN,true)):
							(OUT_set(OUT3_PIN,false));

	//------------------------------------------------


	//------------------MCP23017 GPIO OUT----------------------
	mcp23017_write_gpio(MCP23017_PORTA,MCP23017_GPIO.Ports[0].state);
	mcp23017_write_gpio(MCP23017_PORTB,MCP23017_GPIO.Ports[1].state);
	//---------------------------------------------------------

	//------------------HRTIM GPIO OUT----------------------
	if((_hrtim.statusHRTIM&0x01)==0x01 && (_hrtim.statusHRTIM&0x02)==0x02 && (_hrtim.statusHRTIM&0x04)==0x04)
	{
		uint16_t pwm[3];
		pwm[0]=(_hrtim.pwmDuty[0]*_33KHz_PERIOD)/100;
		pwm[1]=(_hrtim.pwmDuty[1]*_33KHz_PERIOD)/100;
		pwm[2]=(_hrtim.pwmDuty[2]*_33KHz_PERIOD)/100;
		SetDutyHRPWM(pwm[0],pwm[1],pwm[2]);
	}
	//--------------------------------------------------------

}

/**********************Test function for Test********************/
 void Test_transmit_RS485(void)
{
	uint8_t data[17]={0x01,0x05,0x00,0xFF,0xFE,0xFF,
						0x01,0x05,0x00,0xFF,0xFE,0xFF,
						0x01,0x05,0x00,0xFF,0xFE};
	RS485_Transmit(ID_DEV_MCP00,RESPONSE_MPU_Data,data,17);
	//RS485_Transmit_PAYLOAD();

}
