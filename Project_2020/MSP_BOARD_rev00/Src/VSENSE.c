/*
 * VSENSE.c
 *
 *  Created on: Feb 15, 2020
 *      Author: AlexPirs
 */


#include "VSENSE.h"

#define ADC_Obj									hadc1
#define TIM_ADC									htim7
#define VREF_SENSE								(float)(3.3/4096)
extern ADC_INPUT_Def							_adc_chs;

uint32_t ADC_value[ADC_N_CH]={0};


/********************Interrupt ADC TIM READ****************/
void Tim_ADC_IRQ(void)
{

	Get_ADC_values();

}


/***********************Start Convection*****************/
void ADC_Start(void)
{
	HAL_ADC_Start(&ADC_Obj);
	HAL_TIM_Base_Start_IT(&TIM_ADC);

}


/***********************Stop Convection*****************/
void ADC_Stop(void)
{
	HAL_ADC_Stop(&ADC_Obj);
	HAL_TIM_Base_Stop_IT(&TIM_ADC);
}

/***********************Read data Convection*****************/
void Get_ADC_values(void)
{
	float adc[3];
	HAL_ADC_Start(&ADC_Obj);
	HAL_ADC_PollForConversion(&ADC_Obj, 100);
	ADC_value[0] = HAL_ADC_GetValue(&ADC_Obj);


	HAL_ADC_Start(&ADC_Obj);
	HAL_ADC_PollForConversion(&ADC_Obj, 100);
	ADC_value[1] = HAL_ADC_GetValue(&ADC_Obj);


	HAL_ADC_Start(&ADC_Obj);
	HAL_ADC_PollForConversion(&ADC_Obj, 100);
	ADC_value[2] = HAL_ADC_GetValue(&ADC_Obj);

	adc[0]=(float)ADC_value[0]*VREF_SENSE;
	adc[1]=(float)ADC_value[1]*VREF_SENSE;
	adc[2]=(float)ADC_value[2]*VREF_SENSE;

	_adc_chs.ADC_CH[0]=(int16_t)(adc[0]*1000);
	_adc_chs.ADC_CH[1]=(int16_t)(adc[1]*1000);
	_adc_chs.ADC_CH[2]=(int16_t)(adc[2]*1000);



	HAL_ADC_Stop(&ADC_Obj);

}
