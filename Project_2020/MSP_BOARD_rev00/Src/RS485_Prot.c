/*
 * RS485_Prot.c
 *
 *  Created on: Sep 28, 2019
 *      Author: Tatiana_Potrebko
 */

#include "RS485_Prot.h"

#define	RS485_Obj		huart2
extern RS485Prot RS485Data;
extern STATUS_WORK_PROG setting_Work;
//extern MCPrev00_Main _mcprev00_dat;

static uint8_t RS485_DEV_ID=0x0B;
static uint8_t isMuted = 0;
static volatile uint8_t RX_BUFF[BUFFER_SIZE]={0};
static volatile uint8_t TX_BUFF[BUFFER_SIZE]={0};

/***************************Control CRC ****************************************/
static uint16_t TxCalculateCRC16(uint8_t *Data,uint8_t Length);
/********************************Function Check Summ Recieve Buffer***************/
static uint8_t CheckCRC16_RECIEVE(uint8_t *Data,uint8_t Length);
static void RS485_ClearBuffer(void);





void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart==&RS485_Obj)
		{
		RS485_RessiveData_helper();
		}
}

/*******************helper Start Recieve Bytes***********************/
void RS485_RessiveData_Starthelper(void)
{
	HAL_UART_Receive_IT(&RS485_Obj, (uint8_t*)RX_BUFF, BUFFER_SIZE);
}

/*******************helper Save bytes in Buffer***********************/
void RS485_RessiveData_helper(void)
{
	HAL_UART_Receive_IT(&RS485_Obj, (uint8_t*)RX_BUFF, BUFFER_SIZE);
	if(RS485_checkId()==1)
	{
		if(CheckCRC16_RECIEVE((uint8_t *)RX_BUFF,(uint8_t)BUFFER_SIZE)==1)

		{
			if(RX_BUFF[0]==RS485Data.Id)
			{
				ConvertToStruct(&RS485Data);
				if(RS485Data.command==REQUEST_PC)
				{

					MCPrev00_UpdateStateStruct(RS485Data.PayLoad,sizeof(RS485Data.PayLoad));
				}

			}


		}
	}

}

/********************Helper for converted in struct*************************/
void ConvertToStruct(RS485Prot *p)
{
	memcpy(p,(void*)RX_BUFF,BUFFER_SIZE);
}

/************Init Hardware Uart*************************/
void RS485_Init(uint32_t BaudRate)
{

	RS485_Obj.Instance = USART2;
	RS485_Obj.Init.BaudRate = BaudRate;
	RS485_Obj.Init.WordLength = UART_WORDLENGTH_8B;
	RS485_Obj.Init.StopBits = UART_STOPBITS_1;
	RS485_Obj.Init.Parity = UART_PARITY_NONE;
	RS485_Obj.Init.Mode = UART_MODE_TX_RX;
	RS485_Obj.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	RS485_Obj.Init.OverSampling = UART_OVERSAMPLING_16;
	RS485_Obj.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	RS485_Obj.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	  if (HAL_UART_Init(&RS485_Obj) != HAL_OK)
	  {
		Error_Handler();
	  }

  __HAL_UART_ENABLE_IT(&RS485_Obj, UART_IT_RXNE);

}

/***************Check Id RS485 Device************************/
uint8_t RS485_checkId(void)
{
	if(RX_BUFF[0]==RS485_DEV_ID)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


/*****************Function Set Id*********************/
void RS485_setId(uint8_t id)
{
    RS485_DEV_ID = id;
    RS485Data.Id=id;

}


/*********************Function Get Id Rs-485***************************/
uint8_t RS485_getId(void)
{
    return RS485_DEV_ID;
}

/**********************Calculate CRC16*****************************/
static uint16_t TxCalculateCRC16(uint8_t *Data,uint8_t Length)
{
	uint8_t j;
	        uint16_t reg_crc = 0xFFFF;
	        while (Length--)
	        {
	          reg_crc^= *Data++;
	          for(j=0;j<8;j++)
	           {
	             if(reg_crc & 0x01)
	              {
	                reg_crc = (reg_crc >> 1) ^ 0xA001;
	              }
	            else
	              {
	                reg_crc = reg_crc >> 1;
	              }
	           }
	        }
	        return reg_crc;
}


/********************************Function Check Summ Recieve Buffer***************/
static uint8_t CheckCRC16_RECIEVE(uint8_t *Data,uint8_t Length)
{
	uint16_t crc_buf=0;
	    crc_buf=(Data[Length-1]<<8)|Data[Length-2];

	    uint16_t crc = 0xFFFF;
	    uint8_t pos;
	    for(pos=0;pos<Length-2;pos++)
	    {
	        crc^=(uint16_t)Data[pos];
	    }
	    uint8_t i;
	    for (i = 8; i != 0; i--)
	    {
	        if ((crc & 0x0001) != 0)
	                        {
	                            crc >>= 1;
	                            crc ^= 0xA001;
	                        }
	                        else
	                        {
	                            crc >>= 1;
	                        }
	    }
	    if(crc==crc_buf)
	    {
	        return 1;
	    }
	    else
	    {
	        return 0;
	    }
}


/**********************Function Transmit Data ******************/
void RS485_setTxMessage(RS485Prot *p)
{
	if(isMuted)
	{
			return;
	}
	else
	{
		RS485_TRANSMIT;
		memcpy((void *)TX_BUFF, p, BUFFER_SIZE);
		uint16_t tempCRC=TxCalculateCRC16((uint8_t *)TX_BUFF,(uint8_t)BUFFER_SIZE-2);
		p->CRC16=tempCRC;
		TX_BUFF[BUFFER_SIZE-2]=(uint8_t)tempCRC;
		TX_BUFF[BUFFER_SIZE-1]=(uint8_t)(tempCRC>>8);
		HAL_UART_Transmit(&RS485_Obj, (uint8_t*)TX_BUFF, BUFFER_SIZE, 0x1000);
		RS485_ClearBuffer();
		RS485_RECEIVE;
	}

}





/**********************Function Transmit Data ******************/
void RS485_Transmit(uint8_t id,CommandDef dataCmd,uint8_t *dataPay,uint8_t length)
{
	if(length>SIZEBUFF_PAYLOAD)
			return;

	RS485Data.Id=id;
	RS485Data.command=(uint8_t)dataCmd;
	memcpy((void *)(RS485Data.PayLoad),dataPay,SIZEBUFF_PAYLOAD);
	RS485_setTxMessage(&RS485Data);


}

/**********************Function Transmit Data ******************/
void RS485_Transmit_PAYLOAD(CommandDef dataCmd,uint8_t *dataPay,uint8_t length)
{
	if(length>SIZEBUFF_PAYLOAD)
				return;
	RS485Data.command=(uint8_t)dataCmd;
	memcpy((void *)(RS485Data.PayLoad),dataPay,SIZEBUFF_PAYLOAD);
	RS485_setTxMessage(&RS485Data);

}

/**************Test Function for Transmit*****************/
void Test_Transmit__RS485Prot(void)
{
	RS485Data.command=(uint8_t)OK_WORK;
	char data[]="test RS485\r\n";
	memcpy((void *)(RS485Data.PayLoad),data,strlen(data));
	RS485_setTxMessage(&RS485Data);

}


static void RS485_ClearBuffer(void)
{
	uint8_t i;
	for(i=0;i<BUFFER_SIZE;i++)
	{
		TX_BUFF[i]=0;

	}

	uint8_t j;
	for(j=0;j<32;j++)
	{
		RS485Data.PayLoad[j]=0;
	}

}
