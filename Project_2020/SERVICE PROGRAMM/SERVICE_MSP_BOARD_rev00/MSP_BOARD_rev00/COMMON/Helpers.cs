﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSP_BOARD_rev00.COMMON
{

    public enum Order_Bits
    {
        Bit0= (byte)0x01,
        Bit1= (byte)0x02,
        Bit2= (byte)0x04,
        Bit3= (byte)0x08
    }

    public static  class Helpers
    {


        private static byte outval;
        /// <summary>
        /// helpers for converted data Port
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int ConvertToIntPort(string data)
        {
            bool result;
            result = int.TryParse(data, out int resultData);
            if (result)
            {
                return resultData;
            }
            else
            {
                return 0;
            }
           
        }

        /// <summary>
        /// Helper Converted Command Set/Reset Bits
        /// </summary>
        /// <param name="btt"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static byte[] ConvertCommandTransmit(Order_Bits btt, bool state)
        {
            byte[] dt = new byte[1] { 0x00 };
            if(!state)//reset bit
            {
                byte val = 0x00;
                val = (byte)(val | (byte)btt);
                outval = (byte)(outval ^ val);
            }
            else
            {
                outval = (byte)(outval | (byte)btt);
            }
            dt[0] = outval;
            return dt;
        }

       
    }
}
