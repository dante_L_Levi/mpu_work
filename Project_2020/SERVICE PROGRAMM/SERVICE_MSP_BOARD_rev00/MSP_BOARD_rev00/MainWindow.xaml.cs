﻿using MSP_BOARD_rev00.Model;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MSP_BOARD_rev00
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private byte[] countToogleSwitch = new byte[3] { 1, 1, 1 };
        private SerialPort _port;
        private RS485_Prot dataTransfer;
        private readonly byte ID_DEVICE = 0x1F;
        private MSP_rev00_Model Main_data;
        Data_MSP_rev00 Main_dataUI;
        private DispatcherTimer Timer_UI = new DispatcherTimer();
        #region Helpers For Load Data
        private void Load_Data()
        {
            Port_data.ItemsSource = dataTransfer.GetPortNames();
            Port_Baud.ItemsSource = dataTransfer.GetBaudRateAll();

        }


        #endregion


        public MainWindow()
        {
            InitializeComponent();
            _port = new SerialPort();
            dataTransfer = new RS485_Prot(ID_DEVICE, _port);
            Main_data = new MSP_rev00_Model(ID_DEVICE, dataTransfer);
            Load_Data();
            Main_dataUI=Main_data.dataState;

        }

        private void Btn_Connection_Click(object sender, RoutedEventArgs e)
        {
            if(Port_data.SelectedIndex!=-1 && Port_Baud.SelectedIndex!=-1)
            {
                Main_data.Data_trasfer.OpenPort(Port_data.SelectedItem.ToString(), (int)Port_Baud.SelectedItem);
                MainFill.IsEnabled = true;
                MainContainer.SelectedItem = MainContainer.Items[1];
                Timer_UI.Tick += UPDATE_UI;
            }
            else
            {
                MainContainer.SelectedItem = MainContainer.Items[0];
                MainFill.IsEnabled = false;
            }
        }

        private void UPDATE_UI(object sender, EventArgs e)
        {
            //вычитываем поля свойств и обновляем UI

        }

        private void SwOUT1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            countToogleSwitch[0]++;
            if (countToogleSwitch[0] % 2 == 0)
            {
                Main_dataUI.STATE_EXT_GPIO = (byte)(Main_dataUI.STATE_EXT_GPIO | (byte)MASK_EXT_GPIO.SET_GPIO1);
                Main_data.Send_Command(Main_dataUI);
            }
            else
            {
                Main_dataUI.STATE_EXT_GPIO = (byte)(Main_dataUI.STATE_EXT_GPIO & (byte)MASK_EXT_GPIO.RESET_GPIO1);
                Main_data.Send_Command(Main_dataUI);
            }
        }

        private void SwOUT2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            countToogleSwitch[1]++;
            if (countToogleSwitch[1] % 2 == 0)
            {
                Main_dataUI.STATE_EXT_GPIO = (byte)(Main_dataUI.STATE_EXT_GPIO | (byte)MASK_EXT_GPIO.SET_GPIO2);
                Main_data.Send_Command(Main_dataUI);
            }
            else
            {
                Main_dataUI.STATE_EXT_GPIO = (byte)(Main_dataUI.STATE_EXT_GPIO & (byte)MASK_EXT_GPIO.RESET_GPIO2);
                Main_data.Send_Command(Main_dataUI);
            }
        }

        private void SwOUT3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            countToogleSwitch[2]++;
            if (countToogleSwitch[2] % 2 == 0)
            {
                Main_dataUI.STATE_EXT_GPIO = (byte)(Main_dataUI.STATE_EXT_GPIO | (byte)MASK_EXT_GPIO.SET_GPIO3);
                Main_data.Send_Command(Main_dataUI);
            }
            else
            {
                Main_dataUI.STATE_EXT_GPIO = (byte)(Main_dataUI.STATE_EXT_GPIO & (byte)MASK_EXT_GPIO.RESET_GPIO3);
                Main_data.Send_Command(Main_dataUI);
            }
        }

        private void MCP23017_Check(object sender, RoutedEventArgs e)
        {
            byte outData23017 = 0x00;

            outData23017 = MCP_O1.IsChecked == true ? ((byte)(outData23017 | 0x01)) : ((byte)(outData23017 & 0xFE));
            outData23017 = MCP_O2.IsChecked == true ? ((byte)(outData23017 | 0x02)) : ((byte)(outData23017 & 0xFD));
            outData23017 = MCP_O3.IsChecked == true ? ((byte)(outData23017 | 0x04)) : ((byte)(outData23017 & 0xFB));
            outData23017 = MCP_O4.IsChecked == true ? ((byte)(outData23017 | 0x08)) : ((byte)(outData23017 & 0xF7));
            outData23017 = MCP_O5.IsChecked == true ? ((byte)(outData23017 | 0x10)) : ((byte)(outData23017 & 0xEF));
            outData23017 = MCP_O6.IsChecked == true ? ((byte)(outData23017 | 0x20)) : ((byte)(outData23017 & 0xDF));
            outData23017 = MCP_O7.IsChecked == true ? ((byte)(outData23017 | 0x40)) : ((byte)(outData23017 & 0xBF));
            outData23017 = MCP_O8.IsChecked == true ? ((byte)(outData23017 | 0x80)) : ((byte)(outData23017 & 0x7F));
            Main_dataUI.STATE_GPIO_MCP23017 = outData23017;
            Main_data.Send_Command(Main_dataUI);
            
        }

        private void HRTIM_CHANGE_Value(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            UInt16[] HRTIM_val = new UInt16[3];
            
            HRTIM_val[0] = (UInt16)HRTIM_CH1.Value;
            HRTIM_val[1] = (UInt16)HRTIM_CH2.Value; ;
            HRTIM_val[2] = (UInt16)HRTIM_CH3.Value; ;


            Value_pwm1.Text = ((HRTIM_val[0] / 1000.0)*100).ToString();
            Value_pwm2.Text = ((HRTIM_val[1] / 1000.0) * 100).ToString();
            Value_pwm3.Text = ((HRTIM_val[2] / 1000.0) * 100).ToString();

            Main_dataUI.PWM_DUTY = HRTIM_val;
            Main_data.Send_Command(Main_dataUI);



        }

        private void Window_Closed(object sender, EventArgs e)
        {

        }
    }
}
