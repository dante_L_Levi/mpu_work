﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows;
using System.ComponentModel;
using System.Threading;

namespace MSP_BOARD_rev00.Model
{
  public  class SerialDataTransfer
    {
        private const int sizeRecieveBuffer = 21;
        public SerialPort dataPort;
        Parity parity = Parity.None;
        int dataFormat = 8;
        StopBits stBit = StopBits.One;
        //value for Logger
        private string DatainfoTransmit;
        public string DataInfoReceive;
        public BackgroundWorker work;
        private byte[] RECIEVEBUFFER = new byte[sizeRecieveBuffer];
        private string ResBUF;
        private bool statusThreadRead = true;
      

        public bool EnableReadBuffer
        {
            get
            {
                return statusThreadRead;
            }
            set
            {
                statusThreadRead = value;
            }
        }
        public string GetReceiveString()
        {
            return ResBUF;
        }
        public byte[] GetReceiveBuffer()
        {
           // RECIEVEBUFFER = Encoding.ASCII.GetBytes(ResBUF);
            return RECIEVEBUFFER;
        }
        public bool isOpenPort { get; set; }


        public SerialDataTransfer(SerialPort _port)
        {
            dataPort = _port;
           
        }

        

        #region Test Data Control UART
        public void TestOpen()
        {
            dataPort.PortName = "COM9";
            dataPort.BaudRate = 115200;
            dataPort.Parity = Parity.None;
            dataPort.StopBits = StopBits.One;
            dataPort.DataBits = 8;
            dataPort.Open();
            
        }



        public void TestTransmit()
        {
            string s = "++";
            string data = "LSM6DS0 Nucleo64 STM32F401RE\r\n";
            data += s;
            dataPort.WriteLine(data);
        }
        #endregion

        /// <summary>
        /// return names Ports all 
        /// </summary>
        /// <returns></returns>
        public List<string> GetPortNames()
        {
            return SerialPort.GetPortNames().ToList();
        }

        /// <summary>
        /// return All BaudRate
        /// </summary>
        /// <returns></returns>
        public List<int> GetBaudRateAll()
        {
            return new List<int> { 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200 };

        }


        /// <summary>
        /// Открытие порта
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="baud"></param>
        public void OpenPort(string Name,int baud)
        {
            if(Name!="" && baud>0)
            {
                try
                {
                    if (!dataPort.IsOpen)
                    {
                        dataPort.PortName = Name;
                        dataPort.BaudRate = baud;
                        dataPort.Parity = parity;
                        dataPort.StopBits = stBit;
                        dataPort.DataBits = dataFormat;
                        
                        dataPort.DataReceived += Thread_reaciever_Data_Hundler;
                        //dataPort.RtsEnable = true;
                        //dataPort.DtrEnable = true;
                        dataPort.ReadBufferSize = 16;
                        dataPort.Open();
                        isOpenPort = true;

                       // work = new BackgroundWorker();
                       // work.WorkerReportsProgress = true;
                        //work.DoWork += Thread_Recieve;
                        //work.RunWorkerAsync(100);


                    }
                    else
                    {
                        isOpenPort = false;
                        dataPort.Close();
                    }
                }
                catch(Exception exs)
                {
                    MessageBox.Show("Error!!!-" + exs.Message);
                }
       
            }
            else
            {
                isOpenPort = false;
                return;
            }
        }

        private void Thread_reaciever_Data_Hundler(object sender, SerialDataReceivedEventArgs e)
        {
            
            //dataPort.Read(RECIEVEBUFFER, 0, sizeRecieveBuffer);
            //ResBUF = Encoding.ASCII.GetString(RECIEVEBUFFER);
            ResBUF= dataPort.ReadLine();

        }

        //private void Thread_Recieve(object sender, DoWorkEventArgs e)
        //{
        //    for (;;)
        //    {

        //        //Thread.Sleep(25);
                
                
                   
                
        //    }
        //}

        /// <summary>
        /// Закрытие порта
        /// </summary>
        public void ClosePort()
        {
            if(dataPort.IsOpen)
            {
                dataPort.Close();
            }
            else
            {
                MessageBox.Show("Порт и так уже закрыт!!!");
            }
        }


        public void SerialPort_TransmitData(string data)
        {
            if(data!=null)
            {
                dataPort.WriteLine(data);
                DatainfoTransmit = string.Format("Data:{0},Size:{1},Time:{2}", data, data.Length, DateTime.Now);
            }
            else
            {
                return;
            }
        }


        public void SerialPort_TransmitData(byte[]data)
        {
            if (data != null)
            {
                dataPort.Write(data,0,data.Length);
                DatainfoTransmit = string.Format("Data:{0},Size:{1},Time:{2}", data, data.Length, DateTime.Now);
            }
            else
            {
                return;
            }
        }


    }
}
