﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace MSP_BOARD_rev00.Model
{

    /****
     * 
     * MODEL DATA & PAYLOAD:
     * 
     *          Struct:
     *            1  ID_DEV                  uint8_t
     *            2  GPIO_MCP                uint8_t
     *            3  GPIO_EXT                uint8_t->3bit out 3bit in
     *            8 ADC_CH[3]               int16_t 
     *            9  ENR_PWM                 uint8_t->3bit ch
     *            16  PWM_DUTY[3]             uint16_t
     *              RES_HALL_IN             uint8_t->3bit u,v,w
     * 
     * 
     *          SIZE:       17byte
     * */





    public struct Data_MSP_rev00
    {
        public byte ID_DEV;//индефикатор устройства
        public byte STATE_GPIO_MCP23017;//OUTPUT State for MCP23017
        public byte STATE_EXT_GPIO;//0-2(bit out),3-5(bit in)
        public Int16[] ADC_CH;//Analog value
        public byte EN_PWM;//разрешение включение PWM
        public UInt16[] PWM_DUTY;//Duty PWM
        public byte _RES_HALL_IN;//Hall sensoor data
    }

    public enum MASK_EXT_GPIO
    {
       SET_GPIO1=0x01,
       SET_GPIO2 = 0x02,
       SET_GPIO3 = 0x04,

        RESET_GPIO1 = 0xFE,
        RESET_GPIO2 = 0xFD,
        RESET_GPIO3 = 0xFB
    }
    public class MSP_rev00_Model
    {
        private DispatcherTimer tim_updateUI;
      public byte[] TempDataBuffer = new byte[17];
      public const int CNT_CH_ADC = 3;
      public const int CNT_CH_PWM = 3;

        #region PRIVATE varible

        private Data_MSP_rev00 Main_data;
        private RS485_Prot MainProtocol;
        #endregion

        public RS485_Prot Data_trasfer
        {
            get
            {
                return MainProtocol;
            }
        }

        public MSP_rev00_Model(byte _id, RS485_Prot th_data)
        {
            Main_data.ID_DEV = _id;
            MainProtocol = th_data;
            Init_Main_Data();
            tim_updateUI = new DispatcherTimer();
            tim_updateUI.Tick += TIM_READ_RECIEVEBUFFER_HUNDLER;
            tim_updateUI.Interval = new TimeSpan(0, 0, 0, 0, 10);
            tim_updateUI.Start();
        }



        private void TIM_READ_RECIEVEBUFFER_HUNDLER(object sender, EventArgs e)
        {
            if(MainProtocol.GetReceiveBuffer()[0]== Main_data.ID_DEV)
            {
                
                if(MainProtocol.RxCheckCRC16(MainProtocol.GetReceiveBuffer()))
                {
                    byte[] dataPAY = MainProtocol.GET_PAYLOAD_RS485(MainProtocol.GetReceiveBuffer());//выделяем полезные данные
                    //проверяем совпадают ли они с предыдущими 
                    bool isEqual = Enumerable.SequenceEqual(dataPAY, TempDataBuffer);
                    //если совпадают , то не копируем если разные копируем
                    if(!isEqual)
                    {
                        Buffer.BlockCopy(dataPAY, 0, TempDataBuffer, 0, dataPAY.Length);
                        //парсим данные в структуру
                        Parse_datarecieve(TempDataBuffer);

                    }
             
                }
            }
        }

        public Data_MSP_rev00 dataState
        {
            get
            {
                return Main_data;
            }
        }


        #region PRIVATE FUNCTIONS
        void Init_Main_Data()
        {
            Main_data = new Data_MSP_rev00();
            Main_data.ADC_CH = new Int16[CNT_CH_ADC];
            Main_data.PWM_DUTY = new UInt16[CNT_CH_PWM];

        }


        /// <summary>
        /// Возвращаем значение указанного бита.
        /// </summary>
        /// <param name="arrByte">Массив байт содержащий искомый бит.</param>
        /// <param name="intNumberBite">Номер искомого бита.</param>
        /// <returns></returns>
        static private bool arrByte2Bite(byte[] arrByte, int intNumberBite)
        {
            if (arrByte.Length * 8 < intNumberBite)
                return false; //Вернём false если номер бита вышел за приделы массива

            int intNumByte = intNumberBite / 8; // Уточним номер байта
            int intNumBite = intNumberBite % 8; // Уточним номер бита в бите
            return (((arrByte[intNumByte] >> intNumBite) & 1) == 1);
        }




        /// <summary>
        /// Function Update Fill Public for class
        /// </summary>
        /// <param name="main_data">struct for update</param>
        private void Update_Fill_class(Data_MSP_rev00 main_data)
        {

        }
        #endregion


        #region PUBLIC FILL
        /// <summary>
        /// GET Information ID
        /// </summary>
        public byte ID_DEV
        {
            get
            {
                return Main_data.ID_DEV;
            }
        }

        /// <summary>
        /// GET OUT MCP23017
        /// </summary>
        public byte GPIO_MCP23017
        {
            get
            {
                return Main_data.STATE_GPIO_MCP23017;
            }
        }



        /// <summary>
        /// GET OUT EXT_GPIO
        /// </summary>
        public byte EXT_GPIO
        {
            get
            {
                return Main_data.STATE_EXT_GPIO;
            }
        }

        /// <summary>
        /// GET Analog 1
        /// </summary>
        public float ADC_CH1
        {
            get
            {
                return (float)(Main_data.ADC_CH[0] / 1000);
            }
        }

        /// <summary>
        /// GET Analog 2
        /// </summary>
        public float ADC_CH2
        {
            get
            {
                return (float)(Main_data.ADC_CH[1] / 1000);
            }
        }

        /// <summary>
        /// GET Analog 3
        /// </summary>
        public float ADC_CH3
        {
            get
            {
                return (float)(Main_data.ADC_CH[2] / 1000);
            }
        }


        /// <summary>
        /// GET STATUS PWM EN1
        /// </summary>
        public bool PWM_ENABLE_CH1
        {
            get
            {
                return arrByte2Bite(new byte[1] { Main_data.EN_PWM }, 1);
            }
        }


        /// <summary>
        /// GET STATUS PWM EN2
        /// </summary>
        public bool PWM_ENABLE_CH2
        {
            get
            {
                return arrByte2Bite(new byte[1] { Main_data.EN_PWM }, 2);
            }
        }

        /// <summary>
        /// GET STATUS PWM EN3
        /// </summary>
        public bool PWM_ENABLE_CH3
        {
            get
            {
                return arrByte2Bite(new byte[1] { Main_data.EN_PWM }, 3);
            }
        }


        /// <summary>
        /// GET STATUS PWM CH1
        /// </summary>
        public float Duty_CH1
        {
            get
            {
                return Main_data.PWM_DUTY[0] / 1000;
            }
        }

        /// <summary>
        /// GET STATUS PWM CH2
        /// </summary>
        public float Duty_CH2
        {
            get
            {
                return Main_data.PWM_DUTY[1] / 1000;
            }
        }


        /// <summary>
        /// GET STATUS PWM CH3
        /// </summary>
        public float Duty_CH3
        {
            get
            {
                return Main_data.PWM_DUTY[2] / 1000;
            }
        }



        /// <summary>
        /// GET STATUS HALL
        /// </summary>
        public byte HALL_IN
        {
            get
            {
                return Main_data._RES_HALL_IN;
            }
        }






        #endregion

        /// <summary>
        /// Converted data to byte
        /// </summary>
        /// <param name="_newstruct"></param>
        public void  Send_Command(Data_MSP_rev00 _newstruct)
        {
            Main_data = _newstruct;
            Update_Fill_class(Main_data);
            byte[] dataBuffer = new byte[17];
            dataBuffer[0] = Main_data.ID_DEV;
            dataBuffer[1] = Main_data.STATE_GPIO_MCP23017;
            dataBuffer[2] = Main_data.STATE_EXT_GPIO;

            //-------------------------------------
            dataBuffer[3] = (byte)(Main_data.ADC_CH[0] >> 8);
            dataBuffer[4] = (byte)(Main_data.ADC_CH[0]);

            dataBuffer[5] = (byte)(Main_data.ADC_CH[1] >> 8);
            dataBuffer[6] = (byte)(Main_data.ADC_CH[1]);

            dataBuffer[7] = (byte)(Main_data.ADC_CH[2] >> 8);
            dataBuffer[8] = (byte)(Main_data.ADC_CH[2]);
            //---------------------------------------
            dataBuffer[9] = Main_data.EN_PWM;

            //---------------------------------------
            dataBuffer[10] = (byte)(Main_data.PWM_DUTY[0] >> 8);
            dataBuffer[11] = (byte)(Main_data.PWM_DUTY[0]);

            dataBuffer[12] = (byte)(Main_data.PWM_DUTY[1] >> 8);
            dataBuffer[13] = (byte)(Main_data.PWM_DUTY[1]);

            dataBuffer[14] = (byte)(Main_data.PWM_DUTY[2] >> 8);
            dataBuffer[15] = (byte)(Main_data.PWM_DUTY[2]);
            //----------------------------------------
            dataBuffer[16] = Main_data._RES_HALL_IN;
            
            byte[] dataOutBuffer = new byte[MainProtocol.SIZE_BUFFER];
            dataOutBuffer = MainProtocol.Parse_ToTransmit_RS485(CommandProtocol.REQUEST_PC, dataBuffer);
            MainProtocol.SerialPort_TransmitData(dataOutBuffer);



        }


        /// <summary>
        /// Разбираем данные из пакета приема
        /// </summary>
        /// <param name="data"></param>
        public void Parse_datarecieve(byte[] data)
        {
            Main_data.STATE_EXT_GPIO = data[2];

            Main_data.ADC_CH[0] = BitConverter.ToInt16(new byte[] { data[3], data[4] }, 0);
            Main_data.ADC_CH[1] = BitConverter.ToInt16(new byte[] { data[5], data[6] }, 0);
            Main_data.ADC_CH[2] = BitConverter.ToInt16(new byte[] { data[7], data[8] }, 0);

            Main_data._RES_HALL_IN = data[16];


        }


    }
}
