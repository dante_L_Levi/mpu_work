﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MSP_BOARD_rev01.Model;



namespace MSP_BOARD_rev01
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private variable
        private SerialPort _port;
        private RS485_Prot dataTransfer;
        private readonly byte ID_DEVICE = 0x0F;
        private MSP_rev01_BLDC Main_data;
        Data_MSP_rev01_BLDC Main_dataUI;
        private DispatcherTimer Timer_UI = new DispatcherTimer();
        private int count_toogleSwitch = 1;
        #endregion

        #region Helpers For Load Data
        private void Load_Data()
        {
            Port_data.ItemsSource = dataTransfer.GetPortNames();
            Port_Baud.ItemsSource = dataTransfer.GetBaudRateAll();

        }


        #endregion

        public MainWindow()
        {
            InitializeComponent();
            _port = new SerialPort();
            Main_data = new MSP_rev01_BLDC(ID_DEVICE);
            dataTransfer = new RS485_Prot(ID_DEVICE, _port, Main_data);
         
            Load_Data();
            Main_dataUI = Main_data.dataState;
        }

        #region Connection Event
        private void Btn_Connection_Click(object sender, RoutedEventArgs e)
        {
            if (Port_data.SelectedIndex != -1 && Port_Baud.SelectedIndex != -1)
            {
                dataTransfer.OpenPort(Port_data.SelectedItem.ToString(), (int)Port_Baud.SelectedItem);
                MainFill.IsEnabled = true;
                MainContainer.SelectedItem = MainContainer.Items[1];
                Timer_UI.Interval = new TimeSpan(0, 0, 0,0, 100);

                Timer_UI.Tick += UPDATE_UI;
                Timer_UI.Start();
            }
            else
            {
                MainContainer.SelectedItem = MainContainer.Items[0];
                MainFill.IsEnabled = false;
            }
        }


        #endregion

        /// <summary>
        /// Update UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UPDATE_UI(object sender, EventArgs e)
        {
            
            CheckBox[] switches=new CheckBox[8] { sw_start, sw_stop, sw_spup, sw_spdn, sw_reverse, sw_res1, sw_res2, sw_res3 };
            CheckBox[] halls = new CheckBox[3] { Hall_U, Hall_V, Hall_W };
            Label[] ADC_label = new Label[7] { ADC_U, ADC_V, ADC_W, IC_U, IC_V, IC_W, IC_POW };
            Main_data.Udate_UI_MainWindow(ref switches, ref halls, ref ADC_label);
            Update_State_UI(switches, halls, ADC_label);

        }

        void Update_State_UI(CheckBox[] sw, CheckBox[] halls, Label[] ADC_label)
        {
            sw_start = sw[0];
            sw_stop = sw[1];
            sw_spup= sw[2];
            sw_spdn = sw[3];
            sw_reverse = sw[4];
            sw_res1 = sw[5];
            sw_res2 = sw[6];
            sw_res3 = sw[7];
            //---------------------------------------------
            Hall_U = halls[0];
            Hall_V = halls[1];
            Hall_W = halls[2];
            //---------------------------------------------
            ADC_U = ADC_label[0];
            ADC_V = ADC_label[1];
            ADC_W = ADC_label[2];
            IC_U = ADC_label[3];
            IC_V = ADC_label[4];
            IC_W = ADC_label[5];
            IC_POW = ADC_label[6];
        }

        private void Window_Closed(object sender, EventArgs e)
        {

        }

        private void sw_Start_PR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            count_toogleSwitch++;
            Set_Switch(count_toogleSwitch);
        }

        private void Set_Switch(int statusSw)
        {

            if (count_toogleSwitch % 2 == 1)
            {
                Main_dataUI.STATE_Motor = (byte)(Main_dataUI.STATE_Motor | 0x10);
            }
            else
            {
                Main_dataUI.STATE_Motor = (byte)(Main_dataUI.STATE_Motor & 0xEF);
            }
            //ToDO:1)неправильно переключает вкл-0x10, выкл-0x00
            //ToDO:2)устанавливать флаг forward!!!!
            //ToDO:3)убрать кнопку стоп, добавить изменения LAbel!!!!
            dataTransfer.SerialPort_TransmitData(dataTransfer.Parse_ToTransmit_RS485(CommandProtocol.REQUEST_PC,Main_data.Convert_dataToSend(Main_dataUI)));
           
        }

        private void speed_Value_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Box_speed.Text = e.NewValue.ToString();
            int dtSpeed = (int)e.NewValue;
            byte dt = 0;
            if (dtSpeed>100|| dtSpeed<-100)
            {
                dt =(byte)( Math.Abs(dtSpeed));
                if(dtSpeed<0)
                {
                    dt = (byte)(dt | 0x80);
                }
                else
                {
                    dt = (byte)(dt & 0x7F);
                }
                Main_dataUI.speed = dt;
                
                //ToDO:4)устанавливать флаг forward!!!!
              
                dataTransfer.SerialPort_TransmitData(dataTransfer.Parse_ToTransmit_RS485(CommandProtocol.REQUEST_PC, Main_data.Convert_dataToSend(Main_dataUI)));
            }
        }
    }
}
