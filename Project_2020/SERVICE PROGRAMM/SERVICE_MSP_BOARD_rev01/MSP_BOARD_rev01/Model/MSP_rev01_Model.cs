﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace MSP_BOARD_rev01.Model
{

    /****
     * 
     * MODEL DATA & PAYLOAD:
     * 
     *          Struct:
     *          ID:     1byte
     *          CMD:    1byte
     *          ------------PAYLOAD-------------------------
     *          status_CNT      1byte       FOC,BLOC_COM
     *          ADC[7]:         14byte
     *          switch_st:      1byte
     *          speed :         1byte
     *          status_motor    1byte           0-bit -forward,reverse,1bit-hallU,2bit-hallV,3-bit HALLW,4-start/stop
     *           SIZE:       18byte
     *           ------------------------------------------------------
     *          CRC:            2byte
     *          S:              22byte
     *         
     * */





    public struct Data_MSP_rev01_BLDC
    {
        public byte ID_DEV;//индефикатор устройства
        public byte type_CNT;//FE-Block_CNT,FF-FOC
        public byte switch_status;//switch status
        public byte STATE_Motor;//0-bit(start/stop),1-bit(forward/reverse),2,3,4(Hall u,V,W)
        public Int16[] ADC_CH;//VoltageU,V,W,IcurrentU,V,W,ICurrent Power
        public byte speed;//0-100%
    }

    public class MSP_rev01_BLDC
    {
       // private DispatcherTimer tim_updateUI;
      public byte[] TempDataBuffer = new byte[18];
      public const int CNT_CH_ADC = 7;

        #region PRIVATE varible

        private Data_MSP_rev01_BLDC Main_data;
    
        #endregion

       

        public MSP_rev01_BLDC(byte _id)
        {
            Init_Main_Data();
            Main_data.ID_DEV = _id;
         
         
        }


        public void Udate_UI_MainWindow(ref CheckBox[] switches, ref CheckBox[] halls,ref Label[] ADC_CH)
        {
            for(int i=0;i< switches.Length;i++)
            {
                switches[i].IsChecked= this.arrByte2Bite( Main_data.switch_status,i);
            }
  
            ADC_CH[0].Content = Main_data.ADC_CH[0].ToString();
            ADC_CH[1].Content = Main_data.ADC_CH[1].ToString();
            ADC_CH[2].Content = Main_data.ADC_CH[2].ToString();
            ADC_CH[3].Content = Main_data.ADC_CH[3].ToString();
            ADC_CH[4].Content = Main_data.ADC_CH[4].ToString();
            ADC_CH[5].Content = Main_data.ADC_CH[5].ToString();
            ADC_CH[6].Content = Main_data.ADC_CH[6].ToString();

            //---------------------------------------
            for (int i = 1; i < halls.Length; i++)
            {
                halls[i-1].IsChecked= this.arrByte2Bite(Main_data.STATE_Motor, i);
            }


            // dataBuffer[16] = Main_data.STATE_Motor;
            // dataBuffer[17] = Main_data.speed;


        }

        public void Read_ToParse_Buffer_Serial(RS485_Prot _serial)
        {
            if (_serial.GetReceiveBuffer()[0] == Main_data.ID_DEV)
            {
                if (_serial.RxCheckCRC16(_serial.GetReceiveBuffer()))
                {
                    byte[] dataPAY = _serial.GET_PAYLOAD_RS485(_serial.GetReceiveBuffer());//выделяем полезные данные
                   //проверяем совпадают ли они с предыдущими                                                                                               
                   bool isEqual = Enumerable.SequenceEqual(dataPAY, TempDataBuffer);
                    //если совпадают , то не копируем если разные копируем
                    if (!isEqual)
                    {
                        Buffer.BlockCopy(dataPAY, 0, TempDataBuffer, 0, dataPAY.Length);
                        //парсим данные в структуру
                        Parse_datarecieve(TempDataBuffer);
                    }
                }
            }
        }


        //private void TIM_READ_RECIEVEBUFFER_HUNDLER(object sender, EventArgs e)
        //{
        //    if(MainProtocol.GetReceiveBuffer()[0]== Main_data.ID_DEV)
        //    {
                
        //        if(MainProtocol.RxCheckCRC16(MainProtocol.GetReceiveBuffer()))
        //        {
        //            byte[] dataPAY = MainProtocol.GET_PAYLOAD_RS485(MainProtocol.GetReceiveBuffer());//выделяем полезные данные
        //            //проверяем совпадают ли они с предыдущими 
        //            bool isEqual = Enumerable.SequenceEqual(dataPAY, TempDataBuffer);
        //            //если совпадают , то не копируем если разные копируем
        //            if(!isEqual)
        //            {
        //                Buffer.BlockCopy(dataPAY, 0, TempDataBuffer, 0, dataPAY.Length);
        //                //парсим данные в структуру
        //                Parse_datarecieve(TempDataBuffer);

        //            }
             
        //        }
        //    }
        //}

        public Data_MSP_rev01_BLDC dataState
        {
            get
            {
                return Main_data;
            }
        }


        #region PRIVATE FUNCTIONS
        void Init_Main_Data()
        {
            Main_data = new Data_MSP_rev01_BLDC();
            Main_data.ADC_CH = new Int16[CNT_CH_ADC];
            

        }


        /// <summary>
        /// Возвращаем значение указанного бита.
        /// </summary>
        /// <param name="arrByte">Массив байт содержащий искомый бит.</param>
        /// <param name="intNumberBite">Номер искомого бита.</param>
        /// <returns></returns>
        private bool arrByte2Bite(byte value, int bitNumber)
        {
            if (bitNumber < 0 || bitNumber > 7)
            {
                throw new ArgumentOutOfRangeException();
            }

            BitArray array = new BitArray(new byte[] { value });
            return array[bitNumber];
        }




        /// <summary>
        /// Function Update Fill Public for class
        /// </summary>
        /// <param name="main_data">struct for update</param>
        private void Update_Fill_class(Data_MSP_rev01_BLDC main_data)
        {

        }
        #endregion


        #region PUBLIC FILL
        /// <summary>
        /// GET Information ID
        /// </summary>
        public byte ID_DEV
        {
            get
            {
                return Main_data.ID_DEV;
            }
        }

        /// <summary>
        /// GET SWITCH EXT_GPIO
        /// </summary>
        public byte SWITCH_GPIO
        {
            get
            {
                return Main_data.switch_status;
            }
        }

        /// <summary>
        /// GET Analog 1
        /// </summary>
        public float ADC_CH1
        {
            get
            {
                return (float)(Main_data.ADC_CH[0] / 1000);
            }
        }

        /// <summary>
        /// GET Analog 2
        /// </summary>
        public float ADC_CH2
        {
            get
            {
                return (float)(Main_data.ADC_CH[1] / 1000);
            }
        }

        /// <summary>
        /// GET Analog 3
        /// </summary>
        public float ADC_CH3
        {
            get
            {
                return (float)(Main_data.ADC_CH[2] / 1000);
            }
        }

        /// GET Analog 4
        /// </summary>
        public float ADC_CH4
        {
            get
            {
                return (float)(Main_data.ADC_CH[3] / 1000);
            }
        }


        /// GET Analog 5
        /// </summary>
        public float ADC_CH5
        {
            get
            {
                return (float)(Main_data.ADC_CH[4] / 1000);
            }
        }


        /// GET Analog 6
        /// </summary>
        public float ADC_CH6
        {
            get
            {
                return (float)(Main_data.ADC_CH[5] / 1000);
            }
        }


        /// GET Analog 7
        /// </summary>
        public float ADC_CH7
        {
            get
            {
                return (float)(Main_data.ADC_CH[6] / 1000);
            }
        }

        /// HALL State U
        /// </summary>
        public bool HALL_U
        {
            get
            {
                bool hall;
                return hall = (Main_data.STATE_Motor & (1 << 2)) == 1 ? true : false;
            }
        }

        /// HALL State V
        /// </summary>
        public bool HALL_V
        {
            get
            {
                bool hall;
                return hall = (Main_data.STATE_Motor & (1 << 3)) == 1 ? true : false;
            }
        }

        /// HALL State W
        /// </summary>
        public bool HALL_W
        {
            get
            {
                bool hall;
                return hall = (Main_data.STATE_Motor & (1 << 4)) == 1 ? true : false;
            }
        }

        /// <summary>
        /// Status Work Motor
        /// </summary>
        public bool Stop_Start_Motor
        {
            get
            {
                bool statuswork;
                return   statuswork = (Main_data.STATE_Motor & (1 << 0)) == 1 ? true : false;
            }
            set
            {
                if(value==true)
                {
                    Main_data.STATE_Motor =(byte) (Main_data.STATE_Motor | 0x01);

                }
                else
                {
                    Main_data.STATE_Motor = (byte)(Main_data.STATE_Motor & 0xFE);
                }
            }
        }

        /// <summary>
        /// Type Control Motor
        /// </summary>
        public byte TypeCNT
        {
            get
            {
                return Main_data.type_CNT; 
            }
            set
            {
                Main_data.type_CNT = value;
            }
        }
             

        /// <summary>
        ///Motor Direction
        /// </summary>
        public bool Direction_Motor
        {
            get
            {
                bool direction;
                return direction = (Main_data.STATE_Motor & (1 << 1)) == 1 ? true : false;
            }
            set
            {
                if (value == true)
                {
                    Main_data.STATE_Motor = (byte)(Main_data.STATE_Motor | 0x02);

                }
                else
                {
                    Main_data.STATE_Motor = (byte)(Main_data.STATE_Motor & 0xFD);
                }
            }
        }













        #endregion

        /// <summary>
        /// Converted data to byte
        /// </summary>
        /// <param name="_newstruct"></param>
        public byte[] MSP_rev01_BLDC_ToBytes(Data_MSP_rev01_BLDC _newstruct)
        {
            Main_data = _newstruct;
            Update_Fill_class(Main_data);
            byte[] dataBuffer = new byte[18];
            for(int i=0;i<dataBuffer.Length;i++)
            {
                dataBuffer[i] = 0xFF;
            }
           // dataBuffer[1] = Main_data.switch_status;
            dataBuffer[0] = Main_data.type_CNT;

            //-------------------------------------
            //dataBuffer[2] = (byte)(Main_data.ADC_CH[0] >> 8);
            //dataBuffer[3] = (byte)(Main_data.ADC_CH[0]);

            //dataBuffer[4] = (byte)(Main_data.ADC_CH[1] >> 8);
            //dataBuffer[5] = (byte)(Main_data.ADC_CH[1]);

            //dataBuffer[6] = (byte)(Main_data.ADC_CH[2] >> 8);
            //dataBuffer[7] = (byte)(Main_data.ADC_CH[2]);

            //dataBuffer[8] = (byte)(Main_data.ADC_CH[3] >> 8);
            //dataBuffer[9] = (byte)(Main_data.ADC_CH[3]);

            //dataBuffer[10] = (byte)(Main_data.ADC_CH[4] >> 8);
            //dataBuffer[11] = (byte)(Main_data.ADC_CH[4]);

            //dataBuffer[12] = (byte)(Main_data.ADC_CH[5] >> 8);
            //dataBuffer[13] = (byte)(Main_data.ADC_CH[5]);

            //dataBuffer[14] = (byte)(Main_data.ADC_CH[6] >> 8);
            //dataBuffer[15] = (byte)(Main_data.ADC_CH[6]);
            //---------------------------------------
            dataBuffer[17] = Main_data.STATE_Motor;
            dataBuffer[16] = Main_data.speed;

            // byte[] dataOutBuffer = new byte[MainProtocol.SIZE_BUFFER];
            // dataOutBuffer = MainProtocol.Parse_ToTransmit_RS485(CommandProtocol.REQUEST_PC, dataBuffer);
            //MainProtocol.SerialPort_TransmitData(dataOutBuffer);
            return dataBuffer;
        }


        /// <summary>
        /// Function Transmit Data
        /// </summary>
        /// <param name="_newstruct"></param>
        /// <param name="_sp"></param>
        public byte[] Convert_dataToSend(Data_MSP_rev01_BLDC _newstruct)
        {
            byte[] databuffer = MSP_rev01_BLDC_ToBytes(_newstruct);
            return databuffer;


        }

        /// <summary>
        /// Разбираем данные из пакета приема
        /// </summary>
        /// <param name="data"></param>
        public void Parse_datarecieve(byte[] data)
        {
            Main_data.type_CNT = data[0];
            Main_data.switch_status = data[1];

            Main_data.ADC_CH[0] = BitConverter.ToInt16(new byte[] { data[2], data[3] }, 0);
            Main_data.ADC_CH[1] = BitConverter.ToInt16(new byte[] { data[4], data[5] }, 0);
            Main_data.ADC_CH[2] = BitConverter.ToInt16(new byte[] { data[6], data[7] }, 0);
            Main_data.ADC_CH[3] = BitConverter.ToInt16(new byte[] { data[8], data[9] }, 0);
            Main_data.ADC_CH[4] = BitConverter.ToInt16(new byte[] { data[10], data[11] }, 0);
            Main_data.ADC_CH[5] = BitConverter.ToInt16(new byte[] { data[12], data[13] }, 0);
            Main_data.ADC_CH[6] = BitConverter.ToInt16(new byte[] { data[14], data[15] }, 0);


            Main_data.STATE_Motor = data[16];
            Main_data.speed = data[17];


        }


    }
}
