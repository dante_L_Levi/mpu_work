﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MSP_BOARD_rev01.Model
{
    public enum CommandProtocol
    {
        IS_MUTED = 0xFF,//off transmit
        START_PACKET = 0x7F,//start packet 
        CONTINUE_PACKET = 0x3F,//продолжение пакета
        STOP_PACKET = 0x1F,//последний пакет
        OK_WORK = 0x0F,//нормальная работа
        ERROR = 0x1C,//Общая ошибка
        CRC_ERROR = 0x1A,//ошибка подсчета контрольной суммы
        IS_GOTOBOOT = 0x0C,//команда перехода в бут
        RESPONSE_MPU_Data = 0x2C,//ответ от платы одним пакетом
        RESPONSE_to_REQUEST = 0x02,
        REQUEST_PC=0x03
        
    }
   public class RS485_Prot
    {
        private byte Id_dev;
        private MSP_rev01_BLDC _bldc_model;

        public RS485_Prot(byte _id, SerialPort _port, MSP_rev01_BLDC dt)
        {
            Id_dev = _id;
            dataPort = _port;
            _bldc_model = dt;
        }

        #region Variable
        const int sizebufferRecieve = 22;
        const int BufferPAYLOAD = 18;
        byte[] TransmitBuffer = new byte[sizebufferRecieve];
        public byte[] PAYLOAD = new byte[BufferPAYLOAD];
        public CommandProtocol COMMAND;
        public SerialPort dataPort;
        Parity parity = Parity.None;
        int dataFormat = 8;
        StopBits stBit = StopBits.One;
        public string DataInfoReceive;
        public string DataInfoTransmit;
        private byte[] RECIEVEBUFFER = new byte[sizebufferRecieve];
        #endregion

        public string GetReceiveInfoString()
        {
            return DataInfoReceive;
        }
        public byte[] GetReceiveBuffer()
        {
            // RECIEVEBUFFER = Encoding.ASCII.GetBytes(ResBUF);
            return RECIEVEBUFFER;
        }
        public bool isOpenPort { get; set; }
        public int SIZE_BUFFER
        {
            get
            {
                return sizebufferRecieve;
            }
        }



        #region Test Data Control UART
        public void TestOpen()
        {
            dataPort.PortName = "COM9";
            dataPort.BaudRate = 115200;
            dataPort.Parity = Parity.None;
            dataPort.StopBits = StopBits.One;
            dataPort.DataBits = 8;
            dataPort.Open();

        }



        public void TestTransmit()
        {
            string s = "++";
            string data = "LSM6DS0 Nucleo64 STM32F401RE\r\n";
            data += s;
            dataPort.WriteLine(data);
        }
        #endregion


        public List<string> GetPortNames()
        {
            return SerialPort.GetPortNames().ToList();
        }

        /// <summary>
        /// return All BaudRate
        /// </summary>
        /// <returns></returns>
        public List<int> GetBaudRateAll()
        {
            return new List<int> { 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200 };

        }


        /// <summary>
        /// Открытие порта
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="baud"></param>
        public void OpenPort(string Name, int baud)
        {
            if (Name != "" && baud > 0)
            {
                try
                {
                    if (!dataPort.IsOpen)
                    {
                        dataPort.PortName = Name;
                        dataPort.BaudRate = baud;
                        dataPort.Parity = parity;
                        dataPort.StopBits = stBit;
                        dataPort.DataBits = dataFormat;

                        dataPort.DataReceived += Thread_reaciever_Data_Hundler;
                        dataPort.ReadBufferSize = 16;
                        dataPort.Open();
                        isOpenPort = true;
                    }
                    else
                    {
                        isOpenPort = false;
                        dataPort.Close();
                    }
                }
                catch (Exception exs)
                {
                    MessageBox.Show("Error!!!-" + exs.Message);
                }

            }
            else
            {
                isOpenPort = false;
                return;
            }
        }

        private void Thread_reaciever_Data_Hundler(object sender, SerialDataReceivedEventArgs e)
        {
            dataPort.Read(RECIEVEBUFFER, 0, sizebufferRecieve);
            DataInfoReceive = Encoding.ASCII.GetString(RECIEVEBUFFER);
            _bldc_model.Read_ToParse_Buffer_Serial(this);

        }



        public void ClosePort()
        {
            if (dataPort.IsOpen)
            {
                dataPort.Close();
            }
            else
            {
                MessageBox.Show("Порт и так уже закрыт!!!");
            }
        }


        public void SerialPort_TransmitData(string data)
        {
            if (data != null)
            {
                dataPort.WriteLine(data);
                DataInfoTransmit = string.Format("Data:{0},Size:{1},Time:{2}", data, data.Length, DateTime.Now);
            }
            else
            {
                return;
            }
        }


        public void SerialPort_TransmitData(byte[] data)
        {
            if (data != null)
            {
                dataPort.Write(data, 0, data.Length);
                DataInfoTransmit = string.Format("Data:{0},Size:{1},Time:{2}", data, data.Length, DateTime.Now);
            }
            else
            {
                return;
            }
        }


        /// <summary>
        /// Function Transmit Data
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="ParseStr"></param>
        /// <returns></returns>
        public byte[] Parse_ToTransmit_RS485(CommandProtocol cmd, byte[] ParseStr)
        {
            if (ParseStr.Length > BufferPAYLOAD)
            {
                return new byte[sizebufferRecieve];
            }
            else
            {
                TransmitBuffer[0] = Id_dev;
                TransmitBuffer[1] = (byte)cmd;
                COMMAND = cmd;
                
                Buffer.BlockCopy(ParseStr, 0, PAYLOAD, 0, ParseStr.Length);
                int j = 2;

                for (int i = 0; i < ParseStr.Length; i++)
                {
                    TransmitBuffer[j] = ParseStr[i];
                    j++;
                }
                TxCalculateCRC16(ref TransmitBuffer);
                return TransmitBuffer;
            }
        }

        /// <summary>
        /// Function Reciever_data GET PAYLOAD
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="ParseStr"></param>
        /// <returns></returns>
        public byte[] GET_PAYLOAD_RS485(byte[]AllBuffer)
        {
            if(AllBuffer[0]==Id_dev)
            {
               if(RxCheckCRC16(AllBuffer))
                {
                    COMMAND = (CommandProtocol)(AllBuffer[1]);
                    byte[] tempBuffer = new byte[BufferPAYLOAD];
                    Buffer.BlockCopy(AllBuffer, 2, tempBuffer, 0, tempBuffer.Length);
                    return tempBuffer;

                }
                else
                {
                    return new byte[sizebufferRecieve];
                }
            }
            else
            {
                return new byte[sizebufferRecieve];
            }
        }







        #region CRC CALCULATE

        /// <summary>
        /// Calculate CRC Transmit
        /// </summary>
        /// <param name="data"></param>
        public void TxCalculateCRC16(ref byte[] data)
        {
            UInt16 crc = 0xFFFF;
            for (int pos = 0; pos < data.Length - 2; pos++)
            {
                crc ^= (UInt16)data[pos];
            }
            for (int i = 8; i != 0; i--)
            {
                if ((crc & 0x0001) != 0)
                {
                    crc >>= 1;
                    crc ^= 0xA001;
                }
                else
                {
                    crc >>= 1;
                }
            }
            // Помните, что младший и старший байты поменяны местами, используйте соответственно (или переворачивайте)
            data[data.Length - 2] = (byte)crc;
            data[data.Length - 1] = (byte)(crc >> 8);
        }


        /// <summary>
        /// Calculate CRC Recieve
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool RxCheckCRC16(byte[] data)
        {
            UInt16 crc = 0xFFFF;
            for (int pos = 0; pos < data.Length - 2; pos++)
            {
                crc ^= (UInt16)data[pos];
            }
            for (int i = 8; i != 0; i--)
            {
                if ((crc & 0x0001) != 0)
                {
                    crc >>= 1;
                    crc ^= 0xA001;
                }
                else
                {
                    crc >>= 1;
                }


            }
            ushort bufcrc = BitConverter.ToUInt16(new byte[2] { data[data.Length - 2], data[data.Length - 1] }, 0);
            //ushort bufcrc = data[data.Length - 2];//младшая часть
            //bufcrc = (ushort)(data[data.Length - 1] << 8);//старшая часть
            if (crc == bufcrc)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        #endregion

    }
}
