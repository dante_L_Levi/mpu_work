/*
 * ADC_MESSURE.c
 *
 *  Created on: Apr 26, 2020
 *      Author: AlexPirs
 */


#include "ADC_MESSURE.h"

GPIO_InitTypeDef GPIO_InitStruct_ADC = {0};
uint16_t adc_buf	[7];
extern ADC_INPUT_Def							_adc_chs;

/*
void DMA1_Channel1_IRQHandler(void)
{
	DMA1->IFCR = DMA_IFCR_CGIF1 | DMA_IFCR_CTCIF1;		// clear DMA interrupt flags
	DMA1_Channel1->CCR &=~DMA_CCR_EN;					// Запрещаем работу

	for (uint8_t i = 0; i<=ADC_COUNT_CH+1;i++)
	{
		 // Накапливаем данные в структуру
		if(i<=2)
		{
			_adc_chs.ADC_CH[i]=adc_buf[i];
			_adc_chs.ADC_CH[i]=_adc_chs.ADC_CH[i]*3/4096;

		}
		else
		{
			_adc_chs.ADC_CURRENT[i-3]=adc_buf[i];
			_adc_chs.ADC_CURRENT[i]=_adc_chs.ADC_CURRENT[i]*3/4096;
		}

	}

	DMA1_Channel1->CCR |= DMA_CCR_EN;			   // разрешаем работу
	ADC1->CR2 |= ADC_CR2_ADON;
}
*/
/************************************ADC Init********************************/
void ADC_Init(void)
{
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	//-----------------------------GPIO INIT---------------------------------------
	/**ADC1 GPIO Configuration
	    PA3     ------> ADC1_IN3
	    PA4     ------> ADC1_IN4
	    PA5     ------> ADC1_IN5
	    PA6     ------> ADC1_IN6
	    PA7     ------> ADC1_IN7
	    PB0     ------> ADC1_IN8
	    PB1     ------> ADC1_IN9
	    */
	GPIO_InitStruct_ADC.Pin = GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
	                          |GPIO_PIN_7;
	GPIO_InitStruct_ADC.Mode = GPIO_MODE_ANALOG;
	    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct_ADC);

	    GPIO_InitStruct_ADC.Pin = GPIO_PIN_0|GPIO_PIN_1;
	    GPIO_InitStruct_ADC.Mode = GPIO_MODE_ANALOG;
	    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct_ADC);
	    //------------------------------------------------------------------------

	    //------------------------------------DMA---------------------------------
	    RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	    DMA1_Channel1->CPAR = (uint32_t) &ADC1->DR;	 // address pheripheral device
	    DMA1_Channel1->CMAR = (unsigned int) adc_buf;   // adress buffer memory
	    DMA1_Channel1->CNDTR = ADC_COUNT_CH;			  // количество данных для обмена
	    DMA1_Channel1->CCR &=~DMA_CCR_EN;			   // разрешаем работу
	    DMA1_Channel1->CCR |= DMA_CCR_MSIZE_0;		  // размер памяти 16 bit
	    DMA1_Channel1->CCR |= DMA_CCR_PSIZE_0;		 // размер периферии 16 bit
	    DMA1_Channel1->CCR |= DMA_CCR_MINC;			  // memory increment mode
	    DMA1_Channel1->CCR |= DMA_CCR_CIRC;
	    DMA1_Channel1->CCR |= DMA_CCR_TCIE;			  // прерывание по окончанию передачи
	    DMA1_Channel1->CCR |= DMA_CCR_EN;				  // разрешаем работу
	    NVIC_SetPriority(DMA1_Channel1_IRQn, ADC_COUNT_CH);
	    NVIC_EnableIRQ(DMA1_Channel1_IRQn);
	    //------------------------------------------------------------------------
	     RCC->CFGR &= ~RCC_CFGR_ADCPRE;
	     RCC->CFGR |= RCC_CFGR_ADCPRE_DIV8;
	     RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	   // ADC1->CR2|=ADC_CR2_CAL;//start calibration
	  //  while(!(ADC1->CR2 & ADC_CR2_CAL));
	 //   ADC1->CR2|=ADC_CR2_ADON;
	    //последовательность регулярных каналов
	     //ADC1->SQR3  = 0;							// 1
	     //ADC1->SQR3 |= 1<<5;					 // 2 Слева номер канала справа сдвиг
	     //ADC1->SQR3 |= 2<<10;				   // 3
	     ADC1->SQR3 |= 3<<15;				   // 4
	     ADC1->SQR3 |= 4<<20;				   // 5
	     ADC1->SQR3 |= 5<<25;				   // 6
	     ADC1->SQR2  = 6;						   // 7
	     ADC1->SQR2 |= 7<<5;					// 8
	     ADC1->SQR2 |= 8<<10;				   // 9
	     ADC1->SQR2 |= 9<<15;				   // 10

	     ADC1->CR2 = ADC_CR2_EXTSEL_0 | ADC_CR2_EXTSEL_1 | ADC_CR2_EXTSEL_2 | ADC_CR2_EXTTRIG;
	     ADC1->SMPR1   =  0;					  //очистка регистров времени выборки
	     ADC1->SMPR2   =  0;
	     //ADC1->SMPR2  |=  (uint32_t)(6<<(0*3));  //канал 0, время преобразования 6 мкс
	     // ADC1->SMPR2  |=  (uint32_t)(6<<(1*3));  //канал 1, время преобразования 6 мкс
	     // ADC1->SMPR2  |=  (uint32_t)(6<<(2*3));  //канал 2, время преобразования 6 мкс
	      ADC1->SMPR2  |=  (uint32_t)(6<<(3*3));  //канал 3, время преобразования 6 мкс
	      ADC1->SMPR2  |=  (uint32_t)(6<<(4*3));  //канал 4, время преобразования 6 мкс
	      ADC1->SMPR2  |=  (uint32_t)(6<<(5*3));  //канал 5, время преобразования 6 мкс
	      ADC1->SMPR2  |=  (uint32_t)(6<<(6*3));  //канал 6, время преобразования 6 мкс
	      ADC1->SMPR2  |=  (uint32_t)(6<<(7*3));  //канал 7, время преобразования 6 мкс
	      ADC1->SMPR2  |=  (uint32_t)(6<<(8*3));  //канал 8, время преобразования 6 мкс
	      ADC1->SMPR2  |=  (uint32_t)(6<<(9*3));  //канал 9, время преобразования 6 мкс
	     // ADC1->SMPR1  |=  (uint32_t)(6<<(0*3));  //канал 10, время преобразования 6 мкс
	      ADC1->CR2 |= ADC_CR2_ADON; //ON ADC
	      ADC1->CR2 |= ADC_CR2_RSTCAL;//reset calibration
	      while ((ADC1->CR2 & ADC_CR2_RSTCAL) == ADC_CR2_RSTCAL){}//wait reset
	      ADC1->CR2 |= ADC_CR2_CAL;//colibration
	      while ((ADC1->CR2 & ADC_CR2_RSTCAL) == ADC_CR2_CAL)  {}//wait calibration
	      ADC1->SQR1 |= (ADC_COUNT_CH-1)<<20;
	      ADC1->CR1 |= ADC_CR1_SCAN;				// Режим сканирования
	      ADC1->CR2 |= ADC_CR2_DMA;				  // DMA on
	      ADC1->CR2 |= ADC_CR2_ADON;
}
