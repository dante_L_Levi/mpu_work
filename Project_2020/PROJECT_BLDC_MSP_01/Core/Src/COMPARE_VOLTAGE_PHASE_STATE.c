/*
 * COMPARE_VOLTAGE_PHASE_STATE.c
 *
 *  Created on: Apr 26, 2020
 *      Author: AlexPirs
 */


#include "Compare_Voltage_Phase_State.h"

extern GPIO_OUT_INPUT_STATUS 					GPIO_main;



/*******************Function Read Input EXT***********************/
void Read_Input_EXT(void)
{
	uint8_t status_in[3]={0};
	status_in[0]=HAL_GPIO_ReadPin(COMPARE_GPIO_PORT, COMPARE_GPIO_PIN_U);
	status_in[1]=HAL_GPIO_ReadPin(COMPARE_GPIO_PORT, COMPARE_GPIO_PIN_V);
	status_in[2]=HAL_GPIO_ReadPin(COMPARE_GPIO_PORT, COMPARE_GPIO_PIN_W);
	GPIO_main.gpio_input=status_in[0]==0x01?(GPIO_main.gpio_input|0x01):
											(GPIO_main.gpio_input&0x06);

	GPIO_main.gpio_input=status_in[1]==0x01?(GPIO_main.gpio_input|0x02):
											(GPIO_main.gpio_input&0x05);

	GPIO_main.gpio_input=status_in[2]==0x01?(GPIO_main.gpio_input|0x04):
											(GPIO_main.gpio_input&0x03);

}


