/*
 * VSENSE.c
 *
 *  Created on: Feb 25, 2020
 *      Author: AlexPirs
 */


#include "VSENSE.h"


extern ADC_INPUT_Def							_adc_chs;
uint32_t adc_data[ADC_N_CH]={0};
#define ADC_Hundler								hadc1
uint32_t data_Adc[7]={0};

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *adc)
{
	ADC_Read();
}

void ADC_Start(void)
{
	HAL_ADC_Start_DMA(&ADC_Hundler, data_Adc, 7);
}

void ADC_Init_Vsense(void)
{
	MX_DMA_Init();
	MX_ADC1_Init();
}


void ADC_Read(void)
{
	for(int i=0;i<7;i++)
	{
		_adc_chs.ADC_CH[i]=data_Adc[i];
	}
}
