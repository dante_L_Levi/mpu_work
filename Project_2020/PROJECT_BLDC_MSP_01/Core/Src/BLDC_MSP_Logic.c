/*
 * BLDC_MSP_Logic.c
 *
 *  Created on: Feb 22, 2020
 *      Author: AlexPirs
 */


#include "BLDC_MSP_Logic.h"


GPIO_OUT_INPUT_STATUS 					GPIO_main;
MCP23017_hw								MCP23017_GPIO;
ADC_INPUT_Def							_adc_chs;

uint8_t count_Led=0;



/*************************MAIN INIT DATA******************************/
void Main_BLDC_MSPrev01_Init(void)
{

	 MX_GPIO_Init();
	 MX_CAN_Init();
	 MX_I2C1_Init();
	 MX_SPI1_Init();
	 MX_TIM2_Init();

	 ADC_Init_Vsense();
	 ADC_Start();
	 RS485_Init(115200);
	 RS485_RessiveData_Starthelper();
	 //------------------------MSP23017-------------------------------//
	 mcp23017_init(MCP23017_ADDRESS_20);
	 mcp23017_iodir(MCP23017_PORTA,MCP23017_IODIR_ALL_INPUT);
	 mcp23017_iodir(MCP23017_PORTB,MCP23017_IODIR_ALL_OUTPUT);

}



/************************Function Set GPIO OUT********************************/
void OUT_set(uint16_t pinName,bool status)
{
	GPIO_TypeDef *PORT;
		switch(pinName)
		{
			case LED_PIN:
			{
				PORT=LED_STATUS_PORT;

				break;
			}

			default:
			{
				return;
			}

		}
		status!=false?(HAL_GPIO_WritePin(PORT, pinName, GPIO_PIN_SET)):(HAL_GPIO_WritePin(PORT, pinName, GPIO_PIN_RESET));

}


/*******************Test Function Indicate***********************/
void Indicate_Led(uint8_t status)
{
	status%2==0?(OUT_set(LED_PIN,false)):(OUT_set(LED_PIN,true));

}




