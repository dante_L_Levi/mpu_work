/*
 * MCP23017.c
 *
 *  Created on: 19 окт. 2019 г.
 *      Author: Tatiana_Potrebko
 */

#include "MCP23017.h"


extern MCP23017_hw	MCP23017_GPIO;
extern I2C_HandleTypeDef hi2c1;
#define MCP23017_I2C				hi2c1


/********************Error MCP23017*************************/
void MCP23017_Handler_Error(void)
{

}

static uint8_t wait_for_gpio_state_timeout(GPIO_TypeDef *port, uint16_t pin, GPIO_PinState state, uint32_t timeout)
 {
    uint32_t Tickstart = HAL_GetTick();
    uint8_t ret = 1;

    for(;(state != HAL_GPIO_ReadPin(port, pin)) && (1 == ret);) // Wait until flag is set
    {
        if(timeout != HAL_MAX_DELAY) // Check for the timeout
        {
            if((timeout == 0U) || ((HAL_GetTick() - Tickstart) > timeout)) ret = 0;
        }

        asm("nop");
    }
    return ret;
}



static void I2C_ClearBusyFlagErratum(I2C_HandleTypeDef *hi2c, uint32_t timeout)
{
        // 2.13.7 I2C analog filter may provide wrong value, locking BUSY. STM32F10xx8 STM32F10xxB Errata sheet

    GPIO_InitTypeDef GPIO_InitStructure = {0};

    // 1. Clear PE bit.
    CLEAR_BIT(hi2c->Instance->CR1, I2C_CR1_PE);

    //  2. Configure the SCL and SDA I/Os as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).
    HAL_I2C_DeInit(hi2c);

    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStructure.Pull = GPIO_NOPULL;

    GPIO_InitStructure.Pin = GPIO_PIN_8; // SCL // если пин другой, то укажите нужный
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure); // если порт другой, то укажите нужную букву GPIOх, и ниже там все порты и пины поменяйте на своё

    GPIO_InitStructure.Pin = GPIO_PIN_9; // SDA
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

    // 3. Check SCL and SDA High level in GPIOx_IDR.
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);

    wait_for_gpio_state_timeout(GPIOB, GPIO_PIN_8, GPIO_PIN_SET, timeout);
    wait_for_gpio_state_timeout(GPIOB, GPIO_PIN_9, GPIO_PIN_SET, timeout);

    // 4. Configure the SDA I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR).
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);

    // 5. Check SDA Low level in GPIOx_IDR.
    wait_for_gpio_state_timeout(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET, timeout);

    // 6. Configure the SCL I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR).
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);

    // 7. Check SCL Low level in GPIOx_IDR.
    wait_for_gpio_state_timeout(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET, timeout);

    // 8. Configure the SCL I/O as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);

    // 9. Check SCL High level in GPIOx_IDR.
    wait_for_gpio_state_timeout(GPIOB, GPIO_PIN_8, GPIO_PIN_SET, timeout);

    // 10. Configure the SDA I/O as General Purpose Output Open-Drain , High level (Write 1 to GPIOx_ODR).
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);

    // 11. Check SDA High level in GPIOx_IDR.
    wait_for_gpio_state_timeout(GPIOB, GPIO_PIN_9, GPIO_PIN_SET, timeout);

    // 12. Configure the SCL and SDA I/Os as Alternate function Open-Drain.
    GPIO_InitStructure.Mode = GPIO_MODE_AF_OD;
    //GPIO_InitStructure.Alternate = GPIO_AF4_I2C2; // F4

    GPIO_InitStructure.Pin = GPIO_PIN_8;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_InitStructure.Pin = GPIO_PIN_9;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

    // 13. Set SWRST bit in I2Cx_CR1 register.
    SET_BIT(hi2c->Instance->CR1, I2C_CR1_SWRST);
    asm("nop");

    /* 14. Clear SWRST bit in I2Cx_CR1 register. */
    CLEAR_BIT(hi2c->Instance->CR1, I2C_CR1_SWRST);
    asm("nop");

    /* 15. Enable the I2C peripheral by setting the PE bit in I2Cx_CR1 register */
    SET_BIT(hi2c->Instance->CR1, I2C_CR1_PE);
    asm("nop");

    // Call initialization function.
    HAL_I2C_Init(hi2c);
}

/*****************************Write Byte I2C*************************************/
static HAL_StatusTypeDef MCP23017_writeByte_i2c(uint16_t Addr,uint8_t Reg,uint8_t value)
{

	 HAL_StatusTypeDef status = HAL_OK;
     status = HAL_I2C_Mem_Write(&MCP23017_I2C, Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, &value, 1, 0x1000);
     if(status != HAL_OK)
    	 {
    	 MCP23017_Handler_Error();
    	 I2C_ClearBusyFlagErratum(&MCP23017_I2C, 1000);
    	 }
     return status;

}


/*****************************Read Byte I2C*************************************/
static HAL_StatusTypeDef MCP23017_ReadByte_i2c(uint16_t Addr,uint8_t Reg,uint8_t *data)
{
	HAL_StatusTypeDef status = HAL_OK;
    status = HAL_I2C_Mem_Read(&MCP23017_I2C, Addr, Reg, I2C_MEMADD_SIZE_8BIT, data, 1, 0x1000);
    if(status != HAL_OK)
    	{
    	MCP23017_Handler_Error();
    	I2C_ClearBusyFlagErratum(&MCP23017_I2C, 1000);
    	}
    return status;
}


/***********************************Init & Set Addres Device******************************/
void mcp23017_init(uint16_t addr)
{
	MX_I2C1_Init();
	MCP23017_GPIO.addr=addr << 1;
}

/********************************Function SET Direction PORT************************/
HAL_StatusTypeDef  mcp23017_iodir(uint8_t port, uint8_t iodir)
{
	MCP23017_GPIO.Ports[port].configIO=iodir;
	HAL_StatusTypeDef st=MCP23017_writeByte_i2c(MCP23017_GPIO.addr, REGISTER_IODIRA|port, MCP23017_GPIO.Ports[port].configIO);;
	return st;
}

/********************************Function  INPUT POLARITY************************/
HAL_StatusTypeDef mcp23017_ipol(uint8_t port, uint8_t ipol)
{

	uint8_t data[1] = {ipol};
	return MCP23017_writeByte_i2c(MCP23017_GPIO.addr, REGISTER_IPOLA|port, data[0]);
}

/********************************Function  SET Pull-up Resistors************************/
HAL_StatusTypeDef mcp23017_ggpu(uint8_t port, uint8_t pull)
{

	MCP23017_GPIO.Ports[port].configPull=pull;
	return MCP23017_writeByte_i2c(MCP23017_GPIO.addr, REGISTER_GPPUA|port, MCP23017_GPIO.Ports[port].configPull);

}

/*********************************READ SET PORT********************************************/
uint8_t mcp23017_read_gpio(uint8_t port)
{
	uint8_t data[1];
	HAL_StatusTypeDef status;
	status = MCP23017_ReadByte_i2c(MCP23017_GPIO.addr, REGISTER_GPIOA|port, data);

	if (status == HAL_OK)
		MCP23017_GPIO.Ports[port].state=data[0];

	return MCP23017_GPIO.Ports[port].state;
}

/*********************************Write SET PORT********************************************/
HAL_StatusTypeDef mcp23017_write_gpio(uint8_t port,uint8_t statePin)
{

	uint8_t data[1] = {statePin};
	return MCP23017_writeByte_i2c(MCP23017_GPIO.addr, REGISTER_GPIOA|port,data[0]);
}



