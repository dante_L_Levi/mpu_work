/*
 * BLDC_HW.c
 *
 *  Created on: Feb 28, 2020
 *      Author: AlexPirs
 */


#include "BLDC_HW.h"

TIM_ClockConfigTypeDef sClockSourceConfig = {0};
TIM_MasterConfigTypeDef sMasterConfig = {0};
TIM_OC_InitTypeDef sConfigOC = {0};
TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};


/*******************************INTERRUPT**********************************/

/**************************UPDATE EVENT IRQ3***********************************/
void TIM3_IRQHandler(void)
{
	TIM3->SR &= ~TIM_SR_UIF;





}

/**************************UPDATE EVENT IRQ3***********************************/
void TIM4_IRQHandler(void)
{
	TIM4->SR &= ~TIM_SR_UIF;

}

/****************************************************************************/





/******************************Init PWM BLDC **********************/
//100% -3600
void BLDC_PWM_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
		__HAL_RCC_GPIOB_CLK_ENABLE();
	    __HAL_RCC_GPIOA_CLK_ENABLE();
	    /**TIM1 GPIO Configuration
	    PB13     ------> TIM1_CH1N
	    PB14     ------> TIM1_CH2N
	    PB15     ------> TIM1_CH3N
	    PA8     ------> TIM1_CH1
	    PA9     ------> TIM1_CH2
	    PA10     ------> TIM1_CH3
	    */
	    GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
	    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10;
	    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);



	  htim1.Instance = TIM1;
	  htim1.Init.Prescaler = PWM_PRESCALER;
	  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
	  htim1.Init.Period = PWM_PERIOD_MAX;
	  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	  htim1.Init.RepetitionCounter = 0;
	  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  sConfigOC.OCMode = TIM_OCMODE_PWM1;
	  sConfigOC.Pulse = 0;
	  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	  sConfigOC.OCNPolarity = TIM_OCPOLARITY_HIGH;
	  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
	  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
	  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
	  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
	  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
	  sBreakDeadTimeConfig.DeadTime = DEAD_TIME;
	  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
	  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
	  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
	  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  HAL_TIM_MspPostInit(&htim1);

}

/******************************Start PWM**********************/
void BLDC_PWM_Start(void)
{
	HAL_TIM_PWM_Start(&BLDC_pwm_Hundler, CHANNEL_U);
	HAL_TIM_PWM_Start(&BLDC_pwm_Hundler, CHANNEL_V);
	HAL_TIM_PWM_Start(&BLDC_pwm_Hundler, CHANNEL_W);

	HAL_TIMEx_PWMN_Start(&BLDC_pwm_Hundler, CHANNEL_U);
	HAL_TIMEx_PWMN_Start(&BLDC_pwm_Hundler, CHANNEL_V);
	HAL_TIMEx_PWMN_Start(&BLDC_pwm_Hundler, CHANNEL_W);

}


/******************************Stop PWM**********************/
void BLDC_PWM_Stop(void)
{
	HAL_TIM_PWM_Stop(&BLDC_pwm_Hundler, CHANNEL_U);
	HAL_TIM_PWM_Stop(&BLDC_pwm_Hundler, CHANNEL_V);
	HAL_TIM_PWM_Stop(&BLDC_pwm_Hundler, CHANNEL_W);


	HAL_TIMEx_PWMN_Stop(&BLDC_pwm_Hundler, CHANNEL_U);
	HAL_TIMEx_PWMN_Stop(&BLDC_pwm_Hundler, CHANNEL_V);
	HAL_TIMEx_PWMN_Stop(&BLDC_pwm_Hundler, CHANNEL_W);

}


/**************************Set PWM Duty****************************/
void BLDC_Set_PWM_Duty(uint16_t phU,uint16_t phV,uint16_t phW)
{
	PWM_SPEED_HUNDLER->CCR1=phU;
	PWM_SPEED_HUNDLER->CCR2=phV;
	PWM_SPEED_HUNDLER->CCR3=phW;
}

/*************************Init Tim3,Tim4*************************/
void BLDC_SpeedTimerInit(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM3EN;
	TIM3->PSC=BLDC_SPEED_TIMER_PRESCALER;
	TIM3->ARR=BLDC_SPEED_TIMER_PERIOD;
	TIM3->DIER |= TIM_DIER_UIE;
	TIM3->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM3_IRQn);
	//---------------------------------------------
	RCC->APB1ENR|=RCC_APB1ENR_TIM3EN;
	TIM4->PSC=BLDC_SPEED_TIMER_PRESCALER;
	TIM4->ARR=BLDC_SPEED_TIMER_PERIOD;
	TIM4->DIER |= TIM_DIER_UIE;
	TIM4->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM4_IRQn);
}




