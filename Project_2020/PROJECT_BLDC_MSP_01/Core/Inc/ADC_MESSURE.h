/*
 * ADC_MESSURE.h
 *
 *  Created on: Apr 26, 2020
 *      Author: AlexPirs
 */

#ifndef INC_ADC_MESSURE_H_
#define INC_ADC_MESSURE_H_

#include "main.h"
#include "stm32f103xb.h"

#define ADC_COUNT_CH	7

#define ADC_VOLTAGE_U
#define ADC_VOLTAGE_V
#define ADC_VOLTAGE_W

#define ADC_CURRENT_U
#define ADC_CURRENT_V
#define ADC_CURRENT_W

#define ADC_CURR_VOLT_POWER



/************************************ADC Init********************************/
void ADC_Init(void);



#endif /* INC_ADC_MESSURE_H_ */
