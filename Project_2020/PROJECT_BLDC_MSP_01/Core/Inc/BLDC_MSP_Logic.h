/*
 * BLDC_MSP_Logic.h
 *
 *  Created on: Feb 22, 2020
 *      Author: AlexPirs
 */

#ifndef INC_BLDC_MSP_LOGIC_H_
#define INC_BLDC_MSP_LOGIC_H_



#include "main.h"



#define ID_BLDC_MSP01		0x2F

#define LED_STATUS_PORT		GPIOB
#define LED_PIN				GPIO_PIN_12




//Work TIM LED update MPU status
#define MAIN_TIM_INDICATE_OK			htim3
#define ADC_N_CH						7
#define ADC_CURRENT_COUNT				3
#define ADC_VOLTAGE_COUNT				3
#define ADC_SELECTIVITY					1
typedef enum
{
	NORMAL_WORK,
	CONTROL_PC,
	WORK_NULL

}STATUS_WORK_PROG;


typedef struct
{
		unsigned gpio_input:3;

}GPIO_OUT_INPUT_STATUS;

typedef struct
{

	int16_t ADC_CH[ADC_N_CH];
	int16_t temperature;
}ADC_INPUT_Def;

typedef struct
{
	uint8_t statusPWM:3;//Enable Work
	uint8_t pwmDuty[3];//Duty in %		0-100%

}PWM_STATUS;



typedef struct
{
	uint8_t state;//1- high Level,0-Low Level
	uint8_t configPull;//1-pull up, 0-no pull
	uint8_t configIO;//set GPIO input 0, output 1

}MCP23017_PORTS;


typedef struct
{
	uint16_t					addr;
	MCP23017_PORTS				Ports[2];

}MCP23017_hw;




/*************************MAIN INIT DATA******************************/
void Main_BLDC_MSPrev01_Init(void);
/************************Function Set GPIO OUT********************************/
void OUT_set(uint16_t pinName,bool status);

/*******************Test Function Indicate***********************/
void Indicate_Led(uint8_t status);





#endif /* INC_BLDC_MSP_LOGIC_H_ */
