/*
 * BLDC_HW.h
 *
 *  Created on: Feb 28, 2020
 *      Author: AlexPirs
 */

#ifndef INC_BLDC_HW_H_
#define INC_BLDC_HW_H_

#include "main.h"



#define	PWM_PRESCALER			1
#define MCU_CLOCK				72000000
#define	PWM_FREQ				20000
#define PWM_PERIOD_MAX			((MCU_CLOCK/PWM_PRESCALER)/PWM_FREQ)			//100% Duty

#define DEAD_TIME				240

#define BLDC_pwm_Hundler		htim1
#define CHANNEL_U				TIM_CHANNEL_1
#define CHANNEL_V				TIM_CHANNEL_2
#define CHANNEL_W				TIM_CHANNEL_3
#define PWM_SPEED_HUNDLER		TIM1
#define BLDC_SPEED_TIMER_PRESCALER					36
#define BLDC_SPEED_TIMER_PERIOD						0xFFFF
/******************************Init PWM BLDC **********************/
void BLDC_PWM_Init(void);
/******************************Start PWM**********************/
void BLDC_PWM_Start(void);
/******************************Stop PWM**********************/
void BLDC_PWM_Stop(void);
/**************************Set PWM Duty****************************/
void BLDC_Set_PWM_Duty(uint16_t phU,uint16_t phV,uint16_t phW);
/*************************Init Tim3,Tim4*************************/
void BLDC_SpeedTimerInit(void);


#endif /* INC_BLDC_HW_H_ */
