/*
 * Compare_Voltage_Phase_State.h
 *
 *  Created on: Apr 26, 2020
 *      Author: AlexPirs
 */

#ifndef INC_COMPARE_VOLTAGE_PHASE_STATE_H_
#define INC_COMPARE_VOLTAGE_PHASE_STATE_H_


#include "main.h"




#define COMPARE_GPIO_PIN_U	GPIO_PIN_13
#define COMPARE_GPIO_PIN_V	GPIO_PIN_14
#define COMPARE_GPIO_PIN_W	GPIO_PIN_15
#define COMPARE_GPIO_PORT		GPIOC


/*******************Function Read Input EXT***********************/
void Read_Input_EXT(void);


#endif /* INC_COMPARE_VOLTAGE_PHASE_STATE_H_ */
