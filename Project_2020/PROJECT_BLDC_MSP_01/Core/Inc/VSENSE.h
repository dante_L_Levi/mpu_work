/*
 * VSENSE.h
 *
 *  Created on: Feb 25, 2020
 *      Author: AlexPirs
 */

#ifndef INC_VSENSE_H_
#define INC_VSENSE_H_


#include "main.h"

#define POWER_ICURRENT_INDEX		0
#define PHASE_U_ICURRENT_INDEX		1
#define PHASE_V_ICURRENT_INDEX		2
#define PHASE_W_ICURRENT_INDEX		3


#define Temperature_Sensor_Index	7


#define	K_sense		(float)(3.3/4096.0)

#define K_Amp		1


void ADC_Init_Vsense(void);
void ADC_Start(void);
void ADC_Read(void);


#endif /* INC_VSENSE_H_ */
