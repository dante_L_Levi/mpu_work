/*
 * RS485_prot.h
 *
 *  Created on: 22 ����. 2020 �.
 *      Author: AlexPirs
 */

#ifndef INC_RS485_PROT_H_
#define INC_RS485_PROT_H_

#include "main.h"

#define SIZEBUFF_PAYLOAD        12

#pragma pack(push, 1)
typedef struct
{
    uint8_t Id;
    uint8_t command;
    uint8_t PayLoad[SIZEBUFF_PAYLOAD];
    uint16_t CRC16;

}RS485Prot;

typedef enum
{
    IS_MUTED = 0xFF,
    IS_START_WORK=0x2F,
    START_PACKET = 0x7F,
    CONTINUE_PACKET = 0x3F,
    STOP_PACKET = 0x1F,
    OK_WORK = 0x0F,
    ERROR_RS485 = 0x1C,
    CRC_ERROR = 0x1A,
    ECHO_CMD = 0x01,
    IS_GOTOBOOT=0x0C,
    RESPONSE_MPU_Data=0x2C,
    RESPONSE_to_REQUEST=0x02,
    REQUEST_PC = 0x03

}CommandDef;

#define BUFFER_SIZE     sizeof(RS485Prot)

#define RS485_TRANSMIT      GPIOPinWrite(GPIO_PORTB_BASE,GPIO_PIN_2,0xFF)
#define RS485_RECEIVE       GPIOPinWrite(GPIO_PORTB_BASE,GPIO_PIN_2,0x00)


/********************Function Init RS485**********************/
void RS485_init(uint32_t BaudRate);
/********************Function CALLBACK Interrupt RS485 recciver********************/
void RS485_CALLBACK(void);
/*****************Function Set Id*********************/
void RS485_setId(uint8_t id);
/***************Check Id RS485 Device************************/
uint8_t RS485_checkId(void);
/*********************Function Get Id Rs-485***************************/
uint8_t RS485_getId(void);
/**********************Function Transmit Data ******************/
void RS485_setTxMessage(RS485Prot *p);
/**********************Function Transmit Data ******************/
void RS485_Transmit(uint8_t id,CommandDef dataCmd,uint8_t *dataPay,uint8_t length);
/**********************Function Transmit Data ******************/
void RS485_Transmit_PAYLOAD(CommandDef dataCmd,uint8_t *dataPay,uint8_t length);


#endif /* INC_RS485_PROT_H_ */
