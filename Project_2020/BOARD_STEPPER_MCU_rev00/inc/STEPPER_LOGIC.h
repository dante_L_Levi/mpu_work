/*
 * STEPPER_LOGIC.h
 *
 *  Created on: 29 ����. 2020 �.
 *      Author: AlexPirs
 */

#ifndef INC_STEPPER_LOGIC_H_
#define INC_STEPPER_LOGIC_H_


#include "main.h"


typedef struct
{
    int16_t Sense_POWER;
    int16_t Sense_A;
    int16_t Sense_B;
    int16_t Sense_C;
    int16_t Sense_D;
    int16_t Sense_E;
    int16_t Sense_F;
}ADC_CurrentSense;



#endif /* INC_STEPPER_LOGIC_H_ */
