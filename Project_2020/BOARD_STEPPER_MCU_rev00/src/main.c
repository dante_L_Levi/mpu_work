/*
 * main.c
 *
 *  Created on: 27 ���. 2020 �.
 *      Author: AlexPirs
 */


#include "main.h"

RS485Prot RS485Data;
uint32_t MCU_CLOCK;

static void RCC_Init(void)
{
    // Set the clocking to run directly from the PLL at 20 MHz.
        // The following code:
        // -sets the system clock divider to 10 (200 MHz PLL divide by 10 = 20 MHz)
        // -sets the system clock to use the PLL
        // -uses the main oscillator
        // -configures for use of 16 MHz crystal/oscillator input
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                           SYSCTL_XTAL_16MHZ);
}


void main(void)
{
    RCC_Init();
    MCU_CLOCK=SysCtlClockGet();
    while(1)
    {

    }
}
