/*
 * ADC_SENSE.c
 *
 *  Created on: 22 ����. 2020 �.
 *      Author: AlexPirs
 */

#include "ADC_SENSE.h"

extern uint32_t MCU_CLOCK;
#define UPDATE_FRQ      200

extern ADC_CurrentSense Adc_data_sense;


/**************************ADC Interrupt hundler*******************/
void ADC_Interrupt(void)
{
    unsigned int state = ADCIntStatus(ADC1_BASE, 0, true);
    ADCIntClear(ADC1_BASE, 0);
/*
        ADCSequenceDataGet(ADC1_BASE, 0, (uint32_t *)&results.ain3);
        ADCSequenceDataGet(ADC1_BASE, 0, (uint32_t *)&results.phase_a);
        ADCSequenceDataGet(ADC1_BASE, 0, (uint32_t *)&results.phase_b);
        ADCSequenceDataGet(ADC1_BASE, 0, (uint32_t *)&results.phase_c);
        ADCSequenceDataGet(ADC1_BASE, 0, (uint32_t *)&results.bus);
        ADCSequenceDataGet(ADC1_BASE, 0, (uint32_t *)&results.key1_current);
        ADCSequenceDataGet(ADC1_BASE, 0, (uint32_t *)&results.key2_current);
      */
}

/************************ADC Init*********************************/
void ADC_Init_Sensor_Current(void)
{
    SysCtlPeripheralReset(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD))
    {
    }
    GPIODirModeSet(GPIO_PORTD_BASE,GPIO_PIN_1|GPIO_PIN_2 | GPIO_PIN_3,GPIO_DIR_MODE_HW);
    GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_1|GPIO_PIN_2 | GPIO_PIN_3);
    //---------------------------------------------------------------------------------------
    SysCtlPeripheralReset(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE))
    {
    }
    GPIODirModeSet(GPIO_PORTE_BASE,GPIO_PIN_0|GPIO_PIN_1 | GPIO_PIN_2,GPIO_DIR_MODE_HW);
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1 | GPIO_PIN_2);
    //--------------------------------------------------------------------------------------
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);//init peripheral ADC
    ADCHardwareOversampleConfigure(ADC0_BASE, 64);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0))
    {
    }
    ADCSequenceConfigure(ADC0_BASE, 0, ADC_TRIGGER_TIMER, 0);
    //set up sequence
    ADCSequenceStepConfigure(ADC1_BASE, 0, 0, ADC_CTL_CH0);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 1, ADC_CTL_CH1);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 2, ADC_CTL_CH2);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 3, ADC_CTL_CH3);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 4, ADC_CTL_CH4);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 5, ADC_CTL_CH5);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 6, ADC_CTL_CH6);
    //interrupt settings
    ADCIntRegister(ADC1_BASE, 0, ADC_Interrupt);
    ADCIntEnable(ADC1_BASE, 0);
    ADCSequenceEnable(ADC1_BASE, 0);


}
/*********************Function Trigger ADC***********************/
void timerInit(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER1_BASE, TIMER_A, (MCU_CLOCK / UPDATE_FRQ));
    TimerControlTrigger(TIMER1_BASE, TIMER_A, true);
    TimerEnable(TIMER1_BASE, TIMER_A);
}
