/*
 * RS485_prot.c
 *
 *  Created on: 22 ����. 2020 �.
 *      Author: AlexPirs
 */

#include "RS485_prot.h"


#define RS485_Obj       UART1_BASE
extern RS485Prot RS485Data;
extern uint32_t MCU_CLOCK;

static uint8_t RS485_DEV_ID=0x0B;
static uint8_t isMuted = 0;
static  uint8_t RX_BUFF[BUFFER_SIZE]={0};
static  uint8_t TX_BUFF[BUFFER_SIZE]={0};


/**************************Clear PAYLOAD***********************/
static void RS485_ClearBuffer(void);

/********************************Function Check Summ Recieve Buffer***************/
static uint8_t CheckCRC16_RECIEVE(uint8_t *Data,uint8_t Length);
/**********************Calculate CRC16*****************************/
static uint16_t TxCalculateCRC16(uint8_t *Data,uint8_t Length);
/********************Helper for reccieve Data Serial Port**************/
void RS485_Reciever_Helper(void);

/*****************************Function CALLBACK Interrupt RS485 recciver********************/
void RS485_CALLBACK(void)
{
    uint32_t intStatus;
    intStatus=UARTIntStatus(RS485_Obj,true);
    RS485_Reciever_Helper();
    UARTIntClear(RS485_Obj,intStatus);
}


/********************Helper for reccieve Data Serial Port**************/
void RS485_Reciever_Helper(void)
{
    uint8_t i=0;
    for(i=0;i<BUFFER_SIZE;i++)
    {
        RX_BUFF[i]=UARTCharGetNonBlocking(RS485_Obj);
    }

    if(RX_BUFF[0]==RS485Data.Id)
    {
        bool st=CheckCRC16_RECIEVE(RX_BUFF,BUFFER_SIZE);
        if(st)
        {
            memcpy(&RS485Data, RX_BUFF, BUFFER_SIZE);
            switch(RS485Data.command)
            {
                case (uint8_t)IS_START_WORK:
                {
                    uint8_t i=0;
                    for(i=0;i<SIZEBUFF_PAYLOAD;i++)
                    {
                        RS485Data.PayLoad[i]=0xFF;
                    }
                    RS485_Transmit_PAYLOAD(IS_START_WORK,RS485Data.PayLoad,SIZEBUFF_PAYLOAD);
                    break;
                }
                case (uint8_t)REQUEST_PC:
                {
                    //Function edit Logic Status
                    break;
                }
                case (uint8_t)ECHO_CMD:
                {
                    UARTCharPut(RS485_Obj,RS485Data.Id);
                    break;
                }
                case (uint8_t)CONTINUE_PACKET:
                {
                    //function packet Frame Continue
                    break;
                }
                case (uint8_t)STOP_PACKET:
                {
                    //Stop packet end Recieve Command
                    break;
                }
                case (uint8_t)IS_GOTOBOOT:
                {
                    //Go toBOOT
                    break;
                }

            }
        }
        else
        {
            return;
        }
    }
}

/********************Function Init RS485**********************/
void RS485_init(uint32_t BaudRate)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART1))
    {
    }

    GPIOPinConfigure(GPIO_PB0_U1RX);
    GPIOPinConfigure(GPIO_PB1_U1TX);
    GPIOPinTypeUART(GPIO_PORTB_BASE,GPIO_PIN_0|GPIO_PIN_1);

    UARTClockSourceSet(RS485_Obj,UART_CLOCK_SYSTEM);
    UARTConfigSetExpClk(RS485_Obj, SysCtlClockGet(), BaudRate,
                               (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                UART_CONFIG_PAR_NONE));
    UARTIntRegister(RS485_Obj, RS485_CALLBACK);
    IntEnable(INT_UART1);
    UARTIntEnable(RS485_Obj, UART_INT_RX | UART_INT_RT);
    IntMasterEnable();

}


/***************Check Id RS485 Device************************/
uint8_t RS485_checkId(void)
{
    if(RX_BUFF[0]==RS485_DEV_ID)
        {
            return 1;
        }
        else
        {
            return 0;
        }
}



/*****************Function Set Id*********************/
void RS485_setId(uint8_t id)
{
    RS485_DEV_ID = id;
    RS485Data.Id=id;

}



/*********************Function Get Id Rs-485***************************/
uint8_t RS485_getId(void)
{
    return RS485_DEV_ID;
}


/**********************Calculate CRC16*****************************/
static uint16_t TxCalculateCRC16(uint8_t *Data,uint8_t Length)
{
    uint8_t j;
            uint16_t reg_crc = 0xFFFF;
            while (Length--)
            {
              reg_crc^= *Data++;
              for(j=0;j<8;j++)
               {
                 if(reg_crc & 0x01)
                  {
                    reg_crc = (reg_crc >> 1) ^ 0xA001;
                  }
                else
                  {
                    reg_crc = reg_crc >> 1;
                  }
               }
            }
            return reg_crc;
}

/********************************Function Check Summ Recieve Buffer***************/
static uint8_t CheckCRC16_RECIEVE(uint8_t *Data,uint8_t Length)
{
    uint16_t crc_buf=0;
        crc_buf=(Data[Length-1]<<8)|Data[Length-2];

        uint16_t crc = 0xFFFF;
        uint8_t pos;
        for(pos=0;pos<Length-2;pos++)
        {
            crc^=(uint16_t)Data[pos];
        }
        uint8_t i;
        for (i = 8; i != 0; i--)
        {
            if ((crc & 0x0001) != 0)
                            {
                                crc >>= 1;
                                crc ^= 0xA001;
                            }
                            else
                            {
                                crc >>= 1;
                            }
        }
        if(crc==crc_buf)
        {
            return 1;
        }
        else
        {
            return 0;
        }
}

/**********************Function Transmit Data ******************/
void RS485_setTxMessage(RS485Prot *p)
{
    if(isMuted)
        {
                return;
        }
    else
    {
        RS485_TRANSMIT;
        memcpy((void *)TX_BUFF, p, BUFFER_SIZE);
        uint16_t tempCRC=TxCalculateCRC16((uint8_t *)TX_BUFF,(uint8_t)BUFFER_SIZE-2);
        p->CRC16=tempCRC;
        TX_BUFF[BUFFER_SIZE-2]=(uint8_t)tempCRC;
        TX_BUFF[BUFFER_SIZE-1]=(uint8_t)(tempCRC>>8);
        uint8_t i=0;
        for(i=0;i<BUFFER_SIZE;i++)
        {
            UARTCharPut(RS485_Obj,TX_BUFF[i]);
        }
        RS485_ClearBuffer();
        RS485_RECEIVE;
    }

}

/**********************Function Transmit Data ******************/
void RS485_Transmit(uint8_t id,CommandDef dataCmd,uint8_t *dataPay,uint8_t length)
{
    if(length>SIZEBUFF_PAYLOAD)
                return;
        RS485Data.Id=id;
        RS485Data.command=(uint8_t)dataCmd;
        memcpy((void *)(RS485Data.PayLoad),dataPay,SIZEBUFF_PAYLOAD);
        RS485_setTxMessage(&RS485Data);
}


/**********************Function Transmit Data ******************/
void RS485_Transmit_PAYLOAD(CommandDef dataCmd,uint8_t *dataPay,uint8_t length)
{
    if(length>SIZEBUFF_PAYLOAD)
                    return;
    RS485Data.command=(uint8_t)dataCmd;
    memcpy((void *)(RS485Data.PayLoad),dataPay,SIZEBUFF_PAYLOAD);
    RS485_setTxMessage(&RS485Data);
}

/**************************Clear PAYLOAD***********************/
static void RS485_ClearBuffer(void)
{
    uint8_t i;
    for(i=0;i<BUFFER_SIZE;i++)
    {
        TX_BUFF[i]=0;

    }

    uint8_t j;
    for(j=0;j<SIZEBUFF_PAYLOAD;j++)
    {
        RS485Data.PayLoad[j]=0;
    }

}

