/*
 * RGB_Logic.h
 *
 *  Created on: Mar 6, 2020
 *      Author: AlexPirs
 */

#ifndef INC_RGB_LOGIC_H_
#define INC_RGB_LOGIC_H_


#include "main.h"

#define SW_PORT			GPIOA
#define SW_PREV			GPIO_PIN_0
#define SW_NEXT			GPIO_PIN_2
#define SW_MODE			GPIO_PIN_1

#define LED_PORT			GPIOC
#define LED_pin				GPIO_PIN_13


typedef struct
{

	uint8_t Menu;

}Button_State;

/********************Start PWM****************************/
void Start_PWM_RGB(void);
/**************************hundler TIM1************************/
void Tim_Hundler(void);
/********************Stop PWM****************************/
void Stop_PWM_RGB(void);
/*******************Set Duty PWM***************************/
void Set_Duty(uint16_t R,uint16_t G,uint16_t B);
/***********************Function Led Indication*********************/
void Function_LEDIndicate(uint8_t ld);
/*********************Main Init*******************/
void Init_RGB(void);
/*******************Event Switch Push********************/
void Hundler_Switch(void);

#endif /* INC_RGB_LOGIC_H_ */
