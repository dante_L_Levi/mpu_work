/*
 * RGB_Logic.c
 *
 *  Created on: Mar 6, 2020
 *      Author: AlexPirs
 */


#include "RGB_Logic.h"

uint8_t count_Led=1;
uint16_t dt=10;
Button_State btn_state;
/**************************hundler TIM1************************/
void Tim_Hundler(void)
{
	if(count_Led>250)
	{
		count_Led=1;
	}
	count_Led++;
	Function_LEDIndicate(count_Led);
	Hundler_Switch();

}

/*******************Event Switch Push********************/
void Hundler_Switch(void)
{
	if(HAL_GPIO_ReadPin(SW_PORT, SW_PREV)==GPIO_PIN_RESET)
	{
		if(btn_state.Menu%2==0)
		{
			if(dt>11)
			dt-=10;

			Set_Duty(dt,dt,dt);
		}
	}
	if(HAL_GPIO_ReadPin(SW_PORT, SW_NEXT)==GPIO_PIN_RESET)
	{
		if(btn_state.Menu%2==0)
		{
			if(dt<3580)
			{
				dt+=10;
				Set_Duty(dt,dt,dt);
			}
		}
	}
	if(HAL_GPIO_ReadPin(SW_PORT, SW_MODE)==GPIO_PIN_RESET)
	{
		btn_state.Menu++;
	}

	if(btn_state.Menu%2==0)
	{
		Start_PWM_RGB();
		Set_Duty(dt,dt,dt);
	}
	else if(btn_state.Menu%2!=0)
	{
		Stop_PWM_RGB();
		dt=10;
	}



}

/***********************Function Led Indication*********************/
void Function_LEDIndicate(uint8_t ld)
{

	ld%2==0?(HAL_GPIO_WritePin(LED_PORT, LED_pin, GPIO_PIN_RESET)):
					(HAL_GPIO_WritePin(LED_PORT, LED_pin, GPIO_PIN_SET));
}

/********************Start PWM****************************/
void Start_PWM_RGB(void)
{
	HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_4);

}


/********************Stop PWM****************************/
void Stop_PWM_RGB(void)
{
	HAL_TIM_PWM_Stop_IT(&htim3, TIM_CHANNEL_2);
	HAL_TIM_PWM_Stop_IT(&htim3, TIM_CHANNEL_3);
	HAL_TIM_PWM_Stop_IT(&htim3, TIM_CHANNEL_4);
}

/*******************Set Duty PWM***************************/
void Set_Duty(uint16_t R,uint16_t G,uint16_t B)
{
	TIM3->CCR2=R;
	TIM3->CCR3=G;
	TIM3->CCR4=B;
}

/*********************Main Init*******************/
void Init_RGB(void)
{
	MX_GPIO_Init();
	MX_TIM1_Init();
	MX_TIM3_Init();
	btn_state.Menu=1;
	HAL_TIM_Base_Start_IT(&htim1);

}
