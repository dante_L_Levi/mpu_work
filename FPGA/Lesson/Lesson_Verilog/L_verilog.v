`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:13:53 11/08/2019 
// Design Name: 
// Module Name:    L_verilog 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

////////Logic Base//////////////////////

//module L_verilog
//( input [3:0] a,b, // ���������� ������� ��������� ������
//	output [3:0] y,	 // ���������� ��������� ���������� �����
//	output z
 //);
 
// assign y=a&b;
 //assign z=&y;


//endmodule

/////////////////////////////////////////

////////////////////Work Varible/////////////////////////////
//module L_verilog
//(
//);
//wire a, b, c;
//assign {a, b, c} = 3'b010;



//wire [3:0] a; 	// ���������� ���������� 4-� ��������� ����,
					// �� ���������� �������/�������� �������� �����
//wire b, c;
//assign a = {1'b0, b, 1'b1, c}; 
//endmodule

////////////////////////////////////////////////////////////

///////////////////��������� ���������/////////////////////////////
//module relational_operators_1
//(input [3:0] a, b,
//output y);
//assign y = (a == b);
//endmodule
//////////////////////////////////////////////////////////////////

///////////////////������������� ����������� always//////////////
module arithmetic_operators_1
( input [7:0] a, b, c, d,
output reg [9:0] y1, y2);
always @(a or b or c or d)
begin
 y1 = a + b + c + d;
 y2 = (a + b) + (c + d);
end
endmodule
////////////////////////////////////////////////////////////////


/////////////////�������� ������ //////////////////////////////
module condit_stmts_l
( input [7:0] a, b,
input sel,
output reg [7:0] y);
always @(a or b or sel)
begin
 if (sel==1) y = b;
 else y = a;
 // ������������� ������ � ������� ���������� ���������
 // y = sel? b:a;
end
endmodule
///////////////////////////////////////////////////////////////


///////////////////////// �������������� ������ � ����� �����������/////////////////////////
module tbuf4
( input [3:0] a,
input enable,
output reg [3:0] y);
assign y = enable? a: 4'bZ;
endmodule////////////////////////////////////////////////////////////////////////////////////////////////




