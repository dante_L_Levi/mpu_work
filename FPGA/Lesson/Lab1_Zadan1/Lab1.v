`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:29:32 11/09/2019 
// Design Name: 
// Module Name:    Lab1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Lab1(
	 a,
	 b,
	 c,
	 Y
    );
	input a;
	input b;
	input c;
	output Y;
	
	assign Y=(a&(~b))|(b&c);


endmodule
