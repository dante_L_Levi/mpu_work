
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name Lab1_2 -dir "D:/Repository/MPU/FPGA/Lesson/Lab1_2/planAhead_run_1" -part xc3s250etq144-4
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "Lab1_2.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {Lab1_2.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top Lab1_2 $srcset
add_files [list {Lab1_2.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc3s250etq144-4
