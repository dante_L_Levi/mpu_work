`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:14:50 11/09/2019 
// Design Name: 
// Module Name:    Lab1_2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Lab1_2(X,Y,Z,F);
   
input X;
input Y;
input Z;
output F;

assign F=((~X)|(~Y)|(~Z))&(X|Y|Z)&(X|(~Y)|Z);

endmodule
