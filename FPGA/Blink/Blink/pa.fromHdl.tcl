
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name Blink -dir "D:/Repository/MPU/FPGA/Blink/Blink/planAhead_run_2" -part xc3s250etq144-4
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "Blink.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {Blink.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top Blink $srcset
add_files [list {Blink.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc3s250etq144-4
