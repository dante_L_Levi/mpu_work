`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:30:22 11/04/2019 
// Design Name: 
// Module Name:    Blink 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Blink(
	clk,	// clock signal
	ledpin,	// LED pin
	gpio1,
	gpio2,
	gpio3
    );
	
	// inputs and outputs
	input clk;
	output ledpin;
	output gpio1;
	output gpio2;
	output gpio3;
	
	reg ledpin = 0;
	
		// internal variable
	reg [25:0] counter = 50_000_000;	// 26 bit variable
	assign {gpio1, gpio2, gpio3} = 3'b111; 
	
	always @(posedge clk)
		if (counter == 0) begin			// at 1 second
		counter <= 50_000_000;		// reset counter
		ledpin <= !ledpin;		// invert ledpin
		end else begin
		counter <= counter - 1;		// decrease
	end

endmodule
