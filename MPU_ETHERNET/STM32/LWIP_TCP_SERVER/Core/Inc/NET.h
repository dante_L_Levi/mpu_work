/*
 * NET.h
 *
 *  Created on: Feb 2, 2020
 *      Author: AlexPirs
 */

#ifndef NET_H_
#define NET_H_

#include "stm32f4xx_hal.h"
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "lwip.h"
#include "lwip/tcp.h"



#define MCU_SERVER


enum server_states
{
 SERVER_ES_NONE = 0,
 SERVER_ES_ACCEPTED,
 SERVER_ES_RECEIVED,
 SERVER_ES_CLOSING
};

enum client_states
{
  ES_NOT_CONNECTED = 0,
  ES_CONNECTED,
  ES_RECEIVED,
  ES_CLOSING,
};

struct client_struct
{
  enum client_states state; /* connection status */
  struct tcp_pcb *pcb;          /* pointer on the current tcp_pcb */
  struct pbuf *p_tx;            /* pointer on pbuf to be transmitted */
};

struct server_struct
{
  u8_t state; /* current connection state */
  u8_t retries;
  struct tcp_pcb *pcb; /* pointer on the current tcp_pcb */
  struct pbuf *p; /* pointer on the received/to be transmitted pbuf */
};



void UART6_RxCpltCallback(void);
//-----------------------------------------------
typedef struct USART_prop{
  uint8_t usart_buf[26];
  uint8_t usart_cnt;
  uint8_t is_tcp_connect;//статус попытки создать соединение TCP с сервером
  uint8_t is_text;//статус попытки передать текст серверу
} USART_prop_ptr;
//-----------------------------------------------



/****************************Init TCP SERVER****************************/
void tcp_server_init(void);

/**********************Init Debug UART LWIP******************/
void net_ini(void);




#endif /* NET_H_ */
