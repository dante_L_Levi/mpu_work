/*
 * NET.c
 *
 *  Created on: Feb 2, 2020
 *      Author: AlexPirs
 */

#include "NET.h"

#ifdef MCU_CLIENT

//-----------------------------------------------
uint8_t ipaddr_dest[4];
uint16_t port_dest;
extern UART_HandleTypeDef huart6;
USART_prop_ptr usartprop;
char str[30];
char str1[100];
u8_t data[100];
struct tcp_pcb *client_pcb;
__IO uint32_t message_count=0;
//-------------------------------------------------------


//-------------------------------------------------------


struct client_struct *cs;
//-----------------------------------------------

static err_t tcp_client_connected(void *arg, struct tcp_pcb *tpcb, err_t err);
static void tcp_client_connection_close(struct tcp_pcb *tpcb, struct client_struct * es);
static err_t tcp_client_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
static void tcp_client_send(struct tcp_pcb *tpcb, struct client_struct * es);
static err_t tcp_client_sent(void *arg, struct tcp_pcb *tpcb, u16_t len);
static err_t tcp_client_poll(void *arg, struct tcp_pcb *tpcb);
//-----------------------------------------------

/************************Connect Server for TCP***************************/
void tcp_client_connect(void)
{
	ip_addr_t DestIPaddr;
	client_pcb = tcp_new();
	if (client_pcb != NULL)
	{
	    IP4_ADDR( &DestIPaddr, ipaddr_dest[0], ipaddr_dest[1], ipaddr_dest[2], ipaddr_dest[3]);
	    tcp_connect(client_pcb,&DestIPaddr,port_dest,tcp_client_connected);
	    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);
	}
}
//-----------------------------------------------


/***************CALLBACK Function Add when create Conection in Server**************/
static err_t tcp_client_connected(void *arg, struct tcp_pcb *tpcb, err_t err)
{
	struct client_struct *es = NULL;
	if (err == ERR_OK)
	{
		es = (struct client_struct *)mem_malloc(sizeof(struct client_struct));
	    if (es != NULL)
	    {
	    	es->state = ES_CONNECTED;
	    	es->pcb = tpcb;
	    	es->p_tx = pbuf_alloc(PBUF_TRANSPORT, strlen((char*)data) , PBUF_POOL);
	        if (es->p_tx)
	        {
	            /* copy data to pbuf */
	            pbuf_take(es->p_tx, (char*)data, strlen((char*)data));
	            /* pass newly allocated es structure as argument to tpcb */
	            tcp_arg(tpcb, es);
	            /* initialize LwIP tcp_recv callback function */
	            tcp_recv(tpcb, tcp_client_recv);
	            /* initialize LwIP tcp_sent callback function */
	            tcp_sent(tpcb, tcp_client_sent);
	            /* initialize LwIP tcp_poll callback function */
	            tcp_poll(tpcb, tcp_client_poll, 1);
	            /* send data */
	            tcp_client_send(tpcb,es);
	            return ERR_OK;
	        }
	    }
	    else
	    {
	      /* close connection */
	      tcp_client_connection_close(tpcb, es);
	      /* return memory allocation error */
	      return ERR_MEM;
	    }
	}
	else
	{
	/* close connection */
		tcp_client_connection_close(tpcb, es);
	}
	return err;
}
//----------------------------------------------------------

/********************************Close Connection**************************************************/
static void tcp_client_connection_close(struct tcp_pcb *tpcb, struct client_struct * es)
{
	/* remove callbacks */
	tcp_recv(tpcb, NULL);
	tcp_sent(tpcb, NULL);
	tcp_poll(tpcb, NULL,0);
	if (es != NULL)
	{
		mem_free(es);
	}

	/* close tcp connection */
	tcp_close(tpcb);
}
//----------------------------------------------------------
static err_t tcp_client_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err)
{
	struct client_struct *es;
	err_t ret_err;
	es = (struct client_struct *)arg;
	if (p == NULL)
	{
		es->state = ES_CLOSING;
	    if(es->p_tx == NULL)
	    {
	       tcp_client_connection_close(tpcb, es);
	    }
	    ret_err = ERR_OK;
	}
	else if(err != ERR_OK)
	{
		if (p != NULL)
		{
			pbuf_free(p);
		}
		ret_err = err;
	}
	else if(es->state == ES_CONNECTED)
	{
		message_count++;
		tcp_recved(tpcb, p->tot_len);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);
		es->p_tx = p;
		strncpy(str1,es->p_tx->payload,es->p_tx->len);
		str1[es->p_tx->len] = '\0';
		HAL_UART_Transmit(&huart6, (uint8_t*)str1,strlen(str1),0x1000);
		ret_err = ERR_OK;
	}
	else if (es->state == ES_RECEIVED)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
		ret_err = ERR_OK;
	}
	else
	{
		/* Acknowledge data reception */
		tcp_recved(tpcb, p->tot_len);
		/* free pbuf and do nothing */
		pbuf_free(p);
		ret_err = ERR_OK;
	}
	return ret_err;
}
//----------------------------------------------------------
/***************************CALLBACK FUNCTION Transmit Data*******************************/
static void tcp_client_send(struct tcp_pcb *tpcb, struct client_struct * es)
{
	struct pbuf *ptr;
	err_t wr_err = ERR_OK;
	while ((wr_err == ERR_OK) &&
			 (es->p_tx != NULL) &&
			 (es->p_tx->len <= tcp_sndbuf(tpcb)))
	{
		ptr = es->p_tx;
		/* enqueue data for transmission */
		wr_err = tcp_write(tpcb, ptr->payload, ptr->len, 1);
		if (wr_err == ERR_OK)
		{
			es->p_tx = ptr->next;
			if(es->p_tx != NULL)
			{
				pbuf_ref(es->p_tx);
			}
			pbuf_free(ptr);
		}
		else if(wr_err == ERR_MEM)
		{
			es->p_tx = ptr;
		}
		else
		{
		 /* other problem ?? */
		}
	}
}
//----------------------------------------------------------
/***********************CALLBACK AFTER TRANSMIT BUFFER SERVER****************************/
static err_t tcp_client_sent(void *arg, struct tcp_pcb *tpcb, u16_t len)
{
	struct client_struct *es;
	LWIP_UNUSED_ARG(len);
	es = (struct client_struct *)arg;
	if(es->p_tx != NULL)
	{
		tcp_client_send(tpcb, es);
	}
	return ERR_OK;
}
//----------------------------------------------------------
/********************CALLBACK Monitoring NEts*************************************/
static err_t tcp_client_poll(void *arg, struct tcp_pcb *tpcb)
{
	err_t ret_err;
	struct client_struct *es;
	es = (struct client_struct*)arg;
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
	if (es != NULL)
	{
	    if (es->p_tx != NULL)
	    {
	    }
	    else
	    {
	        if(es->state == ES_CLOSING)
	        {
	          tcp_client_connection_close(tpcb, es);
	        }
	    }
	    ret_err = ERR_OK;
	}
	else
	{
	    tcp_abort(tpcb);
	    ret_err = ERR_ABRT;
	}
	return ret_err;
}
//----------------------------------------------------------
void net_ini(void)
{
	  usartprop.usart_buf[0]=0;
	  usartprop.usart_cnt=0;
	  usartprop.is_tcp_connect=0;
	  usartprop.is_text=0;
}
//-----------------------------------------------
uint16_t port_extract(char* ip_str, uint8_t len)
{
  uint16_t port=0;
  int ch1=':';
  char *ss1;
  uint8_t offset = 0;
  ss1=strchr(ip_str,ch1);
  offset=ss1-ip_str+1;
  ip_str+=offset;
  port = atoi(ip_str);
  return port;
}
//--------------------------------------------------
void ip_extract(char* ip_str, uint8_t len, uint8_t* ipextp)
{
uint8_t offset = 0;
  uint8_t i;
  char ss2[5] = {0};
  char *ss1;
  int ch1 = '.';
  int ch2 = ':';
	for(i=0;i<3;i++)
	{
		ss1 = strchr(ip_str,ch1);
		offset = ss1-ip_str+1;
		strncpy(ss2,ip_str,offset);
		ss2[offset]=0;
		ipextp[i] = atoi(ss2);
		ip_str+=offset;
		len-=offset;
	}
	ss1=strchr(ip_str,ch2);
	if (ss1!=NULL)
	{
		offset=ss1-ip_str+1;
		strncpy(ss2,ip_str,offset);
		ss2[offset]=0;
		ipextp[3] = atoi(ss2);
		return;
	}
	strncpy(ss2,ip_str,len);
	ss2[len]=0;
	ipextp[3] = atoi(ss2);
}
//-----------------------------------------------
void net_cmd(char* buf_str)
{
	uint8_t ip[4];
	uint16_t port;
	if(usartprop.is_tcp_connect==1)//статус попытки создать соединение TCP с сервером
	{
		ip_extract(buf_str,usartprop.usart_cnt-1,ipaddr_dest);
		port_dest=port_extract(buf_str,usartprop.usart_cnt-1);
		usartprop.usart_cnt=0;
	    usartprop.is_tcp_connect=0;
	    tcp_client_connect();
		sprintf(str1,"%d.%d.%d.%d:%u\r\n", ipaddr_dest[0],ipaddr_dest[1],ipaddr_dest[2],ipaddr_dest[3],port_dest);
		HAL_UART_Transmit(&huart6,(uint8_t*)str1,strlen(str1),0x1000);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
	}
	if(usartprop.is_tcp_connect==2)//статус попытки разорвать соединение TCP с сервером
	{
		ip_extract(buf_str,usartprop.usart_cnt-1,ip);
		port=port_extract(buf_str,usartprop.usart_cnt-1);
		usartprop.usart_cnt=0;
	    usartprop.is_tcp_connect=0;
		//проверим что IP правильный
		if(!memcmp(ip,ipaddr_dest,4))
		{
			//также проверим, что порт тоже правильный
			if(port==port_dest)
			{
				/* close tcp connection */
				tcp_recv(client_pcb, NULL);
				tcp_sent(client_pcb, NULL);
				tcp_poll(client_pcb, NULL,0);
				tcp_close(client_pcb);
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);
			}
		}
	}
}
//-----------------------------------------------
void sendstring(char* buf_str)
{
	tcp_sent(client_pcb, tcp_client_sent);
	tcp_write(client_pcb, (void*)buf_str, strlen(buf_str), 1);
	tcp_output(client_pcb);
	usartprop.usart_cnt=0;
    usartprop.is_text=0;
}
//-----------------------------------------------
void string_parse(char* buf_str)
{
	HAL_UART_Transmit(&huart6, (uint8_t*)buf_str,strlen(buf_str),0x1000);
	//	если команда попытки соединения ("t:")
	if (strncmp(buf_str,"t:", 2) == 0)
	{
		usartprop.usart_cnt-=1;
		usartprop.is_tcp_connect=1;//статус попытки создать соединение TCP с сервером
		net_cmd(buf_str+2);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET);
	}
	//статус попытки разорвать соединение ("c:")
	else if (strncmp(buf_str,"c:", 2) == 0)
	{
		usartprop.usart_cnt-=1;
		usartprop.is_tcp_connect=2;//статус попытки разорвать соединение TCP с сервером
		net_cmd(buf_str+2);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
	}
	else
	{
		usartprop.is_text=1;
		sendstring(buf_str);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12|GPIO_PIN_13, GPIO_PIN_RESET);
	}
}
//-----------------------------------------------
void UART6_RxCpltCallback(void)
{
	uint8_t b;
	b = str[0];
	//если вдруг случайно превысим длину буфера
	if (usartprop.usart_cnt>25)
	{
		usartprop.usart_cnt=0;
		HAL_UART_Receive_IT(&huart6,(uint8_t*)str,1);
		return;
	}
	usartprop.usart_buf[usartprop.usart_cnt] = b;
	if(b==0x0A)
	{
		usartprop.usart_buf[usartprop.usart_cnt+1]=0;
		string_parse((char*)usartprop.usart_buf);
		usartprop.usart_cnt=0;
		HAL_UART_Receive_IT(&huart6,(uint8_t*)str,1);
		return;
	}
	usartprop.usart_cnt++;
	HAL_UART_Receive_IT(&huart6,(uint8_t*)str,1);
}
//-----------------------------------------------
#else
	uint8_t ipaddr_dest[4];
	uint16_t port_dest;
	extern UART_HandleTypeDef huart6;
	USART_prop_ptr usartprop;
	char str_server[30];
	char str1_server[100];
	u8_t data_server[100];
	struct tcp_pcb *_server_pcb;
	struct server_struct *ss;
	__IO uint32_t message_count_server=0;


	static err_t tcp_server_accept(void *arg, struct tcp_pcb *newpcb, err_t err);
	/**********************Function Server Error******************/
	static void tcp_server_error(void *arg, err_t err);
	//***********************Function Receive Data************************/
	static err_t tcp_server_recv(void *arg, struct tcp_pcb *tpcb,
				struct pbuf *p, err_t err);
	/**************************Function Server Send Data****************************/
	static err_t tcp_server_sent(void *arg, struct tcp_pcb *tpcb, u16_t len);
	static err_t tcp_server_poll(void *arg, struct tcp_pcb *tpcb);
	/****************Send TCP String NET**********************/
	void sendstring(char* buf_str);
	/****************************Function Close Connection**********************************/
	static void tcp_server_connection_close(struct tcp_pcb *tpcb, struct server_struct *es);
	/*********************Function Send Data**************************************/
	static void tcp_server_send(struct tcp_pcb *tpcb, struct server_struct *es);


	/****************************Init TCP SERVER****************************/
	void tcp_server_init(void)
	{
		_server_pcb = tcp_new();
		  if (_server_pcb != NULL)
	  {
		    err_t err;
		    err = tcp_bind(_server_pcb, IP_ADDR_ANY, 7);
		    if (err == ERR_OK)
		    {
		    	_server_pcb = tcp_listen(_server_pcb);
		      tcp_accept(_server_pcb, tcp_server_accept);
		    }
		    else
		    {
		      memp_free(MEMP_TCP_PCB, _server_pcb);
		    }

		  }
	}

	/**********************Init Debug UART LWIP******************/
	void net_ini(void)
	{
		  usartprop.usart_buf[0]=0;
		  usartprop.usart_cnt=0;
		  usartprop.is_tcp_connect=0;
		  usartprop.is_text=0;
	}
	/**********************Parse Function UART******************/
	void Server_string_parse(char* buf_str)
	{
	  HAL_UART_Transmit(&huart6, (uint8_t*)buf_str,strlen(buf_str),0x1000);
	  sendstring(buf_str);
	  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12|GPIO_PIN_13, GPIO_PIN_RESET);

	}
	/**********************Function Listen Connection******************/
	static err_t tcp_server_accept(void *arg, struct tcp_pcb *newpcb, err_t err)
	{
	  err_t ret_err;
	  struct server_struct *es;
	  LWIP_UNUSED_ARG(arg);
	  LWIP_UNUSED_ARG(err);
	  tcp_setprio(newpcb, TCP_PRIO_MIN);
	  es = (struct server_struct *)mem_malloc(sizeof(struct server_struct));
	  ss = (struct server_struct *)mem_malloc(sizeof(struct server_struct));
	  if (es != NULL)
	  {
	    es->state = SERVER_ES_ACCEPTED;
	    es->pcb = newpcb;
	    ss->pcb = newpcb;
	    es->retries = 0;
	    es->p = NULL;
	    tcp_arg(newpcb, es);
	    tcp_recv(newpcb, tcp_server_recv);
	    tcp_err(newpcb, tcp_server_error);
	    tcp_sent(newpcb, tcp_server_sent);
	    tcp_poll(newpcb, tcp_server_poll, 0);
	    ret_err = ERR_OK;
	  }
	  else
	  {
	    tcp_server_connection_close(newpcb, es);
	    ret_err = ERR_MEM;
	  }
	  return ret_err;
	}

	/****************************Function Close Connection**********************************/
	static void tcp_server_connection_close(struct tcp_pcb *tpcb, struct server_struct *es)
	{
	  // remove all callbacks
	  tcp_arg(tpcb, NULL);
	  tcp_sent(tpcb, NULL);
	  tcp_recv(tpcb, NULL);
	  tcp_err(tpcb, NULL);
	  tcp_poll(tpcb, NULL, 0);
	  if (es != NULL)
	  {
	    mem_free(es);
	  }
	  tcp_close(tpcb);
	}

	/**********************Function Server Error******************/
	static void tcp_server_error(void *arg, err_t err)

	{

	}
	//***********************Function Receive Data************************/
	static err_t tcp_server_recv(void *arg, struct tcp_pcb *tpcb,
			struct pbuf *p, err_t err)
	{
		struct server_struct *es;
		err_t ret_err;
		LWIP_ASSERT("arg != NULL",arg != NULL);
		es = (struct server_struct *)arg;
		if (p == NULL)
		{
			es->state = SERVER_ES_CLOSING;
			  if(es->p == NULL)
			  {
			    tcp_recved(tpcb, p->tot_len);
			  }
			  else
			  {
			    //acknowledge received packet
			    tcp_sent(tpcb, tcp_server_sent);
			    //send remaining data
			    tcp_server_send(tpcb, es);
			  }
			  ret_err = ERR_OK;
		}
		else if(err != ERR_OK)
		{
			if (p != NULL)
			  {
			    es->p = NULL;
			    pbuf_free(p);
			  }
			  ret_err = err;
		}
		else if(es->state == SERVER_ES_ACCEPTED)
		{
			tcp_recved(tpcb, p->tot_len);
			  strncpy(str1_server,p->payload,p->len);
			  str1_server[p->len] = '\0';
			  HAL_UART_Transmit(&huart6, (uint8_t*)str1_server,strlen(str1_server),0x1000);
			  ret_err = ERR_OK;
		}
		else if (es->state == SERVER_ES_RECEIVED)
		{
			if(es->p == NULL)
			  {
			    ret_err = ERR_OK;
			  }
			  else
			  {
			    struct pbuf *ptr;
			    ptr = es->p;
			    pbuf_chain(ptr,p);
			  }
			  ret_err = ERR_OK;
		}
		else if(es->state == SERVER_ES_CLOSING)
		{
			tcp_recved(tpcb, p->tot_len);
			  es->p = NULL;
			  pbuf_free(p);
			  ret_err = ERR_OK;
		}
		else
		{
			tcp_recved(tpcb, p->tot_len);
			  es->p = NULL;
			  pbuf_free(p);
			  ret_err = ERR_OK;
		}
		return ret_err;
	}

	/**************************Function Server Send Data****************************/
	static err_t tcp_server_sent(void *arg, struct tcp_pcb *tpcb, u16_t len)
	{
		 struct server_struct *es;
		  LWIP_UNUSED_ARG(len);
		  es = (struct server_struct *)arg;
		  es->retries = 0;
		  if(es->p != NULL)
		  {
		    tcp_server_send(tpcb, es);
		  }
		  else
		  {
		    if(es->state == SERVER_ES_CLOSING)
		      tcp_server_connection_close(tpcb, es);
		  }
		  return ERR_OK;
	}

	/************************Monitoring Net*******************************/
	static err_t tcp_server_poll(void *arg, struct tcp_pcb *tpcb)
	{
		err_t ret_err;
		struct server_struct *es;
		es = (struct server_struct *)arg;
		HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
		if (es != NULL)
		{
		  if (es->p != NULL)
		  {
		  }
		  else
		  {
		    if(es->state == SERVER_ES_CLOSING)
		    {
		      HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);
		      HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET);
		      tcp_server_connection_close(tpcb, es);
		    }
		    if(usartprop.is_text==1)
		    {
		      usartprop.is_text=0;
		    }
		  }
		  ret_err = ERR_OK;
		}
		else
		{
		  tcp_abort(tpcb);
		  ret_err = ERR_ABRT;
		}
		return ret_err;
}
	/*********************Function Send Data**************************************/
	static void tcp_server_send(struct tcp_pcb *tpcb, struct server_struct *es)
	{
		struct pbuf *ptr;
		  err_t wr_err = ERR_OK;
		  while ((wr_err == ERR_OK) &&
		    (es->p != NULL) &&
		    (es->p->len <= tcp_sndbuf(tpcb)))
		  {
		    ptr = es->p;
		    wr_err = tcp_write(tpcb, ptr->payload, ptr->len, 1);
		    if (wr_err == ERR_OK)
		    {
		      u16_t plen;
		      u8_t freed;
		      plen = ptr->len;
		      es->p = ptr->next;
		      if(es->p != NULL)
		      {
		        pbuf_ref(es->p);
		      }
		      do
		      {
		        freed = pbuf_free(ptr);
		      }
		      while(freed == 0);
		      tcp_recved(tpcb, plen);
		    }
		    else if(wr_err == ERR_MEM)
		    {
		      es->p = ptr;
		    }
		    else
		    {
		      //other problem
		    }
		  }
}

	/****************Send TCP String NET**********************/
	void sendstring(char* buf_str)
	{
		usartprop.is_text=1;
		  tcp_sent(ss->pcb, tcp_server_sent);
		  tcp_write(ss->pcb, (void*)buf_str, strlen(buf_str), 1);
		  tcp_recved(ss->pcb, strlen(buf_str));
		  usartprop.usart_cnt=0;
	}

#endif
