/*
 * MCP23017_LL.c
 *
 *  Created on: Apr 29, 2020
 *      Author: AlexPirs
 */


#include "MCP23017_LL.h"


#define MCP23017_I2C				hi2c1

/*****************************Write Byte I2C*************************************/
static HAL_StatusTypeDef MCP23017_writeByte_i2c(uint16_t Addr,uint8_t Reg,uint8_t value);
/*****************************Read Byte I2C*************************************/
static HAL_StatusTypeDef MCP23017_ReadByte_i2c(uint16_t Addr,uint8_t Reg,uint8_t *data);






/********************Error MCP23017*************************/
void MCP23017_Handler_Error(void)
{

}

/*****************************Write Byte I2C*************************************/
static HAL_StatusTypeDef MCP23017_writeByte_i2c(uint16_t Addr,uint8_t Reg,uint8_t value)
{
	
}

/*****************************Read Byte I2C*************************************/
static HAL_StatusTypeDef MCP23017_ReadByte_i2c(uint16_t Addr,uint8_t Reg,uint8_t *data)
{
	
}



