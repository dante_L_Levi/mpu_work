/*
 * MCP23017_LL.h
 *
 *  Created on: Apr 29, 2020
 *      Author: AlexPirs
 */

#ifndef INC_MCP23017_LL_H_
#define INC_MCP23017_LL_H_

#include "main.h"


typedef struct
{
	uint8_t state;//1- high Level,0-Low Level
	uint8_t configPull;//1-pull up, 0-no pull
	uint8_t configIO;//set GPIO input 0, output 1

}MCP23017_PORTS;


typedef struct
{
	uint16_t					addr;
	MCP23017_PORTS				Ports[2];

}MCP23017_hw;





#endif /* INC_MCP23017_LL_H_ */
