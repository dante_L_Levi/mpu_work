/*
 * LOGIC.c
 *
 *  Created on: Apr 27, 2020
 *      Author: AlexPirs
 */


#include "LOGIC.h"
#include "stdbool.h"
#define SizeRxD		15

void Indicate_Led(uint8_t count);
uint8_t data[10]={0x00,0x01,0xFF,0x0C,0x1C,0x2C,0x7F,0x3F,0x0D,0x0A};
bool fl;
char rx_str[30], tx_str[30], tmp_str[10];

//----------------------------------------------------------------------------
#define ARRAY_LEN(x)            (sizeof(x) / sizeof((x)[0]))
uint8_t usart_rx_dma_buffer[64];
//---------------------------------------------------------------------------

uint8_t count_Led=1;
uint8_t index_count=0;
uint8_t dt1;




void  USART3_RX_Callback(void)
{

	if(LL_USART_IsActiveFlag_RXNE(USART3) && LL_USART_IsEnabledIT_RXNE(USART3))
	{
		dt1 = LL_USART_ReceiveData8(USART3);
		fl=1;
	}


}

/*
uint16_t USART_RX_Buffer(uint8_t* rx_dt)
{
	uint16_t ind = 0;
	  while (!fl) {}
	  fl=0;
	   rx_dt[ind] = dt1;

}
*/

void
usart_rx_check(void)
{
	static size_t old_pos;
	    size_t pos;

	    /* Calculate current position in buffer */
	    pos = ARRAY_LEN(usart_rx_dma_buffer) - LL_DMA_GetDataLength(DMA1, LL_DMA_CHANNEL_3);
	    if (pos != old_pos) {                       /* Check change in received data */
	        if (pos > old_pos) {                    /* Current position is over previous one */
	            /* We are in "linear" mode */
	            /* Process data directly by subtracting "pointers" */
	           // usart_process_data(&usart_rx_dma_buffer[old_pos], pos - old_pos);
	        } else {
	            /* We are in "overflow" mode */
	            /* First process data to the end of buffer */
	          //  usart_process_data(&usart_rx_dma_buffer[old_pos], ARRAY_LEN(usart_rx_dma_buffer) - old_pos);
	            /* Check and continue with beginning of buffer */
	            if (pos > 0) {
	            //    usart_process_data(&usart_rx_dma_buffer[0], pos);
	            }
	        }
	    }
	    old_pos = pos;                              /* Save current position as old */

	    /* Check and manually update if we reached end of buffer */
	    if (old_pos == ARRAY_LEN(usart_rx_dma_buffer)) {
	        old_pos = 0;
	    }
}

void
DMA1_Channel3_IRQHandler(void)
{
	if (LL_DMA_IsEnabledIT_HT(DMA1, LL_DMA_CHANNEL_3) && LL_DMA_IsActiveFlag_HT3(DMA1)) {
	        LL_DMA_ClearFlag_HT3(DMA1);             /* Clear half-transfer complete flag */
	        usart_rx_check();                       /* Check for data to process */
	    }

	    /* Check transfer-complete interrupt */
	    if (LL_DMA_IsEnabledIT_TC(DMA1, LL_DMA_CHANNEL_3) && LL_DMA_IsActiveFlag_TC5(DMA1)) {
	        LL_DMA_ClearFlag_TC3(DMA1);             /* Clear transfer complete flag */
	        usart_rx_check();                       /* Check for data to process */
	    }
}



void
USART1_IRQHandler(void) {
    /* Check for IDLE line interrupt */
    if (LL_USART_IsEnabledIT_IDLE(USART3) && LL_USART_IsActiveFlag_IDLE(USART3)) {
        LL_USART_ClearFlag_IDLE(USART3);        /* Clear IDLE line flag */
        usart_rx_check();                       /* Check for data to process */
    }

    /* Implement other events when needed */
}


void Usart_init(void)
{
	LL_USART_InitTypeDef USART_InitStruct = {0};

	  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	  /* Peripheral clock enable */
	  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART3);

	  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);
	  /**USART3 GPIO Configuration
	  PB10   ------> USART3_TX
	  PB11   ------> USART3_RX
	  */
	  GPIO_InitStruct.Pin = LL_GPIO_PIN_10;
	  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	  GPIO_InitStruct.Pin = LL_GPIO_PIN_11;
	  GPIO_InitStruct.Mode = LL_GPIO_MODE_FLOATING;
	  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	  /* USART3 DMA Init */

	  /* USART3_RX Init */
	  LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_3, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

	  LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PRIORITY_LOW);

	  LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MODE_CIRCULAR);

	  LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PERIPH_NOINCREMENT);

	  LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MEMORY_INCREMENT);

	  LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PDATAALIGN_BYTE);

	  LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MDATAALIGN_BYTE);

	  LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_3, (uint32_t)&USART3->DR);
	  LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_3, (uint32_t)usart_rx_dma_buffer);
	  LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_3, ARRAY_LEN(usart_rx_dma_buffer));

	  LL_DMA_EnableIT_HT(DMA1, LL_DMA_CHANNEL_3);
	  LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_3);


	  /* USART3 interrupt Init */
	  NVIC_SetPriority(USART3_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	  NVIC_EnableIRQ(USART3_IRQn);

	  NVIC_SetPriority(DMA1_Channel3_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	   NVIC_EnableIRQ(DMA1_Channel3_IRQn);

	  USART_InitStruct.BaudRate = 115200;
	  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
	  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
	  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
	  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
	  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
	  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
	  LL_USART_Init(USART3, &USART_InitStruct);
	  LL_USART_ConfigAsyncMode(USART3);
	  LL_USART_Enable(USART3);
	  LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);
}


/**************************UPDATE EVENT IRQ3***********************************/
void TIM3_IRQHandler(void)
{
	TIM3->SR &= ~TIM_SR_UIF;
	if(count_Led>100)
			count_Led=1;



	Indicate_Led(count_Led);
	count_Led++;
	//Usart_Transmit(data,sizeof(data));

}

/**************************UPDATE EVENT IRQ3***********************************/
void TIM4_IRQHandler(void)
{
	TIM4->SR &= ~TIM_SR_UIF;

}

void TimerInit(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM3EN;
	TIM3->PSC=35999;
	TIM3->ARR=1000;
	TIM3->DIER |= TIM_DIER_UIE;
	TIM3->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM3_IRQn);
	//---------------------------------------------
	RCC->APB1ENR|=RCC_APB1ENR_TIM3EN;
	TIM4->PSC=35999;
	TIM4->ARR=2000;
	TIM4->DIER |= TIM_DIER_UIE;
	TIM4->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM4_IRQn);
}


void Indicate_Led(uint8_t count)
{
	count%2==0?(LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_13))
			:(LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_13));
}



void Usart_Transmit(uint8_t *dt,uint16_t sz)
{
	 uint16_t ind = 0;
	 while (ind<sz)
	   {
	     while (!LL_USART_IsActiveFlag_TXE(USART3)) {}
	     LL_USART_TransmitData8(USART3,*(uint8_t*)(dt+ind));
	     ind++;
	   }
}
