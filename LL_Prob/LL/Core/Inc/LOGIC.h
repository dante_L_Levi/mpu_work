/*
 * LOGIC.h
 *
 *  Created on: Apr 27, 2020
 *      Author: AlexPirs
 */

#ifndef INC_LOGIC_H_
#define INC_LOGIC_H_


#include "main.h"


void TimerInit(void);
void Usart_Transmit(uint8_t *dt,uint16_t sz);
void  USART3_RX_Callback(void);
void usart_rx_check(void);
void Usart_init(void);
#endif /* INC_LOGIC_H_ */
