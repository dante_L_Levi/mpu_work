#ifndef DS3231_H_
#define DS3231_H_


#include "stm32l1xx_hal.h"
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"


#include "GPIO_LIB.h"



#define DS3231_ADDR			0xD0


typedef enum
{
	 seconds=			0x00,
	 minutes=			0x01,
     hours=				0x02,
	 day=				0x03,
	 date=				0x04,
	 month=				0x05,
     year=				0x06,
	 alarm1_secconds=	0x07,
	 alarm1_minutes=	0x08,
	 alarm1_hours=		0x09,
	 alarm1_day=		0x0A,
	 alarm1_date=		0x0B,
	 alarm2_minutes=	0x0C,
	 alarm2_hours=		0x0D,
	 alarm2_day=		0x0D,
	 alarm2_date=		0x0D,
	 control=			0x0E,
	 status=			0x0F,
	 aging=				0x10,
	 msb_temp=			0x11,
	 lsb_temp=			0x12

}REGISTER_DS3231;



/**********************Convert Value to Decimal*************************/
uint8_t RTC_ConvertFromDec(uint8_t c);
/**********************Convert Value to BinDin*************************/
uint8_t RTC_ConvertFromBinDec(uint8_t c);
/*********************Read All RTC*******************************/
void Read_RTCRegisters(void);
/*****************************Get Date Value***********************/
uint8_t GetDate(void);
/*****************************Get Month Value***********************/
uint8_t GetMonth(void);
/*****************************Get Year Value***********************/
uint8_t GetYear(void);
/*****************************Get Day Value***********************/
uint8_t GetDay(void);
/*****************************Get HourValue***********************/
uint8_t GetHour(void);
/*****************************Get minuts Value***********************/
uint8_t GetMin(void);
/*****************************Get seconds Value***********************/
uint8_t GetSec(void);






#endif /* DS3231_H_ */
