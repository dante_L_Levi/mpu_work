#ifndef __AUDIO_H
#define __AUDIO_H


/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "main.h"
#include "pcf8574.h"
#include "fatfs.h"
#include "usb_host.h"

#define I2S3                            SPI3

/********************Struct work Buffer AUDIO*********************/
typedef enum
{
  BUFFER_OFFSET_NONE = 0,  //None offset
  BUFFER_OFFSET_HALF,  			//HALF offset
  BUFFER_OFFSET_FULL,    		//Full offset
}BUFFER_StateTypeDef;


/**********************Struct Format Frame*******************/
typedef struct
{
  uint32_t   ChunkID;       /* 0 */
  uint32_t   FileSize;      /* 4 */
  uint32_t   FileFormat;    /* 8 */
  uint32_t   SubChunk1ID;   /* 12 */
  uint32_t   SubChunk1Size; /* 16 */  
  uint16_t   AudioFormat;   /* 20 */
  uint16_t   NbrChannels;   /* 22 */  
  uint32_t   SampleRate;    /* 24 */
  uint32_t   ByteRate;      /* 28 */
  uint16_t   BlockAlign;    /* 32 */  
  uint16_t   BitPerSample;  /* 34 */  
  uint32_t   SubChunk2ID;   /* 36 */  
  uint32_t   SubChunk2Size; /* 40 */    
}WAVE_FormatTypeDef;



/* Audio State Structure */    
typedef enum {
  AUDIO_IDLE = 0,
  AUDIO_WAIT,  
  AUDIO_EXPLORE,
  AUDIO_PLAYBACK,
  AUDIO_IN,  
}AUDIO_State;



/* Audio Demo State Machine Structure */
typedef struct _StateMachine {
  __IO AUDIO_State state;
  __IO uint8_t select;  
}AUDIO_StateMachine;

void    AudioPlay_TransferComplete_CallBack(void);
void    AudioPlay_HalfTransfer_CallBack(void);



/************************Common Init*********************************/
void AudioPlay_Init(uint32_t AudioFreq);
/************************Init Register+OUTPUT*********************************/
uint8_t AudioOut_Init(uint16_t OutputDevice, uint8_t Volume, uint32_t AudioFreq);
/******************Read ID CS43L22*******************/
uint32_t cs43l22_ReadID(uint16_t DeviceAddr);
/******************Init parametrs Audio*******************/
uint32_t cs43l22_Init(uint16_t DeviceAddr, uint16_t OutputDevice, uint8_t Volume, uint32_t AudioFreq);
/**********************Function Write Data in Register*****************/
static uint8_t CODEC_IO_Write(uint8_t Addr, uint8_t Reg, uint8_t Value);
/**********************Function Config Volume*****************/
uint32_t cs43l22_SetVolume(uint16_t DeviceAddr, uint8_t Volume);
/********************Init I2S BUS**********************/
static void I2S3_Init(uint32_t AudioFreq);
/*************************Function Start Audio**************************/
void AudioPlay_Start(uint32_t AudioFreq);
/**********************OFF VOLUME*****************/
uint32_t cs43l22_SetMute(uint16_t DeviceAddr, uint32_t Cmd);
/*******************Function Play Audio*******************/
uint32_t cs43l22_Play(uint16_t DeviceAddr);
/**********************STOP Command*****************/
uint32_t cs43l22_Stop(uint16_t DeviceAddr);
/*****************Function STOP AUDIO****************************/
void AudioPlay_Stop(void);




#endif /* __AUDIO_H */


