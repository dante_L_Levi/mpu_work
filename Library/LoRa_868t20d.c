/*
 * LoRa_868t20d.c
 *
 *  Created on: 1 авг. 2019 г.
 *      Author: Tatiana_Potrebko
 */


#include "LoRa_868t20d.h"

extern GPIO_InitTypeDef GPIO_InitStruct;

LoRa_E32 MyLoRaDef;
uint8_t buffer_Read[Buffer_Read_PAYLOAD]={0};
/****************************Init Uart **************************/
static void UART_Init_LoRa_868t20d(void);


/*********************This function handles USART1 global interrupt*******************/
void USART1_IRQHandler_LoRa(void)
{
	HAL_UART_IRQHandler(&LoRaUart_Base);
	Helper_Read_Config_LoRa();
}


/********************Config Pin Configuration******************/
void Init_Config_HRW_868t20d(void)
{

	/*Configure GPIO pin : AUX_Pin */
		GPIO_InitStruct.Pin = AUX_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
	  HAL_GPIO_Init(AUX_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin : M1_LoRa_Pin */
	  GPIO_InitStruct.Pin = M1_LoRa_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(M1_LoRa_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin : M0_LoRa_Pin */
	  GPIO_InitStruct.Pin = M0_LoRa_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(M0_LoRa_GPIO_Port, &GPIO_InitStruct);

	  /*Configure GPIO pin Output Level */
	    //HAL_GPIO_WritePin(M1_LoRa_GPIO_Port, M1_LoRa_Pin, GPIO_PIN_RESET);

	    /*Configure GPIO pin Output Level */
	    //HAL_GPIO_WritePin(M0_LoRa_GPIO_Port, M0_LoRa_Pin, GPIO_PIN_RESET);

	  //=========================================================================
	  UART_Init_LoRa_868t20d();
}

/*************************Function Set M0/M1*****************************/
RET_STATUS LoRa_SetMBits(GPIO_CONTROL_MBITS stat)
{
	if(stat==M0_Bits)
	{
		HAL_GPIO_WritePin(M0_LoRa_GPIO_Port, M0_LoRa_Pin, GPIO_PIN_SET);
		return RET_SUCCESS;
	}
	else if(stat==M1_Bits)
	{
		HAL_GPIO_WritePin(M1_LoRa_GPIO_Port, M1_LoRa_Pin, GPIO_PIN_SET);
		return RET_SUCCESS;
	}
	else
	{
		return RET_INVALID_PARAM;
	}
}



/*************************Function Reset M0/M1*****************************/
RET_STATUS LoRa_ResetMBits(GPIO_CONTROL_MBITS stat)
{
	if(stat==M0_Bits)
	{
		HAL_GPIO_WritePin(M0_LoRa_GPIO_Port, M0_LoRa_Pin, GPIO_PIN_RESET);
		return RET_SUCCESS;
	}
	else if(stat==M1_Bits)
	{
		HAL_GPIO_WritePin(M1_LoRa_GPIO_Port, M1_LoRa_Pin, GPIO_PIN_RESET);
		return RET_SUCCESS;
	}
	else
	{
		return RET_INVALID_PARAM;
	}
}
/****************************Init Uart **************************/
static void UART_Init_LoRa_868t20d(void)
{
	MX_USART1_UART_Init();
}


/********************Set Mode Work***********************/
RET_STATUS LoRa_ConfigModeWork(LoRa_ModeDef mode)
{
	switch(mode)
	{
		case MODE_NORMAL:
		{
			LoRa_ResetMBits(M0_Bits);
			LoRa_ResetMBits(M1_Bits);
			break;
		}
		case MODE_WAKE_UP:
		{
			LoRa_SetMBits(M0_Bits);
			LoRa_ResetMBits(M1_Bits);
			break;
		}
		case MODE_POWER_SAVING:
		{
			LoRa_SetMBits(M1_Bits);
			LoRa_ResetMBits(M0_Bits);
			break;
		}
		case MODE_SLEEP:
		{
			LoRa_SetMBits(M1_Bits);
			LoRa_SetMBits(M0_Bits);
			break;
		}
		default:
		{
			return RET_INVALID_PARAM;
		}


	}
	UART_Init_LoRa_868t20d();
	return RET_SUCCESS;
}


/**************Function Transmit Command********************/
void Get_Configuration_LoRa(void)
{
	 uint8_t geetParam[3]={0xC1,0xC1,0xC1};
	 HAL_UART_Transmit(&huart1, (uint8_t*)geetParam, 3, 0x1000);
}

/*************************Read Data LoRa Config****************************/
void Helper_Read_Config_LoRa(void)
{
	HAL_UART_Receive_IT(&LoRaUart_Base, (uint8_t*)(MyLoRaDef.dataConfig), 6);
}




/*************************Read Data LoRa *********************************/
void Helper_Read_PAYLOAD_LoRa(void)
{
	HAL_UART_Receive_IT(&LoRaUart_Base, (uint8_t*)buffer_Read, Buffer_Read_PAYLOAD);
}

/*******************Config Standart LoRa 433MHz**********************/
void Set_Default_433MHz_LoRa(void)
{
	uint8_t data[6]={0xC0,0x00,0x00,0x1A,0x17,0x44};
	HAL_UART_Transmit(&LoRaUart_Base, (uint8_t *)data, 6, 0x1000);
}


/**********************Transmit LoRa*****************************/
void LoRa_TransmitData(uint8_t *dt,uint8_t length)
{
	if(length>BUFFER_PACKET_MAX)
	{
		return;
	}
	else
	{
		HAL_UART_Transmit(&LoRaUart_Base, (uint8_t *)dt, length, 0x100);
	}
}


