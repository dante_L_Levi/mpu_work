/*
 * SDD1306.c
 *
 *  Created on: Jul 21, 2019
 *      Author: Tatiana_Potrebko
 */


#include "SDD1306.h"

extern I2C_HandleTypeDef 		hi2c1;

#define SSD1306_I2C_PORT		hi2c1

// Screenbuffer
static uint8_t SSD1306_Buffer[SSD1306_WIDTH * SSD1306_HEIGHT / 8];
// Screen object
static SSD1306_t SSD1306;



/******************Send Command******************************/
static void ssd1306_WriteCommand(uint8_t byte)
{
	HAL_I2C_Mem_Write(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x00, 1, &byte, 1, HAL_MAX_DELAY);
}

/******************Send ResetCommand******************************/
static void SDD1306_Reset(void)
{
	//ssd1306_WriteCommand(0x7F);

}

/******************Send Data******************************/
void ssd1306_WriteData(uint8_t* buffer, size_t buff_size)
{
	HAL_I2C_Mem_Write(&SSD1306_I2C_PORT, SSD1306_I2C_ADDR, 0x40, 1, buffer, buff_size, HAL_MAX_DELAY);
}


/******************Init SDD 1306******************************/
void ssd1306_Init(void)
{
	SDD1306_Reset();
	// Wait for the screen to boot
	    HAL_Delay(100);

	    // Init OLED
	    ssd1306_WriteCommand(0xAE); //display off
	    ssd1306_WriteCommand(0x20); //Set Memory Addressing Mode
	    ssd1306_WriteCommand(0x10); // 00,Horizontal Addressing Mode; 01,Vertical Addressing Mode;
	                               // 10,Page Addressing Mode (RESET); 11,Invalid
	    ssd1306_WriteCommand(0xB0); //Set Page Start Address for Page Addressing Mode,0-7
		#ifdef SSD1306_MIRROR_VERT
			ssd1306_WriteCommand(0xC0); // Mirror vertically
		#else
			ssd1306_WriteCommand(0xC8); //Set COM Output Scan Direction
		#endif

			ssd1306_WriteCommand(0x00); //---set low column address
			ssd1306_WriteCommand(0x10); //---set high column address
			ssd1306_WriteCommand(0x40); //--set start line address - CHECK
		    ssd1306_WriteCommand(0x81); //--set contrast control register - CHECK
			ssd1306_WriteCommand(0xFF);

		#ifdef SSD1306_MIRROR_HORIZ
			ssd1306_WriteCommand(0xA0); // Mirror horizontally
		#else
			ssd1306_WriteCommand(0xA1); //--set segment re-map 0 to 127 - CHECK
		#endif

		#ifdef SSD1306_INVERSE_COLOR
			ssd1306_WriteCommand(0xA7); //--set inverse color
		#else
			ssd1306_WriteCommand(0xA6); //--set normal color
		#endif

			ssd1306_WriteCommand(0xA8); //--set multiplex ratio(1 to 64) - CHECK
			ssd1306_WriteCommand(0x3F); //

			ssd1306_WriteCommand(0xA4); //0xa4,Output follows RAM content;0xa5,Output ignores RAM content

			ssd1306_WriteCommand(0xD3); //-set display offset - CHECK
			ssd1306_WriteCommand(0x00); //-not offset

			ssd1306_WriteCommand(0xD5); //--set display clock divide ratio/oscillator frequency
			ssd1306_WriteCommand(0xF0); //--set divide ratio

			ssd1306_WriteCommand(0xD9); //--set pre-charge period
			ssd1306_WriteCommand(0x22); //

			ssd1306_WriteCommand(0xDA); //--set com pins hardware configuration - CHECK
			ssd1306_WriteCommand(0x12);

			ssd1306_WriteCommand(0xDB); //--set vcomh
			ssd1306_WriteCommand(0x20); //0x20,0.77xVcc

			ssd1306_WriteCommand(0x8D); //--set DC-DC enable
			ssd1306_WriteCommand(0x14); //
			ssd1306_WriteCommand(0xAF); //--turn on SSD1306 panel

			// Clear screen
			ssd1306_Fill(White);

			// Flush buffer to screen
			ssd1306_UpdateScreen();

			// Set default values for screen object
			SSD1306.CurrentX = 0;
			SSD1306.CurrentY = 0;

			SSD1306.Initialized = 1;

}

/********************function Fill Screen************************/
void ssd1306_Fill(SSD1306_COLOR color)
{
	/* Set memory */
	    uint32_t i;

	    for(i = 0; i < sizeof(SSD1306_Buffer); i++)
	    {
	        SSD1306_Buffer[i] = (color == Black) ? 0x00 : 0xFF;
	    }
}

/********************function Update************************/
void ssd1306_UpdateScreen(void)
{
    uint8_t i;
    for(i = 0; i < 8; i++)
    {
        ssd1306_WriteCommand(0xB0 + i);
        ssd1306_WriteCommand(0x00);
        ssd1306_WriteCommand(0x10);
        ssd1306_WriteData(&SSD1306_Buffer[SSD1306_WIDTH*i],SSD1306_WIDTH);
    }
}

//    Draw one pixel in the screenbuffer
//    X => X Coordinate
//    Y => Y Coordinate
//    color => Pixel color
void ssd1306_DrawPixel(uint8_t x, uint8_t y, SSD1306_COLOR color)
{
    if(x >= SSD1306_WIDTH || y >= SSD1306_HEIGHT) {
        // Don't write outside the buffer
        return;
    }

    // Check if pixel should be inverted
    if(SSD1306.Inverted) {
        color = (SSD1306_COLOR)!color;
    }

    // Draw in the right color
    if(color == White) {
        SSD1306_Buffer[x + (y / 8) * SSD1306_WIDTH] |= 1 << (y % 8);
    } else {
        SSD1306_Buffer[x + (y / 8) * SSD1306_WIDTH] &= ~(1 << (y % 8));
    }
}

// Draw 1 char to the screen buffer
// ch         => char om weg te schrijven
// Font     => Font waarmee we gaan schrijven
// color     => Black or White
char ssd1306_WriteChar(char ch, FontDef Font, SSD1306_COLOR color)
{
    uint32_t i, b, j;

    // Check remaining space on current line
    if (SSD1306_WIDTH <= (SSD1306.CurrentX + Font.FontWidth) ||
        SSD1306_HEIGHT <= (SSD1306.CurrentY + Font.FontHeight))
    {
        // Not enough space on current line
        return 0;
    }

    // Use the font to write
    for(i = 0; i < Font.FontHeight; i++)
    {
        b = Font.data[(ch - 32) * Font.FontHeight + i];
        for(j = 0; j < Font.FontWidth; j++) {
            if((b << j) & 0x8000)  {
                ssd1306_DrawPixel(SSD1306.CurrentX + j, (SSD1306.CurrentY + i), (SSD1306_COLOR) color);
            } else {
                ssd1306_DrawPixel(SSD1306.CurrentX + j, (SSD1306.CurrentY + i), (SSD1306_COLOR)!color);
            }
        }
    }

    // The current space is now taken
    SSD1306.CurrentX += Font.FontWidth;

    // Return written char for validation
    return ch;
}

// Write full string to screenbuffer
char ssd1306_WriteString(char* str, FontDef Font, SSD1306_COLOR color)
{
    // Write until null-byte
    while (*str) {
        if (ssd1306_WriteChar(*str, Font, color) != *str) {
            // Char could not be written
            return *str;
        }

        // Next char
        str++;
    }

    // Everything ok
    return *str;
}

// Position the cursor
void ssd1306_SetCursor(uint8_t x, uint8_t y)
{
    SSD1306.CurrentX = x;
    SSD1306.CurrentY = y;
}

/*=============================================TEST**********************************************/
void ssd1306_TestBorder()
{
    ssd1306_Fill(Black);

    uint32_t start = HAL_GetTick();
    uint32_t end = start;
    uint8_t x = 0;
    uint8_t y = 0;
    do {
        ssd1306_DrawPixel(x, y, Black);

        if((y == 0) && (x < 127))
            x++;
        else if((x == 127) && (y < 63))
            y++;
        else if((y == 63) && (x > 0))
            x--;
        else
            y--;

        ssd1306_DrawPixel(x, y, White);
        ssd1306_UpdateScreen();

        HAL_Delay(5);
        end = HAL_GetTick();
    } while((end - start) < 8000);

    HAL_Delay(1000);
}

void ssd1306_TestFonts() {
    ssd1306_Fill(Black);
    ssd1306_SetCursor(2, 0);
    ssd1306_WriteString("Font 16x26", Font_16x26, White);
    ssd1306_SetCursor(2, 26);
    ssd1306_WriteString("Font 11x18", Font_11x18, White);
    ssd1306_SetCursor(2, 26+18);
    ssd1306_WriteString("Font 7x10", Font_7x10, White);
    ssd1306_UpdateScreen();
}

void ssd1306_TestFPS() {
    ssd1306_Fill(White);

    uint32_t start = HAL_GetTick();
    uint32_t end = start;
    int fps = 0;
    char message[] = "ABCDEFGHIJK";

    ssd1306_SetCursor(2,0);
    ssd1306_WriteString("Testing...", Font_11x18, Black);

    do {
        ssd1306_SetCursor(2, 18);
        ssd1306_WriteString(message, Font_11x18, Black);
        ssd1306_UpdateScreen();

        char ch = message[0];
        memmove(message, message+1, sizeof(message)-2);
        message[sizeof(message)-2] = ch;

        fps++;
        end = HAL_GetTick();
    } while((end - start) < 5000);

    HAL_Delay(1000);

    char buff[64];
    fps = (float)fps / ((end - start) / 1000.0);
    snprintf(buff, sizeof(buff), "~%d FPS", fps);

    ssd1306_Fill(White);
    ssd1306_SetCursor(2, 18);
    ssd1306_WriteString(buff, Font_11x18, Black);
    ssd1306_UpdateScreen();
}

void ssd1306_TestAll() {
    ssd1306_Init();
    ssd1306_TestFPS();
    HAL_Delay(3000);
    ssd1306_TestBorder();
    ssd1306_TestFonts();
}


