#ifndef AT24C32_H_
#define AT24C32_H_


#include "stm32l1xx_hal.h"
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "GPIO_LIB.h"




#define	AT24XXX_ADDR			0xAE
#define		AT24XXX_EEPROM_SIZE_KBIT										32
#define		AT24XXX_EEPROM_USE_WP_PIN												0
#if (_EEPROM_USE_WP_PIN==1)
#define		_EEPROM_WP_GPIO											EE_WP_GPIO_Port
#define		_EEPROM_WP_PIN											EE_WP_Pin
#endif

typedef enum
{
	AT24xx_Connect_OK,
	AT24xx_Save_OK,
	AT24xx_Load_OK,
	AT24xx_ERROR

}AT24XXStatusDef;


/***********************Check Connected Devices***************************/
AT24XXStatusDef	EEPROM24XX_IsConnected(void);
/***************************Save Values in EEPROM**********************************/
AT24XXStatusDef EEPROM24XX_Save(uint16_t Address,void *data,size_t size_of_data);
/***************************Save Values from EEPROM**********************************/
AT24XXStatusDef EEPROM24XX_Load(uint16_t Address,void *data,size_t size_of_data);









#endif /* AT24C32_H_ */
