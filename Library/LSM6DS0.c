/*
 * LSM6DS0.c
 *
 *  Created on: Jun 29, 2019
 *      Author: Tatiana_Potrebko
 */

#include "LSM6DS0.h"

extern	Data_LSM6Def			dataLSM6DS0;
extern	I2C_HandleTypeDef 		hi2c1;
#define 	LSM6DS0_I2C			hi2c1


/*************************************Write Data LSM6DS0*************************************/
void Accel_IO_Write(uint16_t DeviceAddr, uint8_t RegisterAddr, uint8_t Value);
/**********************************Read Data LSM303****************************************/
uint8_t Accel_IO_Read(uint16_t DeviceAddr, uint8_t RegisterAddr);
/******************Init Register Accel LSM6DS0*************************/
void LSM6DS0_ACCEL_Init_HRW(uint8_t Freq,uint8_t Range);
/******************Init Register Accel LSM6DS0*************************/
void LSM6DS0_GYRO_Init_HRW(uint8_t Freq,uint8_t Range);



/*************************************Write Data LSM6DS0*************************************/
void Accel_IO_Write(uint16_t DeviceAddr, uint8_t RegisterAddr, uint8_t Value)
{
	I2C_Write_Byte(LSM6DS0_I2C,DeviceAddr, RegisterAddr, Value);
}

/**********************************Read Data LSM6DS0****************************************/
uint8_t Accel_IO_Read(uint16_t DeviceAddr, uint8_t RegisterAddr)
{
	return I2C_Read_Byte(LSM6DS0_I2C,DeviceAddr, RegisterAddr);
}

/**********************************Read ID****************************************/
uint8_t Read_Id(void)
{
	return Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_WHO_I_AM);
}

/**********************************Init LSM6DS0***************************/
uint8_t LSM6DS0_Init(uint8_t configAccelGyro,uint8_t Freq,uint8_t Range,uint8_t dps)
{
	if(Read_Id()==ID_DEV_LSM6DS0)
	{
		dataLSM6DS0.id_dev=Read_Id();
		INDICATION_OK;

		switch(configAccelGyro)
		{
			case CONFIG_ACCELEROMETTER_ENABLE:
			{
				LSM6DS0_ACCEL_Init_HRW(Freq,Range);
				break;
			}
			case CONFIG_GYROSCOPE_ENABLE:
			{
				LSM6DS0_GYRO_Init_HRW(Freq,dps);
				break;
			}
			case CONFIG_ACC_GYRO_ENABLE:
			{
				LSM6DS0_ACCEL_Init_HRW(Freq,Range);
				LSM6DS0_GYRO_Init_HRW(Freq,dps);
				break;
			}

			default:
			{

				break;
			}


		}
		return (uint8_t)HAL_OK;
	}
	else
	{
		Error_Handler();
		return (uint8_t)HAL_ERROR;
	}
}

/******************Init Register Accel LSM6DS0*************************/
void LSM6DS0_ACCEL_Init_HRW(uint8_t Freq,uint8_t Range)
{
	uint8_t value=0;
	value = Accel_IO_Read(ADDR_SLAVE_LSM6, LSM6DS0_ACC_GYRO_CTRL_REG8);
	value &= ~LSM6DS0_ACC_GYRO_BDU_MASK;
	value |= LSM6DS0_ACC_GYRO_BDU_ENABLE;
	Accel_IO_Write(0xD6, LSM6DS0_ACC_GYRO_CTRL_REG8, value);

	value = Accel_IO_Read(ADDR_SLAVE_LSM6, LSM6DS0_ACC_GYRO_CTRL_REG6_XL);
	value &= ~LSM6DS0_ACC_GYRO_ODR_XL_MASK;
	value |= LSM6DS0_ACC_GYRO_ODR_XL_POWER_DOWN;
	Accel_IO_Write(0xD6, LSM6DS0_ACC_GYRO_CTRL_REG6_XL, value);

	//Full scale selection
	value = Accel_IO_Read(ADDR_SLAVE_LSM6, LSM6DS0_ACC_GYRO_CTRL_REG6_XL);
    value &= ~LSM6DS0_ACC_GYRO_FS_XL_MASK;
	value |= Range;
	Accel_IO_Write(ADDR_SLAVE_LSM6, LSM6DS0_ACC_GYRO_CTRL_REG6_XL, value);

	value = Accel_IO_Read(ADDR_SLAVE_LSM6, LSM6DS0_ACC_GYRO_CTRL_REG5_XL);
	value &= ~(LSM6DS0_ACC_GYRO_XEN_XL_MASK |
               LSM6DS0_ACC_GYRO_YEN_XL_MASK |
               LSM6DS0_ACC_GYRO_ZEN_XL_MASK);
	value |= (LSM6DS0_ACC_GYRO_XEN_XL_ENABLE |
              LSM6DS0_ACC_GYRO_YEN_XL_ENABLE |
              LSM6DS0_ACC_GYRO_ZEN_XL_ENABLE);
	Accel_IO_Write(ADDR_SLAVE_LSM6, LSM6DS0_ACC_GYRO_CTRL_REG5_XL, value);



	value = Accel_IO_Read(ADDR_SLAVE_LSM6, LSM6DS0_ACC_GYRO_CTRL_REG6_XL);
	value &= ~LSM6DS0_ACC_GYRO_ODR_XL_MASK;
	value |= Freq;
	Accel_IO_Write(ADDR_SLAVE_LSM6, LSM6DS0_ACC_GYRO_CTRL_REG6_XL, value);

}




/******************Init Register Accel LSM6DS0*************************/
void LSM6DS0_GYRO_Init_HRW(uint8_t Freq,uint8_t Range)
{
	uint8_t value = 0;

    value = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
    value&=~LSM6DS0_ACC_GYRO_ODR_G_MASK;
    value|=LSM6DS0_ACC_GYRO_ODR_G_POWER_DOWN;
    Accel_IO_Write(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);

    value = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
    value&=~LSM6DS0_ACC_GYRO_FS_G_MASK;
    value|=Range;
    Accel_IO_Write(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);

    //Включим оси
    	value = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG4);
    	value&=~(LSM6DS0_ACC_GYRO_XEN_G_ENABLE|\
    					 LSM6DS0_ACC_GYRO_YEN_G_ENABLE|\
    					 LSM6DS0_ACC_GYRO_ZEN_G_ENABLE);
    	value|=(LSM6DS0_ACC_GYRO_XEN_G_MASK|\
    					LSM6DS0_ACC_GYRO_YEN_G_MASK|\
    					LSM6DS0_ACC_GYRO_ZEN_G_MASK);
    	Accel_IO_Write(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG4,value);

   //ON HPF и LPF2 (Filters)
    value = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG2_G);
    value&=~LSM6DS0_ACC_GYRO_OUT_SEL_MASK;
    value|=LSM6DS0_ACC_GYRO_OUT_SEL_USE_HPF_AND_LPF2;
    Accel_IO_Write(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG2_G,value);

    //capacity
    uint8_t capacity=0;
    if(Freq==LSM6DS0_ACC_GYRO_ODR_G_15Hz||Freq==LSM6DS0_ACC_GYRO_ODR_G_60Hz )
    {
    	capacity=LSM6DS0_ACC_GYRO_BW_G_LOW;
    }

    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_119Hz)
       {
    	capacity=LSM6DS0_ACC_GYRO_BW_G_NORMAL;
       }
    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_238Hz)
       {
    	capacity=LSM6DS0_ACC_GYRO_BW_G_HIGH;
       }
    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_476Hz)
       {
    	capacity=LSM6DS0_ACC_GYRO_BW_G_ULTRA_HIGH;
       }
    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_952Hz)
       {
    	capacity=LSM6DS0_ACC_GYRO_BW_G_ULTRA_HIGH;
       }
    value = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
    value&=~LSM6DS0_ACC_GYRO_BW_G_MASK;
    value|=capacity;
    Accel_IO_Write(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);

    //Freq
        value = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
        value&=~LSM6DS0_ACC_GYRO_ODR_G_MASK;
        value|=Freq;
        Accel_IO_Write(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);

}

/*********************Get Accel LSM6DS0******************************/
void LSM6DS0_Gyro_GetXYZ(int16_t *Data)
{
	uint8_t buffer[6];
	uint8_t i=0;
	buffer[0]=Accel_IO_Read(0xD6,LSM6DS0_ACC_GYRO_OUT_X_L_G);
    buffer[1]=Accel_IO_Read(0xD6,LSM6DS0_ACC_GYRO_OUT_X_H_G);
    buffer[2]=Accel_IO_Read(0xD6,LSM6DS0_ACC_GYRO_OUT_Y_L_G);
    buffer[3]=Accel_IO_Read(0xD6,LSM6DS0_ACC_GYRO_OUT_Y_H_G);
    buffer[4]=Accel_IO_Read(0xD6,LSM6DS0_ACC_GYRO_OUT_Z_L_G);
    buffer[5]=Accel_IO_Read(0xD6,LSM6DS0_ACC_GYRO_OUT_Z_H_G);
    for(i=0;i<3;i++)
    {
      Data[i] = ((int16_t)((uint16_t)buffer[2*i+1]<<8)+buffer[2*i]);
    }


}


/*********************Get Accel LSM6DS0******************************/
void LSM6DS0_Accel_GetXYZ(int16_t *Data)
{
	uint8_t buffer[6];
	uint8_t i=0;
	buffer[0] = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_OUT_X_L_XL);
    buffer[1] = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_OUT_X_H_XL);
    buffer[2] = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_OUT_Y_L_XL);
    buffer[3] = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_OUT_Y_H_XL);
    buffer[4] = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_OUT_Z_L_XL);
    buffer[5] = Accel_IO_Read(ADDR_SLAVE_LSM6,LSM6DS0_ACC_GYRO_OUT_Z_H_XL);
    for(i=0;i<3;i++)
    {
        Data[i] = ((int16_t)((uint16_t)buffer[2*i+1]<<8)+buffer[2*i]);
    }

}

/*********************Read Accel LSM6DS0******************************/
void LSM6DS0_Accel_Read(void)
{
	int16_t dataAccel[3]={0};
	LSM6DS0_Accel_GetXYZ(dataAccel);
	int16_t x,y,z;
	x=dataAccel[0];
	y=dataAccel[1];
	z=dataAccel[2];
	dataLSM6DS0.DataAccel[0]=x;
	dataLSM6DS0.DataAccel[1]=y;
	dataLSM6DS0.DataAccel[2]=z;

}


/*********************Read Gyro LSM6DS0******************************/
void LSM6DS0_GyRo_Read(void)
{
	int16_t dataGyro[3]={0};
	LSM6DS0_Gyro_GetXYZ(dataGyro);
	int16_t gx,gy,gz;
	gx=dataGyro[0];
	gy=dataGyro[1];
	gz=dataGyro[2];
	dataLSM6DS0.DataGyro[0]=gx;
	dataLSM6DS0.DataGyro[1]=gy;
	dataLSM6DS0.DataGyro[2]=gz;
}

/************************Transmit Value as String***************************/
void Transmit_Value_Gyro_Accel_LSM6DS0(void)
{

	char buffer[64]={0};
	sprintf(buffer,"%d,%d,%d,%d,%d,%d\r\n",dataLSM6DS0.DataAccel[0],dataLSM6DS0.DataAccel[1],dataLSM6DS0.DataAccel[2],
										   dataLSM6DS0.DataGyro[0],dataLSM6DS0.DataGyro[1],dataLSM6DS0.DataGyro[2]);
	Transmit_String(buffer,strlen(buffer));


}



