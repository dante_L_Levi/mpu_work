/*
 * LoRa_868t20d.h
 *
 *  Created on: 1 авг. 2019 г.
 *      Author: Tatiana_Potrebko
 */

#ifndef LORA_868T20D_H_
#define LORA_868T20D_H_


#include "main.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"





#define AUX_Pin GPIO_PIN_12
#define AUX_GPIO_Port GPIOB
#define M1_LoRa_Pin GPIO_PIN_8
#define M1_LoRa_GPIO_Port GPIOA
#define M0_LoRa_Pin GPIO_PIN_13
#define M0_LoRa_GPIO_Port GPIOB


typedef enum
{
	M0_Bits=0,
	M1_Bits=1,

}GPIO_CONTROL_MBITS;

#define AUX_READ_SET	0
#define AUX_READ_RESET	1


#define BUFFER_PACKET_MAX	58

extern UART_HandleTypeDef huart1;
#define LoRaUart_Base huart1

//OPTION+
#define TRSM_TT_MODE		0x00	/*Transparent Transmission*/
#define TRSM_FP_MODE		0x01	/*Fixed-point transmission mode*/

#define OD_DRIVE_MODE		0x00
#define PP_DRIVE_MODE		0x01

typedef enum {
			  RET_SUCCESS = 0,
			  RET_ERROR_UNKNOWN,	/* something shouldn't happened */
			  RET_NOT_SUPPORT,
			  RET_NOT_IMPLEMENT,
			  RET_NOT_INITIAL,
			  RET_INVALID_PARAM,
			  RET_DATA_SIZE_NOT_MATCH,
			  RET_BUF_TOO_SMALL,
			  RET_TIMEOUT,
			  RET_HW_ERROR,
} RET_STATUS;

typedef enum
{
	MODE_NORMAL=0,
	MODE_WAKE_UP=1,
	MODE_POWER_SAVING=2,
	MODE_SLEEP=3

}LoRa_ModeDef;


typedef enum
{
	SAVE_CONFIG_POWERDOWN=0xC2,
	NOT_SAVE_CONFIG_POWERDOWN=0xC0,
}LoRa_868t20_SaveConfigDef;


typedef enum
{
	BPS_BAUD_300bps=0x00,
	BPS_BAUD_1200bps=0x01,
	BPS_BAUD_2400bps_default=0x02,
	BPS_BAUD_4800bps=0x03,
	BPS_BAUD_9600bps=0x04,
	BPS_BAUD_19200bps=0x05,


}LoRa_868t20_SPED_BPSDef;


typedef enum
{

	UART_BAUD_1200=0x00,
	UART_BAUD_2400=0x08,
	UART_BAUD_4800=0x10,
	UART_BAUD_9600_default=0x18,
	UART_BAUD_19200=0x20,
	UART_BAUD_38400=0x28,
	UART_BAUD_57600=0x30,
	UART_BAUD_115200=0x38,


}LoRa_868t20_SPED_UARTDef;

typedef enum
{

	UART_PARITY_8N1_default=0x00,
	UART_PARITY_8O1=0x40,
	UART_PARITY_8E1=0x80,

}LoRa_868t20_SPED_UART_PARITYDef;

typedef enum
{
	LoRa_868t20_FREQ_433MHz=0x17,
	LoRa_868t20_FREQ_470MHz=0xAC,
	LoRa_868t20_FREQ_868MHz=0x06,
	LoRa_868t20_FREQ_915MHz=0x0F,

}LoRa_868t20_CHANNEL_CONFIGDef;


typedef enum
{
	Option_21DBm=0x03,
	Option_24DBm=0x02,
	Option_27DBm=0x01,
	Option_30DBm_Default=0x00,
	Option_TurnFEC_off=0x00,
	Option_TurnFEC_ON_Default=0x04,
	Option_2000ms=0x38,
	Option_1750ms=0x30,
	Option_1500ms=0x28,
	Option_1250ms=0x20,
	Option_1000ms=0x18,
	Option_750ms=0x10,
	Option_500ms=0x08,
	Option_250ms_Default=0x00,
	IO_DRV_1=0x40,
	IO_DRV_0=0x00,
	Option_Transparent_Mode=0x00,
	Option_Fixed_Mode=0x80

}LoRa_868t20_OPTION_Def;
#define MAX_TX_SIZE		58

#define DEVICE_A_ADDR_H 0x05
#define DEVICE_A_ADDR_L 0x03
#define DEVICE_B_ADDR_H 0x05
#define DEVICE_B_ADDR_L 0x02


typedef struct
{
	uint8_t dataConfig[6];
	uint8_t Message_Transmit[MAX_TX_SIZE];
	uint8_t Message_Recieve[MAX_TX_SIZE];

}LoRa_E32;

#define Buffer_Read_PAYLOAD			32
#define UART_LoRa_hundler_Reciever()	__HAL_UART_ENABLE_IT(&LoRaUart_Base, UART_IT_RXNE)
/*************************Function Set M0/M1*****************************/
RET_STATUS LoRa_SetMBits(GPIO_CONTROL_MBITS stat);
/*************************Function Reset M0/M1*****************************/
RET_STATUS LoRa_ResetMBits(GPIO_CONTROL_MBITS stat);
/********************Config Pin Configuration******************/
void Init_Config_HRW_868t20d(void);
/********************Set Mode Work***********************/
RET_STATUS LoRa_ConfigModeWork(LoRa_ModeDef mode);
/**************Function Transmit Command********************/
void Get_Configuration_LoRa(void);
/*************************Read Data LoRa Config****************************/
void Helper_Read_Config_LoRa(void);
/*******************Config Standart LoRa 433MHz**********************/
void Set_Default_433MHz_LoRa(void);
/**********************Transmit LoRa*****************************/
void LoRa_TransmitData(uint8_t *dt,uint8_t length);
/*************************Read Data LoRa *********************************/
void Helper_Read_PAYLOAD_LoRa(void);
/*********************This function handles USART1 global interrupt*******************/
void USART1_IRQHandler_LoRa(void);



#endif /* LORA_868T20D_H_ */
