#ifndef __PCF8574_H
#define __PCF8574_H

#include "Main.h"
#include "stm32f4xx_hal.h"





#define e_set()   pcf8574_wr_byte(portlcd|=0x04) // установка линии E в 1
#define e_reset()   pcf8574_wr_byte(portlcd&=~0x04)  // установка линии E в 0
#define rs_set()    pcf8574_wr_byte(portlcd|=0x01) // установка линии RS в 1
#define rs_reset()   pcf8574_wr_byte(portlcd&=~0x01)  // установка линии RS в 0
#define setled()    pcf8574_wr_byte(portlcd|=0x08) // включение подсветки
#define setwrite()   pcf8574_wr_byte(portlcd&=~0x02)  // установка записи в память дисплея



#define PIN_P0		0x01
#define PIN_P1		0x02
#define PIN_P2		0x03
#define PIN_P3		0x04
#define PIN_P4		0x05
#define PIN_P5		0x06
#define PIN_P6		0x07
#define PIN_P7		0x08





/*********************Prototype Function************************/


/*****************Function Write Byte *****************/
void pcf8574_wr_byte(uint8_t byte);

/***************Function Send Half Byte******************/
void pcf8574_SendHalfByte(uint8_t c);
/***************Function SendByte******************/
void pcf8574_SendByteLCD(uint8_t c,uint8_t mode);
/***************Function Clear LCD******************/
void LCD_Clear(void);
/***************Function Send Char LCD******************/
void LCD_SendChar(char ch);
/***************Function GO XY LCD******************/
void LCD_GoXY(uint8_t x, uint8_t y);
/***************Function INIT LCD******************/
void LCD_ini(void);
/***************Function STring Print LCD******************/
void LCD_SendString(char* st);


/***************Function Pinout pcf8674******************/
void pcf8574_SetPin(uint8_t addrDev,uint8_t Pinout);
/***************Function Reset Pinout pcf8674******************/
void pcf8574_ResetPin(uint8_t addrDev,uint8_t Pinout);



#endif /* __PCF8574_H */


