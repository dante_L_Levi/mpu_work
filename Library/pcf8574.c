#include "pcf8574.h"

uint8_t buf[1]={0};
extern I2C_HandleTypeDef hi2c3;
char str1[100];
uint8_t portlcd; //ячейка для хранения данных порта микросхемы расширения

/**********************MicroDelay*****************************/
__STATIC_INLINE void DelayMicro(__IO uint32_t micros)
{
           micros *= (SystemCoreClock / 1000000) / 5;
   /* Wait till done */
   while (micros--);
}

/*****************Function Write Byte *****************/
void pcf8574_wr_byte(uint8_t byte)
{
	buf[0]=byte;
	HAL_I2C_Master_Transmit(&hi2c3,(uint16_t)0x4E,buf,1,1000);
	
}

/***************Function Pinout pcf8674******************/
void pcf8574_SetPin(uint8_t addrDev,uint8_t Pinout)
{
	buf[0]=Pinout;
	HAL_I2C_Master_Transmit(&hi2c3,(uint16_t)addrDev,buf,1,1000);
}

/***************Function Reset Pinout pcf8674******************/
void pcf8574_ResetPin(uint8_t addrDev,uint8_t Pinout)
{
	buf[0]&=~Pinout;
	HAL_I2C_Master_Transmit(&hi2c3,(uint16_t)addrDev,buf,1,1000);
}


/***************Function Send Half Byte******************/
void pcf8574_SendHalfByte(uint8_t c)
{
	c<<=4;
	e_set();//On line E
	DelayMicro(50);
	pcf8574_wr_byte(portlcd|c);
	e_reset();//off line E
	DelayMicro(50);
	
}

/***************Function SendByte******************/
void pcf8574_SendByteLCD(uint8_t c,uint8_t mode)
{
	 if (mode==0) rs_reset();
	 else         rs_set();
	 uint8_t hc=0;
		hc=c>>4;
		pcf8574_SendHalfByte(hc);pcf8574_SendHalfByte(c);
}

/***************Function Clear LCD******************/
void LCD_Clear(void)
{
  pcf8574_SendByteLCD(0x01, 0);
  HAL_Delay(2);
}
/***************Function Send Char LCD******************/
void LCD_SendChar(char ch)
{
  pcf8574_SendByteLCD(ch,1);
}
/***************Function GO XY LCD******************/
void LCD_GoXY(uint8_t x, uint8_t y)
{
        switch(y)
        {
                case 0:
                  pcf8574_SendByteLCD(x|0x80,0);
                  HAL_Delay(1);
                  break;
                case 1:
                  pcf8574_SendByteLCD((0x40+x)|0x80,0);
                  HAL_Delay(1);
                  break;
                case 2:
                  pcf8574_SendByteLCD((0x14+x)|0x80,0);
                  HAL_Delay(1);
                  break;
                case 3:
                  pcf8574_SendByteLCD((0x54+x)|0x80,0);
                  HAL_Delay(1);
                  break;
        }
}
/***************Function INIT LCD******************/
void LCD_ini(void)
{
        HAL_Delay(15);
        pcf8574_SendHalfByte(0x03);
        HAL_Delay(4);
        pcf8574_SendHalfByte(0x03);
        DelayMicro(100);
        pcf8574_SendHalfByte(0x03);
        HAL_Delay(1);
        pcf8574_SendHalfByte(0x02);
        HAL_Delay(1);
        pcf8574_SendByteLCD(0x28, 0); //4бит-режим (DL=0) и 2 линии (N=1)
        HAL_Delay(1);
        pcf8574_SendByteLCD(0x0C, 0); //включаем изображение на дисплее (D=1), курсоры никакие не включаем (C=0, B=0)
        HAL_Delay(1);
				pcf8574_SendByteLCD(0x01,0);//уберем мусор
				HAL_Delay(2);
        pcf8574_SendByteLCD(0x6, 0); //курсор (хоть он у нас и невидимый) будет двигаться влево
        HAL_Delay(1);
        setled();//подсветка
        setwrite();//запись
}

/***************Function STring Print LCD******************/
void LCD_SendString(char* st)
{
        uint8_t  i=0;
        while(st[i]!=0)
        {
                LCD_SendChar(st[i]);
                i++;
        }
}













