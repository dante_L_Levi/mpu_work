/*
 * TM1637G.h
 *
 *  Created on: 13 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef TM1637G_H_
#define TM1637G_H_


#include "stm32F4xx_hal.h"
#include "main.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "GPIO_LIB.h"


#define DELAY  100
#define ADDR_AUTO  0x40
#define ADDR_FIXED 0x44
#define STARTADDR  0xc0


/**************definitions for brightness***********************/
#define  BRIGHT_DARKEST 0
#define  BRIGHT_TYPICAL 2
#define  BRIGHTEST      7


/************definitions for type of the 4-Digit Display*********************/
#define D4036B 							0
#define D4056A 							1
//Special characters index of tube table
#define INDEX_NEGATIVE_SIGN				16
#define INDEX_BLANK			     		0 // 17 nothing ,0  zero beter for clock

#define TM_DATA_Pin GPIO_PIN_6
#define TM_DATA_GPIO_Port GPIOA
#define TM_CLOCK_Pin GPIO_PIN_7
#define TM_CLOCK_GPIO_Port GPIOA


#define Set_CLK_Pin() 		GPIO_SetBits(TM_CLOCK_GPIO_Port,TM_CLOCK_Pin)
#define Reset_CLK_Pin() 	GPIO_ResetBits(TM_CLOCK_GPIO_Port,TM_CLOCK_Pin)
#define Set_DATA_Pin()   	GPIO_SetBits(TM_DATA_GPIO_Port,TM_DATA_Pin)
#define Reset_DATA_Pin() 	GPIO_ResetBits(TM_DATA_GPIO_Port,TM_DATA_Pin)
#define Read_DATA_Pin() 	GPIO_Read(TM_DATA_GPIO_Port,TM_DATA_Pin)


/************************Function Set Stop Flag(Transmit MLS Byte <next HLS Byte)****************************/
void TM1637_WriteData(uint8_t data);//�������� ���������� � ��������!!!�� ��������
/*************************Function Coder Number Place***************************/
int8_t TM1637_coding(uint8_t DispData);// �������� ���������
/*************************Function Coder Number Place for Array Number***************************/
void TM1637_coding_all(uint8_t DispData[]);//�������� ���������
/********************Function Split Number on Segments***********************/
void separate_Digit_to_digits(int16_t Digit,uint8_t SegArray[]);
/******************************Function Indication Segment in place************************/
void TM1637_display(uint8_t Seg_N,int8_t DispData);
//=========================================================================//
//++++++++++++++++++++���������� ������� �������+++++++++++++++++++++++++++//
//=========================================================================//
void TM1637_brightness(uint8_t brightness);
//=========================================================================//
//+++++++++++++++++++++++������� �������+++++++++++++++++++++++++++++++++++//
//=========================================================================//
void TM1637_clearDisplay(void);
//=========================================================================//
//++++++++++++++++++++����������� ����� � �����++++++++++++++++++++++++++++//
//=========================================================================//
void TM1637_displayTime(uint8_t hours,uint8_t minutes) ;
void tik_delay(uint32_t i);
/******************************Function Indication numbers -999....9999************************/
void TM1637_display_all(uint16_t Digit);



#endif /* TM1637G_H_ */
