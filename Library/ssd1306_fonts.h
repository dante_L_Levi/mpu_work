/*
 * ssd1306.h
 *
 *  Created on: Jul 21, 2019
 *      Author: Tatiana_Potrebko
 */

#ifndef SSD1306_H_
#define SSD1306_H_



#include <stdint.h>

typedef struct
{
	const uint8_t FontWidth;    /*!< Font width in pixels */
	uint8_t FontHeight;   /*!< Font height in pixels */
	const uint16_t *data; /*!< Pointer to data font data array */
} FontDef;


extern FontDef Font_7x10;
extern FontDef Font_11x18;
extern FontDef Font_16x26;





#endif /* SSD1306_H_ */
