#include "AT24C32.h"
extern I2C_HandleTypeDef 	hi2c1;
#define HANDLE_AT24xxx		hi2c1



/***********************Check Connected Devices***************************/
AT24XXStatusDef	EEPROM24XX_IsConnected(void)
{
	#if	(_EEPROM_USE_WP_PIN==1)
	HAL_GPIO_WritePin(_EEPROM_WP_GPIO,_EEPROM_WP_PIN,GPIO_PIN_SET);
	#endif
	if(HAL_I2C_IsDeviceReady(&HANDLE_AT24xxx,AT24XXX_ADDR,1,100)==HAL_OK)
	{
		return AT24xx_Connect_OK;
	}
	else
	{
		return AT24xx_ERROR;
	}
}


/***************************Save Values in EEPROM**********************************/
AT24XXStatusDef EEPROM24XX_Save(uint16_t Address,void *data,size_t size_of_data)
{
#if ((_EEPROM_SIZE_KBIT==1) || (_EEPROM_SIZE_KBIT==2))
	if(size_of_data > 8)
		return AT24xx_ERROR;
	#elif ((_EEPROM_SIZE_KBIT==4) || (_EEPROM_SIZE_KBIT==8) || (_EEPROM_SIZE_KBIT==16))
	if(size_of_data > 16)
		return AT24xx_ERROR;
	#else
	if(size_of_data > 32)
		return AT24xx_ERROR;
	#endif

	#if	(_EEPROM_USE_WP_PIN==1)
	HAL_GPIO_WritePin(_EEPROM_WP_GPIO,_EEPROM_WP_PIN,GPIO_PIN_RESET);
	#endif

	/*******************************************************************************************/
#if ((_EEPROM_SIZE_KBIT==1) || (_EEPROM_SIZE_KBIT==2))
	I2Cx_MemWrite(HANDLE_AT24xxx,AT24XXX_ADDR,Address,data,size_of_data);
	#elif	(_EEPROM_SIZE_KBIT==4)
		I2Cx_MemWrite(HANDLE_AT24xxx,AT24XXX_ADDR|(Address&0x0001),Address>>1,data,size_of_data);
	#elif	(_EEPROM_SIZE_KBIT==8)
		I2Cx_MemWrite(HANDLE_AT24xxx,AT24XXX_ADDR|(Address&0x0003),Address>>2,data,size_of_data);
	#elif	(_EEPROM_SIZE_KBIT==16)
		I2Cx_MemWrite(HANDLE_AT24xxx,AT24XXX_ADDR|(Address&0x0007),Address>>3,data,size_of_data);
	#else
		I2Cx_MemWrite(HANDLE_AT24xxx,AT24XXX_ADDR,Address,data,size_of_data);
	#endif

			HAL_Delay(7);

		#if	(_EEPROM_USE_WP_PIN==1)
		HAL_GPIO_WritePin(_EEPROM_WP_GPIO,_EEPROM_WP_PIN,GPIO_PIN_SET);
		#endif
		#if	(_EEPROM_USE_WP_PIN==1)
		HAL_GPIO_WritePin(_EEPROM_WP_GPIO,_EEPROM_WP_PIN,GPIO_PIN_SET);
		#endif
		return AT24xx_Save_OK;

}



/***************************Save Values from EEPROM**********************************/
AT24XXStatusDef EEPROM24XX_Load(uint16_t Address,void *data,size_t size_of_data)
{
#if	(_EEPROM_USE_WP_PIN==1)
	HAL_GPIO_WritePin(_EEPROM_WP_GPIO,_EEPROM_WP_PIN,GPIO_PIN_SET);
	#endif
	#if ((_EEPROM_SIZE_KBIT==1) || (_EEPROM_SIZE_KBIT==2))
	if(HAL_I2C_Mem_Read(&_EEPROM24XX_I2C,0xa0,Address,I2C_MEMADD_SIZE_8BIT,(uint8_t*)data,size_of_data,100) == HAL_OK)
	#elif (_EEPROM_SIZE_KBIT==4)
	if(HAL_I2C_Mem_Read(&_EEPROM24XX_I2C,0xa0|(Address&0x0001),Address>>1,I2C_MEMADD_SIZE_8BIT,(uint8_t*)data,size_of_data,100) == HAL_OK)
	#elif (_EEPROM_SIZE_KBIT==8)
	if(HAL_I2C_Mem_Read(&_EEPROM24XX_I2C,0xa0|(Address&0x0003),Address>>2,I2C_MEMADD_SIZE_8BIT,(uint8_t*)data,size_of_data,100) == HAL_OK)
	#elif (_EEPROM_SIZE_KBIT==16)
	if(HAL_I2C_Mem_Read(&_EEPROM24XX_I2C,0xa0|(Address&0x0007),Address>>3,I2C_MEMADD_SIZE_8BIT,(uint8_t*)data,size_of_data,100) == HAL_OK)
	#else
		if(HAL_I2C_Mem_Read(&HANDLE_AT24xxx,AT24XXX_ADDR,Address,I2C_MEMADD_SIZE_16BIT,(uint8_t*)data,size_of_data,100) == HAL_OK)
	#endif
		return AT24xx_Load_OK;
	else
		return AT24xx_ERROR;
}











