#include "DS3231.h"

extern I2C_HandleTypeDef hi2c1;
#define HANDLE_DS3231		hi2c1;

uint8_t BufferRTC[20]={0};

/**********************Convert Value to Decimal*************************/
uint8_t RTC_ConvertFromDec(uint8_t c)
{
  uint8_t ch = ((c>>4)*10+(0x0F&c));
        return ch;
}

/**********************Convert Value to BinDin*************************/
uint8_t RTC_ConvertFromBinDec(uint8_t c)
{
        uint8_t ch = ((c/10)<<4)|(c%10);
        return ch;
}

/*********************Read All RTC*******************************/
void Read_RTCRegisters(void)
{
	 I2C_ReadBuffer(hi2c1,(uint16_t)DS3231_ADDR,BufferRTC,19);

}


/*****************************Get Date Value***********************/
uint8_t GetDate(void)
{
	uint8_t date=0;
	date=BufferRTC[4];
	date = RTC_ConvertFromDec(date);
	return date;
}

/*****************************Get Month Value***********************/
uint8_t GetMonth(void)
{
	uint8_t Month=0;
	Month=BufferRTC[5];
	Month = RTC_ConvertFromDec(Month);
	return Month;
}

/*****************************Get Year Value***********************/
uint8_t GetYear(void)
{
	uint8_t Year=0;
	Year=BufferRTC[6];
	Year = RTC_ConvertFromDec(Year);
	return Year;
}



/*****************************Get Day Value***********************/
uint8_t GetDay(void)
{
	uint8_t day=0;
	day=BufferRTC[3];
	day = RTC_ConvertFromDec(day);
	return day;
}

/*****************************Get HourValue***********************/
uint8_t GetHour(void)
{
	uint8_t Hour=0;
	Hour=BufferRTC[2];
	Hour = RTC_ConvertFromDec(Hour);
	return Hour;
}

/*****************************Get minuts Value***********************/
uint8_t GetMin(void)
{
	uint8_t Min=0;
	Min=BufferRTC[1];
	Min = RTC_ConvertFromDec(Min);
	return Min;
}


/*****************************Get seconds Value***********************/
uint8_t GetSec(void)
{
	uint8_t Sec=0;
	Sec=BufferRTC[0];
	Sec = RTC_ConvertFromDec(Sec);
	return Sec;
}




















