/*
 * SDD1306.h
 *
 *  Created on: Jul 21, 2019
 *      Author: Tatiana_Potrebko
 */

#ifndef SDD1306_H_
#define SDD1306_H_

#include "main.h"
#include "stdio.h"
#include <string.h>
#include "stm32l1xx_hal.h"
#include "ssd1306_fonts.h"

#define SSD1306_I2C_ADDR        (0x3C << 1)
#define SSD1306_Reset_Port      GPIOA
#define SSD1306_Reset_Pin       GPIO_PIN_7

#define SSD1306_HEIGHT          64
#define SSD1306_WIDTH           128


// Enumeration for screen colors
typedef enum {
    Black = 0x00, // Black color, no pixel
    White = 0x01  // Pixel is set. Color depends on OLED
} SSD1306_COLOR;



// Struct to store transformations
typedef struct {
    uint16_t CurrentX;
    uint16_t CurrentY;
    uint8_t Inverted;
    uint8_t Initialized;
} SSD1306_t;



/******************Init SDD 1306******************************/
void ssd1306_Init(void);
/********************function Fill Screen************************/
void ssd1306_Fill(SSD1306_COLOR color);
/********************function Update************************/
void ssd1306_UpdateScreen(void) ;
// Write full string to screenbuffer
char ssd1306_WriteString(char* str, FontDef Font, SSD1306_COLOR color);
// Position the cursor
void ssd1306_SetCursor(uint8_t x, uint8_t y);

// Draw 1 char to the screen buffer
// ch         => char om weg te schrijven
// Font     => Font waarmee we gaan schrijven
// color     => Black or White
char ssd1306_WriteChar(char ch, FontDef Font, SSD1306_COLOR color);


//    Draw one pixel in the screenbuffer
//    X => X Coordinate
//    Y => Y Coordinate
//    color => Pixel color
void ssd1306_DrawPixel(uint8_t x, uint8_t y, SSD1306_COLOR color);

void ssd1306_TestAll();
void ssd1306_TestFPS();
void ssd1306_TestFonts();
void ssd1306_TestBorder();


#endif /* SDD1306_H_ */
