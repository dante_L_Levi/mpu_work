#include "Main.h"


void GPIO_Init(void)
{
    SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);   //system clock
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);    // enable port F
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);    //config output Pin

    GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);  // make F4 an input
    GPIOPadConfigSet(GPIO_PORTF_BASE,GPIO_PIN_4,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);


}




int main(void)
{
    GPIO_Init();


    while(1)
    {

        uint32_t btnVal=0;
        btnVal=GPIOPinRead(GPIO_PORTF_BASE,GPIO_PIN_4);//Read button

        if((btnVal&GPIO_PIN_4)==0)
        {
            GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,0xFF);
        }
        else
        {
            GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1,0x00);
            GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_2,0xFF);
        }


    }


}


