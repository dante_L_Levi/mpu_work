#include "Main.h"

// Number of I2C data packets to send.
#define NUM_I2C_DATA 3

//*****************************************************************************
//
// Set the address for slave module. This is a 7-bit address sent in the
// following format:
//                      [A6:A5:A4:A3:A2:A1:A0:RS]
//
// A zero in the "RS" position of the first byte means that the master
// transmits (sends) data to the selected slave, and a one in this position
// means that the master receives data from the slave.
//
//*****************************************************************************
#define SLAVE_ADDRESS 0x3C

void InitConsole(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
      GPIOPinConfigure(GPIO_PA0_U0RX);
      GPIOPinConfigure(GPIO_PA1_U0TX);
      UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);
      GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
      //UARTStdioConfig(0, 115200, 16000000);
}

void Init_I2C(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);//On pripheral I2C
        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
        // Configure the pin muxing for I2C0 functions on port B2 and B3.
        // This step is not necessary if your part does not support pin muxing.
        GPIOPinConfigure(GPIO_PB2_I2C0SCL);
        GPIOPinConfigure(GPIO_PB3_I2C0SDA);
        GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
        GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);
        // Enable loopback mode.
        I2CLoopbackEnable(I2C0_BASE);
        // Enable and initialize the I2C0 master module
        I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), false);
        // Enable the I2C0 slave module
        I2CSlaveEnable(I2C0_BASE);
        // Set the slave address to SLAVE_ADDRESS
        I2CSlaveInit(I2C0_BASE, SLAVE_ADDRESS);
        I2CMasterSlaveAddrSet(I2C0_BASE, SLAVE_ADDRESS, false);

}



void main(void)
{
    uint32_t pui32DataTx[NUM_I2C_DATA];
    uint32_t pui32DataRx[NUM_I2C_DATA];
    uint32_t ui32Index;
    SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
                       SYSCTL_XTAL_16MHZ);
    InitConsole();
    Init_I2C();

    pui32DataTx[0] = 'I';
    pui32DataTx[1] = '2';
    pui32DataTx[2] = 'C';

    for(ui32Index = 0; ui32Index < NUM_I2C_DATA; ui32Index++)
        {
            pui32DataRx[ui32Index] = 0;
        }


    for(ui32Index = 0; ui32Index < NUM_I2C_DATA; ui32Index++)
    {
        //Transmit Data I2C
        I2CMasterDataPut(I2C0_BASE, pui32DataTx[ui32Index]);

                //
                // Initiate send of data from the master.  Since the loopback
                // mode is enabled, the master and slave units are connected
                // allowing us to receive the same data that we sent out.
                //
                I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_SEND);

                //
                // Wait until the slave has received and acknowledged the data.
                //
                while(!(I2CSlaveStatus(I2C0_BASE) & I2C_SLAVE_ACT_RREQ))
                {
                }


          // Read the data from the slave.
          //
          pui32DataRx[ui32Index] = I2CSlaveDataGet(I2C0_BASE);
          //
                  // Wait until master module is done transferring.
                  //
                  while(I2CMasterBusy(I2C0_BASE))
                  {
                  }
    }


    //
        // Reset receive buffer.
        //
        for(ui32Index = 0; ui32Index < NUM_I2C_DATA; ui32Index++)
        {
            pui32DataRx[ui32Index] = 0;
        }

        //
            // Modifiy the data direction to true, so that seeing the address will
            // indicate that the I2C Master is initiating a read from the slave.
            //
            I2CMasterSlaveAddrSet(I2C0_BASE, SLAVE_ADDRESS, true);

            //
                // Do a dummy receive to make sure you don't get junk on the first receive.
                //
                I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);

                //
                // Dummy acknowledge and wait for the receive request from the master.
                // This is done to clear any flags that should not be set.
                //
                while(!(I2CSlaveStatus(I2C0_BASE) & I2C_SLAVE_ACT_TREQ))
                {
                }
                for(ui32Index = 0; ui32Index < NUM_I2C_DATA; ui32Index++)
                {
                    //
                            // Place the data to be sent in the data register
                            //
                            I2CSlaveDataPut(I2C0_BASE, pui32DataTx[ui32Index]);

                            //
                            // Tell the master to read data.
                            //
                            I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);

                            //
                            // Wait until the slave is done sending data.
                            //
                            while(!(I2CSlaveStatus(I2C0_BASE) & I2C_SLAVE_ACT_TREQ))
                            {
                            }

                            //
                            // Read the data from the master.
                            //
                            pui32DataRx[ui32Index] = I2CMasterDataGet(I2C0_BASE);
                }






	while(1)
	{

	}
}
