#include "Main.h"
uint32_t ui32SysClock;
//! - M0PWM4 - PG0
//! - M0PWM1 - PF1
//! - M0PWM2 - PF2


void Init_RCC(void)
{
    //Set Clock MPU
       ui32SysClock=SysCtlClockFreqSet ((SYSCTL_XTAL_25MHZ |
               SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
               SYSCTL_CFG_VCO_480), 40000000);
}



void PWM_Init(void)
{
    //Enable clock PWM0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    //Enable clock Output GPIO
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
    GPIOPinConfigure(GPIO_PG0_M0PWM4);
    GPIOPinConfigure(GPIO_PF1_M0PWM1);
    GPIOPinConfigure(GPIO_PF2_M0PWM2);
    //Set PWM Type
    GPIOPinTypePWM(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2);
    GPIOPinTypePWM(GPIO_PORTG_BASE,GPIO_PIN_0);
    //Set Divider Clock
    PWMClockSet(PWM0_BASE,PWM_SYSCLK_DIV_8);
    //Config PWM
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_UP_DOWN|PWM_GEN_MODE_NO_SYNC);
    PWMGenConfigure(PWM0_BASE, PWM_GEN_1, PWM_GEN_MODE_UP_DOWN|PWM_GEN_MODE_NO_SYNC);
    PWMGenConfigure(PWM0_BASE, PWM_GEN_2, PWM_GEN_MODE_UP_DOWN|PWM_GEN_MODE_NO_SYNC);
    // Set the PWM period to 100Hz.  To calculate the appropriate parameter
            // use the following equation: N = (1 / f) * SysClk.  Where N is the
            // function parameter, f is the desired frequency, and SysClk is the
            // system clock frequency.
            // In this case you get: (1 / 100Hz) * (40MHz/8) = 50000 cycles.  Note that
            // the maximum period you can set is 2^16.
            // TODO: modify this calculation to use the clock frequency that you are
            // using.
            //
            PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 50000);
            PWMGenPeriodSet(PWM0_BASE, PWM_GEN_1, 50000);
            PWMGenPeriodSet(PWM0_BASE, PWM_GEN_2, 50000);
            // Enable the PWM0 Bit4 (PG0) output signal.
            PWMOutputState(PWM0_BASE, PWM_OUT_4_BIT, true);
            // Enable the PWM0 Bit1 (PF1) output signal.
            PWMOutputState(PWM0_BASE, PWM_OUT_1_BIT, true);
            // Enable the PWM0 Bit1 (PF2) output signal.
            PWMOutputState(PWM0_BASE, PWM_OUT_2_BIT, true);
            //set 25% Duty(50000=100%)->25%->12500
            PWMPulseWidthSet(PWM0_BASE, PWM_OUT_4, 12500);
            //set 50% Duty(50000=100%)->50%->25000
            PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 40000);
            //set 75% Duty(50000=100%)->75%->37500
            PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2, 2500);
            // Enable the PWM generator block.
            //
            PWMGenEnable(PWM0_BASE, PWM_GEN_0);
            PWMGenEnable(PWM0_BASE, PWM_GEN_1);
            PWMGenEnable(PWM0_BASE, PWM_GEN_2);
}



void main(void)
{
    Init_RCC();
    PWM_Init();

    while(1)
    {

    }
}

