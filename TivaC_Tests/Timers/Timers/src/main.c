#include "Main.h"

bool taggle=false;


void Int_TIM0(void)
{
    TimerIntClear(TIMER0_BASE,TIMER_TIMA_TIMEOUT);
    taggle!=taggle;
}




void GPIO_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);//on clk GIOF
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE,GPIO_PIN_2);//pin 2 out
    GPIOPadConfigSet(GPIO_PORTF_BASE,GPIO_PIN_2,GPIO_STRENGTH_12MA,GPIO_PIN_TYPE_STD);

}



void Timer_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0); //on clk Timer0
    TimerConfigure(TIMER0_BASE,TIMER_CFG_PERIODIC);//set mode work
    TimerLoadSet(TIMER0_BASE,TIMER_A,SysCtlClockGet()/2);//set value calculate count
    TimerIntRegister(TIMER0_BASE,TIMER_A,Int_TIM0);//register interrupt timmer
    TimerIntEnable(TIMER0_BASE,TIMER_TIMA_TIMEOUT);
    TimerEnable(TIMER0_BASE,TIMER_A);//Timer ON



}



int main(void)
{
    SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);   //system clock
    GPIO_Init();
    Timer_Init();


    while(1)
    {
            if(taggle)
            {
                GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_2,0xFF);
            }
            else
            {
                GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_2,0x00);
            }


    }
}


