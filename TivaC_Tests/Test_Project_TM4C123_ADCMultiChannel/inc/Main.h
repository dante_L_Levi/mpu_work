/*
 * Main.h
 *
 *  Created on: 8 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_MAIN_H_
#define INC_MAIN_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"
#include "driverlib/adc.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"
#include "driverlib/i2c.h"


#include "InitPeripheral.h"
#include "DataTransfer.h"


/*********************Handle Recieve  UART 7*******************/
void Handler_UART7(void);
/*********************Handle ADC*******************/
void Handler_ADC(void);
/*********************Handle TIM1*******************/
void handler_TIM1(void);
/***********************Hundler ADC Cheannle**************************/
void Handler_ADC0_Channel(void);


#endif /* INC_MAIN_H_ */
