/*
 * main.c
 *
 *  Created on: 8 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */


#include "Main.h"

GPIO_Def LED_R;
GPIO_Def LED_G;
GPIO_Def LED_B;

uint32_t CLOCK_SYS;
uint32_t AdcValues[2]={0};
uint32_t Temp;
char buff[100];
uint8_t count_Led=0;


void Transmit_ADCValue(void);
void LED_Indication(uint8_t c);


/******************Hundler Inner Temp Sensor************************/
void Handler_ADC(void)
{

}
/***********************Hundler ADC Cheannle**************************/
void Handler_ADC0_Channel(void)
{
    ADCSequenceDataGet(ADC0_BASE, 1, AdcValues);

}


/********************Hundler TIM1**************************************/
void handler_TIM1(void)
{
    if(count_Led>2)
    {
        count_Led=0;
    }
    LED_Indication(count_Led);
    count_Led++;
    Transmit_ADCValue();
    TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
}

void Transmit_ADCValue(void)
{
    float adcf[2]={0};
    adcf[0]=AdcValues[0]*3.3/4096.0;
    adcf[1]=AdcValues[1]*3.3/4096.0;
    uint32_t data[2]={0};
    data[0]=(uint32_t)(adcf[0]*1000);
    data[1]=(uint32_t)(adcf[1]*1000);
    sprintf(buff,"%u|%u\r\n",data[0],data[1]);
    Transmit_UartString(UART2_BASE,buff);
}

void LED_Indication(uint8_t c)
{
    switch(c)
    {
        case 0:
        {
            SetGPIO_Bit(&LED_R,0x02);
            SetGPIO_Bit(&LED_G,0x00);
            SetGPIO_Bit(&LED_B,0x00);
            break;
        }
        case 1:
        {
            SetGPIO_Bit(&LED_R,0x00);
            SetGPIO_Bit(&LED_G,0x04);
            SetGPIO_Bit(&LED_B,0x00);
            break;
        }
        case 2:
        {
            SetGPIO_Bit(&LED_R,0x00);
            SetGPIO_Bit(&LED_G,0x00);
            SetGPIO_Bit(&LED_B,0x08);
            break;
        }
        default:
        {
            SetGPIO_Bit(&LED_R,0x00);
            SetGPIO_Bit(&LED_G,0x00);
            SetGPIO_Bit(&LED_B,0x00);
            break;
        }
    }
}

void RCC_Init(void)
{
    // Set the clocking to run directly from the PLL at 20 MHz.
        // The following code:
        // -sets the system clock divider to 10 (200 MHz PLL divide by 10 = 20 MHz)
        // -sets the system clock to use the PLL
        // -uses the main oscillator
        // -configures for use of 16 MHz crystal/oscillator input
    SysCtlClockSet(SYSCTL_SYSDIV_10 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                       SYSCTL_XTAL_16MHZ);
}


void main(void)
{
    RCC_Init();
    CLOCK_SYS=SysCtlClockGet();
    GPIO_Init();
    Init_Uart2Base(115200);
    //Init_ADC_Channels();
    //Init_Inter_TempSensor();
    Tim1_Init();
    IntMasterEnable();
    while(true)
    {
        //ADCProcessorTrigger(ADC0_BASE, 1);

    }
}
