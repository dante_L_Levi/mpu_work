/*
 * InitPeripheral.c
 *
 *  Created on: 8 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */


#include "InitPeripheral.h"

extern GPIO_Def LED_R;
extern GPIO_Def LED_G;
extern GPIO_Def LED_B;
extern uint32_t CLOCK_SYS;

void InitGpio_Struct(void)
{
    LED_R.Port=PORT_RGB;
    LED_R.Pin=GPIO_PIN_1;
    LED_R.statusgpio=GPIO_Output;
    LED_R.valuepin=false;

    LED_G.Port=PORT_RGB;
    LED_G.Pin=GPIO_PIN_2;
    LED_G.statusgpio=GPIO_Output;
    LED_G.valuepin=false;

    LED_B.Port=PORT_RGB;
    LED_B.Pin=GPIO_PIN_3;
    LED_B.statusgpio=GPIO_Output;
    LED_B.valuepin=false;

}

/**********************Init GPIO*********************************/
void GPIO_Init(void)
{
    InitGpio_Struct();
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(LED_R.Port,LED_R.Pin);
    GPIOPinTypeGPIOOutput(LED_G.Port,LED_G.Pin);
    GPIOPinTypeGPIOOutput(LED_B.Port,LED_B.Pin);
    GPIOPinWrite(LED_R.Port,LED_R.Pin, 0x00);
    GPIOPinWrite(LED_G.Port,LED_G.Pin, 0x00);
    GPIOPinWrite(LED_B.Port,LED_B.Pin, 0x00);
}


/**********************Set Value GPIO Output***********************/
void SetGPIO_Bit(GPIO_Def *out,uint8_t val)
{
   GPIOPinWrite(out->Port,out->Pin, val);
   out->valuepin=~(out->valuepin);
}


/***********************Init UART 7*****************************/
void Init_Uart2Base(uint32_t Baud)
{
        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
        GPIOPinConfigure(GPIO_PD6_U2RX);
        GPIOPinConfigure(GPIO_PD7_U2TX);
        GPIOPinTypeUART(GPIO_PORTD_BASE,GPIO_PIN_6|GPIO_PIN_7);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD))
        {
        }
        SysCtlPeripheralEnable(SYSCTL_PERIPH_UART2);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART2))
        {
        }

        UARTClockSourceSet(UART2_BASE,UART_CLOCK_SYSTEM);
        UARTConfigSetExpClk(UART2_BASE, SysCtlClockGet(), Baud,
                           (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                            UART_CONFIG_PAR_NONE));
        //UARTIntRegister(UART7_BASE, Handler_UART7);
        //IntEnable(INT_UART7);
        //IntPrioritySet(INT_UART7, 1);
        //UARTIntEnable(UART7_BASE, UART_INT_RX | UART_INT_RT);

        UARTEnable(UART2_BASE);
}

/*****************************Init Inner Temperature Sensor*******************/
void Init_Inter_TempSensor(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);//init peripheral ADC
    ADCHardwareOversampleConfigure(ADC0_BASE, 64);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0))
        {
        }
    //Enable sample
    ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
    //config:Temerature internal,Interrupt Enable
    ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_TS|ADC_CTL_IE|ADC_CTL_END);
    //enable Messume
    ADCSequenceEnable(ADC0_BASE, 3);
    //enable interrupt
    ADCIntEnable(ADC0_BASE,3);
    ADCIntClear(ADC0_BASE, 3);
    ADCIntRegister(ADC0_BASE,3,Handler_ADC);
    IntEnable(INT_ADC0SS3);

}

/*****************************ADC Init Channels*******************/
void Init_ADC_Channels(void)
{

    SysCtlPeripheralReset(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE))
    {
    }
    GPIODirModeSet(GPIO_PORTE_BASE,GPIO_PIN_2 | GPIO_PIN_3,GPIO_DIR_MODE_HW);
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3|GPIO_PIN_2);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);//init peripheral ADC
    ADCHardwareOversampleConfigure(ADC0_BASE, 64);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0))
    {
    }
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE,1,0, ADC_CTL_CH0);
   ADCSequenceStepConfigure(ADC0_BASE,1,1, ADC_CTL_CH1| ADC_CTL_IE | ADC_CTL_END);

   ADCSequenceEnable(ADC0_BASE, 1);
   ADCIntEnable(ADC0_BASE,1);
   ADCIntClear(ADC0_BASE, 1);
   ADCIntRegister(ADC0_BASE,1,Handler_ADC0_Channel);
   IntEnable(INT_ADC0SS1);
   //-------------------------------------------------------------------------------



}




/********************timer update Data struct Motor & Led Status*****************/
void Tim1_Init(void)
{
    //Timer1 - 2Hz
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1))
    {
    }
    TimerClockSourceSet(TIMER1_BASE, TIMER_CLOCK_SYSTEM);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_A_PERIODIC);
    TimerLoadSet(TIMER1_BASE, TIMER_A, CLOCK_SYS/2);
    TimerIntRegister(TIMER1_BASE, TIMER_A, handler_TIM1);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    TimerEnable(TIMER1_BASE, TIMER_A);
    //NVIC
    IntEnable(INT_TIMER1A);

}
