/*
 * main.c
 *
 *  Created on: 27 ���. 2020 �.
 *      Author: AlexPirs
 */


#include "main.h"


uint32_t CLOCK_SYS;

/********************Hundler TIM1**************************************/
void handler_TIM1(void)
{


    TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
}



void RCC_Init(void)
{
    // Set the clocking to run directly from the PLL at 20 MHz.
        // The following code:
        // -sets the system clock divider to 10 (200 MHz PLL divide by 10 = 20 MHz)
        // -sets the system clock to use the PLL
        // -uses the main oscillator
        // -configures for use of 16 MHz crystal/oscillator input
    SysCtlClockSet(SYSCTL_SYSDIV_10 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                       SYSCTL_XTAL_16MHZ);
}

void main(void)
{
    RCC_Init();
    Init_Uart2Base(115200);
    Tim1_Init();
    IntMasterEnable();

    while(1)
    {

    }
}

