#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"

void GPIO_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);    // enable port N
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);    // enable port N
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1); //config output Pin
    GPIOPinTypeGPIOInput(GPIO_PORTJ_BASE, GPIO_PIN_0);//config button
    GPIOPinTypeGPIOInput(GPIO_PORTJ_BASE, GPIO_PIN_1);//config button
    GPIOPadConfigSet(GPIO_PORTJ_BASE,GPIO_PIN_0,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);//config Input: current 2mA,pull-up
    GPIOPadConfigSet(GPIO_PORTJ_BASE,GPIO_PIN_1,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);//config Input: current 2mA,pull-up




}

int main(void)
{
    uint32_t ui32SysClock;

    ui32SysClock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                                           SYSCTL_OSC_MAIN |
                                           SYSCTL_USE_OSC), 25000000);

    GPIO_Init();

	while(1)
	{



	    if(GPIOPinRead(GPIO_PORTJ_BASE, GPIO_PIN_0))
        {
	        GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1,0x01);
        }

	    if(!(GPIOPinRead(GPIO_PORTJ_BASE, GPIO_PIN_0)))
	           {
	                GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1,0x03);

	           }
	   /* if(GPIOPinRead(GPIO_PORTJ_BASE, GPIO_PIN_0))
	           {
	               GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_1,0x00);
	           }

	           if(!(GPIOPinRead(GPIO_PORTJ_BASE, GPIO_PIN_0)))
	                  {
	                       GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_1,0xFF);

	                  }

	                  */


       /* else if((btnVal&GPIO_PIN_1)==0)
        {
            GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_1,0xFF);
        }

        */



	}
}
