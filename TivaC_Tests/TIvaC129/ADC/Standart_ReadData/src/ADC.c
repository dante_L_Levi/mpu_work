#include "ADC.h"




void Int_ADC(void)
{

    uint32_t AdcValues[12]={0};
    uint32_t Temp;
    char buf[100];
    ADCIntClear(ADC0_BASE, 3);
    ADCSequenceDataGet(ADC0_BASE, 3, AdcValues);
    Temp=147.5-((75*(3.3)*AdcValues[0])/4096);
    sprintf(buf,"Temp=%u\r\n",Temp);
    Transmit_String(UART6_BASE,buf);

}


void Init_Inter_TempSensor(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);//init peripheral ADC
    //Enable sample
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
    //config:Temerature internal,Interrupt Enable
    ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_TS|ADC_CTL_IE|ADC_CTL_END);
    //enable Messume
    ADCSequenceEnable(ADC0_BASE, 3);
    //enable interrupt
    ADCIntEnable(ADC0_BASE,3);
    ADCIntClear(ADC0_BASE, 3);
    ADCIntRegister(ADC0_BASE,3,Int_ADC);
    IntEnable(INT_ADC0SS3);




}



