#include "Main.h"
uint32_t ui32SysClock;
bool isADCReadOK=false;

void ISR_ADC(void)
{
    uint32_t AdcValues[2]={0};
    uint32_t Temp;
    ADCSequenceDataGet(ADC0_BASE, 1, AdcValues);
    isADCReadOK=true;

}



void RCC_Init(void)
{
    ui32SysClock = SysCtlClockFreqSet ((SYSCTL_XTAL_25MHZ |
                SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
                SYSCTL_CFG_VCO_480), 120000000);
}



void GPIO_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);//settings gpioE
    GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_2 | GPIO_PIN_3);
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_2);
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3);
}

/*********************Settings Two channel****************/
void ADC_Settings(void)
{
            SysCtlPeripheralReset(SYSCTL_PERIPH_GPIOE);
            SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
            SysCtlPeripheralReset(SYSCTL_PERIPH_ADC0);
            SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
            SysCtlDelay(10);

            GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3|GPIO_PIN_2);

            //ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_EIGHTH, 30);
            ADCHardwareOversampleConfigure(ADC0_BASE,64);
                // Choose Sequencer 2 and set it at the highest Priority.
            ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);

                // Choose Step 0 in Sequencer 2 as Data Buffer, set it as last Step and enable Interrupts
            ADCSequenceStepConfigure(ADC0_BASE,1,0, ADC_CTL_CH1);
            ADCSequenceStepConfigure(ADC0_BASE,1,1, ADC_CTL_CH0 | ADC_CTL_IE | ADC_CTL_END);

            ADCSequenceEnable(ADC0_BASE, 1);
            ADCIntEnable(ADC0_BASE,1);
            //ADCIntClear(ADC0_BASE, 0);
            ADCIntRegister(ADC0_BASE,1,ISR_ADC);
            IntEnable(INT_ADC0SS1);
}


uint32_t WaitAndReadADC (uint32_t * adcValues)
{

    ADCProcessorTrigger (ADC0_BASE, 2 );
    // �������� ���������� ������������������ ��������.

    while (! ADCIntStatus (ADC0_BASE, 2 , false)) {}
    // ������� �������� �� ���.
    return (ADCSequenceDataGet (ADC0_BASE, 2 , adcValues));
}


void main(void)
{
    IntMasterEnable();
    RCC_Init();
    GPIO_Init();
    ADC_Settings();
    //Init_Inter_TempSensor();
   //Uart_Init_std(UARTPeriph_6);

    while(1)
    {
        ADCProcessorTrigger(ADC0_BASE, 1);
        isADCReadOK=false;
        while(!isADCReadOK);
        SysCtlDelay(1000);


    }
}
