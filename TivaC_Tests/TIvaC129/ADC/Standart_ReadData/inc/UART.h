/*
 * UART.h
 *
 *  Created on: 5 ����. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_UART_H_
#define INC_UART_H_


#include "Main.h"
#include "driverlib/pin_map.h"
#include <stdint.h>

//#define MPU_TM4C123
#define MPU_TM4C129

#define UARTPeriph_0    0x00
#define UARTPeriph_1    0x01
#define UARTPeriph_2    0x02
#define UARTPeriph_3    0x03
#define UARTPeriph_4    0x04
#define UARTPeriph_5    0x05
#define UARTPeriph_6    0x06
#define UARTPeriph_7    0x07


#define BaudRate   115200



/**************************Functiion Init Uart ********************/
void Uart_Init_std(uint8_t handler);





void Init_Uart6_Standart(void);
void Transmit_String(uint32_t Base,char *str);

#endif /* INC_UART_H_ */
