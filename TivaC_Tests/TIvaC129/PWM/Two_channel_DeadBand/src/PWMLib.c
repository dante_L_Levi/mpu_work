#include "PWMlib.h"
//! - M0PWM4 - PG0
//! - M0PWM5 - PG1



/**********************Function Init Dead band PWM two channel comlimentary*/
void M0PWM4_5_TM129_Init(void)
{
    //Enable clock PWM0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    //Enable clock Output GPIO
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
    //Config Pins
    GPIOPinConfigure(GPIO_PG0_M0PWM4);
    GPIOPinConfigure(GPIO_PG1_M0PWM5);
    //Set PWM Type
    GPIOPinTypePWM(GPIO_PORTG_BASE, GPIO_PIN_0);
    GPIOPinTypePWM(GPIO_PORTG_BASE, GPIO_PIN_1);

    //Set Mode PWM
    //PWM_GEN_MODE_DOWN or PWM_GEN_MODE_UP_DOWN to specify the counting mode
    //to specify the counter load and comparator update synchronization mode
    PWMGenConfigure(PWM0_BASE, PWM_GEN_2, PWM_GEN_MODE_UP_DOWN |
                    PWM_GEN_MODE_NO_SYNC);
        //Set the PWM period to 500Hz.  To calculate the appropriate parameter
        // use the following equation: N = (1 / f) * SysClk.  Where N is the
        // function parameter, f is the desired frequency, and SysClk is the
        // system clock frequency.
        // In this case you get: (1 / 500Hz) * 40MHz = 64000 cycles.  Note that
        // the maximum period you can set is 2^16 - 1.
        // TODO: modify this calculation to use the clock frequency that you are
        // using.
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_2, 80000);
    // Set PWM0  to a duty cycle of 25%.  You set the duty cycle as a
        // function of the period.  Since the period was set above, you can use the
        // PWMGenPeriodGet() function.  For this example the PWM will be high for
        // 25% of the time or 20000 clock cycles (80000 / 4).
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_4, PWMGenPeriodGet(PWM0_BASE, PWM_GEN_2) / 4);
    // Enable the dead-band generation on the PWM0 output signal.  PWM bit 0
        // (PG0), will have a duty cycle of 25% (set above) and PWM bit 1 will have
        // a duty cycle of 75%.  These signals will have a 10us gap between the
        // rising and falling edges.  This means that before PWM bit 1 goes high,
        // PWM bit 0 has been low for at LEAST 160 cycles (or 10us) and the same
        // before PWM bit 0 goes high.  The dead-band generator lets you specify
        // the width of the "dead-band" delay, in PWM clock cycles, before the PWM
        // signal goes high and after the PWM signal falls.  For this example we
        // will use 160 cycles (or 10us) on both the rising and falling edges of
        // PD0.  Reference the datasheet for more information on dead-band
        // generation.
    PWMDeadBandEnable(PWM0_BASE, PWM_GEN_2, 160, 160);
    //
    // Enable the PWM0 Bit 4 (PG0) and Bit 5 (PG1) output signals.
    //
    PWMOutputState(PWM0_BASE, PWM_OUT_4_BIT | PWM_OUT_5_BIT, true);

    //
    // Enables the counter for a PWM generator block.
    //
    PWMGenEnable(PWM0_BASE, PWM_GEN_2);



}



/**********************Function Init Invert Mode PWM**********************/
void M0PWM4_Invert_TM129_Init(void)
{
    //Enable clock PWM0
     SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    //Enable clock Output GPIO
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
    GPIOPinConfigure(GPIO_PG0_M0PWM4);
    //Set PWM Type
    GPIOPinTypePWM(GPIO_PORTG_BASE, GPIO_PIN_0);
    //Set Divider Clock
    PWMClockSet(PWM0_BASE,PWM_SYSCLK_DIV_8);
    //Config PWM
    PWMGenConfigure(PWM0_BASE, PWM_GEN_2, PWM_GEN_MODE_UP_DOWN|PWM_GEN_MODE_NO_SYNC);
    // Set the PWM period to 100Hz.  To calculate the appropriate parameter
        // use the following equation: N = (1 / f) * SysClk.  Where N is the
        // function parameter, f is the desired frequency, and SysClk is the
        // system clock frequency.
        // In this case you get: (1 / 100Hz) * (40MHz/8) = 50000 cycles.  Note that
        // the maximum period you can set is 2^16.
        // TODO: modify this calculation to use the clock frequency that you are
        // using.
        //
        PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 50000);
        // Enable the PWM0 Bit0 (PD0) output signal.
        PWMOutputState(PWM0_BASE, PWM_OUT_4_BIT, true);
        //set 25% Duty(50000=100%)->25%->12500
        PWMPulseWidthSet(PWM0_BASE, PWM_OUT_4, 12500);
        // Enable the PWM generator block.
        //
        PWMGenEnable(PWM0_BASE, PWM_GEN_2);

}








