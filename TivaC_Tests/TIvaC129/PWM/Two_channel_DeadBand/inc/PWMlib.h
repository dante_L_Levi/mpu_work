
#ifndef INC_PWMLIB_H_
#define INC_PWMLIB_H_


#include "Main.h"

/**********************Function Init Dead band PWM two channel comlimentary*/
void M0PWM4_5_TM129_Init(void);
/**********************Function Init Invert Mode PWM**********************/
void M0PWM4_Invert_TM129_Init(void);


#endif /* INC_PWMLIB_H_ */
