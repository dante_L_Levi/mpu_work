#include "Main.h"

uint32_t ui32SysClock;




void RCC_Init(void)
{
    //Set Clock MPU
           ui32SysClock=SysCtlClockFreqSet ((SYSCTL_XTAL_25MHZ |
                                             SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
                                             SYSCTL_CFG_VCO_480), 40000000);
}


void GPIO_init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);    // Port K-CS
    GPIOPinTypeGPIOOutput(CS_PORT,CS_Pin); //config output Pin CS
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1); //config output Pin LED
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0x01);
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 0x02);

}


void Init_SSI(void)
{
    // The SSI0 peripheral must be enabled for use.
        SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
        //Config GPIO for SSI
        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
        //Config Pins
        GPIOPinConfigure(GPIO_PA2_SSI0CLK);
        GPIOPinConfigure(GPIO_PA3_SSI0FSS);
        GPIOPinConfigure(GPIO_PA4_SSI0XDAT0);
        GPIOPinConfigure(GPIO_PA5_SSI0XDAT1);
        //Config Bits Pins
        GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_5 | GPIO_PIN_4 | GPIO_PIN_3 |
                           GPIO_PIN_2);
        //Config SSI MODULE
        SSIConfigSetExpClk(SSI0_BASE, ui32SysClock, SSI_FRF_MOTO_MODE_0,
                               SSI_MODE_MASTER, 1000000, 8);
        // Enable the SSI0 module.
        SSIEnable(SSI0_BASE);
        GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_1,0x02);

}


void main(void)
{

    RCC_Init();
    GPIO_init();
    Init_SSI();
    Init_7219();
    Clear_7219();
    Send_7219(1,0x01);
    Send_7219(1,0x01);
    Send_7219(1,0x01);
    Send_7219(1,0x01);
    Send_7219(1,0x01);
    Send_7219(1,0x01);
    while(1)
    {

    }
}


