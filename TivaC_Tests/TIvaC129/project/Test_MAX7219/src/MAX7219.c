#include "MAX7219.h"

uint8_t dg=8;//count digits elements
uint8_t aTxBuf[1]={0};//transfer buffer

#define INSTSNCE_SSI            SSI0_BASE


void Toggle_LED(void)
{
    GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_1,0x02);
    SysCtlDelay(15000);
    GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_1,0x00);
    SysCtlDelay(15000);


}

void SendByte_SSI(uint8_t bt)
{
    SSIDataPut(INSTSNCE_SSI,bt);
    while(SSIBusy(INSTSNCE_SSI))
            {

            }

    Toggle_LED();
    Toggle_LED();
    Toggle_LED();
}

/*****************Send Data to register*************************/
void Send_7219 (uint8_t rg, uint8_t dt)
{
    cs_set();
    aTxBuf[0]=rg;
    SendByte_SSI(aTxBuf[0]);
    aTxBuf[0]=dt;
    SendByte_SSI(aTxBuf[0]);
    cs_reset();
}

/***********************Clear Digits Max7219********************/
void Clear_7219 (void)
{
    uint8_t i=dg;
        do
        {
            Send_7219(i,0xF);//������ �������
        } while (--i);
}


/******************Function Output Digit Display*******************/
void Number_7219 (volatile long n)
{
    uint8_t ng=0;
        if(n<0)
        {
            ng=1;
            n*=-1;
        }
        uint8_t i=0;
        do
        {
            Send_7219(++i,n%10);
            n/=10;
        } while(n);
        if(ng)
        {
            Send_7219(i+1,0x0A);
        }
}


/********************Init Digit MAX7219********************/
void Init_7219 (void)
{
    Send_7219(0x09,0xFF);//ON Decoder
    Send_7219(0x0B,dg-1);//Set Number digit
    Send_7219(0x0A,0x02);//set intensive
    Send_7219(0x0C,0x01);//On Module
    Clear_7219();
}
