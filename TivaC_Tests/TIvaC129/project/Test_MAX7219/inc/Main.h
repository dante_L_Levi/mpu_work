/*
 * Main.h
 *
 *  Created on: 10 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_MAIN_H_
#define INC_MAIN_H_



#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"
#include "driverlib/adc.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"

#include "MAX7219.h"



#define CS_Pin      GPIO_PIN_0
#define CS_PORT     GPIO_PORTK_BASE








#endif /* INC_MAIN_H_ */
