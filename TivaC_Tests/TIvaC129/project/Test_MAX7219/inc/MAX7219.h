/*
 * MAX7219.h
 *
 *  Created on: 23 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_MAX7219_H_
#define INC_MAX7219_H_


#include "Main.h"




#define cs_set()                GPIOPinWrite(CS_PORT,CS_Pin,0x00)
#define cs_reset()              GPIOPinWrite(CS_PORT,CS_Pin,0x01)


/*****************Send Data to register*************************/
void Send_7219 (uint8_t rg, uint8_t dt);
/***********************Clear Digits Max7219********************/
void Clear_7219 (void);
/******************Function Output Digit Display*******************/
void Number_7219 (volatile long n);

/********************Init Digit MAX7219********************/
void Init_7219 (void);





#endif /* INC_MAX7219_H_ */
