#include "Main.h"

static volatile bool g_bIntFlag = false;
static volatile bool cap_bIntFlag = false;
uint32_t ui32SysClock;




void TIM0_IRQ(void)
{

        // Clear the timer interrupt flag.
        //
        TimerIntClear(TIMER0_BASE, TIMER_TIMB_TIMEOUT);
        //
        // Set a flag to indicate that the interrupt occurred.
        //
        g_bIntFlag = !g_bIntFlag;
}


void RCC_Init(void)
{
    //Config Clock MPU
    ui32SysClock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                                                  SYSCTL_OSC_MAIN |
                                                  SYSCTL_USE_OSC), 25000000);

}


void Timer_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0))
    {
    }

    //config Timer:half size ,A-periodic work,B-work with capture
    TimerConfigure(TIMER0_BASE, TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PERIODIC|TIMER_CFG_B_CAP_COUNT);
    //Set Prescale Timer
    TimerPrescaleSet (TIMER0_BASE, TIMER_A, 1000);
    TimerPrescaleSet (TIMER0_BASE, TIMER_B, 1000);
    //set value half TimA
    TimerLoadSet(TIMER0_BASE, TIMER_A, 30000);

    TimerControlEvent(TIMER0_BASE, TIMER_B, TIMER_EVENT_BOTH_EDGES);
    //
    // Configure the Timer0B interrupt for timer timeout.
    //
    TimerIntRegister(TIMER0_BASE,TIMER_A,TIM0_IRQ);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    TimerIntEnable(TIMER0_BASE, TIMER_CAPB_EVENT);
    // Enable the Timer0B interrupt on the processor (NVIC).
    //
    IntEnable(INT_TIMER0B);
    IntEnable(INT_TIMER0A);

    TimerEnable(TIMER0_BASE,TIMER_A);
    TimerEnable(TIMER0_BASE,TIMER_B);


}

void GPIO_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);    // enable port N
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1);//config output Pin
    GPIOPadConfigSet(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1,GPIO_STRENGTH_12MA,GPIO_PIN_TYPE_STD);//Config Pins


}





void main(void)
{
    RCC_Init();
    GPIO_Init();
    IntMasterEnable();
    Timer_Init();

    while(1)
    {
        if(g_bIntFlag)
               {
                   GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_0,0xFF);
               }
               else
               {
                   GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_0,0x00);
               }
    }
}

