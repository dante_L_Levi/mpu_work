/*
 * Timers.h
 *
 *  Created on: 26 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_TIMERS_H_
#define INC_TIMERS_H_


#include "Main.h"



/*******************Function set Clock****************/
void SetClockTimer(uint32_t header);
/*****************Init Timer Mode Split**************/
void Timer_InitSplitMode(uint32_t baseTim,uint32_t config);



#endif /* INC_TIMERS_H_ */
