
#include "Main.h"

uint32_t ui32SysClock;






void Uart6_IRQ(void)
{
    char ressieve_char;
    uint32_t intStatus;
    intStatus=UARTIntStatus(UART6_BASE,true);
    UARTIntClear(UART6_BASE,intStatus);
    ressieve_char=UARTCharGetNonBlocking(UART6_BASE);
    if(ressieve_char=='2')
    {
        GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_1,0xFF);
    }
    if((uint32_t)ressieve_char!=-1)
    {
        while(!UARTCharPutNonBlocking(UART6_BASE,ressieve_char));
    }


}



void UART_SendString(uint32_t Uart_Base, char *str)
{
    uint32_t i=0;
    while(str[i]!=0)
    {
        UARTCharPut(Uart_Base,str[i]);
        i++;
    }
}



void GPIO_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1);
    GPIOPadConfigSet(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1,GPIO_STRENGTH_6MA,GPIO_PIN_TYPE_STD);

}

void main(void)
{
            //Base Initilize Clock
    ui32SysClock = SysCtlClockFreqSet ((SYSCTL_XTAL_25MHZ |
            SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
            SYSCTL_CFG_VCO_480), 120000000);

           SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
                                                  //Init UART4 Clock
           SysCtlPeripheralEnable(SYSCTL_PERIPH_UART6);
           while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART6))
           {
           }                                                     //Init GPIO clock for UART4

           IntMasterEnable();
           //Config Pin PK0=RX
          GPIOPinConfigure(GPIO_PP0_U6RX);
                                                                //Config Pin PK0=TX
          GPIOPinConfigure(GPIO_PP1_U6TX);
                                                                //config GPIO on UART
          GPIOPinTypeUART(GPIO_PORTP_BASE,GPIO_PIN_0|GPIO_PIN_1);
          UARTClockSourceSet(UART6_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 25MHz,Baud:115200,8bits,1-stopbit ,none Parity
          UARTConfigSetExpClk(UART6_BASE, 16000000, 115200,
                                  (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                   UART_CONFIG_PAR_NONE));
          UARTIntRegister(UART6_BASE, Uart6_IRQ);
          IntEnable(INT_UART6);
          UARTIntEnable(UART6_BASE, UART_INT_RX | UART_INT_RT);
          GPIO_Init();
          GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_0,0xFF);

    while(1)
    {
        UART_SendString(UART6_BASE,"Hello World UART!!!\r\n");
        SysCtlDelay((ui32SysClock/30000)*5000);

    }
}




