#ifndef INC_RINGBUFFER_H_
#define INC_RINGBUFFER_H_


#include "Main.h"
#include "stdint.h"
#define RING_BUFFER_SIZE 100


typedef struct
{
    uint8_t head;
    uint8_t tail;

    uint8_t data[RING_BUFFER_SIZE];
}ring_buffer_t;




void Ring_Buffer_Init(ring_buffer_t *rb);
uint8_t RB_free(ring_buffer_t *rb);
uint8_t RB_Length(ring_buffer_t *rb);
uint8_t RB_ReadByte(ring_buffer_t *rb);
uint8_t RB_WriteByte(ring_buffer_t *rb,uint8_t data);
uint8_t RB_Read(ring_buffer_t *rb,uint8_t *data,uint8_t dataLength);
uint8_t RB_Write(ring_buffer_t *rb,uint8_t *data,uint8_t dataLength);





#endif /* INC_RINGBUFFER_H_ */
