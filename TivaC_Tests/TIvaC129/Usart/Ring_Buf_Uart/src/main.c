#include "Main.h"

uint32_t ui32SysClock;
ring_buffer_t  rb_rx;
uint8_t Ressieve_Buf[100];


void Uart6_IRQ(void)
{
    uint8_t ressieve_byte;
    uint32_t intStatus;
    intStatus=UARTIntStatus(UART6_BASE,true);
    UARTIntClear(UART6_BASE,intStatus);
    ressieve_byte=UARTCharGetNonBlocking(UART6_BASE);
    if(RB_Length(&rb_rx)>0)
    {
        RB_WriteByte(&rb_rx,ressieve_byte);
    }
    else
    {
        //off resieve data
    }
}

void UART_SendString(uint32_t Uart_Base, char *str)
{
    uint32_t i=0;
    while(str[i]!=0)
    {
        UARTCharPut(Uart_Base,str[i]);
        i++;
    }
}


void Uart_Ressieve_IT(uint8_t *data,uint8_t dataLength)
{
    RB_Read(&rb_rx,data,dataLength);
}


void RCC_Init(void)
{
    ui32SysClock = SysCtlClockFreqSet ((SYSCTL_XTAL_25MHZ |
                SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
                SYSCTL_CFG_VCO_480), 120000000);

    //enable clock GPIOP
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
    //enable clock Uart6
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART6);
}



void Uart_Init(void)
{
    //enable clock GPIOP whete set uart6
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
        //enable clock Uart6
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART6);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART6))
    {

    }

    //config Pins how Uart pin RxD TxD
    GPIOPinConfigure(GPIO_PP0_U6RX);
    GPIOPinConfigure(GPIO_PP1_U6TX);
    GPIOPinTypeUART(GPIO_PORTP_BASE,GPIO_PIN_0|GPIO_PIN_1);

    //Sets the baud clock source for the specified UART.
    UARTClockSourceSet(UART6_BASE,UART_CLOCK_PIOSC);

    //config:ext clock 25MHz,Baud:115200,8bits,1-stopbit ,none Parity
    UARTConfigSetExpClk(UART6_BASE, 16000000, 115200,
                                     (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                      UART_CONFIG_PAR_NONE));
    //Init Register interrupt Ressieve
    UARTIntRegister(UART6_BASE, Uart6_IRQ);
    //Enable Interrupt
    IntEnable(INT_UART6);
    //On Interrupt for Ressieve
    UARTIntEnable(UART6_BASE, UART_INT_RX | UART_INT_RT);

}


void main(void)
{
    RCC_Init();
    Uart_Init();
    Uart_Ressieve_IT(Ressieve_Buf,);

    while(1)
    {
        UART_SendString(UART6_BASE,"Test Transmit Ring Buffer!!!\r\n");
        SysCtlDelay((ui32SysClock/30000)*5000);
    }
}


