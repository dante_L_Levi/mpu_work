#include "RingBuffer.h"

void Ring_Buffer_Init(ring_buffer_t *rb)
{
    rb->tail=0;
    rb->head=0;
}




uint8_t RB_free(ring_buffer_t *rb)
{
    uint8_t free;
    if((rb->head) >= rb->tail)
    {
        free=RING_BUFFER_SIZE-(rb->head - rb->tail);
    }
    else
    {
        free=rb->tail -rb->head;
    }

    return free;
}


uint8_t RB_Length(ring_buffer_t *rb)
{
    uint8_t len;
    if(rb->head >= rb->tail)
    {
        len=rb->tail-rb->head;
    }
    else
    {

        len=RING_BUFFER_SIZE-(rb->head - rb->tail);
    }

    return len;
}


uint8_t RB_ReadByte(ring_buffer_t *rb)
{

    uint8_t tail;
    uint8_t ret;
    tail=rb->tail;

    if(tail>=RING_BUFFER_SIZE)
    {
        tail=0;
    }

    rb->tail=tail;
    return ret;

}

uint8_t RB_WriteByte(ring_buffer_t *rb,uint8_t data)
{
    uint8_t head;
    head=rb->head;
    while(RB_free(rb)<1);

    rb->data[head++]=data;
    if(head>=RING_BUFFER_SIZE)
    {
        head=0;
    }
    rb->head=head;
    return 1;
}

uint8_t RB_Read(ring_buffer_t *rb,uint8_t *data,uint8_t dataLength)
{
    uint8_t tail;
    tail=rb->tail;
    if(dataLength==0|| RB_Length(rb)==0)
    {
        return 0;
    }
    if(RB_Length(rb)<dataLength)
    {
        dataLength=RB_Length(rb);
    }

    while(dataLength--)
    {
        *data++=rb->data[tail++];
        if(tail>=RING_BUFFER_SIZE)
        {
            tail=0;
        }
    }

    rb->tail=tail;
    return 1;

}



uint8_t RB_Write(ring_buffer_t *rb,uint8_t *data,uint8_t dataLength)
{
    uint8_t head;
    head=rb->head;

    if(dataLength==0)
    {
        return 0;
    }
    while(RB_free(rb)<dataLength);
    while(dataLength--)
    {
        rb->data[head++]=*data++;
        if(head>=RING_BUFFER_SIZE)
        {
            head=0;
        }
    }

    rb->head=head;
    return 1;

}


