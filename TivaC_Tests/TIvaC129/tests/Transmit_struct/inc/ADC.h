
#ifndef INC_ADC_H_
#define INC_ADC_H_


#include "Main.h"


#define ADC_CH0         1
#define ADC_CH1         2
#define ADC_CH2         3
#define ADC_CH3         4
#define ADC_CH4         5
#define ADC_CH5         6
#define ADC_CH6         7
#define ADC_CH7         8
#define ADC_CH8         9
#define ADC_CH9         10


/*************************Init ADC***********************/
void ADC_Init(uint32_t ADCBase,uint8_t channel);
/*****************Function Test Init ADC********************/
void Test_ADC_Init(void);


#endif /* INC_ADC_H_ */
