/*
 * UART.h
 *
 *  Created on: 21 ����. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_UART_H_
#define INC_UART_H_


#include "Main.h"


//#define MPU_TM4C123     0x01
#define MPU_TM4C129     0x02

#define UARTPeriph_0    0x00
#define UARTPeriph_1    0x01
#define UARTPeriph_2    0x02
#define UARTPeriph_3    0x03
#define UARTPeriph_4    0x04
#define UARTPeriph_5    0x05
#define UARTPeriph_6    0x06
#define UARTPeriph_7    0x07


/************************Init Uart Standart***************************/
void Standart_UART_Init(uint32_t BaudRate,uint8_t handler);
/******************Function Transmit String*********************/
void UART_Transmit_String(uint32_t handler,char *str);


#endif /* INC_UART_H_ */
