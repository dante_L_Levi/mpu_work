
#ifndef INC_MAIN_H_
#define INC_MAIN_H_

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"
#include "driverlib/adc.h"


#include "UART.h"
#include "ADC.h"


#pragma pack(push, 1)
typedef struct
{
    uint8_t id_dev;
    float tempMPU;
    float ADCchannel;
    uint32_t sizestruct;


}ADC_dataDef;



#endif /* INC_MAIN_H_ */
