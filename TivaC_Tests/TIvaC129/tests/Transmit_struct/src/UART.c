#include "UART.h"



void Uart0_IRQ(void)
{

}

void Uart1_IRQ(void)
{

}

void Uart2_IRQ(void)
{

}

void Uart3_IRQ(void)
{

}

void Uart4_IRQ(void)
{

}

void Uart5_IRQ(void)
{

}


void Uart6_IRQ(void)
{

}

void Uart7_IRQ(void)
{

}

/************************Init Uart Standart***************************/
void Standart_UART_Init(uint32_t BaudRate,uint8_t handler)
{
#if defined(MPU_TM4C123)
        switch(handler)
        {
        case UARTPeriph_0:
                {
                    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
                    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
                    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART0))
                    {
                    }
                   IntMasterEnable();
                   GPIOPinConfigure(GPIO_PA0_U0RX);
                   GPIOPinConfigure(GPIO_PA1_U0TX);
                   GPIOPinTypeUART(GPIO_PORTA_BASE,GPIO_PIN_0|GPIO_PIN_1);
                   UARTClockSourceSet(UART0_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                   UARTConfigSetExpClk(UART0_BASE, 16000000, BaudRate,
                         (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                UART_CONFIG_PAR_NONE));
                  UARTIntRegister(UART0_BASE, Uart0_IRQ);
                  IntEnable(INT_UART0);
                  UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
                    break;
                }
                //====================================================================

                case UARTPeriph_1:
                        {
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
                            while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART1))
                            {
                            }
                           IntMasterEnable();
                           GPIOPinConfigure(GPIO_PB0_U1RX);
                           GPIOPinConfigure(GPIO_PB1_U1TX);
                           GPIOPinTypeUART(GPIO_PORTB_BASE,GPIO_PIN_0|GPIO_PIN_1);
                           UARTClockSourceSet(UART1_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                           UARTConfigSetExpClk(UART1_BASE, 16000000, BaudRate,
                                 (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                        UART_CONFIG_PAR_NONE));
                          UARTIntRegister(UART1_BASE, Uart1_IRQ);
                          IntEnable(INT_UART1);
                          UARTIntEnable(UART1_BASE, UART_INT_RX | UART_INT_RT);
                            break;
                        }

                        //============================================================
                case UARTPeriph_2:
                        {
                           SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
                           SysCtlPeripheralEnable(SYSCTL_PERIPH_UART2);
                           while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART2))
                           {
                            }

                           GPIOPinConfigure(GPIO_PD6_U2RX);
                           GPIOPinConfigure(GPIO_PD7_U2TX);
                           GPIOPinTypeUART(GPIO_PORTD_BASE,GPIO_PIN_6|GPIO_PIN_7);
                           UARTClockSourceSet(UART2_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                           UARTConfigSetExpClk(UART2_BASE, 16000000, BaudRate,
                           (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                           UART_CONFIG_PAR_NONE));
                           UARTIntRegister(UART2_BASE, Uart2_IRQ);
                          IntEnable(INT_UART2);
                          UARTIntEnable(UART2_BASE, UART_INT_RX | UART_INT_RT);

                            break;
                        }
                        //====================================================================
                case UARTPeriph_3:
                 {
                    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
                    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART3);
                    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART3))
                    {
                    }

                    GPIOPinConfigure(GPIO_PC6_U3RX);
                    GPIOPinConfigure(GPIO_PC7_U3TX);
                    GPIOPinTypeUART(GPIO_PORTC_BASE,GPIO_PIN_7|GPIO_PIN_6);
                    UARTClockSourceSet(UART3_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                    UARTConfigSetExpClk(UART3_BASE, 16000000, BaudRate,
                    (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                     UART_CONFIG_PAR_NONE));
                     UARTIntRegister(UART3_BASE, Uart3_IRQ);
                     IntEnable(INT_UART3);
                     UARTIntEnable(UART3_BASE, UART_INT_RX | UART_INT_RT);
                     break;
                }

                                //==================================================================================
                case UARTPeriph_4:
                {


               SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
               SysCtlPeripheralEnable(SYSCTL_PERIPH_UART4);
                 while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART4))
                 {
                 }

                  GPIOPinConfigure(GPIO_PC4_U4RX);
                  GPIOPinConfigure(GPIO_PC5_U4TX);
                  GPIOPinTypeUART(GPIO_PORTC_BASE,GPIO_PIN_4|GPIO_PIN_5);
                  UARTClockSourceSet(UART4_BASE,UART_CLOCK_PIOSC) ;
                  UARTConfigSetExpClk(UART4_BASE, 16000000, BaudRate,
                  (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                  UART_CONFIG_PAR_NONE));
                  UARTIntRegister(UART4_BASE, Uart4_IRQ);
                  IntEnable(INT_UART4);
                  UARTIntEnable(UART4_BASE, UART_INT_RX | UART_INT_RT);

                 break;
                }
                                        //=================================================================================
                case UARTPeriph_5:
                {


                 SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
                 SysCtlPeripheralEnable(SYSCTL_PERIPH_UART5);
                 while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART5))
                 {
                 }
                 IntMasterEnable();
                 GPIOPinConfigure(GPIO_PC6_U5RX);
                 GPIOPinConfigure(GPIO_PC7_U5TX);
                 GPIOPinTypeUART(GPIO_PORTC_BASE,GPIO_PIN_6|GPIO_PIN_7);
                 UARTClockSourceSet(UART5_BASE,UART_CLOCK_PIOSC) ;
                 (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                  UART_CONFIG_PAR_NONE));
                 UARTIntRegister(UART5_BASE, Uart5_IRQ);
                 IntEnable(INT_UART5);
                 UARTIntEnable(UART5_BASE, UART_INT_RX | UART_INT_RT);
                 break;
                }
                                               //=========================================================================
                case UARTPeriph_6:
                {
                    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
                    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART6);
                    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART6))
                    {
                    }
                    IntMasterEnable();
                    GPIOPinConfigure(GPIO_PD4_U6RX);
                    GPIOPinConfigure(GPIO_PD5_U6TX);
                    GPIOPinTypeUART(GPIO_PORTD_BASE,GPIO_PIN_4|GPIO_PIN_5);
                    UARTClockSourceSet(UART6_BASE,UART_CLOCK_PIOSC) ;
                    UARTConfigSetExpClk(UART6_BASE, 16000000, BaudRate,
                    (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                     UART_CONFIG_PAR_NONE));
                     UARTIntRegister(UART6_BASE, Uart6_IRQ);
                    IntEnable(INT_UART6);
                    UARTIntEnable(UART6_BASE, UART_INT_RX | UART_INT_RT);
                     break;
                    }

                                                       //===============================================================================
                case UARTPeriph_7:
                {
                   SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
                   SysCtlPeripheralEnable(SYSCTL_PERIPH_UART7);
                   while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART7))
                   {
                   }
                   GPIOPinConfigure(GPIO_PE1_U7TX);
                   GPIOPinTypeUART(GPIO_PORTE_BASE,GPIO_PIN_0|GPIO_PIN_1);
                   UARTClockSourceSet(UART7_BASE,UART_CLOCK_PIOSC) ;
                   UARTConfigSetExpClk(UART7_BASE, 16000000, BaudRate,
                  (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                   UART_CONFIG_PAR_NONE));
                   UARTIntRegister(UART7_BASE, Uart7_IRQ);
                   IntEnable(INT_UART7);
                   UARTIntEnable(UART7_BASE, UART_INT_RX | UART_INT_RT);
                   break;
                   }
        }
#elif defined(MPU_TM4C129)
        switch(handler)
        {
        case UARTPeriph_0:
                        {
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
                            while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART0))
                            {
                            }
                           IntMasterEnable();
                           GPIOPinConfigure(GPIO_PA0_U0RX);
                           GPIOPinConfigure(GPIO_PA1_U0TX);
                           GPIOPinTypeUART(GPIO_PORTA_BASE,GPIO_PIN_0|GPIO_PIN_1);
                           UARTClockSourceSet(UART0_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                           UARTConfigSetExpClk(UART0_BASE, 16000000, BaudRate,
                                 (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                        UART_CONFIG_PAR_NONE));
                          UARTIntRegister(UART0_BASE, Uart0_IRQ);
                          IntEnable(INT_UART0);
                          UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
                            break;
                        }
                        //====================================================================

                        case UARTPeriph_1:
                                {
                                    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
                                    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
                                    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART1))
                                    {
                                    }
                                   IntMasterEnable();
                                   GPIOPinConfigure(GPIO_PB0_U1RX);
                                   GPIOPinConfigure(GPIO_PB1_U1TX);
                                   GPIOPinTypeUART(GPIO_PORTB_BASE,GPIO_PIN_0|GPIO_PIN_1);
                                   UARTClockSourceSet(UART1_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                                   UARTConfigSetExpClk(UART1_BASE, 16000000, BaudRate,
                                         (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                                UART_CONFIG_PAR_NONE));
                                  UARTIntRegister(UART1_BASE, Uart1_IRQ);
                                  IntEnable(INT_UART1);
                                  UARTIntEnable(UART1_BASE, UART_INT_RX | UART_INT_RT);
                                    break;
                                }
                        case UARTPeriph_2:
                        {
                                SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
                                SysCtlPeripheralEnable(SYSCTL_PERIPH_UART2);
                                while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART2))
                                {
                                }

                                GPIOPinConfigure(GPIO_PD4_U2RX);
                                GPIOPinConfigure(GPIO_PD5_U2TX);
                                GPIOPinTypeUART(GPIO_PORTD_BASE,GPIO_PIN_4|GPIO_PIN_5);
                                UARTClockSourceSet(UART2_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                                UARTConfigSetExpClk(UART2_BASE, 16000000, BaudRate,
                                (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                 UART_CONFIG_PAR_NONE));
                                 UARTIntRegister(UART2_BASE, Uart2_IRQ);
                                 IntEnable(INT_UART2);
                                 UARTIntEnable(UART2_BASE, UART_INT_RX | UART_INT_RT);
                                break;
                        }
                        case UARTPeriph_3:
                        {
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_UART3);
                            while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART3))
                            {
                            }

                            GPIOPinConfigure(GPIO_PJ0_U3RX);
                            GPIOPinConfigure(GPIO_PJ1_U3TX);
                            GPIOPinTypeUART(GPIO_PORTJ_BASE,GPIO_PIN_0|GPIO_PIN_1);
                            UARTClockSourceSet(UART3_BASE,UART_CLOCK_PIOSC) ;
                            UARTConfigSetExpClk(UART3_BASE, 16000000, BaudRate,
                            (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                            UART_CONFIG_PAR_NONE));
                            UARTIntRegister(UART3_BASE, Uart3_IRQ);
                            IntEnable(INT_UART3);
                            break;
                        }

                        case UARTPeriph_4:
                        {
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_UART4);
                            while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART4))
                            {
                            }

                           GPIOPinConfigure(GPIO_PK0_U4RX);
                           GPIOPinConfigure(GPIO_PK1_U4TX);
                           GPIOPinTypeUART(GPIO_PORTK_BASE,GPIO_PIN_0|GPIO_PIN_1);
                           UARTClockSourceSet(UART4_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                           UARTConfigSetExpClk(UART4_BASE, 16000000, BaudRate,
                                 (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                        UART_CONFIG_PAR_NONE));
                          UARTIntRegister(UART4_BASE, Uart4_IRQ);
                          IntEnable(INT_UART4);
                          UARTIntEnable(UART4_BASE, UART_INT_RX | UART_INT_RT);

                            break;
                        }

                        case UARTPeriph_5:
                        {
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_UART5);
                            while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART5))
                            {
                            }

                           GPIOPinConfigure(GPIO_PC6_U5RX);
                           GPIOPinConfigure(GPIO_PC7_U5TX);
                           GPIOPinTypeUART(GPIO_PORTC_BASE,GPIO_PIN_6|GPIO_PIN_7);
                           UARTClockSourceSet(UART5_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                           UARTConfigSetExpClk(UART5_BASE, 16000000, BaudRate,
                                 (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                        UART_CONFIG_PAR_NONE));
                          UARTIntRegister(UART5_BASE, Uart5_IRQ);
                          IntEnable(INT_UART5);
                          UARTIntEnable(UART5_BASE, UART_INT_RX);


                            break;
                        }
                        case UARTPeriph_6:
                        {
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
                            SysCtlPeripheralEnable(SYSCTL_PERIPH_UART6);
                            while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART6))
                            {
                            }

                           GPIOPinConfigure(GPIO_PP0_U6RX);
                           GPIOPinConfigure(GPIO_PP1_U6TX);
                           GPIOPinTypeUART(GPIO_PORTP_BASE,GPIO_PIN_0|GPIO_PIN_1);
                           UARTClockSourceSet(UART6_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                           UARTConfigSetExpClk(UART6_BASE, 16000000, BaudRate,
                                 (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                        UART_CONFIG_PAR_NONE));
                          UARTIntRegister(UART6_BASE, Uart6_IRQ);
                          IntEnable(INT_UART6);
                         //UARTIntEnable(UART6_BASE, UART_INT_RX | UART_INT_RT);

                            break;
                        }

                        case UARTPeriph_7:
                        {
                        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
                        SysCtlPeripheralEnable(SYSCTL_PERIPH_UART7);
                        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART7))
                        {
                        }
                       IntMasterEnable();
                       GPIOPinConfigure(GPIO_PC4_U7RX);
                       GPIOPinConfigure(GPIO_PC5_U7TX);
                       GPIOPinTypeUART(GPIO_PORTC_BASE,GPIO_PIN_4|GPIO_PIN_5);
                       UARTClockSourceSet(UART7_BASE,UART_CLOCK_PIOSC) ;                                                    //config:ext clock 16MHz,Baud:115200,8bits,1-stopbit ,none Parity
                       UARTConfigSetExpClk(UART7_BASE, 16000000, BaudRate,
                             (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                    UART_CONFIG_PAR_NONE));
                      UARTIntRegister(UART7_BASE, Uart7_IRQ);
                      IntEnable(INT_UART7);
                      UARTIntEnable(UART7_BASE, UART_INT_RX | UART_INT_RT);
                            break;
                        }






        }

#endif

}


/******************Function Transmit String*********************/
void UART_Transmit_String(uint32_t handler,char *str)
{
    uint32_t i=0;
    while(str[i]!=0)
    {
        UARTCharPut(handler, str[i]);
        i++;
    }

}



