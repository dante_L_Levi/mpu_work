#include "Main.h"
//=====================Public Vailaible================================
uint32_t CLockMPU;
uint32_t UARTBaud=115200;
ADC_dataDef mainstruct;
#define SIZEBUUFER  sizeof(mainstruct)
uint8_t BUFFER[SIZEBUUFER];
static volatile bool g_bIntFlag = false;

extern float ADC0_CH;
extern float Temp;
//=====================================================
void TIM0_IRQ(void)
{
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    g_bIntFlag = !g_bIntFlag;

}


//====================================================
/*********************Init Clock MPU***********************/
void RCC_Init(void);
/***************************function Set Id************************/
void Set_ID_dev(uint8_t id);
/***************************function get size struct************************/
uint32_t Get_size_struct(ADC_dataDef data);
/******************Init Timer Tick****************************/
void Timer_Init(void);
/*************************Function Set period ms************************/
void Timer_Set_time(uint32_t);
/******************************Init GPIO*********************/
void GPIO_Init(void);
/******************Init Timer Tick****************************/
void Timer_Init(void);
/*******************************Function Convert Data***************************/
void Convert_DataArray(ADC_dataDef *data,uint32_t sizeStr,uint8_t *buffer);
/**********************************Function Transmit Data Serial Port********************/
void Transmit_DataStruct(uint8_t *buffer,uint8_t sizebuf);
/*********************Function Test Data********************/
void Test_Fill_dataStruct(ADC_dataDef *data);
/********************Clear Buffer Transmit********************/
void Clear_Buffer(uint8_t *buf,uint8_t size);
/*********************Function Write Struct Data**************/
void Read_Data(void);



void main(void)
{
    RCC_Init();
    Timer_Init();
    Standart_UART_Init(UARTBaud, UARTPeriph_6);
    GPIO_Init();
    Test_ADC_Init();
    //Test_Fill_dataStruct(&mainstruct);
    //Convert_DataArray(&mainstruct,SIZEBUUFER,BUFFER);



    while(1)
    {
        uint32_t test=TimerClockSourceGet(TIMER0_BASE);
        /*while(!ADCIntStatus(ADC0_BASE, 3, false))
        {
        }*/

        ADCProcessorTrigger(ADC0_BASE, 1);
        ADCIntClear(ADC0_BASE, 1);

        Read_Data();
        if(g_bIntFlag)
        {
            GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0xFF);
            Convert_DataArray(&mainstruct,SIZEBUUFER,BUFFER);
            Transmit_DataStruct(BUFFER,SIZEBUUFER);

        }
        else
        {
            GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_0,0x00);
        }


    }
}




/*********************Init Clock MPU***********************/
void RCC_Init(void)
{
    CLockMPU=SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
                SYSCTL_CFG_VCO_480), 40000000);

    IntMasterEnable();

}


/******************************Init GPIO*********************/
void GPIO_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);    // enable port N
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1);//config output Pin
    GPIOPadConfigSet(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1,GPIO_STRENGTH_12MA,GPIO_PIN_TYPE_STD);//Config Pins

}

/******************Init Timer Tick****************************/
void Timer_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0))
    {
    }
   //config Mode 16-bit Timer split count
   TimerConfigure(TIMER0_BASE,TIMER_CFG_PERIODIC);
   //======================Config for Split Mode 16bit Timer*******************
   //Config: 40000000/4000=10000,prescale 4000,Ttim=4000/40000000=0,0001s
   //TimerPrescaleSet (TIMER0_BASE, TIMER_B, 4000);
   //config:Tism=200ms, n200ms=200ms/0,0001s=2000
   //==========================================================================
   //Config 1s:CLockMPU/2,  2s:CLockMPU,0.5s:CLockMPU/4
   TimerLoadSet(TIMER0_BASE,TIMER_A,CLockMPU);
   TimerIntRegister(TIMER0_BASE,TIMER_A,TIM0_IRQ);
   //
   // Configure the Timer0B interrupt for timer timeout.(����. ������ ������� ������� � ����� �)
   //
   TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
   // Enable the Timer0B interrupt on the processor (NVIC).
  //
   IntEnable(INT_TIMER0A);
   TimerEnable(TIMER0_BASE, TIMER_A);
}




/*************************Function Set period ms************************/
void Timer_Set_time(uint32_t val)
{

}

/***************************function Set Id************************/
void Set_ID_dev(uint8_t id)
{
    mainstruct.id_dev=id;
}
/*********************Function Test Data********************/
void Test_Fill_dataStruct(ADC_dataDef *data)
{
    Set_ID_dev(0x01);
    data->ADCchannel=12.98;
    data->tempMPU=22.98;
    data->sizestruct=SIZEBUUFER;
}

/***************************function get size struct************************/
uint32_t Get_size_struct(ADC_dataDef data)
{
    return sizeof(data);
}

/*******************************Function Convert Data***************************/
void Convert_DataArray(ADC_dataDef *data,uint32_t sizeStr,uint8_t *buffer)
{
    memcpy(buffer,data,sizeStr);
}

/**********************************Function Transmit Data Serial Port********************/
void Transmit_DataStruct(uint8_t *buffer,uint8_t sizebuf)
{
    uint8_t i;
    for(i=0;i<sizebuf;i++)
    {
        UARTCharPut(UART6_BASE,buffer[i]);
    }
    UARTCharPut(UART6_BASE,'\r');
    UARTCharPut(UART6_BASE,'\n');

}

/********************Clear Buffer Transmit********************/
void Clear_Buffer(uint8_t *buf,uint8_t size)
{
    uint8_t i;
    for(i=0;i<size;i++)
    {
        buf[i]=0;
    }
}

/*********************Function Write Struct Data**************/
void Read_Data(void)
{
    Set_ID_dev(0x01);
    mainstruct.tempMPU=Temp;
    mainstruct.ADCchannel=ADC0_CH;
    mainstruct.sizestruct=sizeof(mainstruct);

}





