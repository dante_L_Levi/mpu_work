#include "ADC.h"

uint32_t AdcValues[2]={0};
float ADC0_CH=0;
float Temp;
void ISR_ADC(void)
{
    ADCSequenceDataGet(ADC0_BASE, 1, AdcValues);
    Temp=147.5-((75*(3.3)*AdcValues[1])/4096);
    ADC0_CH=((3.3)*AdcValues[0])/4096;
    ADCIntClear(ADC0_BASE, 1);
}


/*************************Init ADC***********************/
void ADC_Init(uint32_t ADCBase,uint8_t channel)
{

}

/*****************Function Test Init ADC********************/
void Test_ADC_Init(void)
{
    SysCtlPeripheralReset(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralReset(SYSCTL_PERIPH_ADC0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    SysCtlDelay(10);
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3);
    ADCHardwareOversampleConfigure(ADC0_BASE,64);
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
    // Choose Step 0 in Sequencer 2 as Data Buffer, set it as last Step and enable Interrupts
    ADCSequenceStepConfigure(ADC0_BASE,1,0, ADC_CTL_CH0);
    //config:Temerature internal,Interrupt Enable
    ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_CH0|ADC_CTL_TS|ADC_CTL_IE|ADC_CTL_END);
    //Enable Sequence
    ADCSequenceEnable(ADC0_BASE, 1);
    //Enable Start Interrupt
    ADCIntEnable(ADC0_BASE,1);
    ADCIntRegister(ADC0_BASE,1,ISR_ADC);
    IntEnable(INT_ADC0SS1);
    ADCIntClear(ADC0_BASE, 1);

}


