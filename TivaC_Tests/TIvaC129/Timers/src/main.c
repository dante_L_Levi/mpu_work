
#include "Main.h"

static volatile bool g_bIntFlag = false;
uint32_t ui32SysClock;


void TIM0_IRQ(void)
{
    //
    // Clear the timer interrupt flag.
    //
    TimerIntClear(TIMER0_BASE, TIMER_TIMB_TIMEOUT);

    //
    // Set a flag to indicate that the interrupt occurred.
    //
    g_bIntFlag = !g_bIntFlag;
}










int main(void)
{
    //Base Initilize
       ui32SysClock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                                              SYSCTL_OSC_MAIN |
                                              SYSCTL_USE_OSC), 25000000);
       SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);    // enable port N
      GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1);//config output Pin
      GPIOPadConfigSet(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1,GPIO_STRENGTH_12MA,GPIO_PIN_TYPE_STD);//Config Pins
      //��� ������ ������� ���. ���������
        SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
      while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0))
          {
          }

          //������������ �������(����� ������ �������,��� ������� � ���������� �������,������ � ���������� �������)
          TimerConfigure(TIMER0_BASE, TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PERIODIC);
          //������� ������������ 1000(25000000/1000=25000)
          TimerPrescaleSet (TIMER0_BASE, TIMER_B, 1000);
          //1/25000=40us->40us*25000=1s.���������� ������ �� ������� ����� �����������(1/FCu=4e-8,1ms/4e-8=25000)=>25000000/1000=25000
          TimerLoadSet(TIMER0_BASE,TIMER_B,25000);
          //Register interrupt
          TimerIntRegister(TIMER0_BASE,TIMER_B,TIM0_IRQ);

          //
          // Configure the Timer0B interrupt for timer timeout.(����. ������ ������� ������� � ����� �)
          //
          TimerIntEnable(TIMER0_BASE, TIMER_TIMB_TIMEOUT);
          // Enable the Timer0B interrupt on the processor (NVIC).
          //
          IntEnable(INT_TIMER0B);
         //���. ������
          TimerEnable(TIMER0_BASE,TIMER_B);




      IntMasterEnable();

    //GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_0,0xFF);
    //
    // Enable processor interrupts.
    //


	while(1)
	{


        if(g_bIntFlag)
        {
            GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_0,0xFF);
        }
        else
        {
            GPIOPinWrite(GPIO_PORTN_BASE,GPIO_PIN_0,0x00);
        }



	}
}
