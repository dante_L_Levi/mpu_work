#include "Main.h"


void Handler_Ressieve_UART0(void)
{
    char rcv_ch;
    uint32_t IntStatus;
    IntStatus=UARTIntStatus(UART0_BASE,true);
    UARTIntClear(UART0_BASE,IntStatus);

    rcv_ch=UARTCharGetNonBlocking(UART0_BASE);
    if((uint32_t)rcv_ch!=-1)
    {
        while(UARTCharPutNonBlocking(UART0_BASE,rcv_ch));//Send ALL Data
    }
}


void SendUartString(uint32_t nBase,char *str)
{

    while(*str!='\0')
    {
        if(UARTSpaceAvail(nBase))//check null FIFO
       while(UARTCharPutNonBlocking(nBase,*str));//Send ALL Data
    }
    UARTCharPut(UART0_BASE,'\r');
    UARTCharPut(UART0_BASE,'\n');
}


void RCC_Init(void)
{
    SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);//set Clock
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);//clock periph
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);//clock periph

}

void UART_Init(void)
{
    GPIOPinConfigure(GPIO_PA0_U0RX);//config pin UART0 RX
    GPIOPinConfigure(GPIO_PA1_U0TX);//config pin UART0 TX
    GPIOPinTypeUART(GPIO_PORTA_BASE,GPIO_PIN_0|GPIO_PIN_1);//set type pin

    IntDisable(INT_UART0);//off interrupt uart
    UARTClockSourceSet(UART0_BASE,UART_CLOCK_PIOSC);//on clock UART
    UARTConfigSetExpClk(UART0_BASE,16000000,115200,UART_CONFIG_WLEN_8|UART_CONFIG_STOP_ONE|UART_CONFIG_PAR_NONE);//set params UART
    UARTFIFODisable(UART0_BASE);//off Transmit and ressieve
    UARTIntEnable(UART0_BASE,UART_INT_RT|UART_INT_RX);//on interrupt U0
    UARTIntRegister(UART0_BASE,Handler_Ressieve_UART0);//register interrupt
    UARTEnable(UART0_BASE);
    IntEnable(INT_UART0);//on interrupt uart



}


void main(void)
{
    RCC_Init();
    UART_Init();

    while(1)
    {
        UARTCharPut(UART0_BASE,'T');//Send Symvol
        UARTCharPut(UART0_BASE,'I');//Send Symvol
        SysCtlDelay(60000);
        SendUartString(UART0_BASE,"Data TivaC");
        SysCtlDelay(60000);

    }
}


