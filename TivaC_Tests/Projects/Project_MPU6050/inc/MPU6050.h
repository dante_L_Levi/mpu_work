#ifndef INC_MPU6050_H_
#define INC_MPU6050_H_


#include "Main.h"


/* Default I2C address */
#define MPU6050_I2C_ADDR            0x68

/* Who I am register value */
#define ID_MPU6050                  0x68

/* MPU6050 registers */
#define MPU6050_AUX_VDDIO           0x01
#define MPU6050_SMPLRT_DIV          0x19
#define MPU6050_CONFIG              0x1A
#define MPU6050_GYRO_CONFIG         0x1B
#define MPU6050_ACCEL_CONFIG        0x1C
#define MPU6050_MOTION_THRESH       0x1F
#define MPU6050_INT_PIN_CFG         0x37
#define MPU6050_INT_ENABLE          0x38
#define MPU6050_INT_STATUS          0x3A
#define MPU6050_ACCEL_XOUT_H        0x3B
#define MPU6050_ACCEL_XOUT_L        0x3C
#define MPU6050_ACCEL_YOUT_H        0x3D
#define MPU6050_ACCEL_YOUT_L        0x3E
#define MPU6050_ACCEL_ZOUT_H        0x3F
#define MPU6050_ACCEL_ZOUT_L        0x40
#define MPU6050_TEMP_OUT_H          0x41
#define MPU6050_TEMP_OUT_L          0x42
#define MPU6050_GYRO_XOUT_H         0x43
#define MPU6050_GYRO_XOUT_L         0x44
#define MPU6050_GYRO_YOUT_H         0x45
#define MPU6050_GYRO_YOUT_L         0x46
#define MPU6050_GYRO_ZOUT_H         0x47
#define MPU6050_GYRO_ZOUT_L         0x48
#define MPU6050_MOT_DETECT_STATUS   0x61
#define MPU6050_SIGNAL_PATH_RESET   0x68
#define MPU6050_MOT_DETECT_CTRL     0x69
#define MPU6050_USER_CTRL           0x6A
#define MPU6050_PWR_MGMT_1          0x6B
#define MPU6050_PWR_MGMT_2          0x6C
#define MPU6050_FIFO_COUNTH         0x72
#define MPU6050_FIFO_COUNTL         0x73
#define MPU6050_FIFO_R_W            0x74
#define MPU6050_WHO_AM_I            0x75

#define MPU6050_ACCEL_CONFIG_XA_S   0x80
#define MPU6050_ACCEL_CONFIG_YA_ST  0x40
#define MPU6050_ACCEL_CONFIG_ZA_ST  0x20



/* Gyro sensitivities in degrees/s */
#define MPU6050_GYRO_SENS_250       ((float) 131)
#define MPU6050_GYRO_SENS_500       ((float) 65.5)
#define MPU6050_GYRO_SENS_1000      ((float) 32.8)
#define MPU6050_GYRO_SENS_2000      ((float) 16.4)

/* Acce sensitivities in g/s */
#define MPU6050_ACCE_SENS_2         ((float) 16384)
#define MPU6050_ACCE_SENS_4         ((float) 8192)
#define MPU6050_ACCE_SENS_8         ((float) 4096)
#define MPU6050_ACCE_SENS_16        ((float) 2048)






#define SD_MPU6050_DataRate_8KHz       0   /*!< Sample rate set to 8 kHz */
#define SD_MPU6050_DataRate_4KHz       1   /*!< Sample rate set to 4 kHz */
#define SD_MPU6050_DataRate_2KHz       3   /*!< Sample rate set to 2 kHz */
#define SD_MPU6050_DataRate_1KHz       7   /*!< Sample rate set to 1 kHz */
#define SD_MPU6050_DataRate_500Hz      15  /*!< Sample rate set to 500 Hz */
#define SD_MPU6050_DataRate_250Hz      31  /*!< Sample rate set to 250 Hz */
#define SD_MPU6050_DataRate_125Hz      63  /*!< Sample rate set to 125 Hz */
#define SD_MPU6050_DataRate_100Hz      79  /*!< Sample rate set to 100 Hz */



#define MPU6050_DEVICE_RESET_MASK_ON         0x80
#define MPU6050_DEVICE_RESET_MASK_OFF        0x00

#define MPU6050_SLEEP_MASK_ON                0x40
#define MPU6050_SLEEP_MASK_OFF               0x00

#define MPU6050_CYCLE_MASK_ON                0x20
#define MPU6050_CYCLE_MASK_OFF               0x00

#define MPU6050_TEMP_DIS_MASK_OFF            0x20
#define MPU6050_TEMP_DIS_MASK_ON             0x00





/**
 * @brief  MPU6050 can have 2 different slave addresses, depends on it's input AD0 pin
 *         This feature allows you to use 2 different sensors with this library at the same time
 */
typedef enum  {
    SD_MPU6050_Device_0 = 0x00, /*!< AD0 pin is set to low */
    SD_MPU6050_Device_1 = 0x02  /*!< AD0 pin is set to high */
} SD_MPU6050_Device;


/**
 * @brief  MPU6050 result enumeration
 */
typedef enum  {
    SD_MPU6050_Result_Ok = 0x00,          /*!< Everything OK */
    SD_MPU6050_Result_Error,              /*!< Unknown error */
    SD_MPU6050_Result_DeviceNotConnected, /*!< There is no device with valid slave address */
    SD_MPU6050_Result_DeviceInvalid       /*!< Connected device with address is not MPU6050 */
} SD_MPU6050_Result;







typedef enum  {
    SD_MPU6050_Accelerometer_2G = 0x00, /*!< Range is +- 2G */
    SD_MPU6050_Accelerometer_4G = 0x01, /*!< Range is +- 4G */
    SD_MPU6050_Accelerometer_8G = 0x02, /*!< Range is +- 8G */
    SD_MPU6050_Accelerometer_16G = 0x03 /*!< Range is +- 16G */
} SD_MPU6050_Accelerometer;



typedef enum {
    SD_MPU6050_Gyroscope_250s = 0x00,  /*!< Range is +- 250 degrees/s */
    SD_MPU6050_Gyroscope_500s = 0x01,  /*!< Range is +- 500 degrees/s */
    SD_MPU6050_Gyroscope_1000s = 0x02, /*!< Range is +- 1000 degrees/s */
    SD_MPU6050_Gyroscope_2000s = 0x03  /*!< Range is +- 2000 degrees/s */
} SD_MPU6050_Gyroscope;



typedef struct  {
    /* Private */
    uint8_t Address;         /*!< I2C address of device. */
    float Gyro_Mult;         /*!< Gyroscope corrector from raw data to "degrees/s". Only for private use */
    float Acce_Mult;         /*!< Accelerometer corrector from raw data to "g". Only for private use */
    uint8_t Config_Accel;   //Value Register Confugure Accel
    uint8_t Config_Gyro;    //Value Register Confugure Gyro
    uint8_t Config_Rate;    //Value Register Rate

    /* Public */

    uint8_t ID_Device;      /*******ID Device for work**************/

    int16_t Accelerometer_X; /*!< Accelerometer value X axis */
    int16_t Accelerometer_Y; /*!< Accelerometer value Y axis */
    int16_t Accelerometer_Z; /*!< Accelerometer value Z axis */

    int16_t Gyroscope_X;     /*!< Gyroscope value X axis */
    int16_t Gyroscope_Y;     /*!< Gyroscope value Y axis */
    int16_t Gyroscope_Z;     /*!< Gyroscope value Z axis */

    float Accelerometer_X_real; /*!< Accelerometer value X axis */
    float Accelerometer_Y_real; /*!< Accelerometer value Y axis */
    float Accelerometer_Z_real; /*!< Accelerometer value Z axis */

    float Gyroscope_X_real;     /*!< Gyroscope value X axis */
    float Gyroscope_Y_real;     /*!< Gyroscope value Y axis */
    float Gyroscope_Z_real;     /*!< Gyroscope value Z axis */

    bool    isTempEn;
    float   Temperature;       /*!< Temperature in degrees */



} SD_MPU6050;




/*************************Function Read ID ADRESS*****************/
SD_MPU6050_Result Read_ID(SD_MPU6050 *Val);
/*************************Function Init MPU6050*****************/
/*************************Function Init MPU6050*****************/
SD_MPU6050_Result SD_MPU6050_Init(SD_MPU6050 *Val,uint8_t PWR_M,
                                  SD_MPU6050_Gyroscope configGyro,
                                  float configGyroSENSE,
                                  SD_MPU6050_Accelerometer configAccl,
                                  float configAcclSENSE,
                                  uint8_t rate,
                                  bool isFlagTemp);

/*****************Function Read Accelerometer Data******************/
void MPU6050_ReadAccel(SD_MPU6050 *Val);
/*****************Function Read Gyroscope Data******************/
void MPU6050_ReadGyro(SD_MPU6050 *Val);
/*****************Function Read Temperature Data******************/
void MPU6050_ReadTemperature(SD_MPU6050 *DataStruct);
/*****************Function Read Temperature Data******************/
void Convert_Value_MPU6050(SD_MPU6050 *DataStruct);






#endif /* INC_MPU6050_H_ */
