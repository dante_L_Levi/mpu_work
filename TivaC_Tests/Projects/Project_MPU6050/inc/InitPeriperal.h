
#ifndef INC_INITPERIPERAL_H_
#define INC_INITPERIPERAL_H_


#include "Main.h"


/*****************************Settings UART6******************************/
void Init_UART6(uint32_t Baud);
/*****************************Settings Timers******************************/
void Init_Timers(void);
/**********************LED GPIO Config****************************/
void LED_Init(void);

/*********************************Settings I2C0************************/
void setting_i2c0(void);




#endif /* INC_INITPERIPERAL_H_ */
