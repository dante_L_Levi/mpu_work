#ifndef INC_TRANSFERFRAME_H_
#define INC_TRANSFERFRAME_H_


#include "Main.h"


/**************************Function Transmit String UART6 ********************/
void UART_SendString(uint32_t Uart_Base, const char *str);
/**************************Function Transmit Frame Bytes UART6 ********************/
void UART_SendFrame(uint32_t Uart_Base, uint8_t *data,uint32_t size);
/*********************Read Data I2C0******************************/
void read_i2c(uint32_t i2c,
              uint8_t addr_slave,
              uint8_t* data, uint8_t count);
/*********************Read Data I2C0 in Set Register******************************/
void read_i2c_Reg(uint32_t i2c,
              uint8_t addr_slave,uint8_t addr_reg,
              uint8_t* data, uint8_t count);
/********************Write Data I2C0****************************************/
void write_i2c(uint32_t i2c,
                   uint8_t addr_slave,uint8_t* data, uint8_t count);
/********************Write Data I2C0 in Register****************************************/
void write_i2c_Reg(uint32_t i2c,
                   uint8_t addr_slave, uint8_t addr_reg,
                   uint8_t* data, uint8_t count);



uint8_t readI2CByte(uint32_t i2c,uint16_t device_address, uint16_t device_register);
void writeByte(uint32_t i2c,uint16_t device_address, uint16_t device_register, uint8_t device_data);








#endif /* INC_TRANSFERFRAME_H_ */
