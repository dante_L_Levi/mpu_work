#include "Main.h"

uint32_t sysClock;
uint32_t led_blink=1;
#define UART_TRANSFER       UART6_BASE
SD_MPU6050 mpu6050;



/*******************HANDLER UART*****************************/
void Handler_UART6(void)
{

}



/*******************HANDLER TIMER*****************************/
void handler_TIM1(void)
{
    TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    led_blink++;
    UART_SendString(UART_TRANSFER,"Hello World\r\n");

}



void LedBlink(uint32_t count)
{
    if(count%2==0)
    {
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 0x02);
    }
    else
    {
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 0x00);
    }
}

void main(void)
{
    sysClock= SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                                       SYSCTL_OSC_MAIN |
                                       SYSCTL_USE_PLL |
                                       SYSCTL_CFG_VCO_480),
                                       120000000);

    setting_i2c0();


    Init_UART6(115200);
    Init_Timers();
    LED_Init();
    IntMasterEnable();
    mpu6050.Address=MPU6050_I2C_ADDR|(uint8_t)SD_MPU6050_Device_0;

    SD_MPU6050_Init(&mpu6050,
                    MPU6050_DEVICE_RESET_MASK_OFF|MPU6050_SLEEP_MASK_OFF|MPU6050_CYCLE_MASK_OFF,
                    SD_MPU6050_Gyroscope_2000s,
                    MPU6050_GYRO_SENS_2000,
                    SD_MPU6050_Accelerometer_16G,
                    MPU6050_ACCE_SENS_16,
                    SD_MPU6050_DataRate_1KHz,
                    true);

    while(1)
    {
        LedBlink(led_blink);
        if(Read_ID(&mpu6050)==SD_MPU6050_Result_Ok)
        {
            //Print Connected OK
            UART_SendString(UART6_BASE,"Connected MPU6050 OK\r\n");
            MPU6050_ReadAccel(&mpu6050);
            MPU6050_ReadGyro(&mpu6050);
            MPU6050_ReadTemperature(&mpu6050);
            Convert_Value_MPU6050(&mpu6050);
        }
        else
        {
            UART_SendString(UART6_BASE,"Error Connected MPU6050\r\n");
        }


    }
}

