#include "InitPeriperal.h"
#include "inc/hw_i2c.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"

extern uint32_t sysClock;

/*****************************Settings UART6******************************/
void Init_UART6(uint32_t Baud)
{

        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
        GPIOPinConfigure(GPIO_PP0_U6RX);
        GPIOPinConfigure(GPIO_PP1_U6TX);
        GPIOPinTypeUART(GPIO_PORTP_BASE,GPIO_PIN_0|GPIO_PIN_1);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOP))
        {
        }
        SysCtlPeripheralEnable(SYSCTL_PERIPH_UART6);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART6))
        {
        }

        UARTClockSourceSet(UART6_BASE,UART_CLOCK_SYSTEM) ;
        UARTConfigSetExpClk(UART6_BASE, sysClock, Baud,
                           (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                            UART_CONFIG_PAR_NONE));
        //UARTIntRegister(UART6_BASE, Handler_UART6);
        //IntEnable(INT_UART6);
        //UARTIntEnable(UART6_BASE, UART_INT_RX | UART_INT_RT);
        UARTEnable(UART6_BASE);

}




/*****************************Settings Timers******************************/
void Init_Timers(void)
{
        //Timer1 - 1Hz
        SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1))
        {
        }
        TimerClockSourceSet(TIMER1_BASE, TIMER_CLOCK_SYSTEM);
        TimerConfigure(TIMER1_BASE, TIMER_CFG_A_PERIODIC);
        TimerLoadSet(TIMER1_BASE, TIMER_A, sysClock);
        TimerIntRegister(TIMER1_BASE, TIMER_A, handler_TIM1);
        TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
        TimerEnable(TIMER1_BASE, TIMER_A);
        //NVIC
        IntEnable(INT_TIMER1A);

}




/**********************LED GPIO Config****************************/
void LED_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1); //config output Pin LED
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0x01);
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 0x02);
}


/*********************************Settings I2C0************************/
void setting_i2c0(void)
{
    //Setting I2C
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
    SysCtlPeripheralReset(SYSCTL_PERIPH_I2C0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_I2C0))
    {
    }
    //Setting GPIO
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB))
    {
    }
    GPIODirModeSet(GPIO_PORTB_BASE, GPIO_PIN_2 | GPIO_PIN_3, GPIO_DIR_MODE_HW);
    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);
    I2CMasterInitExpClk(I2C0_BASE, sysClock, true);//true - 400kbps
    //clear I2C FIFOs
    //HWREG(I2C0_BASE + I2C_O_FIFOCTL) = 80008000;
    I2CMasterEnable(I2C0_BASE);

}










