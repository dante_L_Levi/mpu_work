#include "MPU6050.h"
#define MPU_6050_I2C    I2C0_BASE


/************************Function Read data in Reg*********************/
uint8_t IO_Read(uint8_t devAddr,uint8_t RegAddr)
{
    uint8_t data=0x00;
    read_i2c_Reg(MPU_6050_I2C,devAddr,RegAddr,&data,1);
    return data;
}

/********************************Function Write data in Reg********************************/
void IO_Write(uint8_t devAddr, uint8_t RegAddr, uint8_t Value)
{
  write_i2c_Reg(MPU_6050_I2C,devAddr,RegAddr,&Value,1);

}

/*************************Function Read ID ADRESS*****************/
SD_MPU6050_Result Read_ID(SD_MPU6050 *Val)
{

    uint8_t ctrl=0x00;
    uint8_t addr=Val->Address;
    //read_i2c_Reg(MPU_6050_I2C,addr,MPU6050_WHO_AM_I,&ctrl,1);
    ctrl=IO_Read(addr,MPU6050_WHO_AM_I);
    if(ctrl==ID_MPU6050)
    {
        Val->ID_Device=ctrl;
        return SD_MPU6050_Result_Ok;
    }
    else
    {
        return SD_MPU6050_Result_DeviceInvalid;
    }


}


/*************************Function Config Accel*****************/
SD_MPU6050_Result Config_Accel(SD_MPU6050 *Val,SD_MPU6050_Accelerometer configAccl,uint8_t configAcclSENSE)
{
    uint8_t temp;
    uint8_t address = Val->Address;
    //===========Read Config Accel Register==================
    temp=IO_Read(address,MPU6050_ACCEL_CONFIG);
    temp|=configAccl|configAcclSENSE;
    IO_Write(address,MPU6050_ACCEL_CONFIG,temp);
    if(IO_Read(address,MPU6050_ACCEL_CONFIG)==0x00)
    {
        return SD_MPU6050_Result_Error;
    }
    Val->Config_Accel=temp;

    switch(configAccl)
    {
        case SD_MPU6050_Accelerometer_2G:
        {
            Val->Acce_Mult = (float)1 / MPU6050_ACCE_SENS_2;
            break;
        }

        case SD_MPU6050_Accelerometer_4G:
        {
            Val->Acce_Mult = (float)1 / MPU6050_ACCE_SENS_4;
            break;
        }
        case SD_MPU6050_Accelerometer_8G:
        {
            Val->Acce_Mult = (float)1 / MPU6050_ACCE_SENS_8;
           break;
        }

        case SD_MPU6050_Accelerometer_16G:
        {
            Val->Acce_Mult = (float)1 / MPU6050_ACCE_SENS_16;
           break;
        }
    }

    return SD_MPU6050_Result_Ok;
}


/*************************Function Config Gyro*****************/
SD_MPU6050_Result Config_Gyro(SD_MPU6050 *Val,SD_MPU6050_Gyroscope configGyro,uint8_t configGyroSENSE)
{
    uint8_t temp;
    uint8_t address = Val->Address;
    //===========Read Config Gyro Register==================
    temp=IO_Read(address,MPU6050_GYRO_CONFIG);
    temp|=configGyro|configGyroSENSE;
    IO_Write(address,MPU6050_GYRO_CONFIG,temp);
    if(IO_Read(address,MPU6050_GYRO_CONFIG)==0x00)
    {
        return SD_MPU6050_Result_Error;
    }
    Val->Config_Gyro=temp;
    switch (configGyro) {
                case SD_MPU6050_Gyroscope_250s:
                    Val->Gyro_Mult = (float)1 / MPU6050_GYRO_SENS_250;
                    break;
                case SD_MPU6050_Gyroscope_500s:
                    Val->Gyro_Mult = (float)1 / MPU6050_GYRO_SENS_500;
                    break;
                case SD_MPU6050_Gyroscope_1000s:
                    Val->Gyro_Mult = (float)1 / MPU6050_GYRO_SENS_1000;
                    break;
                case SD_MPU6050_Gyroscope_2000s:
                    Val->Gyro_Mult = (float)1 / MPU6050_GYRO_SENS_2000;
                    break;
                default:
                    break;
            }


    return SD_MPU6050_Result_Ok;
}

/*************************Function Config Rate*****************/
SD_MPU6050_Result SD_MPU6050_SetDataRate(SD_MPU6050 *Val,uint8_t rate)
{
    uint8_t temp=0x00;
    uint8_t address = Val->Address;
    IO_Write(address,MPU6050_SMPLRT_DIV,rate);
    temp=IO_Read(address,MPU6050_SMPLRT_DIV);
    if(temp!=rate)
    {

        return SD_MPU6050_Result_Error;
    }
    else
    {
        Val->Config_Rate=rate;
        return SD_MPU6050_Result_Ok;
    }
}

/**************Config Power & OSC System*****************/
void PowerManagment_MPU6050(SD_MPU6050 *Val,uint8_t Value)
{
  uint8_t address= Val->Address;
  IO_Write(address,MPU6050_PWR_MGMT_1,Value);
}

/********************Enable work Temperature sensor***********************/
void Enable_TempSensor(SD_MPU6050 *DataStruct)
{
    DataStruct->isTempEn=true;
    uint8_t address=DataStruct->Address;
    uint8_t temp=0x00;
    temp=IO_Read(address, MPU6050_PWR_MGMT_1);
    temp|=MPU6050_TEMP_DIS_MASK_ON;
    IO_Write(address,MPU6050_PWR_MGMT_1,temp);
}
/********************Disable work Temperature sensor***********************/
void Disable_TempSensor(SD_MPU6050 *DataStruct)
{
    DataStruct->isTempEn=false;
    uint8_t address=DataStruct->Address;
    uint8_t temp=0x00;
    temp=IO_Read(address, MPU6050_PWR_MGMT_1);
    temp|=MPU6050_TEMP_DIS_MASK_OFF;
    IO_Write(address,MPU6050_PWR_MGMT_1,temp);
}



/*************************Function Init MPU6050*******************************/
SD_MPU6050_Result SD_MPU6050_Init(SD_MPU6050 *Val,uint8_t PWR_M,
                                  SD_MPU6050_Gyroscope configGyro,
                                  float configGyroSENSE,
                                  SD_MPU6050_Accelerometer configAccl,
                                  float configAcclSENSE,
                                  uint8_t rate,
                                  bool isFlagTemp)
{

   if(Read_ID(Val)==SD_MPU6050_Result_Ok)
   {
     PowerManagment_MPU6050(Val,PWR_M);
     SD_MPU6050_SetDataRate(Val,rate);
     Config_Gyro(Val,configGyro,configGyroSENSE);
     Config_Accel(Val,configAccl,configAcclSENSE);
     if(isFlagTemp==true)
     {
         Enable_TempSensor(Val);
     }
     else
     {
         Disable_TempSensor(Val);
     }

     return SD_MPU6050_Result_Ok;
   }
   else
   {
      return SD_MPU6050_Result_DeviceNotConnected;
   }
}


/*****************Function Read Accelerometer Data******************/
void MPU6050_ReadAccel(SD_MPU6050 *Val)
{
    uint8_t data[6];
    uint8_t reg = MPU6050_ACCEL_XOUT_H;
    uint8_t address = Val->Address;
    read_i2c_Reg(MPU_6050_I2C,address,reg,data,6);
    /* Format */
    Val->Accelerometer_X = (int16_t)(data[0] << 8 | data[1]);
    Val->Accelerometer_Y = (int16_t)(data[2] << 8 | data[3]);
    Val->Accelerometer_Z = (int16_t)(data[4] << 8 | data[5]);
}

/*****************Function Read Gyroscope Data******************/
void MPU6050_ReadGyro(SD_MPU6050 *DataStruct)
{
    uint8_t data[6];
    uint8_t reg = MPU6050_GYRO_XOUT_H;
    uint8_t address = DataStruct->Address;
    read_i2c_Reg(MPU_6050_I2C,address,reg,data,6);

    /* Format */
        DataStruct->Gyroscope_X = (int16_t)(data[0] << 8 | data[1]);
        DataStruct->Gyroscope_Y = (int16_t)(data[2] << 8 | data[3]);
        DataStruct->Gyroscope_Z = (int16_t)(data[4] << 8 | data[5]);
}

/*****************Function Read Temperature Data******************/
void MPU6050_ReadTemperature(SD_MPU6050 *DataStruct)
{
    uint8_t address = DataStruct->Address;
    uint8_t data[2];
    float temp;
    read_i2c_Reg(MPU_6050_I2C,address,MPU6050_TEMP_OUT_H,data,2);
    /* Format temperature */
    temp = (data[0] << 8 | data[1]);
    DataStruct->Temperature = (float)((int16_t)temp / (float)340.0 + (float)36.53);


}




/*****************Function Read Temperature Data******************/
void Convert_Value_MPU6050(SD_MPU6050 *DataStruct)
{
    DataStruct->Gyroscope_X_real=DataStruct->Gyroscope_X*DataStruct->Gyro_Mult;
    DataStruct->Gyroscope_Y_real=DataStruct->Gyroscope_Y*DataStruct->Gyro_Mult;
    DataStruct->Gyroscope_Z_real=DataStruct->Gyroscope_Z*DataStruct->Gyro_Mult;

    DataStruct->Accelerometer_X_real=DataStruct->Accelerometer_X*DataStruct->Acce_Mult;
    DataStruct->Accelerometer_Y_real=DataStruct->Accelerometer_Y*DataStruct->Acce_Mult;
    DataStruct->Accelerometer_Z_real=DataStruct->Accelerometer_Z*DataStruct->Acce_Mult;

}











