#include "TransferFrame.h"






/**************************Function Transmit String UART6 ********************/
void UART_SendString(uint32_t Uart_Base, const char *str)
{
    uint32_t i=0;
    while(str[i]!=0)
    {
        UARTCharPut(Uart_Base,str[i]);
        i++;
    }
}



/**************************Function Transmit Frame Bytes UART6 ********************/
void UART_SendFrame(uint32_t Uart_Base, uint8_t *data,uint32_t size)
{
    uint32_t i=0;
    while(i<size)
    {
        UARTCharPut(Uart_Base,data[i]);
        i++;
    }
}


/*********************Read Data I2C0******************************/
void read_i2c(uint32_t i2c,
              uint8_t addr_slave,
              uint8_t* data, uint8_t count)
{
    uint16_t i = 0;
    uint16_t delay = 13000;
    I2CMasterSlaveAddrSet(i2c, addr_slave, false);
    I2CMasterControl(i2c, I2C_MASTER_CMD_SINGLE_SEND);
           for(i = 0; i < delay; i++);

           if(count == 1)
           {
               I2CMasterSlaveAddrSet(i2c, addr_slave, true);
               I2CMasterControl(i2c, I2C_MASTER_CMD_SINGLE_RECEIVE);
               for(i = 0; i < delay; i++);
               *data = (uint8_t)I2CMasterDataGet(i2c);
           }
           else
           {
               I2CMasterSlaveAddrSet(i2c, addr_slave, true);
               I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_RECEIVE_START);
               for(i = 0; i < delay; i++);
               *data++ = (uint8_t)I2CMasterDataGet(i2c);
               count--;

               while(count)
               {
                   if(count > 1)
                   {
                       I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
                       for(i = 0; i < delay; i++);
                       *data++ = (uint8_t)I2CMasterDataGet(i2c);
                   }
                   else if(count == 1)
                   {
                       I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
                       for(i = 0; i < delay; i++);
                       *data = (uint8_t)I2CMasterDataGet(i2c);
                   }
                   count--;
               }
           }

}


/*********************Read Data I2C0 in Set Register******************************/
void read_i2c_Reg(uint32_t i2c,
              uint8_t addr_slave,uint8_t addr_reg,
              uint8_t* data, uint8_t count)
{
    /* Input data:  i2c - which I2C interface using
        *              addr_slave - I2C address slave device
        *              addr_reg - address register in slave device
        *              data - where to write the read data
        *              count - how many byte need to read
        * */

        uint16_t i = 0;
        uint16_t delay = 3000;

       I2CMasterSlaveAddrSet(i2c, addr_slave, false);
       I2CMasterDataPut(i2c, addr_reg);
       I2CMasterControl(i2c, I2C_MASTER_CMD_SINGLE_SEND);
       for(i = 0; i < delay; i++);

       if(count == 1)
       {
           I2CMasterSlaveAddrSet(i2c, addr_slave, true);
           I2CMasterControl(i2c, I2C_MASTER_CMD_SINGLE_RECEIVE);
           for(i = 0; i < delay; i++);
           *data = (uint8_t)I2CMasterDataGet(i2c);
       }
       else
       {
           I2CMasterSlaveAddrSet(i2c, addr_slave, true);
           I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_RECEIVE_START);
           for(i = 0; i < delay; i++);
           *data++ = (uint8_t)I2CMasterDataGet(i2c);
           count--;

           while(count)
           {
               if(count > 1)
               {
                   I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
                   for(i = 0; i < delay; i++);
                   *data++ = (uint8_t)I2CMasterDataGet(i2c);
               }
               else if(count == 1)
               {
                   I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
                   for(i = 0; i < delay; i++);
                   *data = (uint8_t)I2CMasterDataGet(i2c);
               }
               count--;
           }
       }
}



/********************Write Data I2C0****************************************/
void write_i2c(uint32_t i2c,
                   uint8_t addr_slave,uint8_t* data, uint8_t count)
{
    uint16_t i = 0;
    uint16_t delay = 3000;
    I2CMasterSlaveAddrSet(i2c, addr_slave, false);
    I2CMasterControl(i2c,I2C_MASTER_CMD_SINGLE_SEND);
    for(i = 0; i < delay; i++);
    if(count == 1)
        {
            I2CMasterDataPut(i2c, *data);
            I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_FINISH);
            for(i = 0; i < delay; i++);
        }
        else
        {
            while(count)
            {
                if(count > 1)
                {
                    I2CMasterDataPut(i2c, *data++);
                    I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_CONT);
                    for(i = 0; i < delay; i++);
                }
                else if(count  == 1)
                {
                    I2CMasterDataPut(i2c, *data);
                    I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_FINISH);
                    for(i = 0; i < delay; i++);
                }
                count--;
            }
        }
}


/********************Write Data I2C0 in Register****************************************/
void write_i2c_Reg(uint32_t i2c,
                   uint8_t addr_slave, uint8_t addr_reg,
                   uint8_t* data, uint8_t count)
{
    /* Input data:  i2c - which I2C interface using
     *              addr_slave - I2C address slave device
     *              addr_reg - address register in slave device
     *              data - data to send
     *              count - how many byte need to send
     * */

    uint16_t i = 0;
    uint16_t delay = 3000;

    I2CMasterSlaveAddrSet(i2c, addr_slave, false);
    I2CMasterDataPut(i2c, addr_reg);
    I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_START);
    for(i = 0; i < delay; i++);

    if(count == 1)
    {
        I2CMasterDataPut(i2c, *data);
        I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_FINISH);
        for(i = 0; i < delay; i++);
    }
    else
    {
        while(count)
        {
            if(count > 1)
            {
                I2CMasterDataPut(i2c, *data++);
                I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_CONT);
                for(i = 0; i < delay; i++);
            }
            else if(count  == 1)
            {
                I2CMasterDataPut(i2c, *data);
                I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_FINISH);
                for(i = 0; i < delay; i++);
            }
            count--;
        }
    }
}

//=================================================================================================================================



uint8_t readI2CByte(uint32_t i2c,uint16_t device_address, uint16_t device_register)
{
    //specify that we want to communicate to device address with an intended write to bus
        I2CMasterSlaveAddrSet(i2c, device_address, false);
        //the register to be read
        I2CMasterDataPut(i2c, device_register);

        //send control byte and register address byte to slave device
        I2CMasterControl(i2c, I2C_MASTER_CMD_SINGLE_SEND);

        //wait for MCU to complete send transaction
        while(I2CMasterBusy(i2c));
        //read from the specified slave device
        I2CMasterSlaveAddrSet(i2c, device_address, true);

       //send control byte and read from the register from the MCU
       I2CMasterControl(i2c, I2C_MASTER_CMD_SINGLE_RECEIVE);

       //wait while checking for MCU to complete the transaction
       while(I2CMasterBusy(i2c));

      //Get the data from the MCU register and return to caller
      return( I2CMasterDataGet(i2c));
}


void writeByte(uint32_t i2c,uint16_t device_address, uint16_t device_register, uint8_t device_data)
{
    //specify that we want to communicate to device address with an intended write to bus
    I2CMasterSlaveAddrSet(i2c, device_address, false);

    //register to be read
    I2CMasterDataPut(i2c, device_register);

    //send control byte and register address byte to slave device
    I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_START);

    //wait for MCU to finish transaction
    while(I2CMasterBusy(i2c));

    I2CMasterSlaveAddrSet(i2c, device_address, true);

    //specify data to be written to the above mentioned device_register
    I2CMasterDataPut(i2c, device_data);

    //wait while checking for MCU to complete the transaction
    I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);

    //wait for MCU & device to complete transaction
    while(I2CMasterBusy(i2c));
}










