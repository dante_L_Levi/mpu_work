#include "Main.h"

uint32_t mpuClock;//Real Clock
uint32_t ledBlink=0;
#define CPU_FREQ                    40000000
#define UART6_BAUD                  115200
/*=====================================================================*/

/*********************Clock Config****************************/
void Init_RCC(void);
/**********************LED GPIO Config****************************/
void LED_Init(void);
/******************Init Timer Tick****************************/
void Timer_Init(void);
/*************************************UART 6 Init*****************************/
void UART6_Init(uint32_t Baud);
/**************************Function Transmit String ********************/
void UART_SendString(uint32_t Uart_Base, char *str);
void LedBlink(uint32_t count);
/*****************************Init I2C****************************/
void I2C0_Init(void);





void TIM0_IRQ(void)
{
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    ledBlink++;
    char str[]="Test Transmit UART\r\n";
    UART_SendString(UART6_BASE,str);

}


void Uart6_IRQ(void)
{

}







void main(void)
{
    Init_RCC();
    LED_Init();
    Timer_Init();
    UART6_Init(UART6_BAUD);
    I2C0_Init();
    while(1)
    {
        LedBlink(ledBlink);
    }
}


/*********************Clock Config****************************/
void Init_RCC(void)
{
    mpuClock=SysCtlClockFreqSet ((SYSCTL_XTAL_25MHZ |
                SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
                SYSCTL_CFG_VCO_480), CPU_FREQ);
}


/**********************LED GPIO Config****************************/
void LED_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE,GPIO_PIN_0|GPIO_PIN_1); //config output Pin LED
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0x01);
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 0x02);
}


/******************Init Timer Tick****************************/
void Timer_Init(void)
{
        SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0))
        {
        }
       //config Mode 16-bit Timer split count
       TimerConfigure(TIMER0_BASE,TIMER_CFG_PERIODIC);
       //======================Config for Split Mode 16bit Timer*******************
       //Config: 40000000/4000=10000,prescale 4000,Ttim=4000/40000000=0,0001s
       //TimerPrescaleSet (TIMER0_BASE, TIMER_B, 4000);
       //config:Tism=200ms, n200ms=200ms/0,0001s=2000
       //==========================================================================
       //Config 1s:CLockMPU/2,  2s:CLockMPU,0.5s:CLockMPU/4
       TimerLoadSet(TIMER0_BASE,TIMER_A,mpuClock/2);
       TimerIntRegister(TIMER0_BASE,TIMER_A,TIM0_IRQ);
       //
       // Configure the Timer0B interrupt for timer timeout.(����. ������ ������� ������� � ����� �)
       //
       TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
       // Enable the Timer0B interrupt on the processor (NVIC).
      //
       IntEnable(INT_TIMER0A);
       TimerEnable(TIMER0_BASE, TIMER_A);
}



/*************************************UART 6 Init*****************************/
void UART6_Init(uint32_t Baud)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART6);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART6))
    {
    }
    GPIOPinConfigure(GPIO_PP0_U6RX);
    GPIOPinConfigure(GPIO_PP1_U6TX);
    GPIOPinTypeUART(GPIO_PORTP_BASE,GPIO_PIN_0|GPIO_PIN_1);
    UARTClockSourceSet(UART6_BASE,UART_CLOCK_PIOSC) ;
    UARTConfigSetExpClk(UART6_BASE, 16000000, Baud,
                       (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                        UART_CONFIG_PAR_NONE));
    UARTIntRegister(UART6_BASE, Uart6_IRQ);
    IntEnable(INT_UART6);
    UARTIntEnable(UART6_BASE, UART_INT_RX | UART_INT_RT);

}

/**************************Function Transmit String ********************/
void UART_SendString(uint32_t Uart_Base, char *str)
{
    uint32_t i=0;
    while(str[i]!=0)
    {
        UARTCharPut(Uart_Base,str[i]);
        i++;
    }
}



void LedBlink(uint32_t count)
{
    if(count%2==0)
    {
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0x01);
    }
    else
    {
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0x00);
    }
    if(count%3==0)
    {
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 0x02);
    }
    else
    {
            GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 0x00);
    }



}


/*****************************Init I2C****************************/
void I2C0_Init(void)
{
    //Enable Clock I2C0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_I2C0))
    {
    }
    //Enable Clock GPIOB for I2C0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    //Config GPIO for I2C
    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);

    //Set Pin for I2C
    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);
    //Set Baud
    I2CMasterInitExpClk(I2C0_BASE, mpuClock, true);//true - 400kbps
    //On I2C                                          //false - 100kbps
    I2CMasterEnable(I2C0_BASE);



}


/***********************read One Byte I2C*****************************/
uint8_t Read_I2CByte(uint32_t i2c,uint8_t addr_slave,uint8_t addr_reg)
{
    uint8_t val=0;
    I2CMasterSlaveAddrSet(i2c, addr_slave, false);
    I2CMasterDataPut(i2c, addr_reg);
    I2CMasterControl(i2c, I2C_MASTER_CMD_SINGLE_SEND);
    while(!I2CMasterBusy(i2c));
    while(I2CMasterBusy(i2c));
    I2CMasterSlaveAddrSet(i2c, addr_slave, true);
    I2CMasterControl(i2c, I2C_MASTER_CMD_SINGLE_RECEIVE);
    while(!I2CMasterBusy(i2c));
    while(I2CMasterBusy(i2c));
    val=(uint8_t)I2CMasterDataGet(i2c);
    return val;
}

/***********************************Write Data I2C****************/
void write_I2CByte(uint32_t i2c,
               uint8_t addr_slave,uint8_t addr_reg,
               uint8_t data)
{
    I2CMasterSlaveAddrSet(i2c, addr_slave, false);
    I2CMasterDataPut(i2c, addr_reg);
    I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_START);
    while(!I2CMasterBusy(i2c));
    while(I2CMasterBusy(i2c));
    I2CMasterDataPut(i2c,data);
    I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_FINISH);
    while(!I2CMasterBusy(i2c));
    while(I2CMasterBusy(i2c));









}


