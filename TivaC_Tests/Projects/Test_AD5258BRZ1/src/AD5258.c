#include "AD5258.h"

uint8_t deviceAddress;


void ADDR_SET(void)
{
    deviceAddress=AD5258_ADR1;
}

/************************Write Regiister RDAC****************************/
void writeRDAC(uint8_t value)
{
    write_I2CByte(I2C0_BASE,deviceAddress,RDAC_ADDRESS, value);
}


/************************Write Regiister EEPROM****************************/
void writeEEPROM(uint8_t value)
{
    write_I2CByte(I2C0_BASE,deviceAddress,EEPROM_ADDRESS, value);
}

/************************Read Regiister RDAC****************************/
uint8_t readRDAC(void)
{
  return  Read_I2CByte(I2C0_BASE,deviceAddress,RDAC_ADDRESS);
}

/************************Read Regiister EEPROM****************************/
uint8_t readEEPROM(void)
{
    return  Read_I2CByte(I2C0_BASE,deviceAddress,EEPROM_ADDRESS);
}


/*********************************Protected Write Value ******************************/
void toggleSoftWriteProtect(bool state)
{
    uint8_t value = 0;
      if(state)
      {
          value = SOFT_WRITE_PROTECT_ON;
          write_I2CByte(I2C0_BASE,deviceAddress,0x00,value);
      }
      else
      {
          value = SOFT_WRITE_PROTECT_OFF;
          write_I2CByte(I2C0_BASE,deviceAddress,0x00,value);
      }

}


// =======================Returns the Fraction of Tolerance===============================
float readTolerance(void)
{
    uint8_t integer = Read_I2CByte(I2C0_BASE,deviceAddress, TOLERANCE_ADDRESS_A);
    uint8_t decimal  = Read_I2CByte(I2C0_BASE,deviceAddress, TOLERANCE_ADDRESS_B);
    float answer = 0;

        if((integer & 0x80) == 0) { // Value is positive
            answer = (float) integer;
            answer += ((float) decimal) / 256;
        }
        else { // value is negative
            //integer = integer << 1;
            //integer = integer >> 1; // Clear MSB describing sign.
            integer = integer & 0x7F;
            answer = (float) integer;
            answer += -1 * (((float) decimal) / 256);
        }
     return answer;
}





