/*
 * Main.h
 *
 *  Created on: 15 ���. 2019 �.
 *      Author: Tatiana_Potrebko
 */

#ifndef INC_MAIN_H_
#define INC_MAIN_H_



#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"
#include "driverlib/adc.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"
#include "driverlib/i2c.h"



/***********************************Write Data I2C****************/
void write_I2CByte(uint32_t i2c,
               uint8_t addr_slave,uint8_t addr_reg,
               uint8_t data);
/***********************read One Byte I2C*****************************/
uint8_t Read_I2CByte(uint32_t i2c,uint8_t addr_slave,uint8_t addr_reg);

#endif /* INC_MAIN_H_ */
