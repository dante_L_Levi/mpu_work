#ifndef INC_AD5258_H_
#define INC_AD5258_H_



#include "Main.h"

#define AD5258_ADR1                                             0x18 // AD1 - GND, AD0 - GND
#define AD5258_ADR2                                             0x1A // AD1 - VDD, AD0 - GND
#define AD5258_ADR3                                             0x4C // AD1 - GND, AD0 - VDD
#define AD5258_ADR4                                             0x4E // AD1 - VDD, AD0 - VDD
#define RDAC_ADDRESS                                            0x00
#define EEPROM_ADDRESS                                          0x20
#define TOLERANCE_ADDRESS_A                                     0x3E
#define TOLERANCE_ADDRESS_B                                     0x3F
#define SOFT_WRITE_PROTECT_ADDRESS                              0x40
#define SOFT_WRITE_PROTECT_OFF                                  0x00
#define SOFT_WRITE_PROTECT_ON                                   0x01


void ADDR_SET(void);
/************************Write Regiister RDAC****************************/
void writeRDAC(uint8_t value);
/************************Write Regiister EEPROM****************************/
void writeEEPROM(uint8_t value);
/************************Read Regiister RDAC****************************/
uint8_t readRDAC(void);
/************************Read Regiister EEPROM****************************/
uint8_t readEEPROM(void);
/*********************************Protected Write Value ******************************/
void toggleSoftWriteProtect(bool state);
// =======================Returns the Fraction of Tolerance===============================
float readTolerance(void);




#endif /* INC_AD5258_H_ */
