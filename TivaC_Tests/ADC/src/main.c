#include "Main.h"


bool isAdcReadOK=false;


void Int_ADC(void)
{
    uint32_t AdcValues[4];
    uint32_t Temp;
    ADCIntClear(ADC0_BASE,1);
    ADCSequenceDataGet(ADC0_BASE,1,AdcValues);
    Temp=147.5-((75*(3.3)*AdcValues[1])/4096);
    isAdcReadOK=true;

}


void RCC_Init(void)
{
    SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);//set Clock
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);//clock ADC0

}



void ADC_init(void)
{
    ADCHardwareOversampleConfigure(ADC0_BASE,64);//samplirovanie
    ADCSequenceDisable(ADC0_BASE,1);//off capture value for config
    ADCSequenceConfigure(ADC0_BASE,1,ADC_TRIGGER_PROCESSOR,0);//Config Sequantion
    ADCSequenceStepConfigure(ADC0_BASE,1,0,ADC_CTL_CH0|ADC_CTL_IE|ADC_CTL_END);//config channel 0
    ADCSequenceStepConfigure(ADC0_BASE,1,1,ADC_CTL_TS|ADC_CTL_IE|ADC_CTL_END);//config channel Temp sensor internal
    ADCSequenceEnable(ADC0_BASE,3);//enable mesumere sample 3
    ADCIntEnable(ADC0_BASE,1);//interrupt  channel
    ADCIntRegister(ADC0_BASE,1,Int_ADC);//register interrupt




}


void main(void)
{
    RCC_Init();
    ADC_init();

    IntEnable(INT_ADC0SS3);
    while(1)
    {
        ADCProcessorTrigger(ADC0_BASE,3);
    }
}


